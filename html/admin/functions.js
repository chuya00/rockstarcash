$(document).ready(function(){
	setOrderName();
});

function setOrderName(){
	$('#order_id').change(function(){
		var val = $(this).find('option:selected').text();
		$('#order_name').val(val);
	});
}