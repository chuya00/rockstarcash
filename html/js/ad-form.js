if(!window.jQuery)
{
   var jquery_js = document.createElement('script');
   jquery_js.type = "text/javascript";
   jquery_js.src = "//code.jquery.com/jquery-1.11.2.min.js";
   var head_tag = document.getElementsByTagName('head')[0];
   head_tag.insertBefore(jquery_js, head_tag.firstChild);
}

var check_jquery;

check_jquery = setInterval(function(){

  if(window.jQuery){
    clearInterval(check_jquery);
     $(document).ready(function(){
      $('form').each(function(){
        //var html = '<input type="hidden" id="i" name="i" value="'+getUrlParameter('i')+'" />';
        //$(this).prepend(html);
        //logConversion($(this));
        setCookie('rsc_i', getUrlParameter('i'), 1);
        setCookie('rsc_e', getUrlParameter('e'), 1);
      });
    });
  }

},500)

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function checkIfOtherForm(list){ //check kng search form ba or something
  var ex = ['s','search','q','query'];
  for (var key in list) {
    if ( $.inArray( list[key].name, ex ) > -1 ){
      return true;
    }
  }

  return false;
}

function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}    