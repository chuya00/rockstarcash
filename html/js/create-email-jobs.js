$(document).ready(function(){
	$( "#datepicker" ).datepicker({ dateFormat: "yy-mm-dd" });
	$('select[name=email-job-type]').change(function(){
		displayScheduler();
	});
	
});

function displayScheduler(){
	var job_type = $('select[name=email-job-type] option:selected').val();
	var html = '';
	/*
	if(job_type == '2'){
		html += 'Send email every <select id="monthday" name="month-sched" >'+loopOptionMonth()+'</select><span id="daysuffix">st</span> day of the month';
	}else if(job_type == '4'){
		html += 'Send email every <select name="week-sched" >'+loopOptionWeek()+'</select> of the week';
	}else if(job_type == '3'){
		html += 'Send email every <select name="bi-sched" >'+loopOptionWeek()+'</select> and <select name="bi-sched2" >'+loopOptionWeek(1)+'</select> of the week';
	}
	
	if(job_type == '2' || job_type == '4' || job_type == '3'){
		$('#scheduler').show();
		$('#scheduler #job-desc').html(html+'<div style="font-size: 11px;margin-top: 4px;">*We will send reminders base on the date.</div>');
		
		changeSuffix();
	}else{
		$('#scheduler').hide();
	}
	*/
	if(job_type == '20' || job_type == '2' || job_type == '4' || job_type == '3' || job_type == '5' || job_type == '7' || job_type == '8' || job_type == '9' || job_type == '10' || job_type == '11' || job_type == '12' || job_type == '13' || job_type == '14' || job_type == '15' || job_type == '16'){
		$('input#email').val('');
		$('input#email').parent().parent().parent().hide();
	}else{
		$('input#email').parent().parent().parent().show();
	}
	if(job_type == '21'){
		$('input#email').parent().parent().parent().hide();
		$('input#datepicker').parent().parent().parent().hide();
	}else{
		$('input#email').parent().parent().parent().show();
		$('input#datepicker').parent().parent().parent().show();
	}
}

function changeSuffix(){
	$('#monthday').change(function(){
		var val = $(this).val();
		
		if(val == '1' || val == '21'){
			$('#daysuffix').html('st');
		}else if(val == '2' || val == '22'){
			$('#daysuffix').html('nd');
		}else if(val == '3' || val == '23'){
			$('#daysuffix').html('rd');
		}else{
			$('#daysuffix').html('th');
		}
	});
}

function loopOptionMonth(){
	var html = '';
	var d = new Date();
	var month = parseInt(d.getMonth()) + 1;
	var year = parseInt(d.getFullYear());
	var num = daysInMonth(month,year);
	for(var x = 1; x <= num; x++){
		if(x >= 1 && x <= 9){
			html += '<option value="0'+x+'">'+x+'</option>';
		}else{
			html += '<option value="'+x+'">'+x+'</option>';
		}		
	}
	
	return html;
}

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

function loopOptionWeek(pos){
	var html = '';
	
	html += '<option value="7" >Sunday</option>';
	if(pos){
		html += '<option selected="selected" value="mon" >Monday</option>';
	}else{
		html += '<option value="1" >Monday</option>';
	}	
	html += '<option value="2" >Tuesday</option>';
	html += '<option value="3" >Wednesday</option>';
	html += '<option value="4" >Thursday</option>';
	html += '<option value="5" >Friday</option>';
	html += '<option value="6" >Saturday</option>';
	
	return html;
}