$(document).ready(function(){
	//showEditSection();
	addContestButton();
	cancelContestAdd();
	initFileUploader();
	setEditFormValues();
	showAddVideoForm();
	saveVideo();
	showContestLink();
	showPreviewLightbox();
	displayModuleText();
	showEmailCreate();
	getMCLists();
});

function showEditSection(){
	$('.edit-player').click(function(){
		var _this = $(this);
		var player_id = $(this).attr('player_id');
		var row = $(this).attr('count-row');
		_this.html('Loading...');
		$.ajax({
			url: url+'/admin/manage-amplify-page/get-media',
			type: 'POST',
			data: 'player_id='+player_id,
			success: function(r){
				var res = jQuery.parseJSON(r);
				var html = '';
				for(var x = 0; x < res.length; x++){
					html += '<tr>';
					html += '<td><input type="radio" name="media-selected" value="'+res[x].id+'-'+res[x].source+'"></td>';
                    html += '<td style="text-align: left">'+res[x].title+' - '+res[x].source+'</td>';                                                    
					html += '</tr>';
				}

				$('#tbody'+row).append(html);
				$('#tr'+row).slideDown();
				_this.html('Edit');
			},
			error: function (x,y,z) {
				console.log(x);
				console.log(y);
				console.log(z);
			}
		});
	});
}

function addContestButton(){

	$('.add-contest-button').click(function(){
		$('.add-contest').slideDown();
	});

}

function cancelContestAdd(){

	$('.add-contest-form button[type=reset]').click(function(){
		$('.add-contest').slideUp();
	});

}

function initFileUploader(){
	'use strict';	
    $('#player-image').fileupload({
        url: url+'/assets/contest_album_image/',
        dataType: 'json',
		acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 5000000, // 5 MB
		add: function (e, data) {
			var goUpload = true;
			var uploadFile = data.files[0];
			if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(uploadFile.name)) {
				alert('You must select an image file only');
				goUpload = false;
			}
			if (uploadFile.size > 5000000) { // 2mb
				alert('Please upload a smaller image, max size is 5 MB');
				goUpload = false;
			}
			if (goUpload == true) {
				data.submit();
			}
		},
        done: function (e, data) {
		
            $.each(data.result.files, function (index, file) {
                $('<p/>').text(file.name).appendTo('#files');
            });
			//console.log("test 1 2 3");
        },
        progressall: function (e, data) {
        	var proccess_text_html = $('#process-text').length;
        	if(proccess_text_html <= 0){
        		$('#player-image').after('<div id="process-text"></div>');
	        	if($('#process-text').html() != 'Uploading...'){
	        		$('#process-text').html('Uploading...');
	        	} 
        	}        	       	
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        },
        error: function (jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    }).on('fileuploaddone', function (e, data) {
		savePlayerImg(data.files[0].name);
		//$("#image-gallery").show();
	}).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
}

function savePlayerImg(filename){
	$.ajax({
		url: url+"/admin/contest/save-album-img",
		type: "POST",
		dataType: "json",
		data: "filename="+filename,
		success: function(resp){
			
			if(resp.resp == "1"){
				//$(".image-picker").append('<option data-img-src="assets/player-image/'+filename+'" data-id="'+resp.id+'" value="'+filename+'">  '+filename+'  </option>');
				//$("select.image-picker").imagepicker();
				//$("#image-gallery").show();
				$('#image-filename').val(resp.filename);
				$('#image-id').val(resp.id);
				$('#process-text').html('Done.');
				updateCoverImageView();
			}else{
				console.log("Error: "+resp);
			}
			//console.log(resp);
		},
		error: function(z,y,x){
			console.log(z+" "+y+" "+x);
		}
	});

}

function showEditBox(elem){	
	$('#bg-light').fadeIn();
	$('#ret-data').center().fadeIn();

	$('.close-edit-form').click(function(){
		elem.removeAttr('disabled');
		elem.html('Edit');
		$('#bg-light').fadeOut();
		$('.edit-contest-form').fadeOut();
	});
}

function setEditFormValues(){
	$('.edit-contest').click(function(){
		var contest_id = $(this).attr('contest-id');
		var _this = $(this);
		_this.attr('disabled','disabled');
		_this.html('Editing...');
		showEditBox(_this);
		$.ajax({
			url: url+'/admin/contest/get-contest',
			type: 'POST',
			data: 'c_id='+contest_id,
			success: function (r) {
				console.log(r);
				var data = $.parseJSON(r);
				console.log(data);
				insertEditValues(data);
				$('#ret-data').hide();
				$('.edit-contest-form').center().fadeIn();
			},
			error: function (x,y,z) {
				console.log(x);
				console.log(y);
				console.log(z);
			}
		});

	});
}

function insertEditValues(data){
	$('#contest-edit-form input#c_id').val(data.id);
	$('#contest-edit-form input#title').val(data.title);
	$('#contest-edit-form select#music option').each(function(){
		if($(this).val() == data.video_id){
			$(this).attr('selected','selected');
		}
	});
	if(data.play == 1){
		$('#contest-edit-form input#checkbox').attr('checked','checked');
	}
	$('#contest-edit-form input#itunes-link').val(data.itunes);
	$('#contest-edit-form input#amazon-link').val(data.amazon);
	$('#contest-edit-form textarea#description').html(data.description);
}

function showAddVideoForm(){
	$('#add-video-btn').click(function(){
		$('#add-video-form-bg').fadeIn();
		$('#add-video-form-container').center().fadeIn();
	});

	$('#cancel-add-video').click(function() {
		$('#add-video-form-bg').fadeOut();
		$('#add-video-form-container').fadeOut();
	});
}

function saveVideo(){
	$('.add-video-form').submit(function(e){
		var submit_btn = $(this).find('button[type=submit]');
		submit_btn.attr('disabled','disabled');
		submit_btn.html('Saving. Pleast wait.');
		var data = $(this).serialize();
		$.ajax({
			url: url+'/admin/video/save',
			type: 'POST',
			data: data,
			success: function(r){
				var resp = jQuery.parseJSON(r);
				console.log(resp);
				if(resp.success == 1){
					changeSelectedMusic(resp);
					$('#add-video-form-bg').fadeOut();
					$('#add-video-form-container').fadeOut();
				}
				submit_btn.removeAttr('disabled');
				submit_btn.html('Save');
			},
			error: function(x,y,z){
				console.log(x);
				console.log(y);
				console.log(z);
			}
		});
		e.preventDefault();
	});
	
}

function changeSelectedMusic(resp){
	$('#video').children().removeAttr('selected');
	$('#video').append('<option value="'+resp.player_id+'" selected="selected">'+resp.player_title+'</option>');
}

function changeToSavingStatus(elem){	
	/*var curr_html = $(elem).html();
	if(curr_html.toLowerCase() == 'save'){
		$(elem).attr('disabled','disabled');
		$(elem).html('Saving...');
	}*/
	$( "body" ).click(function( event ) {
	  	
	});	
}

function showContestLink(){
	$('.contest-link').click(function(){
		var row = $(this).attr('count-row');
		$('#tr'+row+'-link').show();
		$(this).addClass('hide-contest-link');
		$(this).html('Hide Link');
		$('.hide-contest-link').click(function(){
			$('#tr'+row+'-link').hide();
			$(this).removeClass('hide-contest-link');
			$(this).html('Show Link');
			showContestLink();
		});
	});
}

function showPreviewLightbox() {
	$('#preview-lightbox').click(function(){
		$('#lightbox').center();
		$('#lightbox-container').fadeIn();

		$('#lightbox-container > div#preview-bg, #lightbox-container a#close-popup').click(function() {
			$('#lightbox-container').fadeOut();
		});
	});
}

function displayModuleText(){
	$('.add-contest-form input#title').keyup(function(){
		var contents = $(this).val();
		$('#prev-top_left h2#title').html(contents);
	});
	$('.add-contest-form #top_left').keyup(function(){
		var contents = $(this).val();
		$('#prev-top_left div#contents').html(contents);
	});
	$('.add-contest-form #bottom').keyup(function(){
		var contents = $(this).val();
		$('#prev-bottom div#contents').html(contents);
	});
	$('.add-contest-form #contest_type').change(function(){
		var img = $(this).find('option:selected').attr('image-name');
		$("#lightbox").css('background-image','url('+url+'/images/contest-type/'+img+')');
		console.log(url+'/images/contest-type/'+img);
	});

	updateCoverImageView();
}

function updateCoverImageView(){
	var cover_image = $('#image-filename').val();
	if(cover_image != ''){
		$('#prev-cover-image').attr('src',url+'/assets/contest_album_image/files/'+cover_image);
	}
}

function savingStatus(){
	$('.add-contest-form').find('button[type=submit]').click(function(){
		$(this).attr('disabled','disabled');
		$(this).html('Saving. Please wait.');
	});
}

function showEmailCreate(){
	$('#create-email-btn').click(function(e) {
		var _href = $(this).attr('data-href');
		//$(this).html('<iframe src="'+_href+'" style="width: 100%; height: 100%" ></iframe>');
		$('#email-create iframe').attr('src',_href);
		$('#email-create').center().fadeIn();

		$('a#close-email-iframe').click(function() {
			$('#email-create').fadeOut();
		});

		e.preventDefault();
		return false;
	});
}

function closeEmailCreate(data){
	console.log(data.filename);
	$('#email_template').children().removeAttr('selected');
	$('#email_template').append('<option value="'+data.filename+'" selected="selected">'+data.filename+'</option>');
	$('#email-create').fadeOut();
}

function getMCLists(){
	$('.email-option').click(function(){
		var data_sel = $(this).attr('data-sel');
		$('#email-template-sel').val(data_sel);
	});

	$('#get-mc-list').click(function(){
		$('#mc-lists').html('<img src="'+url+'/images/loader2.gif" style="margin-right: 5px;" />Retrieving lists...');
		var mc_key = $('#mc-api-key-sel option:selected').val();
		if(mc_key == ''){
			mc_key = $('#mc-api-key').val();
		}
		$.ajax({
			url: url+'/admin/contest/get-mc-lists',
			type: 'POST',
			data: 'key='+mc_key,
			success: function(r){
				var data = jQuery.parseJSON(r);
				console.log(data);

				if(data.status && data.status == 'error'){
					$('#mc-lists').html(data.error);
					return false;
				}

				if(data.total == 0){
					$('#mc-lists').html('There are no lists in your mailchimp account. Create one <a href="https://us4.admin.mailchimp.com/lists/" target="_blank" >here</a>');
					return false;
				}

				var html = '<select id="mc_list_id" name="mc_list_id">';
				for(var x = 0; x < data.total; x++){
					html += '<option value="'+data.data[x].id+'" >'+data.data[x].name+' - '+data.data[x].id+'</option>';
				}
				html += '</select>';

				$('#mc-id').val(data.mc_id);
				$('#mc-lists').html(html);


			},
			error: function(x,y,z){
				console.log(x);
				console.log(y);
				console.log(z);
			}
		});
	});	
}

jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + 
                                                $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + 
                                                $(window).scrollLeft()) + "px");
    return this;
}