$(document).ready(function(){
	addAudioButton();
	cancelAudioAdd();

});

function addAudioButton(){

	$('.add-audio-button').click(function(){
		$('.add-audio').slideDown();
	});

}

function cancelAudioAdd(){

	$('.add-audio-form button[type=reset]').click(function(){
		$('.add-audio').slideUp();
	});

}