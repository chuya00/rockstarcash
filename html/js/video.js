$(document).ready(function(){
	addVideoButton();
	cancelVideoAdd();

});

function addVideoButton(){

	$('.add-video-button').click(function(){
		$('.add-video').slideDown();
	});

}

function cancelVideoAdd(){

	$('.add-video-form button[type=reset]').click(function(){
		$('.add-video').slideUp();
	});

}