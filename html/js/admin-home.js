$(document).ready(function(){
	//console.log('test');
	changeTab()

});

function changeTab(){

	$('.nav-tabs li > a').click(function(){
		var tab = $(this).attr('href');
		var stat_name = $(this).attr('stat-name');
		var data_set = $(this).parent().parent().attr('data-set');
		//console.log(stat_name);
		//console.log(data_set);
		var elem = $(tab+' table tbody');
		//console.log(elem);
		var html = elem.html();
		
		var ignore_tab = $(this).attr('stat-ignore')
		
		if(!(ignore_tab && ignore_tab == '1') && $.trim(html) == '' || $.trim(html) == '<tr><td colspan="5"><h4 style="text-align: center;">No Data</h4></td></tr>'){
			elem.html('<tr><td colspan="5"><h4 style="text-align: center;">Loading...</h4></td></tr>');
			ajaxGetStat(elem,stat_name,data_set);
		}
	});

}

function ajaxGetStat(elem,stat_name,data_set){
	$.ajax({
		url: url+'/admin/get-stat',
		type: 'POST',
		data: 'stat='+stat_name+'&data_set='+data_set,
		success: function(r){
			//console.log(r);			
			elem.html(r);
			var html = elem.html();
			if($.trim(html) == ''){
				elem.html('<tr><td colspan="5"><h4 style="text-align: center;">No Data</h4></td></tr>');
			}
		},
		error: function(x,y,z){
			console.log(x+' '+y+' '+z);
		}
	});
}