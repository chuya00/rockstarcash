$(document).ready(function(){
	showEditSection();
});

function showEditSection(){
	$('.edit-player').click(function(){
		var _this = $(this);
		var player_id = $(this).attr('player_id');
		var row = $(this).attr('count-row');
		_this.html('Loading...');
		$.ajax({
			url: url+'/admin/manage-amplify-page/get-media',
			type: 'POST',
			data: 'player_id='+player_id,
			success: function(r){
				var res = jQuery.parseJSON(r);
				var html = '';
				for(var x = 0; x < res.length; x++){
					html += '<tr>';
					html += '<td><input type="radio" name="media-selected" value="'+res[x].id+'-'+res[x].source+'"></td>';
                    html += '<td style="text-align: left">'+res[x].title+' - '+res[x].source+'</td>';                                                    
					html += '</tr>';
				}

				$('#tbody'+row).append(html);
				$('#tr'+row).slideDown();
				_this.html('Edit');
			},
			error: function (x,y,z) {
				console.log(x);
				console.log(y);
				console.log(z);
			}
		});
	});
}