$(document).ready(function(){
	$("select.image-picker").imagepicker();
	
	$(".select-album-image").click(function(){
		$("#image-gallery").show();
	});
	//initImageUplaoder();
	deletePlayer();
	
	$(".album-image-close").click(function(){
		$("#image-gallery").hide();
	});
	
	$('.embed-btn').click(function(){
		$(this).next().next().slideDown('fast');
		$(this).next('a').show();
		$(this).hide();
	});
	
	$('.hide-embed-btn').click(function(){
		$(this).next().slideUp('fast');
		$(this).prev('a').show();
		$(this).hide();
	});
	
	$(".embed-code input").click(function(){
		$(this).select();
	});
	
	$(".embed-code-val").focus(function() {
		var $this = $(this);
		$this.select();

		// Work around Chrome's little problem
		$this.mouseup(function() {
			// Prevent further mouseup intervention
			$this.unbind("mouseup");
			return false;
		});
	});
	
	$(".iframe-size").change(function(){
		var val = $(this).val();
		
		var code = '<iframe width="'+val+'" height="'+val+'" src="http://www.rockstarcash.com/admintheme-dev2/player.php?player='+val+'" frameborder="0" allowfullscreen></iframe>';
		
		$('#embed-code-val').html(code);
		
	});
	
	$(".add-audio").click(function(){
		var id = $(this).attr("audio-id");
		$(this).hide();
		$(this).next().show();
		addAudio(id,"add");
	});
	
	$(".remove-audio").click(function(){
		var id = $(this).attr("audio-id");
		$(this).hide();
		$(this).prev().show();
		addAudio(id,"remove");
	});
	
	$(".preview-player").click(function(){
		reloadPreview();
	});
	
	$(".save-player").click(function(){
		saveAudio();
	});
	
	reloadPlayerPreview();
	showEditSection();
	
	$(".edit-saved-player").click(function(){
		//alert();
		var _this = $(this);
		var player_id = _this.attr("player-id");
		_this.siblings(".preview-saved-player").trigger("click");
		//$("#edit-saved-player").trigger("click");
	});
});
/*
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    //var url = window.location.hostname === 'blueimp.github.io' ?
               // '//jquery-file-upload.appspot.com/' : 'assets/plugins/jquery-fileupload-img/server/php/';
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        maxFileSize: 50000000,
        url: url+'/assets/album_image/',
        acceptFileTypes: /(\.|\/)(mp3)$/i,
        success: function(result, data, z){
            var count_rows = $('.files-list tr').length;
            //console.log(count+" "+count_rows);
            if(count < count_rows){
                var file = result.files;
                files.push(file[0].name);
            }

            if(count == (count_rows-1)){
                saveAudio(files);
            }

            count++;
            //console.log('1:');
            //console.log(result);
            //console.log('2:');
            //console.log(data.result);
            //console.log('3:');
            //console.log(jqXHR);
            //saveAudio(result);
            //prepareResultForAjax(result)
            //window.location = url+'/admin/media/audio-upload'
        },
        error: function (jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });*/
$(function () {
    'use strict';	
    $('#player-image').fileupload({
        url: url+'/assets/album_image/',
        dataType: 'json',
		acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 5000000, // 5 MB
		add: function (e, data) {
			var goUpload = true;
			var uploadFile = data.files[0];
			if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(uploadFile.name)) {
				alert('You must select an image file only');
				goUpload = false;
			}
			if (uploadFile.size > 5000000) { // 2mb
				alert('Please upload a smaller image, max size is 5 MB');
				goUpload = false;
			}
			if (goUpload == true) {
				data.submit();
			}
		},
        done: function (e, data) {
		
            $.each(data.result.files, function (index, file) {
                $('<p/>').text(file.name).appendTo('#files');
            });
			//console.log("test 1 2 3");
        },
        progressall: function (e, data) {
        	$('#process-text').html('Uploading...');
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        },
        error: function (jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    }).on('fileuploaddone', function (e, data) {
		savePlayerImg(data.files[0].name);
		//$("#image-gallery").show();
	}).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

function savePlayerImg(filename){
	$.ajax({
		url: url+"/admin/media/audio-players/save-album-img",
		type: "POST",
		dataType: "json",
		data: "filename="+filename,
		success: function(resp){
			
			if(resp.resp == "1"){
				//$(".image-picker").append('<option data-img-src="assets/player-image/'+filename+'" data-id="'+resp.id+'" value="'+filename+'">  '+filename+'  </option>');
				//$("select.image-picker").imagepicker();
				//$("#image-gallery").show();
				$('#image-filename').val(resp.filename);
				$('#image-id').val(resp.id);
				$('#process-text').html('Done.');
			}else{
				console.log("Error: "+resp);
			}
			//console.log(resp);
		},
		error: function(z,y,x){
			console.log(z+" "+y+" "+x);
		}
	});

}

function addAudio(id,method){
	$.ajax({
		url: url+"/admin/media/audio-players/add-audio",
		type: "POST",
		data: "id="+id+"&method="+method,
		success: function(resp){
			//console.log(resp);
		},
		error: function(z,y,x){
			console.log(z+" "+y+" "+x);
		}
	});
}

function saveAudio(){
	//alert();
	//$("#bg-overlay").show();
	//$("#loading-gif").center().show();
	$('.save-player').attr('disabled','disabled');
	$('.save-player').html('Saving...');
	var artist_name = $("#artist_name").val();
	var album_name = $("#album_name").val();
	var image = $("#image-filename").val();
	var image_id = $("#image-id").val();
	$.ajax({
		url: url+"/admin/media/audio-players/save-audio-player",
		type: "POST",
		data: "artist_name="+artist_name+"&album_name="+album_name+"&image="+image+"&image_id="+image_id,
		success: function(resp){
			$("#bg-overlay").hide();
			$("#loading-gif").center().hide();
			console.log(resp);
			if(resp == "1"){
				alert('Successfully generated player!');
				location.reload();
			}else{
				alert(resp);
			}
		},
		error: function(z,y,x){
			console.log(z+" "+y+" "+x);
		}
	});
}

jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + 
                                                $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + 
                                                $(window).scrollLeft()) + "px");
    return this;
}

function reloadPreview(){
	//document.getElementById('iframe-player').contentWindow.location.reload();
	addDetails();
}

function addDetails(){
	var artist_name = $("#artist_name").val();
	var album_name = $("#album_name").val();
	var image = $("#image-filename").val();
	var image_id = $("#image-id").val();
	console.log(image_id);
	$.ajax({
		url: url+"/admin/media/audio-players/add-player-details",
		type: "POST",
		data: "artist_name="+artist_name+"&album_name="+album_name+"&image="+image+"&image_id="+image_id,
		success: function(resp){
			document.getElementById('iframe-player').contentWindow.location.reload();
			
		},
		error: function(z,y,x){
			console.log(z+" "+y+" "+x);
		}
	});
}


function deleteAudioFile(){
	$('.delete-audio-file').click(function(){
		var file = $(this).attr('data-file');
		file = file.trim();
		var id = $(this).attr('data-id');
		id = id.trim();
		var audio_tbl_row = $(this).parent().parent();
		//console.log(file);
		$('.upload-filename').each(function(){
			var filename = $(this).html();
			filename = filename.replace("&gt;","");
			filename = filename.trim();
			//console.log(filename);
			if(file == filename){
				var parent = $(this).parent().parent();
				var td = parent.next().next();
				//$(td).find('button.delete').trigger('click');
				deleteAudioFileAjax(id,td,audio_tbl_row);
				return false;
				//console.log(td.html());
				//search ang delete button sa jquery file upload
			}		
		});
	});
}

function deleteAudioFileAjax(id,td,audio_tbl_row){
	$.ajax({
		url: 'delete-audio.php',
		type: 'POST',
		data: 'id='+id,
		success: function(resp){
			if(resp.trim() == '1'){
				$(td).find('button.delete').trigger('click');
				$(audio_tbl_row).remove();
			}
			console.log(resp);
		},
		error: function(x,y,z){
			console.log(x+' '+y+' '+z);
		}
	});
}

function displayAudioForm(){
	$('#upload-audio-btn').click(function(){
		$('#audio-upload-form').show();
		$('#upload-audio-btn').hide();
		$('#upload-audio-btn-hide').show();
	});
	
	$('#upload-audio-btn-hide').click(function(){
		$('#audio-upload-form').hide();
		$('#upload-audio-btn-hide').hide();
		$('#upload-audio-btn').show();
	});
}

function setDetailsBtn(){
	$('.show-details-section').click(function(){
		var count = $(this).attr('details-count');
		$('.details-section-td'+count).show();
		//$(this).next('a').show();
		$(this).hide();
	});
	
	$('.hide-details-section').click(function(){
		$(this).siblings('div.details-section').hide();
		$(this).prev('a').show();
		$(this).hide();
	});
}

function prefill(){	
	$.ajax({
		url: 'get-audio.php',
		type: 'GET',
		dataType: 'json',
		success: function(resp){
			for(var x=0; x<resp.length; x++){
				$('#album'+x).val(resp[x].album);
				$('#title'+x).val(resp[x].title);
				$('#tag'+x).val(resp[x].tag);				
			}
		},
		error: function(x,y,z){
			console.log(x+' '+y+' '+z);
		}
	});
}

function init(){
	$('.audio-details-btn').click(function(){
		var _this = $(this);
		var btn_val = _this.html();
		_this.html('Processing...');
		_this.attr('disabled','disabled');
		
		var count = _this.attr('details-count');
		
		var audio_id = $('#audio-id'+count).val();
		var album = $('#album'+count).val();
		var title = $('#title'+count).val();
		var filename = $('#filename'+count).val();
		var tag = $('#tag'+count).val();
		console.log(album+" "+title+" "+filename+" "+tag);
		if((album != "" && album.length > 0) && (title != "" && title.length > 0) && (tag != "" && tag.length > 0)){		
			$.ajax({
				url: 'save-audio.php',
				type: 'POST',
				data: { id: audio_id,album: album, title: title, filename: filename, tag: tag },
				success: function(resp){
					//console.log(resp);
					if(resp == 1 || resp == '1'){
						$('#audio-alert'+count).html('<div class="alert alert-success fade in"><button type="button" class="close close-sm" data-dismiss="alert"> <i class="icon-remove"></i> </button><strong>Success!</strong> Audio updated. </div>');
						_this.html(btn_val);
						_this.removeAttr('disabled');
					}else{
						$('#audio-alert'+count).html('<div class="alert alert-block alert-danger fade in"><button type="button" class="close close-sm" data-dismiss="alert"> <i class="icon-remove"></i> </button><strong>Oops!</strong> Failed to update audio details. </div>');
						_this.html(btn_val);
						_this.removeAttr('disabled');
					}
				},
				error: function(x,y,z){
					console.log(x+' '+y+' '+z);
					_this.html(btn_val);
					_this.removeAttr('disabled');
				}
			});
		}else{
			$('#audio-alert'+count).html('<div class="alert alert-block alert-danger fade in"><button type="button" class="close close-sm" data-dismiss="alert"> <i class="icon-remove"></i> </button><strong>Oops!</strong> All fields are required. </div>');
			_this.html(btn_val);
			_this.removeAttr('disabled');
		}
		
		
	});
}

function setUploadForm(){
	$('.audio-img-btn').click(function(){
		var count = $(this).attr('details-count');
		
		$('#audio-img-container'+count).fadeIn();
		$('#bg-overlay').fadeIn();
		uploadImg(count);
		$('#bg-overlay').click(function(){
			$('#audio-img-container'+count).fadeOut();
			$('#bg-overlay').fadeOut();
		});
	});
	
}

function deletePlayer(){
	$(".delete-player").click(function(){
		var _this = $(this);
		var p_id = _this.attr("player-id");
		$.ajax({
			url: 'delete-player.php',
			type: 'POST',
			data: 'p_id='+p_id,
			success: function(resp){
				if(resp == 1 || resp == '1'){
					_this.parent().parent().remove();
				}
			},
			error: function(x,y,z){
				console.log(x+' '+y+' '+z);
			}
		});
	});
	
}

function reloadPlayerPreview(){
	$(".preview-saved-player").click(function(){
		var id = $(this).attr("player-id");
		$(".edit-saved-player").attr("player-id",id);		
		$(".edit-saved-player").show();
		$("#edit-section-container").hide();
		//$("#iframe-player-preview").attr("src","http://dev.amplify.fm/player.php?player="+id);
		//console.log("http://rockstarcash.com/admintheme-dev2/player.php?player="+id);
		//document.getElementById('iframe-player-preview').contentWindow.location.reload();
	});
	
}

function showEditSection(){
	$(".edit-saved-player").click(function(){
		var _this = $(this);
		var p_id = _this.attr("player-id");
		//alert('wa')
		if(p_id == "0"){return false;}
		_this.hide();
		//alert('we')
		$.ajax({
			url: url+'/admin/media/audio-players/set-sessions-embed',
			type: 'POST',
			data: 'id='+p_id,
			success: function(resp){
				$("#edit-section-container").html(resp);
				$("#edit-section-container").show();
				$("select.image-picker-edit").imagepicker();
				$(".select-album-image-edit").click(function(){
					$("#image-gallery-edit").show();
				});
				fileuploadEdit();
				reloadPrevSavedPlayer();
				addRemoveSavedAudio();
				$(".update-player").click(function(){
					updateAudioPlayer();
				});
				$(".album-image-close-edit").click(function(){
					$("#image-gallery-edit").hide();
				});
			},
			error: function(x,y,z){
				console.log(x+' '+y+' '+z);
			}
		});
	});
}

function reloadPrevSavedPlayer(){
	$(".preview-player-saved").click(function(){		
		addDetailsSaved();		
	});
	
}

function addDetailsSaved(){
	var artist_name = $("#artist_name_saved").val();
	var album_name = $("#album_name_saved").val();
	var image = $(".image-picker-edit").val();
	var image_id = $(".image-picker-edit option:selected").attr('data-id');
	console.log(image_id);
	$.ajax({
		url: url+"/admin/media/audio-players/add-saved-player-details",
		type: "POST",
		data: "artist_name="+artist_name+"&album_name="+album_name+"&image="+image+"&image_id="+image_id,
		success: function(resp){
			var src = $("#iframe-player-preview").attr("src");
			//if(src != "http://dev.amplify.fm/embed-prev2.php"){
				//$("#iframe-player-preview").attr("src","http://dev.amplify.fm/embed-prev2.php");
			//}else{
				document.getElementById('iframe-player-preview').contentWindow.location.reload();
			//}
		},
		error: function(z,y,x){
			console.log(z+" "+y+" "+x);
		}
	});
}

function fileuploadEdit(){
	'use strict';
    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'assets/plugins/jquery-fileupload-img/server/php/';
    $('#player-image-edit').fileupload({
        url: url,
        dataType: 'json',
		acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 5000000, // 5 MB
        done: function (e, data) {
		/*
            $.each(data.result.files, function (index, file) {
                $('<p/>').text(file.name).appendTo('#files');
            });*/
			//console.log("test 1 2 3");
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress-edit .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).on('fileuploaddone', function (e, data) {
		savePlayerImg(data.files[0].name);
		//$("#image-gallery").show();
	}).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
}

function updateAudioPlayer(){
	var player_id = $(".update-player").attr("player-id");
	var artist_name = $("#artist_name_saved").val();
	var album_name = $("#album_name_saved").val();
	var image = $(".image-picker-edit").val();
	var image_id = $(".image-picker-edit option:selected").attr('data-id');
	$.ajax({
		url: "update-saved-audio-player.php",
		type: "POST",
		data: "id="+player_id+"&artist_name="+artist_name+"&album_name="+album_name+"&image="+image+"&image_id="+image_id,
		success: function(resp){
			if(resp == 1){
				alert('Successfully updated player!');
				location.reload();
			}else{
				alert('Something went wrong');
				console.log(resp);
			}
			
			
		},
		error: function(z,y,x){
			console.log(z+" "+y+" "+x);
		}
	});
}

function addRemoveSavedAudio(){
	$(".add-audio-saved").click(function(){
		var id = $(this).attr("audio-id");
		$(this).hide();
		$(this).next().show();
		addSavedAudio(id,"add");
	});
	
	$(".remove-audio-saved").click(function(){
		var id = $(this).attr("audio-id");
		$(this).hide();
		$(this).prev().show();
		addSavedAudio(id,"remove");
	});
}

function addSavedAudio(id,method){
	$.ajax({
		url: url+"/admin/media/audio-players/add-saved-audio",
		type: "POST",
		data: "id="+id+"&method="+method,
		success: function(resp){
			console.log(resp);
		},
		error: function(z,y,x){
			console.log(z+" "+y+" "+x);
		}
	});
}

/*
 * jQuery File Upload Plugin JS Example 8.9.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
/*
$(function () {
	//alert();
    'use strict';
    // Change this to the location of your server-side upload handler:
    $('#fileupload').fileupload({
        url: url+'/assets/album_image/',
        maxFileSize: 50000000,
        acceptFileTypes: /(\.|\/)(jpg|png)$/i,
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('<p/>').text(file.name).appendTo('#files');
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        },
        error: function (jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
*/
/*
function saveAudio(names){
    console.log(names);
    $.ajax({
        url: url+'/admin/media/save-audio',
        type: 'POST',
        data: {names:names},
        success: function(r){
            //console.log(r)
            if(r == '1'){
                window.location = url+'/admin/media/audio-upload';
            }
        },
        error: function(x,y,z){
            console.log(x);
            console.log(y);
            console.log(z);
        }
    })
}*/

function prepareResultForAjax(result){
    var files = result.files;
    var data = new Array();
    for(x = 0; x < files.length; x++){
        data.push(files[x].name);
    }

    console.log(data);
}

function showProcessingText(){
    $('.start').click(function(){
        $('.files-list').before('<h3 id="processing-text" >Processing...</h3>')
    });
}