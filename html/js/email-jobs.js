$(document).ready(function(){
	init();	
});

function init(){
	//$('.cancel-job').hide();
	jobAction();
	deleteJob();
	getJobTable();
}

function deleteJob(){
	$('.delete').click(function(){
		var _this = $(this);
		_this.find('span').html('Deleting...');
		var id = _this.attr('data-id');
		
		$.ajax({
			type: 'POST',
			data: 'job_id='+id,
			url: 'delete-job.php',
			success: function(r){
				console.log(r);
				//alert('Email '+r);
				if(r == '1'){
					_this.parent().parent().remove();
					alert('Email job moved to trash');
				}
			},
			error: function(x,y,z){
				console.log(x+' '+y+' '+z);
			}
		});
	});
}

function getConfirmMsg(id,type_id,job,_this){
	var msg = 'No message';
	var button = null;
	if(type_id == '2' || type_id == '3' || type_id == '4'){
		var sched = '';
		if(type_id == '2'){
			sched = 'month';
		}else
		if(type_id == '3'){
			sched = 'day within this week';
		}else
		if(type_id == '4'){
			sched = 'week';
		}
		msg = 'This is a <span style="text-transform: uppercase;">'+job+'</span> email newsletter option, this means you must start preparing the email for next '+sched+'. Consistency is the key to profit. You can pre-make as many as you like in the admin.<br /><br /><div style="text-align: center">Are you sure you want to send this <span style="text-transform: uppercase;">'+job+'</span> email?</div>';
		button = {"Yes, I am sure": function () {
					//console.log(_this);
					sendEmail(_this,id,type_id);
					$(this).dialog("close");
				},
				"No, let me send just one": function () {
					//doFunctionForNo();
					$(this).dialog("close");
				},
				Cancel: function () {
					_this.html('Start Job');
					$(this).dialog("close");
				}
			};
	}else{
		msg = '<div style="text-align: center">Are you sure you want to activate <span style="text-transform: uppercase;">'+job+'</span> email job?</div>';
		button = {"Yes, I am sure": function () {
					//console.log(_this);
					sendEmail(_this,id,type_id);
					$(this).dialog("close");
				},
				Cancel: function () {
					_this.html('Start Job');
					$(this).dialog("close");
				}
			};
	}
	
	return {msg: msg, button: button};
}

function sendEmail(_this,id,type_id){
	if(type_id == '6'){
		$(_this).next().show();
		$(_this).hide();
	}		
	performJob(id,_this);
}

function jobAction(){
	
	$('.perform-job').click(function(){
		var _this = $(this);
		_this.html('Sending...');
		var job = _this.attr('data-job-type-name');
		var id = _this.attr('data-id');
		var type_id = _this.attr('data-job-type');
		
		var d = getConfirmMsg(id,type_id,job,_this);
		
		$('<div></div>').appendTo('body')
			.html('<div style="width: 500px;"><h5>'+d.msg+'</h5></div>')
			.dialog({
			modal: true,
			title: 'Confirm',
			zIndex: 10000,
			autoOpen: true,
			width: 'auto',
			resizable: false,
			buttons: d.button,
			close: function (event, ui) {
				$(this).remove();
			}
		});
		/*
		var _this = $(this);		
		
		
		var id = _this.attr('data-id');
		var type_id = _this.attr('data-job-type');
		if(type_id != '1'){
			$(_this).next().show();
			$(_this).hide();
		}		
		performJob(id);*/
	});
	
	$('.cancel-job').click(function(){
		var _this = $(this);		
		_this.html('Cancelling...');
		var id = _this.attr('data-id');
		
		$('<div></div>').appendTo('body')
			.html('<div style="width: 500px;"><h5>Are you sure you want to cancel this email job?</h5></div>')
			.dialog({
			modal: true,
			title: 'Confirm',
			zIndex: 10000,
			autoOpen: true,
			width: 'auto',
			resizable: false,
			buttons: {"Yes, I am sure": function () {
					//console.log(_this);
					cancelJob(id,_this);
					$(this).dialog("close");
				},
				No: function () {
					_this.html('Cancel Job');
					$(this).dialog("close");
				}
				},
			close: function (event, ui) {
				$(this).remove();
			}
		});
		
		
	});
}
/*backup
function jobAction(){
	
	$('.perform-job').click(function(){
		var _this = $(this);		
		
		
		var id = _this.attr('data-id');
		var type_id = _this.attr('data-job-type');
		if(type_id != '1'){
			$(_this).next().show();
			$(_this).hide();
		}		
		performJob(id);
	});
	
	$('.cancel-job').click(function(){
		var _this = $(this);		
		var id = _this.attr('data-id');
		$(_this).prev().show();
		$(_this).hide();
		cancelJob(id);
	});
}
*/
function performJob(id,_this){
	var type_name = _this.attr('data-job-type-name');
	var job_type = _this.attr('data-job-type');
	$.ajax({
		type: 'POST',
		data: 'job_id='+id,
		url: url+'/admin/email/jobs/perform',
		success: function(r){
			//console.log(r);
			
			if(r == '1'){
				_this.after('<button class="btn btn-s-md btn-warning cancel-job" data-id="'+id+'" data-job-type-name="'+type_name+'" data-job-type="'+job_type+'" >Cancel Job</button>');
				_this.remove();
				jobAction();
				alert('Email job scheduled!');				
			}else{
				_this.html('Start Job');
				alert('Error while sending your email.');
				console.log(r);
			}
		},
		error: function(x,y,z){
			console.log(x+' '+y+' '+z);
		}
	});
}

function cancelJob(id,_this){
	var job = _this.attr('data-job-type-name');
	var type_id = _this.attr('data-job-type');
	$.ajax({
		type: 'POST',
		data: 'job_id='+id,
		url: 'cancel-job.php',
		success: function(r){
			
			//alert('Email '+r);
			if(r == '1'){
				_this.before('<button class="btn btn-s-md btn-primary perform-job" data-id="'+id+'" data-job-type-name="'+job+'" data-job-type="'+type_id+'" >Start Job</button>');
				_this.remove();
				jobAction();
				alert('Email job cancelled');
			}else{
				_this.html('Cancel Job');
				console.log(r);
				alert('Error while sending your email.');
			}
		},
		error: function(x,y,z){
			console.log(x+' '+y+' '+z);
		}
	});
}

function getJobTable(){
	$('.email-tab-menu').click(function(){
		var tab_id = $(this).attr('data-tab-id');
		
		var elem = $('#'+tab_id+' table tbody');
		var html = elem.html();
		if($.trim(html) == '' || $.trim(html) == '<tr><td colspan="5"><h4 style="text-align: center;">No Data</h4></td></tr>'){
			elem.html('<tr><td colspan="5"><h4 style="text-align: center;">Loading...</h4></td></tr>');
			ajaxJobList(elem,tab_id);
		}
	});
}

function ajaxJobList(elem,status){
	$.ajax({
		url: url+'/admin/email/jobs/ajax-job-list',
		type: 'POST',
		data: 'status='+status,
		success: function(r){
			elem.html(r);
			var html = elem.html();
			if($.trim(html) == ''){
				elem.html('<tr><td colspan="5"><h4 style="text-align: center;">No Data</h4></td></tr>');
			}
		},
		error: function(x,y,z){
			console.log(x+' '+y+' '+z);
		}
	});
}