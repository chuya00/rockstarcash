jQuery(document).ready(function($){
	$('.summernote').summernote({toolbar: [
		//[groupname, [button list]]
		['style', ['bold', 'italic', 'underline', 'clear']],
	], height: 300});

	$('.summernote-full').summernote({height: 300});
	$( "#datepicker" ).datepicker({ dateFormat: "yy-mm-dd" });

	$('#post-form').submit(function(){
		var msg = $('div.note-editable').html();
		$('#message').html(msg);
	});
});