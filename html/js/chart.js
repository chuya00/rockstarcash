//REVENUE DATA


var revenue_monthly_data = {
	labels : ["Data 1","Data 2","Data 3","Data 4","Data 5","Data 6","Data 7"],
	datasets : [
		{
			label: "Monthly Revenue",
			fillColor : "rgba(220,220,220,0.2)",
			strokeColor : "#3399FF",
			pointColor : "#3399FF",
			pointStrokeColor : "#fff",
			pointHighlightFill : "#fff",
			pointHighlightStroke : "rgba(220,220,220,1)",
			data : [10,20,30,50,50,60,70]
		}
	]
}


$(document).ready(function(){
	////console.log(dateArray);
	$(document).on('click', 'ul.rev-chart li', function(){
		
		var target_canvas_tab = $(this).find('a').attr('href');
		var $target_canvas = $(target_canvas_tab).find('canvas');		
		var target_canvas_width = $target_canvas.attr('width');

		var tab = $(this).find('a').attr('tab');
		var chart_name = $(this).find('a').attr('chart-name');
		//console.log(chart_name);
		////console.log(target_canvas_width);
		if(target_canvas_width == "100%"){
			////console.log('wa');
			renderChart(tab,target_canvas_tab,chart_name);
			
		}

		/*		
		var target_canvas_id = $target_canvas.attr('id');
		var target_canvas_data = $target_canvas.data('info');

		//console.log(target_canvas_tab);
		//console.log(target_canvas_id);
		//console.log(target_canvas_data);
		//console.log('------------');

		if(target_canvas_width == "100%"){

			var canvas_ctx = document.getElementById(target_canvas_id).getContext("2d");

			window.myLine = new Chart(canvas_ctx).Line(window[target_canvas_data], {
				responsive: true
			});

		}*/
	});

	init();

});
var test;
function setData(r){
	var res = [0];
	console.log(r);
	for(var x = 0; x < r.length; x++){
		var rev = r[x].revenue;		
		if(typeof rev !== 'string'){
			rev = r[x].revenue[0];
			console.log(rev+'wa');
		}
		console.log(rev);
		console.log('we')
		test = rev;
		if(rev.indexOf("$") > -1){
			rev = rev.replace("$","");
		}
		
		rev = parseFloat(rev);
		////console.log(rev);
		res.push(rev);
	}

	return res;
}

var revenue_daily_data = {
				labels : ["","Today"],
				datasets : [
					{
						label: "Daily Revenue",
						fillColor : "rgba(220,220,220,0.2)",
						strokeColor : "#3399FF",
						pointColor : "#3399FF",
						pointStrokeColor : "#fff",
						pointHighlightFill : "#fff",
						pointHighlightStroke : "rgba(220,220,220,1)",
						height: "500px",
						data : [0,0]
					}
				]
			}

			var revenue_source_daily_data = {
				labels : ["","Today"],
				datasets : [
					{
						label: "Ads Revenue",
						fillColor : "rgba(220,220,220,0.2)",
						strokeColor : "#3399FF",
						pointColor : "#3399FF",
						pointStrokeColor : "#fff",
						pointHighlightFill : "#fff",
						pointHighlightStroke : "rgba(220,220,220,1)",
						data : [0,0]
					},
					{
						label: "Downloads Revenue",
						fillColor : "rgba(220,220,220,0.2)",
						strokeColor : "#FF8080",
						pointColor : "#FF8080",
						pointStrokeColor : "#fff",
						pointHighlightFill : "#fff",
						pointHighlightStroke : "rgba(220,220,220,1)",
						data : [0,0]
					}
				]
			}

function legend(parent, data) {
    var datas = data.datasets

    // remove possible children of the parent
    /*(while(parent.hasChildNodes()) {
        parent.removeChild(parent.lastChild);
    }*/

    datas.forEach(function(d) {
        var html = '<span class="title" style="background: '+d.strokeColor+';color: #fff;padding: 3px;margin-right: 3px;">'+d.label+'</span>';
        parent.append(html);
    });
}

function init(){
	$('#revenue-daily').prepend('<div class="data-fetch" style="text-align: center"><img src="images/loader.gif" />Fetching data. Please wait...</div>');
	$('#revenue-source-daily').prepend('<div class="data-fetch" style="text-align: center"><img src="images/loader.gif" />Fetching data. Please wait...</div>');
	if(!isDataOnCookie('#revenue-daily-chart')){	
		$.ajax({	
			url: url+'/admin/get-init-report',
			type: 'POST',
			data: 'date_type=daily',
			//dataType: 'json',
			success: function(r){
				////console.log(setData(r));
				var store = r;
				r = jQuery.parseJSON(r);
				////console.log(r);
				$('.data-fetch').remove();
				
				revenue_daily_data.datasets[0].data = setData(r.dfp);

				revenue_source_daily_data.datasets[0].data = setData(r.dfp);
				revenue_source_daily_data.datasets[1].data = setData(r.cake);

				$default_charts = $('canvas.default-chart');

				$default_charts.each(function(){

					$(this).parent().append('<div></div>');

					var target_canvas_id = $(this).attr('id');
					var target_canvas_data = $(this).data('info');
					////console.log('waa '.target_canvas_data);
					var canvas_ctx = document.getElementById(target_canvas_id).getContext("2d");

					window.myLine = new Chart(canvas_ctx).Line(window[target_canvas_data], {
						responsive: true
					});

					legend($(this).parent().find('div'), window[target_canvas_data]);

				});
				setCookie('#revenue-daily-chart',store,20);
				//setCookie('#revenue-source-daily-chart',r.cake,20);
			},
			error: function(x,y,z){
				//console.log(x);
				//console.log(y);
				//console.log(z);
			}
		});
	}else{
		//console.log('1');
		var data = getCookie('#revenue-daily-chart');
		var r = jQuery.parseJSON(data);

		$('.data-fetch').remove();
				
				revenue_daily_data.datasets[0].data = setData(r.cake);

				revenue_source_daily_data.datasets[0].data = setData(r.dfp);
				revenue_source_daily_data.datasets[1].data = setData(r.cake);

				$default_charts = $('canvas.default-chart');



				$default_charts.each(function(){

					$(this).parent().append('<div></div>');

					var target_canvas_id = $(this).attr('id');
					var target_canvas_data = $(this).data('info');
					////console.log('waa '.target_canvas_data);
					var canvas_ctx = document.getElementById(target_canvas_id).getContext("2d");

					window.myLine = new Chart(canvas_ctx).Line(window[target_canvas_data], {
						responsive: true
					});

					legend($(this).parent().find('div'), window[target_canvas_data]);

				});
	}

}


var revenue_weekly_data = {
	labels : ["Data 1","Data 2","Data 3","Data 4","Data 5","Data 6","Data 7"],
	datasets : [
		{
			label: "Weekly Revenue",
			fillColor : "rgba(220,220,220,0.2)",
			strokeColor : "#3399FF",
			pointColor : "#3399FF",
			pointStrokeColor : "#fff",
			pointHighlightFill : "#fff",
			pointHighlightStroke : "rgba(220,220,220,1)",
			data : [10,20,50,40,50,60,70]
		}
	]
}

function setLabels(r,tab){
	var res = [];

	//for(var x = 0; x < r.length; x++){
	var start_date = r[0].start_date;
	var split_date = start_date.split('-');
	//res.push(start_date);
	var end_date = r[0].end_date;
		
	switch(tab){
		case 'weekly':
			//var date = new Date(parseInt(split_date[0]),parseInt(split_date[1]),parseInt(split_date[2]));
			//var temp;

			for(var x = 0; x < 7; x++){
				//temp = date - x;
				//temp = new Date(temp);
				var temp = new Date(parseInt(split_date[0]),parseInt(split_date[1])-1,parseInt(split_date[2])+x);
				var str =  (temp.getMonth()+1) + "/" + temp.getDate() + "/" + (temp.getFullYear() - 2000);
				res.push(str);
			}

			break;
	
		case 'monthly':

			for(var x = 0; x < 31; x++){
				var temp = new Date(parseInt(split_date[0]),parseInt(split_date[1])-1,parseInt(split_date[2])+x);
				var str =  (temp.getMonth()+1) + "/" + temp.getDate() + "/" + (temp.getFullYear() - 2000);
				res.push(str);
			}

			break;
	}

	return res;
}
/*
function setDataWithLabels(r,labels){
	var res = [];
	var count = labels.length;

	var i = 0;
	for(var x = 0; x < count; x++){
		var date_label = labels[x];
		var res_date = r[i].date[0];

		//console.log(date_label);
		//console.log(r[i].date[0]);
		//console.log(date_label == res_date);

		if(date_label == res_date){
			var rev = r[i].revenue[0]; 
			rev = rev.replace("$","");
			rev = parseFloat(rev);
			res.push(rev);
			i++;
		}else{
			res.push(0);
		}
	}

	return res;
}*/

function setDataWithLabels(revenue,labels){
	var res = [];
	var count = labels.length;

	var i = 0;
	for(var x = 0; x < count; x++){
		var date_label = labels[x];
		var rev_arr = getRevenueFromArr(revenue,date_label);

		//console.log(date_label);
		//console.log(revenue);
		//console.log(rev_arr);

		if(rev_arr){
			res.push(rev_arr);
		}else{
			res.push(0);
		}
	}

	return res;
}

function getRevenueFromArr(arr,date){
	for(var x = 0; x < arr.length; x++){
		var temp = arr[x];
		if(temp['date'] == date){
			return temp['revenue'];
		}
	}

	return false;	
}

function getRevenue(r){
	var res = [];

	for(var x = 0; x < r.length; x++){
		var date = "";
		if(r[x].date instanceof Object){
			date = r[x].date[0];
		}else{
			date = r[x].date;
		}
		

		var rev = "";
		if(r[x].revenue instanceof Object){
			rev = r[x].revenue[0];
		}else{
			rev = r[x].revenue;
		}
		

		//console.log(rev);
		//console.log(r[x].revenue instanceof Object);
		rev = rev.replace("$","");
		var temp = [];

		var exists = checkDateExists(res,date);
		if(exists !== false){
			var tmp_rev = res[exists]['revenue'];
			res[exists]['revenue'] = parseFloat(tmp_rev) + parseFloat(rev);
		}else{
			temp['date'] = date;
			temp['revenue'] = parseFloat(rev);

			res.push(temp);
		}
	}

	return res;
}

function checkDateExists(arr,date){
	for(var x = 0; x < arr.length; x++){
		var temp = arr[x];
		if(temp['date'] == date){
			return x;
		}
	}

	return false;	
}

var raw_data = {
	labels : ["Data 1","Data 2","Data 3","Data 4","Data 5","Data 6","Data 7"],
	datasets : [
		{
			label: "Weekly Revenue",
			fillColor : "rgba(220,220,220,0.2)",
			strokeColor : "#3399FF",
			pointColor : "#3399FF",
			pointStrokeColor : "#fff",
			pointHighlightFill : "#fff",
			pointHighlightStroke : "rgba(220,220,220,1)",
			data : [10,20,50,40,50,60,70]
		}
	]
}

function renderChart(tab,target_canvas_tab,chart_name){
	$(target_canvas_tab).prepend('<div class="data-fetch" style="text-align: center"><img src="images/loader.gif" />Fetching data. Please wait...</div>');
	//$('#revenue-source-daily').prepend('<div class="data-fetch" style="text-align: center"><img src="images/loader.gif" />Fetching data. Please wait...</div>');
	if(!isDataOnCookie(target_canvas_tab)){
		$.ajax({
			url: url+'/admin/get-report',
			type: 'POST',
			data: 'date_type='+tab+"&chart_name="+chart_name,
			//dataType: 'json',
			success: function(r){
				var store = r;
				r = jQuery.parseJSON(r);
				if(chart_name == 'revenue-source'){
					processRevenueSourceChart(r,tab,target_canvas_tab);
					setCookie(target_canvas_tab,store,20);
				}else{
					processDataChart(r,tab,target_canvas_tab);
					setCookie(target_canvas_tab,store,20);
				}				
				
			},
			error: function(x,y,z){
				console.log(x);
				console.log(y);
				console.log(z);
			}
		});
	}else{
		var data = getCookie(target_canvas_tab);
		data = jQuery.parseJSON(data);
		if(chart_name == 'revenue-source'){
			processRevenueSourceChart(data,tab,target_canvas_tab);
		}else{
			processDataChart(data,tab,target_canvas_tab);
		}
	}
}

function isDataOnCookie(target_canvas_tab){
	var data = getCookie(target_canvas_tab);

	if(data == ""){
		return false;
	}

	return true;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
    }
    return "";
}

function checkCookie() {
    var username=getCookie("username");
    if (username!="") {
        alert("Welcome again " + username);
    }else{
        username = prompt("Please enter your name:", "");
        if (username != "" && username != null) {
            setCookie("username", username, 365);
        }
    }
}

function setCookie(cname, cvalue, mins) {
    var d = new Date();
    d.setTime(d.getTime() + (mins*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function processDataChart(r,tab,target_canvas_tab){
	//console.log(r);
			if(r.message && r.message == 'error'){
				$('.data-fetch').html('Please check your connection.');
				return false;
			}
			$('.data-fetch').remove();

			//var target_canvas_tab = $(this).find('a').attr('href');

			var $target_canvas = $(target_canvas_tab).find('canvas');

			var target_canvas_width = $target_canvas.attr('width');
			var target_canvas_id = $target_canvas.attr('id');
			var target_canvas_data = $target_canvas.data('info');
			//console.log(target_canvas_data);
			var labels = setLabels(r,tab);
			var data = setDataWithLabels(getRevenue(r),labels);

			raw_data.datasets[0].data = data;
			raw_data.labels = labels;

			if(target_canvas_width == "100%"){

				var data = raw_data;

				var target_chart = $(target_canvas_tab+"-chart").parent();

				target_chart.append('<div></div>');

				var canvas_ctx = document.getElementById(target_canvas_id).getContext("2d");

				window.myLine = new Chart(canvas_ctx).Line(data, {
					responsive: true
				});

				legend(target_chart.find('div'), data);

			}
}

var revenue_source_raw_data = {
	labels : ["Data 1","Data 2","Data 3","Data 4","Data 5","Data 6","Data 7"],
	datasets : [
		{
			label: "Ads Revenue",
			fillColor : "rgba(220,220,220,0.2)",
			strokeColor : "#3399FF",
			pointColor : "#3399FF",
			pointStrokeColor : "#fff",
			pointHighlightFill : "#fff",
			pointHighlightStroke : "rgba(220,220,220,1)",
			data : [10,20,50,70,50,60,70]
		},
		{
			label: "Downloads Revenue",
			fillColor : "rgba(220,220,220,0.2)",
			strokeColor : "#FF8080",
			pointColor : "#FF8080",
			pointStrokeColor : "#fff",
			pointHighlightFill : "#fff",
			pointHighlightStroke : "rgba(220,220,220,1)",
			data : [10,20,50,70,50,60,70]
		}
	]
}

function processRevenueSourceChart(r,tab,target_canvas_tab){
			//console.log(r);
			$('.data-fetch').remove();

			//var target_canvas_tab = $(this).find('a').attr('href');

			var $target_canvas = $(target_canvas_tab).find('canvas');

			var target_canvas_width = $target_canvas.attr('width');
			var target_canvas_id = $target_canvas.attr('id');
			var target_canvas_data = $target_canvas.data('info');
			//console.log(target_canvas_data);
			var labels = setLabels(r.dfp,tab);
			var data_ads = setDataWithLabels(getRevenue(r.dfp),labels);
			var data_downloads = setDataWithLabels(getRevenue(r.cake),labels);

			revenue_source_raw_data.datasets[0].data = data_ads;
			revenue_source_raw_data.datasets[1].data = data_downloads;
			revenue_source_raw_data.labels = labels;

			if(target_canvas_width == "100%"){

				var data = revenue_source_raw_data;

				var target_chart = $(target_canvas_tab+"-chart").parent();

				target_chart.append('<div></div>');

				var canvas_ctx = document.getElementById(target_canvas_id).getContext("2d");

				window.myLine = new Chart(canvas_ctx).Line(data, {
					responsive: true
				});

				legend(target_chart.find('div'), data);
			}
}