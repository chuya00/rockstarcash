<?php
/*
Plugin Name: User Profiles Made Easy
Plugin URI: http://codecanyon.net/item/user-profiles-plugin-for-wordpress/4109874?ref=ThemeFluent
Description: An awesome user profiles plugin for WordPress.
Version: 1.3.8
Author: ThemeFluent
Author URI: http://themeforest.net/user/ThemeFluent?ref=ThemeFluent
*/

define('upme_url',plugin_dir_url(__FILE__ ));
define('upme_path',plugin_dir_path(__FILE__ ));

	// Add settings link on plugin page
	function upme_settings_link($links) {
		$settings_link = '<a href="options-general.php?page=wp-upme">Settings</a>';
		array_push($links, $settings_link);
		return $links;
	}
	$plugin = plugin_basename(__FILE__); 
	add_filter("plugin_action_links_$plugin", 'upme_settings_link' );

	/* Init */
	require_once upme_path . 'init/init.php';
	
	/* Classes */
	require_once upme_path . 'classes/class-upme-predefined.php';
	require_once upme_path . 'classes/class-upme.php';
	require_once upme_path . 'classes/class-upme-save.php';
	require_once upme_path . 'classes/class-upme-register.php';
	require_once upme_path . 'classes/class-upme-login.php';

	/* Shortcodes */
	require_once upme_path . 'shortcodes/shortcode-init.php';
	require_once upme_path . 'shortcodes/shortcodes.php';
	
	/* Registration customizer */
	require_once upme_path . 'registration/upme-register.php';
	
	/* Widgets */
	require_once upme_path . 'widgets/upme-widgets.php';
	
	/* Scripts - dynamic css */
	add_action('wp_footer', 'upme_custom_scripts');
	function upme_custom_scripts(){
		require_once upme_path . 'js/upme-custom-js.php';
	}

	/* Admin panel */
	if (is_admin()) {

		require_once(upme_path . 'classes/class-upme-admin.php');
		require_once(upme_path . 'classes/class-upme-sync-woocommerce.php');
		require_once(upme_path . 'admin/admin-icons.php');
	
	}