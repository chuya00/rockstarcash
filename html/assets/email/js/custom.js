$(document).ready(function(){
	$('#template').hide();
	$('#cancelTabs').click(function(){
		recordCloseGuide();
	});
	disableGuide();
	enableGuide();
});

$( window ).unload(function() {
  alert( "Bye now!" );
});

function enableGuide(){
	$('#seeGuide').click(function(){
		$('#mask1').show();
	});
	
	setCookie('guideClosed','',0.01);
}

function disableGuide(){
	var guide = getCookie('guideClosed');
	
	if(guide == '1'){
		$('#mask1').hide();
	}
}

function recordCloseGuide(){
	setCookie('guideClosed','1',1);
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
    }
    return "";
}