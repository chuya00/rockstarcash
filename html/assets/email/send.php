<?php 
session_start();
if(!isset($_SESSION['user_id'])){header("location: http://rockstarcash.com/login.php?msg=Please%20login");}
?>
<!DOCTYPE html>
<html>
<head>
<title>Thin Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap -->
<link href="css/bootstrap.css" rel="stylesheet" media="screen">
<link href="css/thin-admin.css" rel="stylesheet" media="screen">
<link href="css/font-awesome.css" rel="stylesheet" media="screen">
<link href="style/style.css" rel="stylesheet">
<link href="style/dashboard.css" rel="stylesheet">
<link rel='stylesheet' type='text/css' href='assets/jquery-fileupload/css/jquery.fileupload-ui.css' /> 

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<div class="container">
  <div class="top-navbar header b-b"> <a data-original-title="Toggle navigation" class="toggle-side-nav pull-left" href="#"><i class="icon-reorder"></i> </a>
    <div class="brand pull-left"> <a href="index.html"><img src="images/logo.png" width="173" height="87"></a></div>
    <ul class="nav navbar-nav navbar-right  hidden-xs">
      <li class="dropdown"> <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <i class="icon-warning-sign"></i> <span class="badge">5</span> </a>
        <ul class="dropdown-menu extended notification">
          <li class="title">
            <p>You have 5 new notifications</p>
          </li>
          <li> <a href="#"> <span class="label label-success"><i class="icon-plus"></i></span> <span class="message">New user registration.</span> <span class="time">1 mins</span> </a> </li>
          <li> <a href="#"> <span class="label label-danger"><i class="icon-warning-sign"></i></span> <span class="message">High CPU load on cluster #2.</span> <span class="time">5 mins</span> </a> </li>
          <li> <a href="#"> <span class="label label-success"><i class="icon-plus"></i></span> <span class="message">New user registration.</span> <span class="time">10 mins</span> </a> </li>
          <li> <a href="#"> <span class="label label-info"><i class="icon-bullhorn"></i></span> <span class="message">New items are in queue.</span> <span class="time">25 mins</span> </a> </li>
          <li> <a href="#"> <span class="label label-warning"><i class="icon-bolt"></i></span> <span class="message">Disk space to 85% full.</span> <span class="time">35 mins</span> </a> </li>
          <li class="footer"> <a href="#">View all notifications</a> </li>
        </ul>
      </li>
      <li class="dropdown"> <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <i class="icon-tasks"></i> <span class="badge">7</span> </a>
        <ul class="dropdown-menu extended notification">
          <li class="title">
            <p>You have 7 pending tasks</p>
          </li>
          <li> <a href="#"> <span class="task"> <span class="desc">Preparing new release</span> <span class="percent">30%</span> </span>
            <div class="progress progress-small">
              <div class="progress-bar progress-bar-info" style="width: 30%;"></div>
            </div>
            </a> </li>
          <li> <a href="#"> <span class="task"> <span class="desc">Change management</span> <span class="percent">80%</span> </span>
            <div class="progress progress-small progress-striped active">
              <div class="progress-bar progress-bar-danger" style="width: 80%;"></div>
            </div>
            </a> </li>
          <li> <a href="#"> <span class="task"> <span class="desc">Mobile development</span> <span class="percent">60%</span> </span>
            <div class="progress progress-small">
              <div class="progress-bar progress-bar-success" style="width: 60%;"></div>
            </div>
            </a> </li>
          <li> <a href="#"> <span class="task"> <span class="desc">Database migration</span> <span class="percent">20%</span> </span>
            <div class="progress progress-small">
              <div class="progress-bar progress-bar-warning" style="width: 20%;"></div>
            </div>
            </a> </li>
          <li class="footer"> <a href="#">View all tasks</a> </li>
        </ul>
      </li>
      <li class="dropdown"> <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <i class="icon-envelope"></i> <span class="badge">1</span> </a>
        <ul class="dropdown-menu extended notification">
          <li class="title">
            <p>You have 3 new messages</p>
          </li>
          <li> <a href="#"> <span class="photo"> <img src="images/profile.png" width="34" height="34"></span> <span class="subject"> <span class="from">John Doe</span> <span class="time">Just Now</span> </span> <span class="text"> Consetetur sadipscing elitr...</span> </a> </li>
          <li> <a href="#"> <span class="photo"><img src="images/profile.png" width="34" height="34"></span> <span class="subject"> <span class="from">John Doe</span> <span class="time">35 mins</span> </span> <span class="text"> Sed diam nonumy... </span> </a> </li>
          <li> <a href="#"> <span class="photo"><img src="images/profile.png" width="34" height="34"></span> <span class="subject"> <span class="from">John Doe</span> <span class="time">5 hours</span> </span> <span class="text"> No sea takimata sanctus... </span> </a> </li>
          <li class="footer"> <a href="#">View all messages</a> </li>
        </ul>
      </li>
      <li class="dropdown user  hidden-xs"> <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <i class="icon-male"></i> <span class="username">John Doe</span> <i class="icon-caret-down small"></i> </a>
        <ul class="dropdown-menu">
          <li><a href="user_profile.html"><i class="icon-user"></i> My Profile</a></li>
          <li><a href="fullCalendar.html"><i class="icon-calendar"></i> My Calendar</a></li>
          <li><a href="#"><i class="icon-tasks"></i> My Tasks</a></li>
          <li class="divider"></li>
          <li><a href="login.html"><i class="icon-key"></i> Log Out</a></li>
        </ul>
      </li>
    </ul>
    
  </div>
</div>
<div class="wrapper">
  <div class="left-nav">
    <div id="side-nav">
      <ul id="nav">
        <li class="current"> <a href="index.html"> <i class="icon-dashboard"></i> Dashboard </a> </li>
        <li> <a href="stats.php"> <i class="icon-desktop"></i> Stats </a> </li>
        <li> <a href="#"> <i class="icon-edit"></i> Tools </i> </a></li>
		<li> <a href="#"> <i class="icon-edit"></i> Emails <i class="arrow icon-angle-left"></i> </a>
		<ul class="sub-menu opened">
            <li class="current"> <a href="email.php"> <i class="icon-angle-right"></i> Emails </a> </li>
			<li class="current"> <a href="send.php"> <i class="icon-angle-right"></i> Send Email/s </a> </li>
          </ul>
		</li>
        <li> <a href="#"> <i class="icon-table"></i> Media <i class="arrow icon-angle-left"></i></a>
		<ul class="sub-menu">
            <li> <a href="audio.php"> <i class="icon-angle-right"></i> Upload MP3</a> </li>
            <li> <a href="video.html"> <i class="icon-angle-right"></i> Upload Video </a> </li>
			<li> <a href="audio-player.php"> <i class="icon-angle-right"></i> Audio Player </a> </li>
          </ul>
		</li>
        <li> <a href="chart.html"> <i class="icon-bar-chart"></i> Fans </a> </li>
        <li> <a href="gallery.html"> <i class="icon-picture"></i> Concerts </a> </li>
        <li> <a href="timeline.html"> <i class="icon-time"></i> Press &amp; Promo </a> </li>
        <li> <a href="#"> <i class="icon-folder-open-alt"></i> Todo </a></li>
		<li>
		<form >
			<input type="search" placeholder="Search..." class="search" id="search-input">
		</form>
		</li>
      </ul>
    </div>
  </div>
  <div class="page-content">
    <div class="content container">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="page-title">Email &#9654;</h2>
        </div>
      </div>
      <div style="margin-bottom: 10px">
		<a class="btn btn-s-md btn-primary" href="email/?id=<?php echo $_SESSION['id']; ?>" target="_blank">Create Email Template</a>
	  </div>
	  <div class="row" id="audio-upload-form" style="display: none">
        <div class="col-lg-12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-external-link"></i>
              <h3> Audio File Upload </h3>
            </div>
            <div class="widget-content">
              
              	
                
               <div class="panel-body">
					<div class="col-lg-12">
						<div class="widget">
							<div class="widget-content">
							<!-- The file upload form used as target for the file upload widget -->
								<form id="fileupload" action="" method="POST" enctype="multipart/form-data" />
								<!--
									<legend class="section">Album</legend>
									<div class="row">
									<div class="col-lg-6">
									<div class="control-group">
									  <div class="col-md-3">
										<label for="normal-field" class="control-label">Album Name</label>
										</div>
										<div class="col-md-9">
										<div class="form-group">
										  <input type="text" placeholder="name" class="form-control" id="normal-field">
										</div>
										</div>
									  </div>
									</div>
									</div>
									-->
									<!-- Redirect browsers with JavaScript disabled to the origin page -->
									<noscript><input type="hidden" name="redirect" value="http://blueimp.github.io/jQuery-File-Upload/" /></noscript>
									<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
									<legend class="section">Music/s</legend>
									<div class="row fileupload-buttonbar">
										<div class="col-lg-7">
										<div class="btn-toolbar">
											<!-- The fileinput-button span is used to style the file input field as button -->
											<span class="btn btn-success fileinput-button">
												<i class="icon-plus"></i>
												<span>Add mp3 files</span>
												<input type="file" name="files[]" multiple />
											</span>
											<button type="submit" class="btn btn-primary start">
												<i class="icon-upload"></i>
												<span>Start upload</span>
											</button>
											<button type="reset" class="btn btn-warning cancel">
												<i class="icon-ban-circle"></i>
												<span>Cancel upload</span>
											</button>
											<button type="button" class="btn btn-danger delete">
												<i class="icon-trash"></i>
												<span>Delete</span>
											</button>
											<input type="checkbox" class="toggle" />
											<!-- The loading indicator is shown during file processing -->
											<span class="fileupload-loading"></span>
										</div>
										</div>
										<!-- The global progress information -->
										<div class="col-lg-5 fileupload-progress fade">
											<!-- The global progress bar -->
											<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
												<div class="progress-bar progress-bar-success" style="width:0%;"></div>
											</div>
											<!-- The extended global progress information -->
											<div class="progress-extended">&nbsp;</div>
										</div>
									</div>
									<!-- The table listing the files available for upload/download -->
									<table role="presentation" class="table table-striped">
										<tbody class="files"></tbody>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
              
            </div>
          </div>
        </div>
      </div>
      
	  <div class="row">
		<div class="col-lg-12">
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Email Templates</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered" id="audio-table">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Filename</th>
					<th>Action</th>
                  </tr>
                </thead>
                <tbody>
				  <?php 
				  $con=mysqli_connect("mmww.webair.com","quitos","*p1n0ywarr10r*","14183_rockstarcash");
				  $user_id = $_SESSION['id'];
				  $audio_query = "SELECT * FROM email_templates WHERE user_id = ".$user_id."";
				  $res = mysqli_query($con,$audio_query);
				  $count = 0;
				  while($row = mysqli_fetch_array($res)){
				  ?>
                  <tr id="tr<?php echo $count; ?>">
                    <td><?php echo $row['id']; ?></td>
                    <td><?php echo $row['filename']; ?></td>
					<?php //<td class="audio_title" ><?php echo $row['album']; </td> 
					?>
					<td>
						<a class="btn btn-s-md btn-default btn-sm show-details-section" details-count="<?php echo $count; ?>" href="javascript:void(0)">Edit</a>
						<button class="btn btn-danger delete delete-audio-file" data-id="<?php echo $row['id']; ?>" data-file="<?php echo $row['filename']; ?>" data-type="DELETE" data-url="http://rockstarcash.com/admintheme-dev/assets/plugins/jquery-fileupload/server/php/?file=<?php echo str_replace(" ","%20",$row['filename']); ?>">
							<i class="icon-trash "></i>
							<span>Delete</span>
						</button>
					</td>
                  </tr>
				  <tr>
					<td colspan="5" class="details-section-td<?php echo $count; ?>" style="display: none" >
						<div class="details-section">
							<span id="audio-alert<?php echo $count; ?>" ></span>
							<?php /*
							<input type="text" placeholder="Album name" class="form-control" id="album<?php echo $count; ?>" value="<?php echo isset($row['album'])? $row['album'] : '' ; ?>" style="width: 200px; display: inline; margin-right: 10px;">
							*/ ?>
							<input type="text" placeholder="Title" class="form-control" id="title<?php echo $count; ?>" value="<?php echo isset($row['title'])? $row['title'] : '' ; ?>" style="width: 200px; display: inline; margin-right: 10px;">
							<input type="text" placeholder="Tags" class="form-control" id="tag<?php echo $count; ?>" value="<?php echo isset($row['tag'])? $row['tag'] : '' ; ?>" style="width: 200px; display: inline; margin-right: 10px;">
							<input 	type="hidden" id="filename<?php echo $count; ?>" value="<?php echo $row['filename']; ?>" />
							<input 	type="hidden" id="audio-id<?php echo $count; ?>" value="<?php echo $row['id']; ?>" />
							
							<div data-toggle="buttons" class="btn-group" style="margin-right: 10px;">
							  <label class="btn btn-sm btn-default">
								<input type="checkbox" id="option1" name="options">
								Itunes </label>
							  <label class="btn btn-sm btn-default">
								<input type="checkbox" id="option2" name="options">
								Amazon </label>
							</div>
							
							<button class="btn btn-primary start audio-details-btn" details-count="<?php echo $count; ?>" >
								<i class="icon-upload"></i>
								<span>Save</span>
							</button>
							<!--Audio Image uploader-->
							<div id="audio-img-container-backup{%=i%}" style="display: none; position: fixed; top: 30%; left: 30%; z-index: 1" >
								<div class="row">
									<div class="col-lg-12">
										<div class="widget">
											<div class="widget-header"> <i class="icon-external-link"></i>
											  <h3> Audio Image Upload </h3>
											</div>
											<div class="widget-content">
												<input type="hidden" id="a-filename" >
												<br /><br />
												<span class="btn btn-success fileinput-button">
													<i class="icon-plus"></i>
													<span>Add files...</span>
													<!-- The file input field used as target for the file upload widget -->
													<input id="album-img-upload{%=i%}" type="file" name="files[]" multiple>
												</span>
												<br><br>
												<!-- The global progress bar -->
												<div id="a-progress{%=i%}" class="a-progress{%=i%}">
													<div class="progress-bar progress-bar-success"></div>
												</div>
												<!-- The container for the uploaded files -->
												<div id="a-files{%=i%}" class="a-files{%=i%}"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</td>
				  </tr>
                  <?php $count++; } ?>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
	  </div>
	  
    </div>
  </div>
</div>
<div class="bottom-nav footer"> 2013 &copy; Thin Admin by Riaxe Systems. </div>


<div id="bg-overlay" style="display:none; position: fixed; top: 0px; left: 0px; height: 100%; width: 100%; background-color: #000000; opacity:0.8; filter:alpha(opacity=80);" >&nbsp;</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="js/jquery.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script type="text/javascript" src="js/smooth-sliding-menu.js"></script> 

<!--switcher html start-->
<div class="demo_changer active" style="right: 0px;">
  <div class="demo-icon"></div>
  <div class="form_holder">
    <div class="predefined_styles"> <a class="styleswitch" rel="a" href=""><img alt="" src="images/a.jpg"></a> <a class="styleswitch" rel="b" href=""><img alt="" src="images/b.jpg"></a> <a class="styleswitch" rel="c" href=""><img alt="" src="images/c.jpg"></a> <a class="styleswitch" rel="d" href=""><img alt="" src="images/d.jpg"></a> <a class="styleswitch" rel="e" href=""><img alt="" src="images/e.jpg"></a> <a class="styleswitch" rel="f" href=""><img alt="" src="images/f.jpg"></a> <a class="styleswitch" rel="g" href=""><img alt="" src="images/g.jpg"></a> <a class="styleswitch" rel="h" href=""><img alt="" src="images/h.jpg"></a> <a class="styleswitch" rel="i" href=""><img alt="" src="images/i.jpg"></a> <a class="styleswitch" rel="j" href=""><img alt="" src="images/j.jpg"></a> </div>
  </div>
</div>

<!--switcher html end--> 
<script src="assets/switcher/switcher.js"></script> 
<script src="assets/switcher/moderziner.custom.js"></script>
<link href="assets/switcher/switcher.css" rel="stylesheet">
<link href="assets/switcher/switcher-defult.css" rel="stylesheet">
<link rel="alternate stylesheet" type="text/css" href="assets/switcher/a.css" title="a" media="all" />
<link rel="alternate stylesheet" type="text/css" href="assets/switcher/b.css" title="b" media="all" />
<link rel="alternate stylesheet" type="text/css" href="assets/switcher/c.css" title="c" media="all" />
<link rel="alternate stylesheet" type="text/css" href="assets/switcher/d.css" title="d" media="all" />
<link rel="alternate stylesheet" type="text/css" href="assets/switcher/e.css" title="e" media="all" />
<link rel="alternate stylesheet" type="text/css" href="assets/switcher/f.css" title="f" media="all" />
<link rel="alternate stylesheet" type="text/css" href="assets/switcher/g.css" title="g" media="all" />
<link rel="alternate stylesheet" type="text/css" href="assets/switcher/h.css" title="h" media="all" />
<link rel="alternate stylesheet" type="text/css" href="assets/switcher/i.css" title="i" media="all" />
<link rel="alternate stylesheet" type="text/css" href="assets/switcher/j.css" title="j" media="all" />


<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            {% if (file.error) { %}
            <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <p class="size">{%=o.formatFileSize(file.size)%}</p>
            {% if (!o.files.error) { %}
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"
                 aria-valuenow="0">
                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
            </div>
            {% } %}
        </td>
        <td>
            {% if (!o.files.error && !i && !o.options.autoUpload) { %}
            <button class="btn btn-primary start">
                <i class="icon-upload"></i>
                <span>Start</span>
            </button>
            {% } %}
            {% if (!i) { %}
            <button class="btn btn-warning cancel">
                <i class="icon-ban-circle"></i>
                <span>Cancel</span>
            </button>
            {% } %}
        </td>
    </tr>
	<tr><td colspan="4">
	<!--Audio Image uploader-->
<div id="audio-img-container{%=i%}" style="display: none; position: fixed; top: 30%; left: 30%; z-index: 1" >
	<div class="row">
		<div class="col-lg-12">
			<div class="widget">
				<div class="widget-header"> <i class="icon-external-link"></i>
				  <h3> Audio Image Upload </h3>
				</div>
				<div class="widget-content">
					<input type="hidden" id="a-filename" >
					<br /><br />
					<span class="btn btn-success fileinput-button">
						<i class="icon-plus"></i>
						<span>Add files...</span>
						<!-- The file input field used as target for the file upload widget -->
						<input id="album-img-upload{%=i%}" type="file" name="files[]" multiple>
					</span>
					<br><br>
					<!-- The global progress bar -->
					<div id="a-progress{%=i%}" class="a-progress{%=i%}">
						<div class="progress-bar progress-bar-success"></div>
					</div>
					<!-- The container for the uploaded files -->
					<div id="a-files{%=i%}" class="a-files{%=i%}"></div>
				</div>
			</div>
		</div>
	</div>
</div></td>
	</tr>
    {% } %}
</script>

<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
<?php /*
$con = connect();
$result = getAudios($con);
$i = 0;
while($row = mysqli_fetch_array($result)){*/ ?>

	{% var count = 0; %}
	{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade" style="display: none;" >
        <td>
<span class="preview">
    {% if (file.thumbnailUrl) { %}
        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img
                src="{%=file.thumbnailUrl%}"></a>
    {% } %}
</span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" class="upload-filename">
                    {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
            <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
            <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}">{% if
                (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}
                <i class="icon-trash "></i>
                <span>Delete</span>
            </button>
            <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
            <button class="btn btn-warning cancel">
                <i class="icon-ban-circle"></i>
                <span>Cancel</span>
            </button>
            {% } %}
        </td>
    </tr>
	<?php /*
	<tr class="template-download fade" style="border-bottom: 3px solid rgba(255,255,255, 0.2); " >
		<td colspan="4" >
			<a class="btn btn-s-md btn-info show-details-section-dev" style="margin-bottom: 5px;" href="javascript:void(0)">Add Details</a>
			<a class="btn btn-s-md btn-info hide-details-section" style="margin-bottom: 5px; display: none" href="javascript:void(0)">Hide Details</a>
			<div class="details-section" style="display: none">
				<span id="audio-alert{%= i %}" ></span>
				<input type="text" placeholder="Album name" class="form-control" id="album{%= i %}" value="<?php echo isset($row['album'])? $row['album'] : '' ; ?>" style="width: 200px; display: inline; margin-right: 10px;">
				<input type="text" placeholder="Title" class="form-control" id="title{%= i %}" value="<?php echo isset($row['title'])? $row['title'] : '' ; ?>" style="width: 200px; display: inline; margin-right: 10px;">
				<input type="text" placeholder="Tags" class="form-control" id="tag{%= i %}" value="<?php echo isset($row['tag'])? $row['tag'] : '' ; ?>" style="width: 200px; display: inline; margin-right: 10px;">
				<input 	type="hidden" id="filename{%= i %}" value="{%=file.name%}" />
				
				<div data-toggle="buttons" class="btn-group" style="margin-right: 10px;">
                  <label class="btn btn-sm btn-default">
                    <input type="checkbox" id="option1" name="options">
                    Itunes </label>
                  <label class="btn btn-sm btn-default">
                    <input type="checkbox" id="option2" name="options">
                    Amazon </label>
                </div>
				
				<button class="btn btn-primary start audio-img-btn" details-count="{%= i %}" >
					<i class="icon-plus"></i>
					<span>Upload Image</span>
				</button>
				
				<button class="btn btn-primary start audio-details-btn" details-count="{%= i %}" >
					<i class="icon-upload"></i>
					<span>Save</span>
				</button>
				<!--Audio Image uploader-->
				<div id="audio-img-container{%=i%}" style="display: none; position: fixed; top: 30%; left: 30%; z-index: 1" >
					<div class="row">
						<div class="col-lg-12">
							<div class="widget">
								<div class="widget-header"> <i class="icon-external-link"></i>
								  <h3> Audio Image Upload </h3>
								</div>
								<div class="widget-content">
									<input type="hidden" id="a-filename" >
									<br /><br />
									<span class="btn btn-success fileinput-button">
										<i class="icon-plus"></i>
										<span>Add files...</span>
										<!-- The file input field used as target for the file upload widget -->
										<input id="album-img-upload{%=i%}" type="file" name="files[]" multiple>
									</span>
									<br><br>
									<!-- The global progress bar -->
									<div id="a-progress{%=i%}" class="a-progress{%=i%}">
										<div class="progress-bar progress-bar-success"></div>
									</div>
									<!-- The container for the uploaded files -->
									<div id="a-files{%=i%}" class="a-files{%=i%}"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</td>
	</tr>
	*/ ?>
	{% count++; } %}
   <?php //$i++; } 
   ?>
   <div style="display: none" id="data-count" total="{%= count %}"  ></div> 
</script>
<script type='text/javascript' src='javascript/jquery-1.10.2.min.js'></script> 
<script type='text/javascript' src='assets/jquery-fileupload/js/vendor/jquery.ui.widget.js'></script> 
<script type='text/javascript' src='assets/jquery-fileupload/js/tmpl.min.js'></script> 
<script type='text/javascript' src='assets/jquery-fileupload/js/load-image.min.js'></script> 
<script type='text/javascript' src='assets/jquery-fileupload/js/canvas-to-blob.min.js'></script> 
<script type='text/javascript' src='assets/jquery-fileupload/js/jquery.blueimp-gallery.min.js'></script> 
<script type='text/javascript' src='assets/jquery-fileupload/js/jquery.fileupload.js'></script> 
<script type='text/javascript' src='assets/jquery-fileupload/js/jquery.fileupload-process.js'></script> 
<script type='text/javascript' src='assets/jquery-fileupload/js/jquery.fileupload-image.js'></script> 
<script type='text/javascript' src='assets/jquery-fileupload/js/jquery.fileupload-audio.js'></script> 
<script type='text/javascript' src='assets/jquery-fileupload/js/jquery.fileupload-video.js'></script> 
<script type='text/javascript' src='assets/jquery-fileupload/js/jquery.fileupload-validate.js'></script> 
<script type='text/javascript' src='assets/jquery-fileupload/js/jquery.fileupload-ui.js'></script> 
<script type='text/javascript' src='assets/jquery-fileupload/js/main-audio.js'></script> 
<script type='text/javascript' src='assets/jquery-fileupload/js/audio.js'></script>
<script type='text/javascript'>
/*jslint unparam: true, regexp: true */
/*global window, $ */
$(function () {
    
});
</script>
</body>
</html>