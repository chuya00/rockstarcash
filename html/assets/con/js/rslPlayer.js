var rslPlayerJS = function() {
	
	var STATE_STOP = 'state_stop', 
		STATE_PLAY = 'state_play', 
		STATE_PAUSE = 'state_pause', 
		STATE_ERROR = 'state_error',
		STATE_CLOSED = 'state_closed',
		STATE_OPEN = 'state_open',
		TYPE_MP3 = 'type_mp3', 
		TYPE_OGG = 'type_ogg', 
		DEFAULT_WIDTH = 600;
		MIN_WIDTH = 300;
	
	var player, jsonData, audio, btnPlay, shareButtons, trackListContainer;
	var albumCover, albumCoverShine, artistNameLabel, trackTimeLabel;
	var autoPlay, mediaType, size;
	var btnDownload, btnShare, btnShareClose, btnTrack, btnTrackClose, btnTrackPlay, btnTrackPrev, btnTrackNext;
	var scrub, track, trackOver, scrollOffset, playTrack, playTrackOver;
	var statePlayer = '', bolPlaying = false, stateShare, stateTrackList;
	var shareFacebook, shareTwitter, shareTumbler, shareInstagram, shareLink, shareEmail, shareEmbed;
	var btnBuyItunes, btnBuyAmazon, btnBuyEmusic;
	var btnLogo;
	var trackID, trackListData, trackListMenuTrack, btnListMenuScrub, trackListMenuContainer, trackListMenu, trackListMenuItems;
	
	return {

		init:function(id, data) {
	
			//console.log(id + " " + data.albumCoverURL);
			
			audio = new Audio();
	
			var canPlayMp3 = !!audio.canPlayType && audio.canPlayType('audio/mpeg; codecs="mp3"') != "";
			var canPlayOgg = !!audio.canPlayType && audio.canPlayType('audio/ogg; codecs="vorbis"') != "";
			
			if(canPlayMp3 || canPlayOgg)
			{
			
				if(canPlayMp3)
				{
					mediaType = TYPE_MP3;
				} else {	
					mediaType = TYPE_OGG;
				}
			
				rslPlayerJS.initPlayer(id,data);
			
			} else {
				
				// Insert Error Code Here
			
			}
			
		},
		
		initPlayer:function(id, data) {
			
			jsonData = data;
			
			player = $("#" + id);
			albumCover = player.find(".albumCover");
			albumCoverShine = player.find(".albumCoverShine");
			artistNameLabel = player.find(".artistNameLabel");
			//trackNameLabel = player.find(".trackNameLabel");
			trackTimeLabel = player.find(".trackTimeLabel");
			track = player.find(".playTrack");
			trackOver = player.find(".playTrackOver");
			scrubber = player.find(".playScrub");
			btnPlay = player.find(".buttonPlay");
			btnTrackPlay = player.find(".buttonTrackPlay");
			btnTrackPrev = player.find(".buttonTrackPrev");
			btnTrackNext = player.find(".buttonTrackNext");
			btnLogo = player.find(".buttonLogo");
			playTrack = player.find(".playTrack");
			playTrackOver = player.find(".playTrackOver");
			
			btnDownload = player.find('.buttonDownload');
			btnDownload.on('mousedown', rslPlayerJS.onMouseDownButtonDownload);
			
			btnShare = player.find(".buttonShare");
			btnShare.on('mousedown', rslPlayerJS.onMouseDownButtonShare);
			
			btnShareClose = player.find(".buttonShareClose");
			btnShareClose.on('mousedown', rslPlayerJS.onMouseDownButtonShareClose);
			
			shareButtons = player.find(".shareButtons");
			
			shareFacebook = player.find(".buttonShareFacebook");
			shareFacebook.data('link', data.shares.facebook);
			shareFacebook.on('mousedown', rslPlayerJS.onMouseDownShare);
			
			shareTwitter = player.find(".buttonShareTwitter");
			shareTwitter.data('link', data.shares.twitter);
			shareTwitter.on('mousedown', rslPlayerJS.onMouseDownShare);
			
			shareTumbler = player.find(".buttonShareTumbler");
			shareTumbler.data('link', data.shares.tumbler);
			shareTumbler.on('mousedown', rslPlayerJS.onMouseDownShare);
			
			shareInstagram = player.find(".buttonShareInstagram");
			shareInstagram.data('link', data.shares.instagram);
			shareInstagram.on('mousedown', rslPlayerJS.onMouseDownShare);
			
			shareLink = player.find(".buttonShareLink");
			shareLink.data('link', data.shares.link);
			shareLink.on('mousedown', rslPlayerJS.onMouseDownShare);
			
			shareEmail = player.find(".buttonShareEmail");
			shareEmail.data('link', data.shares.email);
			shareEmail.on('mousedown', rslPlayerJS.onMouseDownShare);
			
			shareEmbed = player.find(".buttonShareEmbed");
			shareEmbed.data('link', data.shares.embed);
			shareEmbed.on('mousedown', rslPlayerJS.onMouseDownShare);

			
			btnBuy = player.find(".buttonBuy");
			btnBuy.on('mousedown', rslPlayerJS.onMouseDownShare);
			
			btnBuyItunes = player.find(".buttonBuyItunes");
			btnBuyItunes.data('link', data.buyItNow.itunes);
			
			btnBuyAmazon = player.find(".buttonBuyAmazon");
			btnBuyAmazon.data('link', data.buyItNow.amazon);
			
			btnBuyEmusic = player.find(".buttonBuyEmusic");
			btnBuyEmusic.data('link', data.buyItNow.emusic);
			
			
			btnTrack = player.find(".buttonMore");
			btnTrack.on('mousedown', rslPlayerJS.onMouseDownButtonTrackList);
			
			btnTrackClose = player.find(".buttonTrackClose");
			btnTrackClose.on('mousedown', rslPlayerJS.onMouseDownButtonTrackListClose);

			trackListContainer = player.find(".trackListContainer");
			
			trackListMenuContainer = player.find(".trackListMenuContainer");
			trackListMenu = player.find(".trackListMenu");
			btnListMenuScrub = player.find(".btnListMenuScrub");
			trackListMenuTrack  = player.find(".trackListMenuTrack");
			
			btnListMenuScrub.bind('mousedown', rslPlayerJS.onMouseDownTrackListScrub);
			trackListMenuTrack.bind('mouseup', rslPlayerJS.onMouseUpTrackListTrack);
						
			audio.volume = 0.1;
			
			size = (data.size > MIN_WIDTH) ? data.size : MIN_WIDTH;
			
			var fontSize = (size / DEFAULT_WIDTH) * parseInt(player.css('fontSize'), 10);
			
			player.css({"width" : size + "px", "height" : size + "px", "fontSize" : fontSize + 'px' });
			
			albumCover.attr({"src" : data.albumCoverURL, "width" : size + "px", "height" : size + "px"});
			
			$('.rslPlayer .trackListContainer .albumIcon').css({"backgroundImage" : "url(" + data.albumCoverURL + ")" });
	
			rslPlayerJS.setTitle(data.artistName + "<br><span class=\"artistNameLabelSub colorYellow\">" + data.albumName + "</span>");
			
			rslPlayerJS.initTrackList(data.trackList);
			trackListMenuItems = player.find(".trackListItem");
			//trackListMenuItems.on('mousedown', rslPlayerJS.onMouseDownTrackListItem);
			//trackListMenuItems.on('touchstart', rslPlayerJS.onTouchDownTrackListItem);
			trackListMenuItems.on('mouseup', rslPlayerJS.onMouseUpTrackItem);
			
			btnPlay.on('click', rslPlayerJS.onMouseDownPlay);
			btnTrackPlay.on('click', rslPlayerJS.onMouseDownPlay);
			btnTrackPrev.on('click', rslPlayerJS.onMouseDownTrackPrev);
			btnTrackNext.on('click', rslPlayerJS.onMouseDownTrackNext);
			btnLogo.on('click', rslPlayerJS.onMouseDownLogo)

			autoPlay = data.autoStart;
			
			rslPlayerJS.setState(STATE_STOP);
			
			if(autoPlay)
			{
				rslPlayerJS.setState(STATE_PLAY);
			}
			
			rslPlayerJS.setStateShare(STATE_CLOSED);
			
			rslPlayerJS.setStateTrackList(STATE_CLOSED);
			
		},
		
		initTrackList:function(data) {
			
			trackListData = data;
			
			for(var i = 0; i < trackListData.length; i++)
			{
				
				var childLi = $(document.createElement('li'));
				childLi.addClass('trackListItem');
				childLi.data('index', i);
				
				if((i+1) % 2)
				{
					childLi.addClass('backTrackLight');
				} else {
					childLi.addClass('backTrackDark');					
				}
				
				childLi.html("<p class'trackNum'>" + pad(i + 1, 2) + "</p><p class='trackName'>"+ data[i].title +"</p><p class='trackDuration'>" + data[i].data.duration + "</p><a class='trackLink' href='" + data[i].data.download + "' target='_blank'>Download</a>");
				trackListMenu.append(childLi);
				
			}

			rslPlayerJS.listenersAudioAdd();
			rslPlayerJS.listenersMouseAdd();
			rslPlayerJS.setTrack(0);
			
		},
		
		listenersAudioAdd:function() {
			
			audio.addEventListener('durationchange', rslPlayerJS.onMediaEvent);
			audio.addEventListener('ended', rslPlayerJS.onMediaEvent);
			audio.addEventListener('pause', rslPlayerJS.onMediaEvent);
			audio.addEventListener('play', rslPlayerJS.onMediaEvent);
			audio.addEventListener('timeupdate', rslPlayerJS.onMediaEvent);
			audio.addEventListener('volumechange', rslPlayerJS.onMediaEvent);
			audio.addEventListener('canplay', rslPlayerJS.onMediaEvent);
			audio.addEventListener('canplaythrough', rslPlayerJS.onMediaEvent);
			audio.addEventListener('progress', rslPlayerJS.onMediaEvent);
			
		},
		
		listenersAudioRemove:function() {
			
			audio.removeEventListener('durationchange', rslPlayerJS.onMediaEvent);
			audio.removeEventListener('ended', rslPlayerJS.onMediaEvent);
			audio.removeEventListener('pause', rslPlayerJS.onMediaEvent);
			audio.removeEventListener('play', rslPlayerJS.onMediaEvent);
			audio.removeEventListener('timeupdate', rslPlayerJS.onMediaEvent);
			audio.removeEventListener('volumechange', rslPlayerJS.onMediaEvent);
			audio.removeEventListener('canplay', rslPlayerJS.onMediaEvent);
			audio.removeEventListener('canplaythrough', rslPlayerJS.onMediaEvent);
			audio.removeEventListener('progress', rslPlayerJS.onMediaEvent);
			
		},
		
		listenersMouseAdd:function() {
			scrubber.on('mousedown', rslPlayerJS.onMouseDownScrub);
			track.on('mouseup', rslPlayerJS.onMouseUpTrack);
		},
		
		listenersMouseRemove:function() {
			scrubber.off('mousedown', rslPlayerJS.onMouseDownScrub);
			track.off('mouseup', rslPlayerJS.onMouseUpTrack);
		},
		
		getState:function() {
			
			return statePlayer;
			
		},
		
		setState:function(state) {
		
			//console.log('rslPlayer:setState:' + state);
		
			if(state != statePlayer) {
				
				switch(state) {
					
					case STATE_STOP:
						//btnPlay.text("Play");
						trackOver.width('0%');
						audio.pause();
						rslPlayerJS.listenersMouseRemove();
						bolPlaying = false;
						rslPlayerJS.updateScrubberByPercent(0);
						statePlayer = state;
						break;
					
					case STATE_PLAY:
						//btnPlay.text("Pause");
						if(!bolPlaying) 
						{
							rslPlayerJS.listenersMouseAdd();
							bolPlaying = true;
						}
						audio.play();
						statePlayer = state;
						break;

					case STATE_PAUSE:
						//btnPlay.text("Play");
						audio.pause();
						statePlayer = state;
						break;

					case STATE_ERROR:
						//btnPlay.text("Error");
						audio.pause();
						audio.setAttribute("src", "");
						audio.removeAttribute("src");
						bolPlaying = false;
						rslPlayerJS.updateScrubberByPercent(0);
						btnPlay.off('click', rslPlayerJS.onMouseDownPlay);
						btnTrackPlay.off('click', rslPlayerJS.onMouseDownPlay);
						statePlayer = state;
						break;

					default:
						console.error('RockstarLab:Play State Error:' + state + ' not known.');
						break;

				}
				
			}
			
		},
		
		setStateShare:function(state) {
		
			if(state != stateShare) {
				
				switch(state) {
			
					case STATE_CLOSED:
						shareButtons.css('display', 'none');
						stateShare = state;
						break;
						
					case STATE_OPEN:
						shareButtons.css('display', 'block');
						stateShare = state;
						break;
					
					default:
						console.error('RockstarLab:Set Share State Error:' + state + ' not known.');
						break;
				
				}
			
			}
		
		},
		
		setStateTrackList:function(state) {
		
			if(state != stateTrackList) {
				
				switch(state) {
			
					case STATE_CLOSED:
						trackListContainer.css('display', 'none');
						stateTrackList = state;
						break;
						
					case STATE_OPEN:
						trackListContainer.css('display', 'block');
						stateTrackList = state;
						break;
					
					default:
						console.error('RockstarLab:Set Track List State Error:' + state + ' not known.');
						break;
				
				}
			
			}
		
		},
		
		setTitle:function(title) {
			
			artistNameLabel.html(title);
			$('.trackListLabel').html(title);
		},
		
		setTrack:function(id) {
					
			if(trackID != id)
			{
				
				switch(mediaType) {
				
					case TYPE_MP3:
						audio.setAttribute("src", trackListData[id].data.mp3);
						break;
						
					case TYPE_OGG:
						audio.setAttribute("src", trackListData[id].data.ogg);
						break;

					default:						
						console.error('RockstarLab:Set Track Type Error:' + mediaType + ' not known.');
						break;
					
				}
				
				audio.load(); // required for 'older' browsers
				
				//trackNameLabel.text(pad(id + 1, 2) + '   ' + trackList[id].title);

				trackListMenu.children().removeClass('colorBlue');
				trackListMenu.children().eq(id).addClass('colorBlue');

				trackTimeLabel.text('0:00');

				playTrack.css({"backgroundImage" : "url(" + trackListData[id].data.waveBack + ")" });
				playTrackOver.css({"backgroundImage" : "url(" + trackListData[id].data.waveFront + ")" });
				
				if(statePlayer == STATE_PLAY)
				{
					rslPlayerJS.setState(STATE_PAUSE);
					rslPlayerJS.setState(STATE_PLAY);
				}
			
				trackID = id;
			
			}
			
		},
		
		// This updates the position of the scrubber on the track based on the mouse position when the user clicks on the track.
		updateScrubber:function( pos ) {
		
			if(pos - scrollOffset > track.offset().left && pos < track.offset().left + (track.width() - scrubber.width()))
			{
				
				//rslPlayerJS.updateScrubberByPercent((pos - scrollOffset) / (track.offset().left + (track.width() - scrubber.width())) );

				scrubber.offset({ left: pos - scrollOffset });
				
				trackOver.width(Math.round(((pos - track.offset().left) / track.width()) * 100).toString() + '%');
			
			} else if(pos + scrollOffset < track.offset().left) {
			
				rslPlayerJS.updateScrubberByPercent(0);

				//scrubber.offset({ left: track.offset().left });
			
			} else if(pos > track.offset().left + (track.width() - scrubber.width()) )  {
				
				rslPlayerJS.updateScrubberByPercent(1);

				//scrubber.offset({ left: track.offset().left + (track.width() - scrubber.width()) });
			
			} else {
			
				rslPlayerJS.updateScrubberByPercent(0);
			
				//scrubber.offset({ left: track.offset().left });
			
			}
		
		},
		
		updateScrubberByPercent:function(percent) {
			
			if(percent <= 0)
			{

				scrubber.offset({ left: track.offset().left });
				
				trackOver.width('0%');

			} else if(percent >= 1) {
				
				scrubber.offset({ left: track.offset().left + (track.width() - scrubber.width()) });
				
				trackOver.width('100%');

			} else {
				
				scrubber.offset({ left: track.offset().left + ((track.width() - scrubber.width()) * percent ) });
				
				trackOver.width((percent * 100) + '%');

			}
			
		},
		
		onMediaEvent:function(event) {
		
			//console.log('rslPlayer:' + event.type );
			
			switch(event.type)
			{
				
				case 'durationchange':
				case 'timeupdate':
					trackTimeLabel.text(
						Math.round(audio.currentTime / 60) + ":" + pad(Math.round(audio.currentTime) % 60, 2)
					);
					rslPlayerJS.updateScrubberByPercent(audio.currentTime/audio.duration);
					break;
				
				case 'ended':
					trackTimeLabel.text(
						"0:00"
					);
					
					if( trackID < trackListData.length - 1) {
						var id = (trackID < trackListData.length - 1) ? trackID + 1 : 0;
						rslPlayerJS.setTrack(id);
					} else
						rslPlayerJS.setState(STATE_STOP);
						
					break;
				
				default:
					//console.log('rslPlayer:uncaught event:' + event.type);
					break;
				
			}
			
		},
		popUp: function(URL) {
			newwindow = window.open(URL, 'Facebook', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=400,height=400,left = 760,top = 340');
			if (window.focus) {
				newwindow.focus();
				var checker = window.setInterval(function() {
					if (newwindow.closed) {
						//var winn = window.open('http://dev.amplify.fm/offers/offer.php', '_blank');
						//console.log(window.opener);
						//winn.focus();
						clearInterval(checker);
					}
				}, 1000);
				/*
				newwindow.onunload = function() {
					var win = window.opener;
					if (win.closed) {
						win.someFunctionToCallWhenPopUpCloses();
					}
				};*/
				/*
				window.setTimeout(function() {
					if (newwindow.closed) {
						alert("Pop-up definitely closed");
					}
					//alert("Pop-up definitely closed");
				}, 1);*/
			}
			return false;
		},
		onMouseDownButtonDownload:function(event) {
		/*
			var has_login = $(".buttonDownload").hasClass("need-fb-login");
			var link = trackListData[trackID].data.download;;
			if(has_login){
				rslPlayerJS.popUp(trackListData[trackID].data.download);
				/*
				var _opener = fb.opener;
				var checker = window.setInterval(function() {
					if (fb.closed) {
						var winn = _opener.open('http://dev.amplify.fm/offers/offer.php', '_blank');
						//winn.focus();
						clearInterval(checker);
					}
					console.log(link);
				}, 1000);
			}else{*/
				var win = window.open(trackListData[trackID].data.download, '_blank');
				win.focus();
			//}
		},
		onMouseDownButtonShare:function(event) {
			rslPlayerJS.setStateShare(STATE_OPEN);
		},
		
		onMouseDownButtonShareClose:function(event) {
			rslPlayerJS.setStateShare(STATE_CLOSED);
		},
		
		onMouseDownButtonTrackList:function(event) {
			rslPlayerJS.setStateTrackList(STATE_OPEN);
		},
		
		onMouseDownButtonTrackListClose:function(event) {
			rslPlayerJS.setStateTrackList(STATE_CLOSED);
		},
		onMouseDownLogo:function(event) {
			var win = window.open(jsonData.ampURL, '_blank');
		  	win.focus();
		},
		onMouseDownPlay:function(event) {
			
			//console.log('rslPlayer:onMouseDownPlay');
			
			if(statePlayer == STATE_STOP || statePlayer == STATE_PAUSE) {
				
				rslPlayerJS.setState(STATE_PLAY);
				
			} else {
				
				rslPlayerJS.setState(STATE_PAUSE);	
							
			}
			
		},
		
		// What happens when the user clicks down on the scrubber.
		onMouseDownScrub:function( event )
		{
			scrollOffset = event.pageX - scrubber.offset().left;
			$(document).bind('mouseup', rslPlayerJS.onMouseUpScrub);
			$(document).bind('mousemove', rslPlayerJS.onMouseMoveScrub);
			rslPlayerJS.setState(STATE_PAUSE);
		},

		onMouseDownShare:function( event )
		{
			var win = window.open($(this).data('link'), '_blank');
		  	win.focus();
		},
		
		onMouseUpTrackItem:function( event )
		{
			rslPlayerJS.setTrack($(this).data('index'));
		},
		onMouseDownTrackNext:function( event )
		{
			var id = (trackID < trackListData.length - 1) ? trackID + 1 : 0;
			rslPlayerJS.setTrack(id);
		},
		onMouseDownTrackPrev:function( event )
		{
			var id = (trackID > 0) ? trackID - 1 : trackListData.length - 1;
			rslPlayerJS.setTrack(id);
		},
		
		// What happens when the user releases the mouse up from the scrubber.
		onMouseUpScrub:function( event )
		{
		
			$(document).unbind('mouseup', rslPlayerJS.onMouseUpScrub);
			$(document).unbind('mousemove', rslPlayerJS.onMouseMoveScrub);
			
			//console.log(Math.round(audio.duration * ( (event.pageX - track.offset().left) / (track.width() - scrubber.width()) )));
			
			audio.currentTime = Math.round(audio.duration * ( ((event.pageX - (scrubber.width() / 2)) - track.offset().left) / (track.width() - scrubber.width()) ));
			
			rslPlayerJS.setState(STATE_PLAY);
			
		},

		// What happens when the user moves the scrubber around on the track.
		onMouseMoveScrub:function( event )
		{
			rslPlayerJS.updateScrubber(event.pageX);
		},
	
		// What happens when the user clicks directly on the scroll track.
		onMouseUpTrack:function( event )
		{
			scrollOffset = 0;
			audio.currentTime = Math.round(audio.duration * ( ((event.pageX - (scrubber.width() / 2)) - track.offset().left) / (track.width() - scrubber.width()) ));
			rslPlayerJS.updateScrubber(event.pageX - (scrubber.width() / 2));
		},
		
		updateTrackListScrollContainer:function( ) {
				
			var offset = (trackListMenu.height() - trackListMenuContainer.height());
			
			if(offset > 0)
			{					
				
				var percent = Math.round((btnListMenuScrub.offset().top - trackListMenuTrack.offset().top)  / (trackListMenuTrack.height() - btnListMenuScrub.height()) *100);
				
				trackListMenu.css('top', -(Math.round((percent / 100) * offset)) );
			
			}	
								
		},
		
		updateTrackListScrollContainerFromTrackItem:function( pos ) {

			var offset = (trackListMenu.height() - trackListMenuContainer.height());

			if(offset > 0)
			{					

				var diff = -(scrollOffset - pos);

				if(diff < 0 && diff > -offset)
				{
					trackListMenu.css('top', diff );
				}
				
			}	

		},
		
		// This updates the position of the btnScrub on the track based on the mouse position when the user clicks on the track.
		updateTrackListScrubber:function( pos ) {
		
			//console.log('rslPlayerJS:updateScrubber:' + pos);
		
			if(pos - scrollOffset > trackListMenuTrack.offset().top && pos < trackListMenuTrack.offset().top + (trackListMenuTrack.height() - btnListMenuScrub.height()))
			{
			
				btnListMenuScrub.offset({ top: pos - scrollOffset });
			
			} else if(pos + scrollOffset < trackListMenuTrack.offset().top) {
			
				btnListMenuScrub.offset({ top: trackListMenuTrack.offset().top });
			
			} else if(pos > trackListMenuTrack.offset().top + (trackListMenuTrack.height() - btnListMenuScrub.height()) )  {
			
				btnListMenuScrub.offset({ top: trackListMenuTrack.offset().top + (trackListMenuTrack.height() - btnListMenuScrub.height()) });
			
			} else {
			
				btnListMenuScrub.offset({ top: trackListMenuTrack.offset().top });
			
			}
		
		},
		
		updateTrackListScrubberFromTrackItem:function() {
			var per = ((trackListMenuContainer.offset().top - trackListMenu.offset().top) / trackListMenuContainer.height());
			
			var offset = trackListMenuTrack.offset().top + ((trackListMenuTrack.height() - btnListMenuScrub.height()) * per);
			
			if(offset > trackListMenuTrack.offset().top + (trackListMenuTrack.height() - btnListMenuScrub.height()))
			{
				offset = trackListMenuTrack.offset().top + (trackListMenuTrack.height() - btnListMenuScrub.height());
			}
			
			btnListMenuScrub.offset({ top: offset });
			
		},
		
		// What happens when the user clicks down on the btnScrub.
		onMouseDownTrackListScrub:function( event )
		{
			//console.log('rslPlayerJS:onMouseDownTrackListScrub');
			scrollOffset = event.pageY - btnListMenuScrub.offset().top;
			$(document).bind('mouseup', rslPlayerJS.onMouseUpTrackListScrub);
			$(document).bind('mousemove', rslPlayerJS.onMouseMoveTrackListScrub);
		},

		// What happens when the user releases the mouse up from the btnScrub.
		onMouseUpTrackListScrub:function( event )
		{
			//console.log('rslPlayerJS:onMouseUpScrub');
			$(document).unbind('mouseup', rslPlayerJS.onMouseUpTrackListScrub);
			$(document).unbind('mousemove', rslPlayerJS.onMouseMoveTrackListScrub);
		},

		// What happens when the user moves the btnScrub around on the track.
		onMouseMoveTrackListScrub:function( event )
		{
			//console.log('rslPlayerJS:onMouseMoveScrub');
			rslPlayerJS.updateTrackListScrubber(event.pageY);
			rslPlayerJS.updateTrackListScrollContainer();
		},
		
		// What happens when the user clicks directly on the scroll track.
		onMouseUpTrackListTrack:function( event )
		{
			//console.log('rslPlayerJS:onMouseUpTrack');
			scrollOffset = 0;
			rslPlayerJS.updateTrackListScrubber(event.pageY - (btnListMenuScrub.height() / 2));
			rslPlayerJS.updateTrackListScrollContainer();
			//bbshirtslider._updateImages(event.pageX);
		},
		
		
		// List Item Move
		onMouseDownTrackListItem:function( event )
		{
			//console.log('rslPlayerJS:onMouseDownTrackListScrub');
			scrollOffset = event.pageY - trackListMenu.offset().top;
			$(document).bind('mouseup', rslPlayerJS.onMouseUpTrackListItem);
			$(document).bind('mousemove', rslPlayerJS.onMouseMoveTrackListItem);
		},
		
		onMouseMoveTrackListItem:function( event )
		{
			//console.log('rslPlayerJS:onMouseMoveScrub');
			//rslPlayerJS.updateTrackListScrubber(event.pageY);
			trackListMenuItems.off('mouseup', rslPlayerJS.onMouseUpTrackItem);
			rslPlayerJS.updateTrackListScrollContainerFromTrackItem(event.pageY - trackListMenuContainer.offset().top);
			rslPlayerJS.updateTrackListScrubberFromTrackItem();
		},

		onMouseUpTrackListItem:function( event )
		{
			//console.log('rslPlayerJS:onMouseUpScrub');
			trackListMenuItems.on('mouseup', rslPlayerJS.onMouseUpTrackItem);
			$(document).unbind('mouseup', rslPlayerJS.onMouseUpTrackListItem);
			$(document).unbind('mousemove', rslPlayerJS.onMouseMoveTrackListItem);
		},
		
		onTouchDownTrackListItem:function(event)
		{
			//console.log('he_amenities_detail:onTouchDownScrub');
			scrollOffset = event.originalEvent.touches[0].pageY - btnScrub.offset().top;
			$(document).bind('touchend', rslPlayerJS.onTouchUpTrackListItem);
			$(document).bind('touchmove', rslPlayerJS.onTouchMoveTrackListItem);
		},
		
		onTouchMoveTrackListItem:function( event )
		{
		    var touch = event.originalEvent.touches[0];
			trackListMenuItems.off('mouseup', rslPlayerJS.onMouseUpTrackItem);
			rslPlayerJS.updateTrackListScrollContainerFromTrackItem(touch.pageY - trackListMenuContainer.offset().top);
			rslPlayerJS.updateTrackListScrubberFromTrackItem();		
						// he_amenities_detail.updateScrubber(touch.pageY);
						// he_amenities_detail.updateScrollContainer();
		},
		
		// What happens when the user releases the mouse up from the btnScrub.
		onTouchUpTrackListItem:function( event )
		{
			trackListMenuItems.on('mouseup', rslPlayerJS.onMouseUpTrackItem);
			$(document).unbind('touchend', rslPlayerJS.onTouchUpTrackListItem);
			$(document).unbind('touchmove', rslPlayerJS.onTouchMoveTrackListItem);

		}
		
	}
	
}();

function pad(n, width, z) {
	
	z = z || '0';
	n = n + '';
	return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
	
}

function printObject(object) {	
	
	for (var key in object) {
		console.log("    " + key + "::" + object[key]);
	}
	
}

function stripNumber(str) {
	return str;
}