$(document).ready(function(){

	var $ticker_container = $('#nt-example1');
	var ticker_height = 54;
	var example_ticker;

	example_ticker = $ticker_container.newsTicker({
                row_height: ticker_height,
                max_rows: 4,
                duration: 4000,
                speed:1000,
                direction: 'down',
                prevButton: $('#nt-example1-prev'),
                nextButton: $('#nt-example1-next'),
                autostart: 1
            });

	$(document).on('click', '.add-ticker', function(e){

		e.preventDefault();
		example_ticker.newsTicker('add', '<div class="col-xs-3 nopadding"><img src="http://cdn2.vox-cdn.com/thumbor/weKcu4Dnc5Xp_tgIeKOtf4xswa4=/800x0/filters:no_upscale()/cdn1.vox-cdn.com/uploads/chorus_asset/file/2401392/Jupiter.NAmerica.0.jpg" class="img-responsive"></div><div class="col-xs-9">This is what North America would look like on Jupiter</div>');
		//example_ticker.newsTicker('moveDown'); //if you want to movedown onclick or onadd
		
	});

	//extra js styling
	$ticker_container.find('.ticker-row').css('height', ticker_height+'px').css('overflow','hidden');
	$ticker_container.find('.ticker-row').find('.img-responsive').css('height', ticker_height+'px');
});