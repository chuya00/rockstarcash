/*
 * jQuery File Upload Plugin JS Example 8.9.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */
var files = new Array();
var count = 0;
$(function () {
    'use strict';

    showUploader();
    hideUploader();
    showProcessingText();
    deleteAudioFile();
    //console.log(url);
    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        maxFileSize: 50000000,
        url: url+'/assets/audio/',
        acceptFileTypes: /(\.|\/)(mp3)$/i,
        success: function(result, data, z){
            var count_rows = $('.files-list tr').length;
            //console.log(count+" "+count_rows);
            if(count < count_rows){
                var file = result.files;
                files.push(file[0].name);
            }

            if(count == (count_rows-1)){
                saveAudio(files);
            }

            count++;
            //console.log('1:');
            //console.log(result);
            //console.log('2:');
            //console.log(data.result);
            //console.log('3:');
            //console.log(jqXHR);
            //saveAudio(result);
            //prepareResultForAjax(result)
            //window.location = url+'/admin/media/audio-upload'
        },
        error: function (jqXHR, textStatus, errorThrown){
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

    /*$('#fileupload').fileupload('send',{files: filesList})
    .success(function (result, textStatus, jqXHR) {
        window.location = url+'/admin/media/audio-upload'
    });*/

    // Load existing files:
        $('#fileupload').addClass('fileupload-processing');
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function () {
            $(this).removeClass('fileupload-processing');
        }).done(function (result) {
            $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {result: result});
        });

});

function deleteAudioFile(){
    $('.delete-audio-file-confirm').click(function(){        
        var row_count = $(this).attr('row-count');
        $('.delete-audio-file'+row_count).html('Deleting...');
        var id = $(this).attr('data-id');
        var filename = $(this).attr('data-file');
        $.ajax({
            url: url+'/admin/media/delete-audio',
            type: 'POST',
            data: 'id='+id+"&filename="+filename,
            success: function(r){
                console.log(r)
                if(r == '1'){
                    $('#tr'+row_count).remove();
                }else{
                    $(this).html('Delete');
                }
            },
            error: function(x,y,z){
                console.log(x);
                console.log(y);
                console.log(z);
            }
        });
    });
}

function saveAudio(names){
    console.log(names);
    $.ajax({
        url: url+'/admin/media/save-audio',
        type: 'POST',
        data: {names:names},
        success: function(r){
            //console.log(r)
            if(r == '1'){
                window.location = url+'/admin/media/audio-upload';
            }
        },
        error: function(x,y,z){
            console.log(x);
            console.log(y);
            console.log(z);
        }
    })
}

function prepareResultForAjax(result){
    var files = result.files;
    var data = new Array();
    for(x = 0; x < files.length; x++){
        data.push(files[x].name);
    }

    console.log(data);
}

function showProcessingText(){
    $('.start').click(function(){
        $('.files-list').before('<h3 id="processing-text" >Processing...</h3>')
    });
}

function showUploader(){
    $('.upload-audio-file-btn').click(function(){
        $('#uploader-container').slideDown();
        $(this).hide();
        $('.hide-uploader-btn').show()
        
    });

    
}

function hideUploader(){
    $('.hide-uploader-btn').click(function(){
        $('#uploader-container').slideUp();
        $(this).hide();
        $('.upload-audio-file-btn').show()
    });

    
}