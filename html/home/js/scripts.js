include('js/jquery.easing.1.3.js');
include('js/jquery.animate-colors-min.js');
include('js/jquery.backgroundpos.min.js');
include('js/superfish.js');
include('js/switcher.js');
include('js/forms.js');
include('js/googleMap.js');
include('js/bgStretch.js');
include("js/jquery.cycle.all.min.js");
include("js/jquery.jcarousel.min.js");
include('js/jquery.fancybox-1.3.4.pack.js');



//----Include-Function----
function include(url){ 
  document.write('<script src="'+ url + '" type="text/javascript"></script>'); 
}
//--------global-------------
var isSplash = true;
var isFirst = true;

var spinner;
var mapSpinner;
var bgSpinner;

var MSIE = ($.browser.msie) && ($.browser.version <= 8)
//------DocReady-------------
$(document).ready(function(){ 
    if(location.hash.length == 0){
        location.hash="!/"+$('#content > ul > li:first-child').attr('id');
    }
    ///////////////////////////////////////////////////////////////////

$("body").css({'min-height':'568px'});

///////////////////////////////////////////////////////////////////

	jQuery('#mycarousel-2').jcarousel({
				horizontal: true,
				wrap:false,
				scroll:1,
				easing:'easeInOutBack',
				animation:1200
			});
			
			jQuery('#mycarousel-3').jcarousel({
				horizontal: true,
				wrap:false,
				scroll:1,
				easing:'easeInOutBack',
				animation:1200
			});
			
			jQuery('#mycarousel-5').jcarousel({
				horizontal: true,
				wrap:false,
				scroll:1,
				easing:'easeInOutBack',
				animation:1200
			});
			
			jQuery('#mycarousel-6').jcarousel({
				horizontal: true,
				wrap:false,
				scroll:1,
				easing:'easeInOutBack',
				animation:1200
			});
			
			jQuery('#mycarousel-4').jcarousel({
				vertical: true,
				wrap:false,
				scroll:1,
				easing:'easeInOutBack',
				animation:1200
			});

     $('ul#menu').superfish({
          delay:       800,
          animation:   {height:'show'},
          speed:       600,
          autoArrows:  false,
         dropShadows: false,
         	onInit: function(){
  				$("#menu > li > a").each(function(index){
  					var conText = $(this).find('.mText').text();
                      $(this).append(""); 
                       
  				})
  	 		}
        });
});
  
 //------WinLoad-------------  
$(window).load(function(){  

	$(function() {
			var BV = new $.BigVideo({useFlashForFirefox:false});
			BV.init();

			
			if (Modernizr.touch) {
				BV.show('images/gallery_big_01.jpg');
			
			} else {
				BV.show('vids/video.mp4', {altSource:'vids/video.webm', ambient:true});
			}
		});

   
   $('.fancyPic').fancybox({'titlePosition': 'inside', 'overlayColor':'#000'}); 
   $('.fancyPic2').fancybox({'titlePosition': 'inside', 'overlayColor':'#000'}); 
	$('.fancyPic3').fancybox({'titlePosition': 'inside', 'overlayColor':'#000'}); 
	$('.fancyPic4').fancybox({'titlePosition': 'inside', 'overlayColor':'#000'}); 
   
   if(!MSIE){ $('.fancyPic').find(".zoomSp").fadeTo(500, 0)}else{ $('.fancyPic').find(".zoomSp").css({"display":"none"})  }
   if(!MSIE){ $('.fancyPic2').find(".zoomSp2").fadeTo(500, 0)}else{ $('.fancyPic2').find(".zoomSp2").css({"display":"none"})  }
   if(!MSIE){ $('.fancyPic3').find(".zoomSp3").fadeTo(500, 0)}else{ $('.fancyPic3').find(".zoomSp3").css({"display":"none"})  }
	if(!MSIE){ $('.fancyPic4').find(".zoomSp4").fadeTo(500, 0)}else{ $('.fancyPic4').find(".zoomSp4").css({"display":"none"})  }
	
    $('.fancyPic').hover(function(){
    if(!MSIE){ 
        $(this).find(".zoomSp").stop().fadeTo(500, 1)
		
    }else{
        $(this).find(".zoomSp").css({"display":"block"})   
		
    }
    },
     function(){
            if(!MSIE){ 
                $(this).find(".zoomSp").stop().fadeTo(500, 0)
		
            }else{
                     $(this).find(".zoomSp").css({"display":"none"})   
				   
            }   
        }
		
 )   
 
  $('.fancyPic2').hover(function(){
    if(!MSIE){ 
      
		 $(this).find(".zoomSp2").stop().fadeTo(500, 1)
    }else{
     
		$(this).find(".zoomSp2").css({"display":"block"})   
    }
    },
     function(){
            if(!MSIE){ 
              
				 $(this).find(".zoomSp2").stop().fadeTo(500, 0)
            }else{
                 
					 $(this).find(".zoomSp2").css({"display":"none"})    
            }   
        }	
 )   
 $('.fancyPic3').hover(function(){
    if(!MSIE){ 
		 $(this).find(".zoomSp3").stop().fadeTo(500, 1)
    }else{
		$(this).find(".zoomSp3").css({"display":"block"})   
    }
    },
     function(){
            if(!MSIE){ 
				 $(this).find(".zoomSp3").stop().fadeTo(500, 0)
            }else{
					 $(this).find(".zoomSp3").css({"display":"none"})    
            }   
        }
 ) 
  $('.fancyPic4').hover(function(){
    if(!MSIE){ 
		 $(this).find(".zoomSp4").stop().fadeTo(500, 1)
    }else{
		$(this).find(".zoomSp4").css({"display":"block"})   
    }
    },
     function(){
            if(!MSIE){ 
				 $(this).find(".zoomSp4").stop().fadeTo(500, 0)
            }else{
					 $(this).find(".zoomSp4").css({"display":"none"})    
            }   
        }
 ) 
 

	
	$('#prev,.prev').hover(
        function(){
		  $(this).children('span').stop().animate({opacity:1}, 600, 'easeOutCubic');
		   
        },
        function(){
		  $(this).children('span').stop().animate({opacity:0.2}, 600, 'easeOutCubic');
        }
    );
    $('#next,.next').hover(
        function(){
		    $(this).children('span').stop().animate({opacity:1}, 600, 'easeOutCubic');
        },
        function(){
		    $(this).children('span').stop().animate({opacity:0.2}, 600, 'easeOutCubic');
        }
    );
	
	if ($('.slogans').length) {
        $('.slogans').cycle({

			//fx:      'turnDown', 
      		easing:  'easeInOutBack',
            fx: 'fade',
            speed: 1000,
    		timeout: 0,              
    		//easing: 'swing',
    		cleartypeNoBg: true,
            rev:0,
            startingSlide: 0,
            wrap: true,
            next: '#next',
    		prev: '#prev',
			before: function(curr, next, opts) {
				opts.animOut.opacity = 0;
			}
  		})}
		
		
		setTimeout(function(){
        $('#bgStretch').bgStretch({
    	   align:'leftTop',
           autoplay:false,
        })
    },0);
   
		

       
var menuItems = $('#menu >li'); 
var currentIm = 0;
var lastIm = 0;



///////////////////////////////////////////////
    var navItems = $('.menu > ul >li');

    //$('.menu > ul >li').eq(0).css({'display':'none'});
	var content=$('#content'),
		nav=$('.menu');
		//nav2=$('.menu2');

    	$('#content').tabs({
		preFu:function(_){
			_.li.css({left:"-1700px",'display':'none'});
		}
		,actFu:function(_){			
			if(_.curr){
				_.curr.css({'display':'block', left:'-1700px'}).stop().delay(400).animate({left:"0px"},800,'easeOutCubic');
                if ((_.n == 0) && ((_.pren>0) || (_.pren==undefined))){splashMode();}
                if (((_.pren == 0) || (_.pren == undefined)) && (_.n>0) ){contentMode(); }
            }
			if(_.prev){
			     _.prev.stop().animate({left:'1700px'},600,'easeInOutCubic',function(){_.prev.css({'display':'none'});} );
             }
		}
	})
    

    function splashMode(){
        isSplash = true;
		
			 $('#bgStretch').stop(true).delay(100).animate({opacity:1}, 1000, 'easeOutCubic');
			$('footer').stop(true).delay(100).animate({'backgroundPosition':'0 0px'}, 500, 'easeOutCubic');
			$('#big-video-wrap').stop(true).delay(100).animate({opacity:1}, 900, 'easeOutCubic');
			
			setTimeout(function(){
				$("body").css({'min-height':'800px'});
			
			centrRepos();   },200);

			
            
            setTimeout(
                function(){
                    $(".main").css({"z-index":1})
       
                }, 800)
    }
    
    function contentMode(){  
        isSplash = false;
			 $('#bgStretch').stop(true).delay(300).animate({opacity:.3}, 900, 'easeOutCubic');
			 
			  $('#big-video-wrap').stop(true).delay(300).animate({opacity:.3}, 900, 'easeOutCubic');
			 
			$('footer').stop(true).delay(100).animate({'backgroundPosition':'0 10px'}, 500, 'easeOutCubic');
              
              $(".main").css({"z-index":2})
			  setTimeout(function(){
			  $("body").css({'min-height':'800px'});
			  centrRepos();
			     },200);
   
    }		
    
    
	nav.navs({
			useHash:true,
             hoverIn:function(li){
				
				 $(".mText", li).stop(true).animate({color:'#ff2d00'}, 600, 'easeOutCubic');
                   // if(($.browser.msie) && ($.browser.version <= 8)){}else{}
             },
                hoverOut:function(li){
                    if ((!li.hasClass('with_ul')) || (!li.hasClass('sfHover'))) {
						$(".mText", li).stop(true).animate({color:'#fff'}, 600, 'easeOutCubic');
                    } 
                } 
		})

		.navs(function(n){			
			$('#content').tabs(n);
		})
        


//////////////////////////////////////////
   	var h_cont=400;
	function centrRepos() {
		var h=$(window).height();
		if (h>(h_cont+40)) {
			m_top=~~(h-h_cont)/2;
			h_new=h;
		} else {
			m_top=20;
			h_new=h_cont+40;
		}
        
            if(m_top > 200){
		          $('.center').stop().animate({paddingTop:m_top}, 800, 'easeOutExpo');
          }else{
            	if(isSplash == true){
				setTimeout(function(){
					$('.center').stop().animate({paddingTop:"150px"}, 800, 'easeOutExpo');
					$("body").css({'min-height':'800px'});
					   },200);
				
					} else{
						setTimeout(function(){
            			$('.center').stop().animate({paddingTop:"200px"}, 800, 'easeOutExpo');
						$("body").css({'min-height':'800px'});
						 },200);
					}
          }
        
	}
	centrRepos();
    ///////////Window resize///////
    
    function windowW() {
 return (($(window).width()>=parseInt($('body').css('minWidth')))?$(window).width():parseInt($('body').css('minWidth')));
}
    
    
	$(window).resize(function(){
        centrRepos();
         
        }
    );

    } //window function
) //window load