<?php
return array( 

    /*
    |--------------------------------------------------------------------------
    | oAuth Config
    |--------------------------------------------------------------------------
    */

    /**
     * Storage
     */
    'storage' => 'Session', 

    /**
     * Consumers
     */
    'consumers' => array(

        /**
         * Facebook
         */
        'Facebook' => array(
            'client_id'     => '1485198521744786',
            'client_secret' => '66d8ade618061f3532fc09f07d3bfa13',
            'scope'         => array('email', 'user_location', 'user_about_me', 'manage_pages'),
        ),    

    )

);