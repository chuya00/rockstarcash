<?php

return array(
    'items' => array(
    				array(
    					'href'  => 'admin',
    					'icon'  => 'fa-home',
    					'title' => 'Dashboard',
    				),
    				array(
    					'href'    => '#',
    					'icon'    => 'fa-file-text-o',
    					'title'   => 'Stats',
    					'subMenu' => array(
    									array(
	    									'href'  => 'admin/stats',
	    									'icon'  => '',
	    									'title' => 'Stats'
    									),
    								    array(
	    								 	'href' => 'admin/stats/songs',
	    								 	'icon'  => '',
	    									'title' => 'Songs'
    								 	),
    								    array(
	    								 	'href' => 'admin/stats/referrers',
	    								 	'icon'  => '',
	    									'title' => 'Referrers'
    								 	),
    								    array(
	    								 	'href' => 'admin/stats/tickets',
	    								 	'icon'  => '',
	    									'title' => 'Tickets'
    								 	),
    								    array(
	    								 	'href' => 'admin/stats/geo',
	    								 	'icon'  => '',
	    									'title' => 'Geo Reports'
    								 	)
    								 )
    				),
    				array(
    					'href'  => '#',
    					'icon'  => 'fa-envelope-o',
    					'title' => 'Emails',
    					'subMenu' => array(
    									array(
	    									'href'  => 'admin/email/inbox',
	    									'icon'  => '',
	    									'title' => 'Inbox'
    									),
    								    array(
	    								 	'href' => 'admin/email/templates',
	    								 	'icon'  => '',
	    									'title' => 'Email Templates'
    								 	),
    								    array(
	    								 	'href' => 'admin/email/jobs',
	    								 	'icon'  => '',
	    									'title' => 'Email Jobs'
    								 	),
    								 )
    				),
    				array(
    					'href'  => '#',
    					'icon'  => 'fa-rocket',
    					'title' => 'Media',
    					'subMenu' => array(
    									array(
	    									'href'  => 'admin/media/audio-upload',
	    									'icon'  => '',
	    									'title' => 'Upload Audio File'
    									),
    								    array(
	    								 	'href' => 'admin/media/vide-upload',
	    								 	'icon'  => '',
	    									'title' => 'Upload Video File'
    								 	),
    								    array(
	    								 	'href' => 'admin/media/audio-players',
	    								 	'icon'  => '',
	    									'title' => 'Audio Players'
    								 	),
    								 )
    				),
                    array(
                        'href'  => 'admin/logout',
                        'icon'  => 'fa-power-off',
                        'title' => 'Logout',
                    ),
    			),
);