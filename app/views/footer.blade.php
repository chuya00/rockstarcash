<footer>
  <div class="container">
    <div class="row">
      <div class="span12">
        <p class="copyright">&copy; 2014, <a href="http://www.rockstarlabs.fm">Rockstar Labs</a>. All rights reserved.</p>
        <ul id="footer-navigation">
          <li><a title="Twitter" href="http://www.twitter.com/#/rockstarlabs"><img src="{{ asset('home/images/icons/social/twitter.png')}}" alt="Twitter"></a></li>
          <li><a title="Facebook" href="http://www.facebook.com/rockstarlabs"><img src="{{ asset('home/images/icons/social/facebook.png')}}" alt="Facebook"></a></li>
          <li><a title="Google+" href="https://plus.google.com/b/108092704532958150577/108092704532958150577/about"><img src="images/icons/social/google.png" alt="Google+"></a></li>
          <li><a title="LinkedIn" href="http://www.linkedin.com/company/rockstar-labs"><img src="{{ asset('home/images/icons/social/linkedin.png')}}" alt="LinkedIn"></a></li>
        </ul>
      </div>
    </div>
  </div>
</footer>
<!-- JAVASCRIPTS
================================================== -->
<script src="{{ asset('home/js/jquery-1.9.1.min.js')}}"></script> <!--  jQuery  -->
<script src="{{ asset('home/js/filterable.js')}}" type="text/javascript"></script><!-- Portfolio filtering -->
@if(!isset($exclude_nagging_js))
<script src="{{ asset('home/js/nagging-menu.js')}}" type="text/javascript"></script><!-- Sticky menu -->
@endif
<script src="{{ asset('home/js/jquery.flexslider.js')}}" type="text/javascript"></script><!-- Flex slider -->
<script src="{{ asset('home/js/jquery.form.js')}}" type="text/javascript"></script><!-- Form -->
<script src="{{ asset('home/js/jquery.prettyPhoto.js')}}" type="text/javascript"></script><!-- Lightbox -->
<script src="{{ asset('home/js/jquery.validate.js')}}" type="text/javascript"></script><!-- Sticky menu -->
<script src="{{ asset('home/js/jquery.inview.js')}}" type="text/javascript"></script><!-- Sticky menu -->
<script src="{{ asset('home/js/common.js')}}" type="text/javascript"></script><!-- Custom JS effects, tweaks and inits -->
@if(isset($js))
    @foreach($js as $j)
      <script src="{{ $j }}" type="text/javascript"></script>
    @endforeach
@endif
@if(isset($inline_js))
{{ $inline_js; }}
@endif
</body>
</html>