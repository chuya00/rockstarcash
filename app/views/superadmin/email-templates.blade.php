@section('content')
<div class="row">

    <div class="col-lg-12">
        <div class="box"><h2>Super Admin | Manage Email</h2></div>
        <div class="box">
            <a href="{{ Url::to('/admin/email/jobs/create') }}" ><button type="button" class="btn btn-primary">Create Email Job</button></a>
        </div>

        <div class="box">

            <!-- Content Start -->
            <div class="content no-padding">

                <!-- Tabs Navigation Start -->
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#all" data-toggle="tab">All</a></li>
                    <li><a href="#waiting" data-tab-id="waiting" class="email-tab-menu" data-toggle="tab">Waiting for Action</a></li>
                    <li><a href="#scheduled" data-tab-id="scheduled" class="email-tab-menu" data-toggle="tab">Scheduled</a></li>
                    <li><a href="#done" data-tab-id="done" class="email-tab-menu" data-toggle="tab">Job Done</a></li>
                    <li><a href="#trash" data-tab-id="trash" class="email-tab-menu" data-toggle="tab">Trash</a></li>
                </ul>
                <!-- Tabs Navigation End -->

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="all">
                        <div class="box">

                            <!-- Title Bar Start -->
                            <div class="box-title green">
                                <span>Created Email Jobs</span>
                            </div>
                            <!-- Title Bar End -->

                            <!-- Content Start -->
                            <div class="content">
                                <table class="regular-table non-stripped bordered hoverable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Job Name</th>
                                            <th>Type</th>
                                            <th>Date to Send</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(isset($email_jobs))
                                        @foreach($email_jobs as $ej)
                                        <!-- Table Row Start -->
                                        <tr id="tr{{ $count }}">
                                            <td>
                                                {{ $ej->id }}
                                            </td>
                                            <td>
                                                {{ $ej->name }}
                                            </td>
                                            <td>
                                                {{ $ej->emailJobType->name; }}
                                            </td>
                                            <td>
                                                {{ CustomHelper::getDateToSend($ej->job_type,$ej->assigned_date) }}
                                            </td>
                                            
                                            <td id="action{{ $ej->id }}" style="text-align: left;">
                                                <?php if(!CustomHelper::hasPerformed($ej->id) && !CustomHelper::isJobDone($ej->id)){ ?>
                                                <button class="btn btn-s-md btn-primary perform-job" data-id="{{ $ej->id }}" data-job-type-name="{{ $ej->emailJobType->name; }}" data-job-type="{{ $ej->emailJobType->id; }}" >Start Job</button>
                                                <?php }else if(CustomHelper::isJobDone($ej->id)){ ?> 
                                                <a class="btn btn-s-md btn-warning" href="javascript:void(0)">Job Done</a>
                                                <?php }else if(CustomHelper::hasPerformed($ej->id) && !CustomHelper::isJobDone($ej->id)){ ?>
                                                <button class="btn btn-s-md btn-warning cancel-job" data-id="{{ $ej->id }}" data-job-type-name="{{ $ej->emailJobType->name; }}" data-job-type="{{ $ej->emailJobType->id; }}" >Cancel Job</button>
                                                <?php } ?>
                                                <!--<a class="btn btn-default show-details-section" details-count="" href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i>Edit</a>-->
                                                <button class="btn btn-danger delete delete-audio-file{{ $count }}" data-toggle="modal" data-target="#myModalForm{{ $count }}" >
                                                    <i class="icon-trash "></i>
                                                    <span>Delete</span>
                                                </button>
                                                <div class="modal fade" id="myModalForm{{ $count }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-body text-center">
                                                                <h3 style="font-weight: bold">Are you sure you want to delete template<br /><br />"<span id="delete-filename">{{ $ej->name }}</span>"?</h3>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                <button type="button" data-id="{{ $ej->id }}" data-file="{{ $ej->filename }}" row-count="{{ $count++ }}" class="btn btn-primary delete-audio-file-confirm" data-dismiss="modal" >Yes, I am sure</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <!-- Table Row End -->
                                        @endforeach
                                        @endif
                                        

                                    </tbody>
                                </table>
                            </div>
                            <!-- Content End -->

                        </div>
                    </div>

                    <div class="tab-pane" id="waiting">
                        <div class="box">

                            <!-- Title Bar Start -->
                            <div class="box-title green">
                                <span>Created Email Jobs</span>
                            </div>
                            <!-- Title Bar End -->

                            <!-- Content Start -->
                            <div class="content">
                                <table class="regular-table non-stripped bordered hoverable">
                                    <thead>
                                      <tr>
                                        <th>ID</th>
                                        <th>Job Name</th>
                                        <th>Type</th>
                                        <th>Date to Send</th>
                                        <th>Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <!-- Content End -->

                        </div>
                    </div>

                    <div class="tab-pane" id="scheduled">
                        <div class="box">

                            <!-- Title Bar Start -->
                            <div class="box-title green">
                                <span>Created Email Jobs</span>
                            </div>
                            <!-- Title Bar End -->

                            <!-- Content Start -->
                            <div class="content">
                                <table class="regular-table non-stripped bordered hoverable">
                                    <thead>
                                      <tr>
                                        <th>ID</th>
                                        <th>Job Name</th>
                                        <th>Type</th>
                                        <th>Date to Send</th>
                                        <th>Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <!-- Content End -->

                        </div>
                    </div>

                    <div class="tab-pane" id="done">
                        <div class="box">

                            <!-- Title Bar Start -->
                            <div class="box-title green">
                                <span>Created Email Jobs</span>
                            </div>
                            <!-- Title Bar End -->

                            <!-- Content Start -->
                            <div class="content">
                                <table class="regular-table non-stripped bordered hoverable">
                                    <thead>
                                      <tr>
                                        <th>ID</th>
                                        <th>Job Name</th>
                                        <th>Type</th>
                                        <th>Date to Send</th>
                                        <th>Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <!-- Content End -->

                        </div>
                    </div>

                    <div class="tab-pane" id="trash">
                        <div class="box">

                            <!-- Title Bar Start -->
                            <div class="box-title green">
                                <span>Created Email Jobs</span>
                            </div>
                            <!-- Title Bar End -->

                            <!-- Content Start -->
                            <div class="content">
                                <table class="regular-table non-stripped bordered hoverable">
                                    <thead>
                                      <tr>
                                        <th>ID</th>
                                        <th>Job Name</th>
                                        <th>Type</th>
                                        <th>Date to Send</th>
                                        <th>Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <!-- Content End -->

                        </div>
                    </div>
                </div>

            </div>
            <!-- Content End -->

        </div>

        
    </div>

</div>
<script>
var url = '{{ $url }}';
</script>
@stop