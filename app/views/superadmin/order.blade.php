@section('content')



<div class="row">

	<!-- Left Side Start -->
	<div class="col-lg-8">

		<!-- Box Start -->
		<div class="box">

			<!-- Title Bar Start -->
			<div class="box-title red">
				<span>Super Admin | Orders CPA</span>
				
			</div>
			<!-- Title Bar End -->

			<!-- Content Start -->
			<div class="content">

				<div class="row">
					<div class="col-lg-12">

						<!-- Box Start -->
						<div class="box">

							<!-- Title Bar Start -->
							<div class="box-title">
								<span class="gray">Add Order CPA</span>
							</div>
							<!-- Title Bar End -->

							<!-- Content Start -->
							<div class="content">

								<form method="post" action="save-order" class="basic-form">
								<label for="order_id">Order Name</label>
								<select name="order_id" id="order_id">
									<option value="" >Select Order</option>
									@if(isset($orders))
									@foreach($orders as $o)
									{{ '<option value="'.$o['id'].'" data-ad="'.$o['advertiser_id'].'" >'.$o['name'].'</option>' }}
									@endforeach
									@endif
								</select>
								<input type="hidden" name="order_name" id="order_name" />
								<label for="cpa">CPA</label>
								<input type="text" name="cpa" id="cpa" placeholder="Enter CPA">

								<div class="row">
									<div class="col-md-12 text-right">
										<button type="submit" class="btn btn-sm btn-success">Save</button>
									</div>
								</div>

							</form>

							</div>
							<!-- Content End -->

						</div>
						<!-- Box End -->

					</div>
				</div>

			</div>
			<!-- Content End -->

		</div>
		<!-- Box End -->
	</div>
	<!-- Left Side End -->

</div>

<script type="text/javascript">
var url = '{{ $url }}';
//console.log(dateArray);
</script>

@stop