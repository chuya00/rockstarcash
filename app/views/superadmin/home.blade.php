@section('content')



<div class="row">

	<!-- Left Side Start -->
	<div class="col-lg-12">

		<!-- Box Start -->
		<div class="box">

			<!-- Title Bar Start -->
			<div class="box-title red">
				<span>Super Admin Dashboard</span>
				
			</div>
			<!-- Title Bar End -->

			<!-- Content Start -->
			<div class="content">

				<!-- Standard button -->
				<button type="button" class="btn btn-primary" style="width: 200px;height: 50px;">
					<a href="{{ URL::to('super-admin/orders') }}" style="color: #ffffff;">Manage Orders CPA</a>
				</button>

				<button type="button" class="btn btn-primary" style="width: 200px;height: 50px;">
					<a href="{{ URL::to('super-admin/email-templates') }}" style="color: #ffffff;">Manage Email Templates</a>
				</button>

			</div>
			<!-- Content End -->

		</div>
		<!-- Box End -->
	</div>
	<!-- Left Side End -->

</div>

<script type="text/javascript">
var url = '{{ $url }}';
//console.log(dateArray);
</script>

@stop