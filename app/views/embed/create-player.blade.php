<?php 
session_start();
define("FILES_HOST","http://rockstarcash.com/admintheme-dev2/");
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8"/>
	<title>HTML5 Vinyl-styled Music Player</title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1" />

	<script type="text/javascript" src="{{ asset('assets/con/1/js/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/con/1/js/jquery-ui.js') }}"></script>
	
	<link rel="stylesheet" href="{{ asset('assets/con/1/css/plate.css') }}"/>
	<link rel="stylesheet" href="{{ asset('assets/con/1/css/style.css') }}"/>
	<script type="text/javascript" src="{{ asset('assets/con/1/js/plate.js') }}"></script>
</head>
<body style="overflow: hidden;">
	<div class="player">
		<div class="player-inner">
			<div class="share-menu" style="display: none;">
				<ul class="share-icons">
					<li>
<!--						<a target="_blank" href="https://www.facebook.com/dialog/feed?app_id=549915431722413&display=popup&link=&picture=http%3A%2F%2Ffiles/convers/1.jpg&source=http%3A%2F%2Ffiles/song.mp3&name=&caption=Kashmire%20by%20Led%20Zeppelin%20on%20Amplify.fm&description=">-->
						<a target="_blank" href="http://www.facebook.com/sharer.php?u=http%3A%2F%2Fzoom_player.dabbmedia.com%2Fvinyl_player%2F&t=Check+out+Kashmir+by+Led+Zeppelin+and+other+great+tunes+on+Amplify.fm">
							<img src="con/1/img/social_icons/facebook_stone.png" alt="facebook" />
						</a>
					</li>
					<li>
						<a target="_blank" href="http://twitter.com/share?text=Check+out+this+song+on+amplify.fm.&amp;url=http://amplify.fm" title="Click to share this song on Twitter">
							<img src="con/1/img/social_icons/twitter_stone.png" alt="tweet" />
						</a>
					</li>
					<li>
						<a target="_blank" href="http://www.tumblr.com/share/link?url=http%3A%2F%2Fhttp://zoom_player.dabbmedia.com/vinyl_player/&amp;name=Song+Name&amp;description=amplify.fm+has+the+latest+independent+artists%27+songs+available.">
							<img src="con/1/img/social_icons/tumblr_stone.png" alt="tumblr" />
						</a>
					</li>
					<li>
						<a target="_blank" href="https://plus.google.com/share?url=http://zoom_player.dabbmedia.com/vinyl_player/" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" title="Share on Google Plus.">
							<img src="con/1/img/social_icons/google_stone.png" alt="link" />
						</a>
					</li>
					<li>
						<a class="embed-btn" onclick="$('.playlist .track.active').html('<div class=embed-link>Link<br /><input class=\'embed-link-textbox embed-code-val\' type=\'text\' value=\'http://dev.amplify.fm/player.php?player=<?php //echo $player_id; ?>\'><br /><br />Embed<br /><textarea class=\'embed-code-val\' ><div style=\'width: 650px; height: 600px\'><iframe  style=\'width: 100%; height: 100%\' src=\'http://dev.amplify.fm/player.php?player=<?php //echo $player_id; ?>\' frameborder=\'0\' allowfullscreen></iframe></div></textarea><br /><input id=\'resp-yes\' type=\'checkbox\' /><span class=\'resp-msg\'>Responsive</span></div>'); $('.playlist').animate({width: 'toggle'});" target="_blank" href="javascript:void(0);" title="Share on Pinterest.">
							<img src="con/1/img/social_icons/embed_stone.png" alt="email" />
						</a>
					</li>
					<li>
						<a target="_blank" href="mailto:?subject=Song on amplify.fm&amp;body=Check out this song on amplify.fm." title="Click to email this song">
							<img src="con/1/img/social_icons/email_stone.png" alt="Email" />
						</a>
					</li>
				</ul>
			</div>
			<div class="player-song" style="margin: 0 auto;"></div>
			<div class="download-link">
				<a target="_blank" href="<?php //echo $redir; ?>" title="Download Song" download>
					DOWNLOAD NOW
				</a>
			</div>
		</div>
	</div>
	<?php
	echo '<pre>';
	var_dump($player_details);
	echo '</pre>';
	 ?>
	<script type="text/javascript">	

	$(function(){
			$('.player-song').plate({
				playlist: [
					{"title":"{{ $album }}", "artist":"{{ $artist }}", "cover":"{{ $cover }}", "file":"{{ $file }}"}
				]
			});
		});
	</script>
	
		
	<script type="text/javascript">	
		$(document).ready(function() {
			$('.pl_btn').html('amplify.fm');
			$('.pl_btn').off('click');
//			$('.pl_btn').click(function() {
//				window.open('http://amplify.fm/');
//			});
			
			
			$(".embed-btn").click(function(){
				$(".embed-code-val").focus(function() {
					var $this = $(this);
					$this.select();

					// Work around Chrome's little problem
					$this.mouseup(function() {
						// Prevent further mouseup intervention
						$this.unbind("mouseup");
						return false;
					});
				});
				
				$('#resp-yes').click(function(){
					changeEmbedCode();
				});
			});
		});
		
		function changeEmbedCode(){
			var html = '<div style=\'width: 650px; height: 600px\'><iframe  style=\'width: 100%; height: 100%\' src=\'http://dev.amplify.fm/player.php?player=\' frameborder=\'0\' allowfullscreen></iframe></div>';
			var checked = $('#resp-yes:checked').length > 0;
			
			if(checked){
				html = '<div style=\'width: 100%; height: 100%\'><iframe  style=\'width: 100%; height: 100%\' src=\'http://dev.amplify.fm/player.php?player=\' frameborder=\'0\' allowfullscreen></iframe></div>';
			}
			
			$(".embed-code-val").html(html);
		}
	</script>
</body>
</html>