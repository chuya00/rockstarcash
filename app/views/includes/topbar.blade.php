<!-- Top Content Bar Start -->
	<div class="top-bar">
		<div class="main-container">
		<div class="container">
			<div class="row">

				<div class="col-lg-6 col-sm-6 hidden-xs">
					<ul class="left-icons icon-list">
						<li><a href="#" class="sidebar-collapse"><i class="fa fa-dedent"></i></a></li>
						<li><a href="#"><i class="fa fa-bell"></i></a></li>
						<li>
							<a href="#">
								<i class="fa fa-envelope"></i>
								<span class="notification"></span>
							</a>
						</li>
					</ul>
				</div>

				<div class="col-lg-6 col-sm-6 col-xs-12">
					<ul class="right-icons icon-list">
						<li><a href="#"><i class="fa fa-comments"></i></a></li>
						<li><a href="search.html"><i class="fa fa-search"></i></a></li>
						<li>
							<a href="profile.html" class="welcome-user">Welcome, {{ CustomHelper::displayName() }}</a>
						</li>
					</ul>
				</div>

			</div>
		</div>
		</div>
	</div>
	<!-- Top Content Bar End -->