	<!-- Left Sidebar Start -->
	<div class="sidebar sidebar-left">

		<!-- Logo Start -->
		<div class="logo-container">
			<a href="index.html"><h1>RockstarCash</h1></a>
		</div>
		<!-- Logo End -->

		<!-- User Profile Start -->
		<div class="sidebar-user-profile">

			<div class="avatar">
				
				<img src="{{ CustomHelper::getFbProfilePicture() }}" style="width: inherit;" alt="{{ CustomHelper::displayName() }}" />
			</div>

			<div class="ul-icons">
				<span class="user-info">{{ CustomHelper::displayName() }}</span>
				<ul class="icon-list">
					<li><a href="{{ URL::to('admin/logout') }}"><i class="fa fa-power-off"></i></a></li>
					<li><a href="{{ URL::to('admin/user') }}"><i class="fa fa-cog"></i></a></li>
					<li><a href="#"><i class="fa fa-comments"></i></a></li>
				</ul>
			</div>

		</div>
		<!-- User Profile End -->

		@include('includes.nav')

		<!-- Newsfeed Ticker
		<div id="ticker-container">
			<hr />
			<h3>What's new</h3>
			<div id="sidebar-ticker-container">
				<div class="ticker-list" id="nt-example1" style="overflow-y:scroll;">
	                	<div class="row ticker-row">
	                		<div class="col-xs-3 nopadding">
	                			<img class="img-responsive" src="http://l2.yimg.com/bt/api/res/1.2/PFwHLOX_JhfC9cOAdCeYBQ--/YXBwaWQ9eW5ld3M7cT04NQ--/http://globalfinance.zenfs.com/en_us/Finance/US_AFTP_SILICONALLEY_H_LIVE/Michael_Jordan_Explains_The_Biggest-e92f9723dea5b9c5fdc621a920ae6c36" alt="">
	                		</div>
	                		<div class="col-xs-9">
	                			Michael Jordan Explains The Biggest Way The NBA Has Changed For The Worse Since He Played
	                		</div>
	                	</div>
	                	<div class="row ticker-row">
	                		<div class="col-xs-3 nopadding">
	                			<img class="img-responsive" src="http://www.bloomberg.com/image/i7XM5jDJJ8Yo.jpg" alt="">
	                		</div>
	                		<div class="col-xs-9">
	                			Spatting Tycoons Stunt Philippine Infrastructure Growth
	                		</div>
	                	</div>
	                	<div class="row ticker-row">
	                		<div class="col-xs-3 nopadding">
	                			<img class="img-responsive" src="http://l3.yimg.com/bt/api/res/1.2/HFdH2FOQowtFP90oYTvhDg--/YXBwaWQ9eW5ld3M7Zmk9ZmlsbDtoPTYzNDtweW9mZj0wO3E9NzU7dz05NjA-/http://media.zenfs.com/en_us/News/afp.com/173ec2a844cd401c5cc44526b7c7d3bf4acacf6f.jpg" alt="">
	                		</div>
	                		<div class="col-xs-9">
	                			NATO reports 'large scale' Russian air activity in European airspace
	                		</div>
	                	</div>
	                	<div class="row ticker-row">
	                		<div class="col-xs-3 nopadding">
	                			<img class="img-responsive" src="http://l3.yimg.com/bt/api/res/1.2/HFdH2FOQowtFP90oYTvhDg--/YXBwaWQ9eW5ld3M7Zmk9ZmlsbDtoPTYzNDtweW9mZj0wO3E9NzU7dz05NjA-/http://media.zenfs.com/en_us/News/afp.com/173ec2a844cd401c5cc44526b7c7d3bf4acacf6f.jpg" alt="">
	                		</div>
	                		<div class="col-xs-9">
	                			NATO reports 'large scale' Russian air activity in European airspace
	                		</div>
	                	</div>
	                </div>
			</div>
		</div>
		<!-- Newsfeed Ticker END -->

	</div>
	<!-- Left Sidebar End -->