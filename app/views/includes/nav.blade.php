<!-- Menu Start -->
		<ul class="main-menu">
			<?php $menu = Navigation::getMenu(); $nav = $menu['items'] ?>
			@foreach($nav as $n)
				<li class="{{ (isset($active) && $active['title'] == $n['title'])? 'active': ''; }} {{ isset($n['subMenu'])? 'has-submenu' : '' }}">
					<a href="{{ URL::to($n['href']) }}" class="{{ (isset($active) && $active['title'] == $n['title'] && isset($n['subMenu']))? 'close-child': ''; }} ">
						<i class="fa {{ $n['icon'] }}"></i>
						<span>{{ $n['title'] }}</span>
					</a>
					@if(isset($n['subMenu']))
					<ul class="submenu">
						@foreach($n['subMenu'] as $sub)
						<li>
							<a href="{{ $sub['href'] }}">{{ $sub['title'] }}</a>
						</li>
						@endforeach
					</ul>
					@endif
				</li>
			@endforeach
		</ul>
		<!-- Menu End -->