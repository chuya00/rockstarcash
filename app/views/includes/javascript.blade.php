<!-- Javascript -->
<script src="{{ asset('assets/jquery/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/bootstrap3/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<!-- Assets -->
<script src="{{ asset('assets/flotcharts/flotcharts-common.js') }}"></script>
<script src="{{ asset('assets/flotcharts/jquery.flot.min.js') }}"></script>
<script src="{{ asset('assets/flotcharts/jquery.flot.resize.min.js') }}"></script>
<script src="{{ asset('assets/flotcharts/jquery.flot.canvas.min.js') }}"></script>
<script src="{{ asset('assets/flotcharts/jquery.flot.image.min.js') }}"></script>
<script src="{{ asset('assets/flotcharts/jquery.flot.categories.min.js') }}"></script>
<script src="{{ asset('assets/flotcharts/jquery.flot.crosshair.min.js') }}"></script>
<script src="{{ asset('assets/flotcharts/jquery.flot.errorbars.min.js') }}"></script>
<script src="{{ asset('assets/flotcharts/jquery.flot.fillbetween.min.js') }}"></script>
<script src="{{ asset('assets/flotcharts/jquery.flot.navigate.min.js') }}"></script>
<script src="{{ asset('assets/flotcharts/jquery.flot.pie.min.js') }}"></script>
<script src="{{ asset('assets/flotcharts/jquery.flot.selection.min.js') }}"></script>
<script src="{{ asset('assets/flotcharts/jquery.flot.stack.min.js') }}"></script>
<script src="{{ asset('assets/flotcharts/jquery.flot.symbol.min.js') }}"></script>
<script src="{{ asset('assets/flotcharts/jquery.flot.threshold.min.js') }}"></script>
<script src="{{ asset('assets/flotcharts/jquery.colorhelpers.min.js') }}"></script>
<script src="{{ asset('assets/flotcharts/jquery.flot.time.min.js') }}"></script>

<!-- jQuery UI -->
<script src="{{ asset('assets/jquery-ui/jquery-ui-1.10.3.custom.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('_demo/sidebar.min-height.js') }}" type="text/javascript"></script>
<script src="{{ asset('_demo/all-pages.js') }}" type="text/javascript"></script>
<!--<script src="{{ asset('_demo/dashboard-charts.js') }}" type="text/javascript"></script>-->
<script src="{{ asset('_demo/custom-google-maps.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/ticker/newsticker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/ticker/ticker.js') }}" type="text/javascript"></script>

@if(isset($js))
    @foreach($js as $j)
    	<script src="{{ $j }}" type="text/javascript"></script>
    @endforeach
@endif