	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>{{ isset($title)? $title : 'Admin' }} | Rockstarcash Admin</title>

	<link rel="stylesheet" type="text/css" href="{{ asset('assets/bootstrap3/css/bootstrap.css'); }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/fontawesome/css/font-awesome.min.css'); }}">

	<!-- Google Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Lato:400,100italic,100,300italic,300,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->

	<!-- Theme Styles -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/demo.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/weather-icons/weather-icons.min.css') }}">

	<!-- Assets -->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/jquery-ui/ui-lightness/jquery-ui-1.10.3.custom.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/ticker/ticker.css') }}">

    @if(isset($css))
	    @foreach($css as $c)
	    	<link rel="stylesheet" type="text/css" href="{{ $c }}">
	    @endforeach
    @endif
	<!-- Google Maps -->
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>

	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/png">
	<link rel="shortcut icon" type="image/png" href="{{ asset('favicon.ico') }}" />