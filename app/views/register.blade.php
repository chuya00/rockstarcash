@include('head')
 <section>
  <div class="container cont-background">
    <div class="row">
      <div class="span12">
        <div id="contact" class="section-heading">
          <div class="section-title">Register as {{ $user }}</div>
          <div class="section-subtitle"></div>
        </div>
      </div>
      <div class="clear"></div>
      <div class="span12">
        <div class="inner">
          <div class="content-logo" style="text-align: center"><img src="http://www.rockstarcash.com/images/logo-sxsw.png" alt="Ensconce"></div>
      <div id="msg-container" ><?php echo isset($_GET['err'])? $_GET['err'] : ""; ?></div>
      <div>
      
        {{ $form }}

      </div>
        </div>
      </div>
    </div>
  </div>
</section>
@include('footer')