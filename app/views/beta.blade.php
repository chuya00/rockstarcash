@include('head')
<section>
  <div class="container cont-background">
    <div>
      <div class="span12">
        <div id="contact" class="section-heading">
          <div class="section-title">Get in touch</div>
          <div class="section-subtitle">We like being touched by strangers.</div>
        </div>
      </div>
      <div class="clear"></div>
      <div class="span6" style="float: none; margin: 0 auto">
        <div class="inner">
          <div class="content-logo"><img src="http://www.rockstarcash.com/images/logo-sxsw.png" alt="Rockstar Cash Beta Signup"></div>
		  <div id="msg-container" ><?php echo isset($_GET['err'])? $_GET['err'] : ""; ?></div>
          <form method="post" action="signup-process.php" id="contact_form">
            <label for="name">Your name <span>*</span></label>
            <input type="text" class="input-field" id="name" name="name" value="">
            <label for="email">Your e-mail <span>*</span></label>
            <input type="text" class="input-field" id="email" name="email" value="">
			<label for="type">Rockstar Cash Beta Signup<span>*</span></label>
			<label class="radio radio-adjust"><input type="radio" name="type-user" id="type1" value="Labels" style="width: 22px; height: 22px;" ><span class="radio-name">Labels</span></label>
			<label class="radio radio-adjust" style="top: 5px; left: -2px;"><input type="radio" name="type-user" id="type2" value="Musicians" style="width: 22px; height: 22px;" ><span class="radio-name">Musicians</span></label>
			<label class="radio radio-adjust" style="top: 10px; left: -4px;"><input type="radio" name="type-user" id="type3" value="Developers" style="width: 22px; height: 22px;" ><span class="radio-name">Developers</span></label>
			<label class="radio radio-adjust" style="top: 15px; left: -6px;"><input type="radio" name="type-user" id="type4" value="Bloggers" style="width: 22px; height: 22px;" ><span class="radio-name">Bloggers</span></label>
			<div style="margin-top: 39px;" >
			What is <span id="captcha-img" style="margin-right: 11px;"></span><input type="text" name="captcha" id="captcha" style="width: 50px;" />
			</div>
			<input type="submit" class="form-btn" value="Put me on the list!" style="margin-top: 30px;" />
           </form>
        </div>
      </div>
    </div>
  </div>
</section>
@include('footer')