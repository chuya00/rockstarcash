<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<title>{{ isset($title)? $title: 'RockstarCash'; }}</title>
<meta name="description" content="Rockstarcash.com is a platform designed to make musicians/labels, developers and bloggers make money with the best songs in existance. ">
<meta name="author" content="Rockstar Labs">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta property="og:title" content="Rockstarcash.com" />
<meta property="og:url" content="http://www.rockstarcash.com" />
<meta property="og:image" content="http://www.rockstarcash.com/images/logo-sxsw.png" />
<meta property="og:description" 
  content="Rockstarcash.com is a platform designed to make musicians/labels, developers and bloggers make money with the best songs in existance.
" />
<!-- stylesheets -->
<link rel="stylesheet" href="{{ asset('home/css/reset.css') }}"> <!-- CSS reset -->
<link rel="stylesheet" href="{{ asset('home/css/bootstrap.min.css') }}"> <!-- Twitter Bootstrap -->
<link rel="stylesheet" href="{{ asset('home/css/style.css?v53') }}"> <!-- main stylesheet (change this to modify template) -->
<link rel="stylesheet" href="{{ asset('home/css/flexslider.css?v3') }}"> <!-- flexslider -->
<link rel="stylesheet" href="{{ asset('home/css/style-responsive.css?v463') }}"> <!-- main stylesheet (responsive) -->
<link rel="stylesheet" href="{{ asset('home/css/font-awesome.min.css') }}"> <!-- font awesome -->
<link rel="stylesheet" href="{{ asset('home/css/prettyPhoto.css') }}"> <!-- lightbox -->
<link rel="stylesheet" href="{{ asset('home/css/color-theme.css') }}"> <!-- color theme -->
<link rel="stylesheet" href="{{ asset('home/css/fonts/oxygen.css') }}"> <!-- font -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--  IE8 fixes -->
<!--[if IE 8]>
  <link rel="stylesheet" href="css/ie8.css">
<![endif]-->
<!--[if IE 7]>
  <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
<![endif]-->
<!-- favicons -->
<link rel="shortcut icon" href="{{ asset('home/images/favicon.ico') }}">
<link rel="apple-touch-icon" href="{{ asset('home/images/apple-touch-icon.png')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{asset('home/images/apple-touch-icon-72x72.png')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('home/images/apple-touch-icon-114x114.png')}}">
<style>
#error-msg{
	border: 1px solid #FF0000;
	padding: 5px;
	margin-bottom: 5px;
	background-color: #FF9999;
	color: #000;
	text-align: left;
}
</style>
@if(isset($css))
	@foreach($css as $c)
	    <link rel="stylesheet" type="text/css" href="{{ $c }}">
	@endforeach
@endif
</head>