@if ($errors->has())
	{{ '<div id="error-msg" class="notification notification-error">' }}
	@foreach ($errors->all() as $error)
			{{ $error.'<br />' }}		
	@endforeach
	{{ '</div>' }}
@endif