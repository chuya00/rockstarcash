@include('head')
<body>

<!-- go to top button -->
<div id="totop" class="hidden-phone hidden-tablet"><a class="btn btn-inverse"><img src="{{ asset('home/images/arrow-totop.png')}}" alt="Go to top" ></a></div>
<header id="home">
  <div id="top"> 
    <div id="sticky" class="default">
      <div class="container">
            <div class="row">
                <div class="span12">
            <h1 id="logo"><a class="smoothscroll" href="#home"><img src="{{ asset('images/logo-sxsw.png')}}" alt="Rockstar Cash Logo"></a></h1>
            <div id="main-menu">
              <ul id="main-navigation" class="hidden-phone hidden-tablet">
                <li><a class="smoothscroll" href="#home">Home</a></li>
                <li><a class="smoothscroll" href="#about">About</a></li>
                <li><a class="smoothscroll" href="admin/login">Login</a></li>
                <li><a class="smoothscroll" href="#contact">Contact</a></li>
              </ul>
              <form name="nav">
                          <select id="main-menu-select" class="hidden-desktop" data-autogenerate="true"></select>
                      </form>
            </div>
          </div>
            </div>
        </div>
    </div>
  </div>
  <div id="teaser" class="hidden-phone hidden-tablet">
    <div class="container">
        <div class="row">
            <div class="span12 ">
          <h2 style="text-shadow: 1px 1px 1px #000;">Stop getting paid Peanuts!</h2>
          <a class="box-btn smoothscroll" href="#about">We make music make REAL money, find out more...</a>
        </div>
        </div>
    </div>
  </div>
  <video class="supervideo hidden-phone hidden-tablet" poster="video/cash3.png" autoplay loop>
    <source src="{{ asset('home/video/cash3.mp4')}}" />
    <source src="{{ asset('home/video/cash2.3gp')}}" />
    Your browser does not support the HTML5 video tag. Please upgrade it.
  </video>
  <div class="clear"></div>
</header>
<div id="tagline">
  <div class="container">
    <div class="row">
      <div class="span12">
        <h2>Want to signup for the Beta?  Reserve your spot today!</h2>
        <a class="tagline-btn" href="{{ URL::to('sign-up') }}">Click here now</a>
      </div>
    </div>
  </div>
</div>
<section id="about">
  <div class="container">
    <div class="row">
      <div class="span12">
        <div class="fp-boxes">
          <div class="row">
            <div class="span8">
              <div class="row">
                <div class="span4 box">
                  <div class="icon-cog box-icon"></div>
                  <h3>Musicians</h3>
                  <p>No more checking between the digital couch cushions for pennies. We make sure that you make what you are worth!</p>
                </div>
                <div class="span4 box">
                  <div class="icon-leaf box-icon"></div>
                  <h3>Labels</h3>
                  <p>It doesn't matter if you have one artist or thousands, we make it easy to get as much revenue possible for even the baby acts.</p>
                </div>
                <div class="span4 box">
                  <div class="icon-comments-alt box-icon"></div>
                  <h3>Bloggers</h3>
                  <p>Embed the hottest songs online and share in the revenue.  It's your traffic, you worked hard for it. We think you should profit too. </p>
                </div>
                <div class="span4 box">
                  <div class="icon-th-large box-icon"></div>
                  <h3>Developers</h3>
                  <p>Do you want to legaly add music to your project that users can listen to AND get paid for it?  Yes, it's possible.  </p>
                </div>
                <div class="span4 box">
                  <div class="icon-cloud box-icon"></div>
                  <h3>Agencies</h3>
                  <p>Are you ready to use music to connect with your customers in the best way possible?  Now you can with no friction.</p>
                </div>
                <div class="span4 box">
                  <div class="icon-beaker box-icon"></div>
                  <h3>Wives/Husbands</h3>
                  <p>By signing up your significant other will finally make the money you hoped he or she would. Congratulations.</p>
                </div>
              </div>
            </div>
            <div class="span4">
              <div class="special-box-inner">
                <h3 class="box-title">Why did we create Rockstar Cash?</h3>
                <p>If you are reading this & you are looking for new ways to make money from your traffic, visitors or fans, you have come to the right place. Rockstar Cash was created by online marketers & music industry professionals that have been making money on the internet since 1999.
We aren't new to this. We don't speak in small numbers. We aggressively monetize the web...</p>
          <a class="box-btn smoothscroll" href="#who">Read more</a>
                <div class="clear"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="custom-section pull-top">
  <div class="container cont-background">
    <div class="row">
      <div class="span7">
        <div class="inner">
          <h3>So, how does Rockstar Cash work?</h3>
          <h5>It's all about the point of discovery...</h5>
          <p>When a person finds a new hot song that they love, they will do anything to get that song.  Buy it, stream it, steal it. Whatever it takes. All they know is they want THAT song, RIGHT NOW.  This is called "the point of discovery" and there's few things online more powerful than it when it comes to shaping the actions of consumers online.</p>
        <h5>Don't let that opportunity pass...</h5>
        <p>We have created a platform that makes it possible to monetize that moment better than anything currently available.  We use a combination of our own proprietary targeting with an ad delivery system that goes beyond the boring banner and video ads to make the value of the interaction exponentially better for the advertiser.
        <p>It's a simple equation that other companies tend to overlook: when the experince is great for the fans, and it's great for the advertisers, the content owner makes more money.  Period. 
        </div>
      </div>
      <div class="span5">
        <div class="row">
          <ul class="thumbnails">
            <li class="span5">
              <a href="{{ asset('home/images/content-gallery-1.jpg')}}" data-gal="prettyPhoto[gallery2]" class="thumbnail">
                <img alt="simple thumb" src="{{ asset('home/images/girl-on-couch.jpg')}}"></a>
            </li>
          </ul>
           
        <div class="inner" style="padding-left: 0px;"><h5>We make sure that everyone gets what they want...</h5>
        <p>Our team spends the majority of our days finding new ways to solve that equation.  We have done it for sure, but, we think we can make it even better. Expect us to continue to evolve & put more money in everyone's pockets as we go along.
        </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="who">
<section class="custom-section pull-top">
  <div class="container cont-background">
    <div class="row">
      <div class="span12">
        <div class="inner">
          <h3>We Are Here To Make You Money.</h3>
          <p>If you are reading this & you are looking for new ways to make money from your traffic, visitors or fans. Rockstar Cash was created by online marketers & music industry professionals that have been making money on the internet since 1999.
We aren't new to this. We don't speak in small numbers. We aggressively monetize the web. We don't settle for tiny CPMs. The owners of the company have also been in the music business working for major labels, promoting multi city large scale concerts and representing artists so we understand the need for profit.
We care about making sure everyone in the chain gets paid the maximum amount possible from the first click to the last. We give you the tools, the links, the apps and more to make money from your entertainment based traffic. 
<p>
We are not here for fans to "make money referring their friends." We are not here for pirate sites to monetize thier illegal traffic. 
We are here for the professionals that have audiences... 
The legal websites owners that have traffic... 
The professional app makers that have installs and users... 
The bands, managers and labels that have a real need for aggressive monetization. 
<p>
This is who we live for and this is why you must be referred and/or approved by us to be able to join. If you have legal traffic, 
legal apps, or you own content feel free to signup and we will be in touch.</p>
        </div>
      </div>
    </div>
  </div>
</section>
</section>
<section>
  <div class="container cont-background">
    <div class="row">
      <div class="span12">
        <div id="contact" class="section-heading">
          <div class="section-title">Get in touch</div>
          <div class="section-subtitle">We'd love to hear from you.</div>
        </div>
      </div>
      <div class="clear"></div>
      <div class="span6">
        <div class="inner">
          <div class="content-logo"><img src="http://www.rockstarcash.com/images/logo.png" alt="Ensconce"></div>
          <div id="msg-container" style="display:none"></div>
          {{ Form::open(array('url' => 'contact-us', 'id' => 'contact_form')) }}          	
            
            {{ HTML::decode(Form::label('name','Your name <span>*</span>')); }}
            {{ Form::text('name','',array('class' => 'input-field', 'id' => 'name')); }}

            {{ HTML::decode(Form::label('email','Your e-mail <span>*</span>')); }}
            {{ Form::email('email','',array('class' => 'input-field', 'id' => 'email')); }}

            {{ HTML::decode(Form::label('message','Message <span>*</span>')); }}
            {{ Form::textarea('message','',array('id' => 'message','cols' => '50', 'rows' => '4')); }}
            <p class="captcha-container">
              <span>How much is <strong id="captcha-img">2 + 2</strong> =</span>
              <input type="text" class="captcha-input-field" id="captcha" name="captcha">
            </p>
            <input type="submit" class="form-btn" value="Send the message!" />
          {{ Form::close() }}
        </div>
      </div>
      <div class="span6">
        <div class="inner">
          <iframe data-address="Miami Beach, USA" data-zoom="14" data-bubble="true" height="560" class="gmap"></iframe>
        </div>
      </div>
    </div>
  </div>
</section>
@include('footer')