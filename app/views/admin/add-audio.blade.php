@section('content')
<div class="row">

    <div class="col-lg-6">
        <div class="box"><h2>Manage 3rd Party Audios</h2></div>
        <div class="box">
            <button type="button" class="btn btn-primary add-audio-button">Add Audio</button>
        </div>
        @include('error')
        <!-- Box Start -->
        <div class="box add-audio" style="display: none">

            <!-- Title Bar Start -->
            <div class="box-title">
                <span class="gray">Audio Information</span>
            </div>
            <!-- Title Bar End -->

            <!-- Content Start -->
            <div class="content">

                <form method="post" action="save-third" class="basic-form add-audio-form">
                    <label for="title">Title</label>
                    <input type="text" name="title" id="title" placeholder="Enter audio title">
                    <label for="url">URL</label>
                    <input type="text" name="link" id="url" placeholder="Enter audio url. (e.g https://soundcloud.com/jacku/take-u-there-feat-kiesza-zeds-dead-remix)">
                    <label for="source">Audio Source</label>
                    <select id="source" name="source">
                        <option value="soundcloud">Soundcloud</option>
                    </select>
                    <div class="col-md-12 text-right">
                        <button type="reset" class="btn btn-sm btn-warning">Cancel</button>
                        <button type="submit" class="btn btn-sm btn-success">Save</button>
                    </div>
                </form>

            </div>
            <!-- Content End -->

        </div>
        <!-- Box End -->

    </div>

    <div class="col-lg-12">
        <div class="box">
            <!-- Title Bar Start -->
            <div class="box-title green">
                <span>Audios</span>
            </div>
            <!-- Content Start -->
            <div class="content no-padding">

             <div class="box">                          

                <!-- Content Start -->
                <div class="content">
                    <table class="regular-table non-stripped bordered hoverable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>URL</th>
                                <th>Source</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if(isset($audios))
                            @foreach($audios as $aud)
                            <!-- Table Row Start -->
                            <tr id="tr{{ $count }}">
                                <td>
                                    {{ $aud->id }}
                                </td>
                                <td>
                                    {{ $aud->title }}
                                </td>
                                <td>
                                    {{ $aud->link; }}
                                </td>
                                <td>
                                    {{ $aud->source; }}
                                </td>
                                
                                <td style="text-align: left;">
                                    
                                </td>
                            </tr>
                            <!-- Table Row End -->
                            @endforeach
                            @endif
                            

                        </tbody>
                    </table>
                </div>
                <!-- Content End -->

            </div>


        </div>
        <!-- Content End -->

    </div>


</div>

</div>
<script>
var url = '{{ $url }}';
</script>
@stop