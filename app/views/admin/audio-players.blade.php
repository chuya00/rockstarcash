@section('content')
<!--
<div id="image-gallery" style="display: none">
    <div id="contents">
        <select class="image-picker show-html">
            @if(isset($images))
            @foreach($images as $i)
            <option data-img-src="{{ asset('assets/album_image/files/'.$i->filename) }}" data-id="{{ $i->id }}" value="{{ $i->filename }}">  {{ $i->filename }}  </option>
            @endforeach
            @endif
        </select>
        <a class="btn btn-s-md btn-success album-image-close" href="javascript: void(0)">Use Image</a>
        <a class="btn btn-s-md btn-default album-image-close" href="javascript: void(0)">Close</a>
    </div>
</div>-->
<div class="row">

    <div class="col-lg-12">
        <div class="box"><h2>Manage Audio Players</h2></div>
        <div class="box">
            <div class="box">

				<!-- Content Start -->
                <div class="content no-padding">

                    <!-- Tabs Navigation Start -->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#create-player" data-toggle="tab">Create Player</a>
                        </li>
                        <li class=""><a href="#generated-players" data-toggle="tab">Generated Players</a>
                        </li>
                    </ul>
                    <!-- Tabs Navigation End -->

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="create-player">
                            <div class="box">

                                <!-- Title Bar Start -->
                                <div class="box-title green">
                                    <span>Select Audio File</span>
                                </div>
                                <!-- Title Bar End -->

                                <!-- Content Start -->
                                <div class="content">
                                    <table class="regular-table non-stripped bordered hoverable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Title</th>
                                                <th>Filename</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @if(isset($audio_files))
                                                @foreach($audio_files as $file)
                                                <!-- Table Row Start -->
                                                <tr id="tr{{ $count }}">
                                                    <td>
                                                        {{ $file->id }}
                                                    </td>
                                                    <td class="audio_title">
                                                        {{ $file->title }}
                                                    </td>
                                                    <td>
                                                        {{ $file->filename }}
                                                    </td>
                                                    
                                                    <td style="text-align: left;">
                                                        <a class="btn btn-warning add-audio" 
                                                           audio-id="{{ $file->id }}" 
                                                           href="javascript: void(0)" 
                                                           <?php $temp = null; if(Session::has('audio_ses')){ $temp = Session::get('audio_ses');} echo (isset($temp[$file->id]))? 'style="display: none"' : ''; ?> >
                                                           Include to Player
                                                       </a>
                                                        <a class="btn btn-danger remove-audio" 
                                                           audio-id="{{ $file->id }}" 
                                                           href="javascript: void(0)" 
                                                           <?php $temp = null; if(Session::has('audio_ses')){ $temp = Session::get('audio_ses');} echo (isset($temp[$file->id]))? '' : 'style="display: none"'; ?> >
                                                           Remove
                                                       </a>
                                                        <!--
                                                        <button class="btn btn-danger delete delete-audio-file{{ $count }}" data-toggle="modal" data-target="#myModalForm{{ $count }}" >
                                                            <i class="icon-trash "></i>
                                                            <span>Delete</span>
                                                        </button>
                                                        <div class="modal fade" id="myModalForm{{ $count }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-body text-center">
                                                                        <h3 style="font-weight: bold">Are you sure you want to delete file <span id="delete-filename">{{ $file->filename }}</span>?</h3>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                        <button type="button" data-id="{{ $file->id }}" data-file="{{ $file->filename }}" row-count="{{ $count++ }}" class="btn btn-primary delete-audio-file-confirm" data-dismiss="modal" >Yes, I am sure</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>-->
                                                    </td>
                                                </tr>
                                                <!-- Table Row End -->
                                                @endforeach
                                            @endif
                                            

                                        </tbody>
                                    </table>
                                </div>
                                <!-- Content End -->

                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="box" style="border-top: 1px solid #CFD3D6;">
                                        <div class="box-title">
                                            <span class="gray">Player Details</span>
                                        </div>
                                        <div class="content">
                                            <form method="post" action="#" class="basic-form">
                                                <div class="form-group">
                                                    <label for="search-input">Artist Name</label>
                                                    <input type="text" id="artist_name" value="{{ $artist }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="search-input">Album Name</label>
                                                    <input type="text" id="album_name" value="{{ $album }}">
                                                </div>
                                                <!--
                                                <div class="form-group">
                                                    <a class="btn btn-s-md btn-success select-album-image" href="javascript: void(0)">Select Image</a>
                                                </div>

                                                <h3>Or</h3>
                                                -->
                                                <div class="form-group" id="audio-img-container1">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="widget">
                                                                <div class="widget-header"> <i class="icon-external-link"></i>
                                                                    <h3> Audio Image Upload </h3>
                                                                </div>
                                                                <div class="widget-content">
                                                                    <input type="hidden" id="a-filename">
                                                                    <span class="btn btn-success fileinput-button">
                                                                        <i class="glyphicon glyphicon-plus"></i>
                                                                        <span>Select files...</span>
                                                                        <!-- The file input field used as target for the file upload widget -->
                                                                        <input id="player-image" type="file" name="files[]" multiple>
                                                                        <input type="hidden" id="image-filename" value="{{ $image_filename }}" />
                                                                        <input type="hidden" id="image-id" value="{{ $cover_id }}" />
                                                                    </span>
                                                                    <br>
                                                                    <br>
                                                                    <!-- The global progress bar -->
                                                                    <div id="progress" class="progress">
                                                                        <div class="progress-bar progress-bar-success"></div>
                                                                    </div>
                                                                    <div id="process-text"></div>
                                                                    <!-- The container for the uploaded files -->
                                                                    <div id="files" class="files"></div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>




                                                <a class="btn btn-s-md btn-warning preview-player" href="javascript: void(0)">Preview</a>
                                                <a class="btn btn-s-md btn-primary save-player" href="javascript: void(0)">Save</a>
                                            </form>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-lg-6">
                                    <div class="box" style="border-top: 1px solid #CFD3D6;">
                                        <div class="box-title">
                                            <span class="gray">Player Preview</span>
                                        </div>
                                        <div class="content">
                                            <div>
                                                <iframe width="650" height="600" id="iframe-player" src="{{ Url::to('/admin/media/audio-players/embed-preview') }}" frameborder="0" allowfullscreen=""></iframe>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="tab-pane" id="generated-players">
                            <div class="box">

                                <!-- Title Bar Start -->
                                <div class="box-title green">
                                    <span>Generated Players</span>
                                </div>
                                <!-- Title Bar End -->

                                <!-- Content Start -->
                                <div class="content">
                                    <table class="regular-table non-stripped bordered hoverable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Artist Name</th>
                                                <th>Album Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @if(isset($players))
                                                @foreach($players as $p)
                                                <!-- Table Row Start -->
                                                <tr id="tr{{ $count }}">
                                                    <td>
                                                        {{ $p->id }}
                                                    </td>
                                                    <td class="audio_title">
                                                        {{ $p->artist_name }}
                                                    </td>
                                                    <td>
                                                        {{ $p->album_name }}
                                                    </td>
                                                    
                                                    <td style="text-align: left;">
                                                        <a class="btn btn-s-md btn-default btn-sm edit-saved-player" player-id="{{ $p->id }}" href="#iframe-player-preview-container" >Edit</a>
                                                        <a class="btn btn-s-md btn-warning btn-sm delete-player" player-id="{{ $p->id }}" href="javascript: void(0)" >Delete</a>
                                                        <a class="btn btn-s-md btn-info btn-sm preview-saved-player" player-id="{{ $p->id }}" href="#iframe-player-preview-container">Preview Player</a>
                                                        <a class="btn btn-s-md btn-primary btn-sm embed-btn" href="javascript: void(0)" >Show Embed Code</a>
                                                        <a class="btn btn-s-md btn-danger btn-sm hide-embed-btn" href="javascript: void(0)" style="display: none">Hide Embed Code</a>                       
                                                        <div style="display: none" class="embed-code" >
                                                            <br />  
                                                            <textarea style="width: 466px;height: 95px;" class="embed-code-val" ><iframe width="600" height="600" src="http://dev.amplify.fm/player.php?player={{ $p->id }}" frameborder="0" allowfullscreen></iframe></textarea>
                                                        </div>
                                                        <a class="btn btn-s-md btn-primary btn-sm embed-btn" href="javascript: void(0)" >Show Direct Link</a>
                                                        <a class="btn btn-s-md btn-danger btn-sm hide-embed-btn" href="javascript: void(0)" style="display: none">Hide Direct Link</a>
                                                        <div style="display: none" class="embed-code" >
                                                            <br />
                                                            <input type="text" value="{{ CustomHelper::getDownloadShortLink(CustomHelper::getPlayerDownloadLink(Session::get('id'),$p->id),$p->artist_name) }}" style="width: 612px;" />
                                                        </div>
                                                        <a class="btn btn-warning add-audio"
                                                           href="http://dev.amplify.fm/player.php?player={{ $p->id }}"
                                                           target="_blank" >
                                                           Click to go to player's page.
                                                       </a>
                                    
                                                    </td>
                                                </tr>
                                                <!-- Table Row End -->
                                                @endforeach
                                            @endif
                                            

                                        </tbody>
                                    </table>
                                </div>
                                <!-- Content End -->

                            </div>

                            <div class="row">
                                <div class="col-lg-6" >
                                    <div id="edit-section-container" style="display: none"></div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="box" style="border-top: 1px solid #CFD3D6;" id="iframe-player-preview-container" >
                                        <div class="box-title">
                                            <span class="gray">Player Preview</span>
                                        </div>
                                        <div class="content">
                                            <div>
                                                <iframe width="650" height="600" id="iframe-player-preview" src="{{ Url::to('/admin/media/audio-players/embed-preview') }}" frameborder="0" allowfullscreen=""></iframe>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
                <!-- Content End -->

			</div>
            
        </div>
        
    </div>

</div>
<script>
var url = '{{ $url }}';
</script>
@stop