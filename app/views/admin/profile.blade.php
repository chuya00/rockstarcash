@section('content')
<div class="row">

    <div class="col-lg-12">

        <div class="row">

            <div class="col-lg-4">
                <!-- Box Start -->
                <div class="box profile">

                    <!-- Title Bar Start -->
                    <div class="box-title">
                        <span class="gray">My Profile</span>
                    </div>
                    <!-- Title Bar End -->

                    <!-- Content Start -->
                    <div class="content">

                        <div class="row profile-data">
                            <!-- Left Side Start -->
                            <div class="col-lg-5 col-md-5 col-sm-3 col-xs-12 text-center">

                                <!-- Profile Avatar Start -->
                                <div class="profile-avatar">
                                    <img src="{{ CustomHelper::getFbProfilePicture() }}" style="width: 100%" alt="{{ $user->name }}" />
                                </div>
                                <!-- Profile Avatar End -->

                                <!-- Send Message Start -->
                                <div class="send-msg">
                                    <button class="btn btn-sm btn-default" type="button">Send Message</button>
                                </div>
                                <!-- Send Message End -->

                            </div>
                            <!-- Left Side End -->

                            <!-- Right Side Start -->
                            <div class="col-lg-7 col-md-7 col-sm-9 col-xs-12">

                                <h3>{{ $user->name }}</h3>
                                <ul class="icon-list">
                                    <li><i class="fa fa-envelope"></i> {{ $user->email }}</li>
                                    <li><i class="fa fa-building-o"></i> London University</li>
                                    <li><i class="fa fa-map-marker"></i> Baker Street, London</li>
                                    <li><i class="fa fa-suitcase"></i> CEO</li>
                                    <li><i class="fa fa-link"></i> <a href="#">www.creativico.com</a></li>
                                </ul>

                            </div>
                            <!-- Right Side End -->
                        </div>

                        <!-- Profile Stats Start -->
                        <div class="row">
                            <ul class="profile-stats">
                                <li class="followers col-md-3 col-lg-3 col-sm-3 col-xs-6">
                                    <p>1,315</p>
                                    <h3>Followers</h3>
                                </li>
                                <li class="following col-md-3 col-lg-3 col-sm-3 col-xs-6">
                                    <p>218</p>
                                    <h3>Following</h3>
                                </li>
                                <li class="projects col-md-3 col-lg-3 col-sm-3 col-xs-6">
                                    <p>28</p>
                                    <h3>Projects</h3>
                                </li>
                                <li class="courses col-md-3 col-lg-3 col-sm-3 col-xs-6">
                                    <p>39</p>
                                    <h3>Courses</h3>
                                </li>
                            </ul>
                        </div>
                        <!-- Profile Stats End -->

                        <!-- Profile About Me Start -->
                        <div class="row about-me">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h3>About Me</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi eget lobortis dui. Quisque congue tellus at sapien vestibulum aliquam. Phasellus in felis congue, lacinia justo id, feugiat urna. Nullam sit amet dolor nec arcu rhoncus condimentum. Pellentesque ut orci velit.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi eget lobortis dui. Quisque congue tellus at sapien vestibulum aliquam. Phasellus in felis congue, lacinia justo id, feugiat urna. Nullam sit amet dolor nec arcu rhoncus condimentum. Pellentesque ut orci velit.</p>
                            </div>
                        </div>
                        <!-- Profile About Me End -->

                        <!-- Profile Footer Start -->
                        <div class="footer">
                            <div class="col-lg-5 col-md-12 col-sm-6 col-xs-12">
                                <h4>Follow my work at</h4>
                            </div>
                            <div class="col-lg-7 col-md-12 col-sm-6 col-xs-12">
                                <ul class="profile-socials">
                                    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
                                    <li><a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- Profile Footer End -->

                    </div>
                    <!-- Content End -->

                </div>
                <!-- Box End -->
            </div>

        </div>

    </div>
    

</div>
<script>
var url = '{{ $url }}';
</script>
@stop