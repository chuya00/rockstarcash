@section('content')
<div id="email-create" style="display: none; position: absolute; width: 80%; height: 40%; z-index: 1; background-color: #fff">
    <a href="javascript:void(0)" id="close-email-iframe" style="position: absolute;right: -13px;top: -13px;z-index: 2;" >
        <img src="{{ asset('images/close.png'); }}" style="width: 30px;" >
    </a>
    <iframe src="" style="width: 100%; height: 100%" ></iframe>
</div>
<div id="lightbox-container" style="display: none; position: absolute; z-index: 1;top: 0px; left: 0px; width: 100%; height: 100%;">
    <div id="preview-bg" style="position: absolute;top: 0px; left: 0px; width: 100%; height: 100%;background-color: #000; opacity: 0.7; filter: alpha(opacity=70);" >&nbsp;</div>
    <div id="lightbox" style="position:absolute; height: 513px; width: 800px; background-color: #fff;" >
        <a href="javascript:void(0)" id="close-popup" style="position: absolute;right: -13px;top: -13px;z-index: 2;">
            <img src="{{ asset('images/close.png'); }}" style="width: 30px;" />
        </a>
        <div id="prev-top_left" style="color: #fff;position: absolute;padding: 10px;width: 100%;text-align: center;background-color: #000;width: 50%;height: 70%;opacity: 0.9;filter: alpha(opacity=90);position: absolute">
            <div style="text-align: center;font-size: 20px;font-weight: bold;" >
                <h2 id="title" style="color: #fff;"></h2>
                <div id="contents"></div>
            </div>
            <button type="button" style="background-color: #da00db;border: 0;color: #fff;padding: 5px 10px 5px;position: absolute;left: 38%;top: 300px;font-size: 20px;font-weight: bold;">Start Here</button>
        </div>
        <div id="cover-img-container" style="position: absolute;width: 50%;right: 0px;overflow: hidden;height: 359px;">
            <img src="{{ asset('images/default.png') }}" id="prev-cover-image" style="width: 100%;">
        </div>
        <div id="prev-bottom" style="position: absolute;bottom: 0px;width: 100%;height: 154px;text-align: center;text-shadow: -1px 0 #fff, 0 1px #fff, 1px 0 #fff, 0 -1px #fff;color: #000;">
            <h2 id="title" style="font-weight: bold;margin-top: 22px;color: #000;">Share This Video And</h2>
            <div id="contents" style="font-size: 45px;font-weight: bold;"></div>
        </div>
    </div>
</div>
<div id="add-video-form-bg" style="display: none; z-index: 1; background-color: #000; position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; opacity: 0.7; filter: alpha(opacity=70);">&nbsp;</div>
<div id="add-video-form-container" style="display: none; z-index: 2;   width: 500px;">
    <!-- Box Start -->
    <div class="box add-video" >

        <!-- Title Bar Start -->
        <div class="box-title">
            <span class="gray">Video Information</span>
        </div>
        <!-- Title Bar End -->

        <!-- Content Start -->
        <div class="content">

            <form method="post" action="" class="basic-form add-video-form">
                <label for="artist">Artist</label>
                <input type="text" name="artist_name" id="artist" placeholder="Enter artist name">
                <label for="album">Album</label>
                <input type="text" name="album_name" id="album" placeholder="Enter album name">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" placeholder="Enter video title">
                <label for="url">URL</label>
                <input type="text" name="link" id="url" placeholder="Enter video url. (e.g https://www.youtube.com/watch?v=tntOCGkgt98)">
                <label for="source">Video Source</label>
                <select id="source" name="source">
                    <option value="youtube">Youtube</option>
                    <option value="vimeo">Vimeo</option>
                </select>
                <div class="col-md-12 text-right">
                    <input type="hidden" name="submit" value="add-video-contest" />
                    <button type="reset" id="cancel-add-video" class="btn btn-sm btn-warning">Cancel</button>
                    <button type="submit" class="btn btn-sm btn-success">Save</button>
                </div>
            </form>

        </div>
        <!-- Content End -->

    </div>
    <!-- Box End -->
</div>
<div class="row">

    <div class="col-lg-6">
        <div class="box"><h2>Manage Contests</h2></div>
        <div class="box">
            <button type="button" class="btn btn-primary add-contest-button">Add Contest</button>
        </div>
        @include('error')
        @include('message')
        <!-- Box Start -->
        <div class="box add-contest" style="display: none">

            <!-- Title Bar Start -->
            <div class="box-title">
                <span class="gray">Contest Information</span>
            </div>
            <!-- Title Bar End -->

            <!-- Content Start -->
            <div class="content">

                <form method="post" action="contest/save" class="basic-form add-contest-form">
                    <label for="title">Title</label>
                    <input type="text" name="title" id="title" placeholder="Enter contest title">
                    <button type="button" id="add-video-btn" class="btn btn-primary">Add Video</button>
                    <label for="video">Video</label>
                    <select id="video" name="video">
                        <option value="">Select Youtube Video Album</option>
                        @if(isset($players))
                        @foreach($players as $p)
                        <option value="{{ $p->id }}">
                            {{ $p->video->title }}
                        </option>
                        @endforeach
                        @endif
                    </select>
                    <input type="checkbox" name="play" id="checkbox" value="1" class="icheck-blue" style="">
                    Play on page load
                    <label for="itunes-link">Itunes Link</label>
                    <input type="text" name="itunes-link" id="itunes-link" placeholder="Enter Itunes link">
                    <span>*Don't know where to get it? Get it <a href="https://linkmaker.itunes.apple.com/us/" target="_blank">here</a>.</span>
                    <label for="amazon-link">Amazon MP3 Link</label>
                    <input type="text" name="amazon-link" id="amazon-link" placeholder="Enter Amazon MP3 link">

                    <label for="cover-image">Upload Cover Image (Default is the player album cover image.)</label>
                    <div class="widget-content">
                        <input type="hidden" id="a-filename">
                        <span class="btn btn-success fileinput-button">
                            <i class="glyphicon glyphicon-plus"></i>
                            <span>Select files...</span>
                            <!-- The file input field used as target for the file upload widget -->
                            <input id="player-image" type="file" name="files[]" multiple>
                            <input type="hidden" id="image-filename" value="" />
                            <input type="hidden" id="image-id" name="image-id" value="" />
                        </span>
                        <br>
                        <br>
                        <!-- The global progress bar -->
                        <div id="progress" class="progress">
                            <div class="progress-bar progress-bar-success"></div>
                        </div>
                        <div id="process-text"></div>
                        <!-- The container for the uploaded files -->
                        <div id="files" class="files"></div>
                    </div>
                    <hr />
                    <label for="email_template">Welcome Email:</label>
                    <div class="col-lg-12" style="margin-bottom: 20px;">

                        <!-- Box Start -->
                        <div class="box">

                            <!-- Content Start -->
                            <div class="content no-padding">

                                <!-- Tabs Navigation Start -->
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#rsc" class="email-option" data-sel="rsc-email" data-toggle="tab">RockstarCash Email Builder</a></li>
                                    <li class=""><a href="#mc" class="email-option" data-sel="mc-email" data-toggle="tab">Mailchimp</a></li>
                                </ul>
                                <input type="hidden" name="email-template-sel" id="email-template-sel" value="" />
                                <!-- Tabs Navigation End -->

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="rsc">
                                        <div>Use our own email template builder</div><br />
                                        <select id="email_template" name="email-template">
                                            <option>Select Email Template</option>
                                            @if(isset($email_templates))
                                            @foreach($email_templates as $et)
                                            <option value="{{ $et->filename }}">
                                                {{ $et->filename }}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                        
                                        <button data-href="{{ URL::to('admin/email/templates/create/1') }}" id="create-email-btn" type="button" class="btn btn-primary">Create Welcome Email Template</button>
                                        
                                    </div>

                                    <div class="tab-pane" id="mc">
                                        <div>Use your mailchimp account for sending the welcome email</div>
                                        <label>Select from previously entered Mailchimp API keys:</label>
                                        <select id="mc-api-key-sel" name="mc-api-key-sel">
                                            <option value="">Select API key</option>
                                            @if(isset($mailchimp))
                                            @foreach($mailchimp as $mc)
                                            <option value="{{ $mc->api_key }}">
                                                {{ $mc->api_key }}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <label>Or enter a new one here:</label>
                                        <label>Mailchimp API key: (Get your api key <a href="https://us4.admin.mailchimp.com/account/api-key-popup/" target="_blank" >here</a>)</label>
                                        <input type="text" id="mc-api-key" name="mc-api-key" value="" />
                                        <button type="button" class="btn btn-sm btn-info" id="get-mc-list">Get Email List/s</button>
                                        <div id="mc-lists"></div>
                                        <input type="hidden" id="mc-id" name="mc_id" value="" />
                                    </div>
                                </div>

                            </div>
                            <!-- Content End -->

                        </div>
                        <!-- Box End -->

                    </div>
                    
                    <hr />
                    <label for="contest_type">Contest Type</label>
                    <select id="contest_type" name="contest_type">
                        <option value="">Select Contest Type</option>
                        @if(isset($contest_types))
                        @foreach($contest_types as $ct)
                        <option value="{{ $ct->id }}" image-name="{{ $ct->image }}" >{{ $ct->name }}</option>
                        @endforeach
                        @endif
                    </select>
                    <label for="top_left">Description (This goes to the top left corner of the box)<br />Tip: Add {{ htmlspecialchars('<br />'); }} to go to next line.</label>
                    <textarea name="top_left" id="top_left" placeholder="Enter description"></textarea>                    
                    <label for="bottom">Complete the statement: Share This Video And ...</label>
                    <textarea name="bottom" id="bottom" placeholder="Enter win statement (e.g. Win a Trip to Miami!)"></textarea>
                    <div class="col-md-12 text-right">
                        <button type="button" id="preview-lightbox" class="btn btn-sm btn-info">Preview</button>
                    </div>
                    <hr />
                    <div class="col-md-12 text-right">
                        <button type="reset" class="btn btn-sm btn-warning">Cancel</button>
                        <button type="submit" class="btn btn-sm btn-success">Save</button>
                    </div>
                </form>

            </div>
            <!-- Content End -->

        </div>
        <!-- Box End -->
    </div>

    <div class="col-lg-12">
        <div class="box">
            <!-- Title Bar Start -->
            <div class="box-title green">
                <span>Contests</span>
            </div>
            <!-- Content Start -->
            <div class="content no-padding">

             <div class="box">                          

                <!-- Content Start -->
                
                <div class="content">

                    <table class="regular-table non-stripped bordered hoverable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Album</th>
                                <th>Play on Page Load</th>
                                <th>Enabled</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                           
                            @if(isset($contests))
                            @foreach($contests as $c)
                            <!-- Table Row Start -->
                            
                            <tr>
                                <td>
                                    {{ $c->id }}
                                </td>
                                <td class="audio_title">
                                    {{ $c->title }}
                                </td>
                                <td class="audio_title">
                                    {{ $c->player->video->title }}
                                </td>
                                <td class="audio_title">
                                    {{ ($c->play == 1)? 'Yes' : 'No' }}
                                </td>
                                <td>
                                    {{ ($c->enabled == 1)? 'Yes' : 'No' }}
                                </td>

                                <td style="text-align: left;">
                                    <a class="btn btn-s-md btn-default btn-sm edit-contest" count-row="{{ $count }}" contest-id="{{ $c->id }}" href="javascript:void(0)" >Edit</a>
                                    <a class="btn btn-s-md btn-default btn-sm contest-link" count-row="{{ $count }}" contest-id="{{ $c->id }}" href="javascript:void(0)" >Show Link</a>                                    
                                </td>
                            </tr>
                            <tr id="tr{{ $count }}" style="display: none" >
                             <td colspan="5">
                                <form method="post" action="contest/update" class="basic-form">
                                    <table class="regular-table non-stripped bordered hoverable">
                                        <tbody id="tbody{{ $count }}">
                                            <tr>
                                                <td colspan="2" style="text-align: left;">Media to show on page:</td>
                                            </tr>
                                            <tr>
                                                <td><input type="radio" name="media-selected" value="rsc" checked></td>
                                                <td style="text-align: left">RockstarCash Player</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    
                                    <div class="col-md-12 text-left">
                                        <input type="hidden" name="contest_id" value="{{ $c->id }}" />
                                        <button type="reset" class="btn btn-sm btn-warning">Cancel</button>
                                        <button type="submit" class="btn btn-sm btn-success">Save</button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        <tr id="tr{{ $count++ }}-link" style="display: none" >
                            <td colspan="4"></td>
                             <td colspan="2" style="text-align: left;">
                                Contest Link: <textarea style="height: 50px;">{{ CustomHelper::getContestLink($c->player->id,Session::get('id'),$c->id,1,$c->player->artist_name) }}</textarea>
                            </td>
                        </tr>
                        <!-- Table Row End -->
                        @endforeach
                        @endif


                    </tbody>
                </table>

            </div>

            <!-- Content End -->

        </div>


    </div>
    <!-- Content End -->

</div>


</div>

</div>
<div id="bg-light" style="display: none">&nbsp;</div>
<div class="box" id="ret-data" style="display: none; background-color: #fff;padding: 21px 10px 10px 10px;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;" ><h3>Retrieving Data...</h3></div>
<!-- Box Start -->
<div class="box edit-contest-form" style="display: none">

    <!-- Title Bar Start -->
    <div class="box-title">
        <span class="gray">Contest Information</span>
    </div>
    <!-- Title Bar End -->

    <!-- Content Start -->
    <div class="content">

        <form method="post" id="contest-edit-form" action="contest/update" class="basic-form">
            <label for="title">Title</label>
            <input type="text" name="title" id="title" placeholder="Enter contest title">
            <label for="music">Music</label>
            <select id="music" name="music">
                <option value="">Select Youtube Video Album</option>
                @if(isset($players))
                @foreach($players as $p)
                <option value="{{ $p->id }}">
                    {{ $p->video->title }}
                </option>
                @endforeach
                @endif
            </select>
            <input type="checkbox" name="play" id="checkbox" value="1" class="icheck-blue" style="">
            Play on page load
            <label for="itunes-link">Itunes Link</label>
            <input type="text" name="itunes-link" id="itunes-link" placeholder="Enter Itunes link">
            <span>*Don't know where to get it? Get it <a href="https://linkmaker.itunes.apple.com/us/" target="_blank">here</a>.</span>
            <label for="amazon-link">Amazon MP3 Link</label>
            <input type="text" name="amazon-link" id="amazon-link" placeholder="Enter Amazon MP3 link">

            <label for="cover-image">Change Cover Image</label>
            <div class="widget-content">
                <input type="hidden" id="a-filename">
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Select files...</span>
                    <!-- The file input field used as target for the file upload widget -->
                    <input id="player-image" type="file" name="files[]" multiple>
                    <input type="hidden" id="image-filename" value="" />
                    <input type="hidden" id="image-id" value="" />
                </span>
                <br>
                <br>
                <!-- The global progress bar -->
                <div id="progress" class="progress">
                    <div class="progress-bar progress-bar-success"></div>
                </div>
                <div id="process-text"></div>
                <!-- The container for the uploaded files -->
                <div id="files" class="files"></div>
            </div>

            <label for="description">Description</label>
            <textarea name="description" id="description" placeholder="Enter contest description"></textarea>
            <input type="hidden" name="c_id" id="c_id" value="" />
            <div class="col-md-12 text-right">
                <button type="button" class="btn btn-sm btn-warning close-edit-form">Cancel</button>
                <button type="submit" class="btn btn-sm btn-success">Save</button>
            </div>
        </form>

    </div>
    <!-- Content End -->

</div>
<!-- Box End -->

<script>
var url = '{{ $url }}';
</script>
@stop