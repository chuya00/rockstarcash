@section('content')
<div class="row">

    <div class="col-lg-6">
        <div class="box"><h2>Manage Amplify Page</h2></div>
        <div class="box">
            <a href="{{URL::to('admin/video')}}" ><button type="button" class="btn btn-primary add-video-button">Add Video</button></a>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="box">
            <!-- Title Bar Start -->
            <div class="box-title green">
                <span>Amplify Page</span>
            </div>
            <!-- Content Start -->
            <div class="content no-padding">

               <div class="box">                          

                <!-- Content Start -->
                
                <div class="content">

                    <table class="regular-table non-stripped bordered hoverable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Artist Name</th>
                                <th>Album Name</th>
                                <th>Media on Page</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if(isset($players))
                            @foreach($players as $p)
                            <!-- Table Row Start -->
                            <tr>
                                <td>
                                    {{ $p->id }}
                                </td>
                                <td class="audio_title">
                                    {{ $p->artist_name }}
                                </td>
                                <td>
                                    {{ $p->album_name }}
                                </td>
                                <td>
                                    <?php $media = CustomHelper::getMediaOnPage($p->id); ?>
                                    {{ isset($media->title) ? $media->title : '' }} - {{ $media->source; }}
                                </td>

                                <td style="text-align: left;">
                                    <a class="btn btn-s-md btn-default btn-sm edit-player" count-row="{{ $count }}" player-id="{{ $p->id }}" href="#iframe-player-preview-container" >Edit</a>                                    
                                </td>
                            </tr>
                            <tr id="tr{{ $count }}" style="display: none" >
                               <td colspan="5">
                                <form method="post" action="manage-amplify-page/save" class="basic-form add-video-form">
                                        <table class="regular-table non-stripped bordered hoverable">
                                            <tbody id="tbody{{ $count++ }}">
                                                <tr>
                                                    <td colspan="2" style="text-align: left;">Media to show on page:</td>
                                                </tr>
                                                <tr>
                                                    <td><input type="radio" name="media-selected" value="rsc" checked></td>
                                                    <td style="text-align: left">RockstarCash Player</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    
                                    <div class="col-md-12 text-left">
                                        <input type="hidden" name="player_id" value="{{ $p->id }}" />
                                        <button type="reset" class="btn btn-sm btn-warning">Cancel</button>
                                        <button type="submit" class="btn btn-sm btn-success">Save</button>
                                    </div>
                                </form>
                                </td>
                            </tr>
                            <!-- Table Row End -->
                            @endforeach
                            @endif


                        </tbody>
                    </table>

                </div>

                <!-- Content End -->

            </div>


        </div>
        <!-- Content End -->

    </div>


</div>

</div>
<script>
var url = '{{ $url }}';
</script>
@stop