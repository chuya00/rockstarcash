@section('content')
<div class="row">

  <div class="col-lg-12">
    <div class="box"><h2>Create Email Job</h2></div>
    <div class="col-lg-6">
      <div class="box">

        <!-- Title Bar Start -->
        <div class="box-title">
          <span class="gray">Create</span>
        </div>
        <!-- Title Bar End -->

        <!-- Content Start -->
        <div class="content">
          @if(!$errors->isEmpty() || Session::has('message-err'))
          <div class="notification notification-error">
            <strong>Error!</strong> 
            {{ Session::has('message-err') ? Session::get('message-err') : '' }}
            @foreach($errors->all() as $e)
            <div>{{ $e }}</div>
            @endforeach
          </div>
          @endif
          @if(Session::has('message-suc'))
          <div class="notification notification-success">
            <strong>Hooray!</strong> {{ Session::get('message-suc') }}
          </div>
          @endif
          <form method="post" action="save" class="basic-form">
            <fieldset>
              <div class="control-group">
                <div class="col-md-3">
                  <label for="normal-field" class="control-label">Job Name</label>
                </div>
                <div class="col-md-9">
                  <div class="form-group">
                    <input type="text" placeholder="" name="job-name" class="form-control" id="job-name">
                  </div>
                </div>
              </div>
              <div class="control-group">
                <div class="col-md-3">
                  <label for="normal-field" class="control-label">Job Type</label>
                </div>
                <div class="col-md-9 with-select">
                  <select class="form-control" name="email-job-type">
                    <option>Select job type</option>
                    @foreach($email_job_types as $ejt)
                    <?php if((Session::get('id') != 6 && $ejt->is_admin == 1)) continue; ?>                                    
                    <option value="{{ $ejt->id }}" >{{ ($ejt->name == 'Monthly' || $ejt->name == 'Weekly' || $ejt->name == 'Bi Weekly')? $ejt->name.' Newsletter' : $ejt->name }}</li>
                      @endforeach
                    </select>
                    
                  </div>
                  <?php /*
                  <div class="control-group" id="scheduler" style="display:none">
                    <div class="col-md-3">
                        <label for="normal-field" class="control-label">Job Description</label>
                    </div>
                    <div class="col-md-9 with-select" style="padding-top: 5px;" id="job-desc">
                    </div>
                    </div> */
                    ?>
                    <div class="control-group">
                      <div class="col-md-3">
                        <label for="normal-field" class="control-label">Subject</label>
                      </div>
                      <div class="col-md-9">
                        <div class="form-group">
                          <input type="text" class="form-control" name="subject" id="subject">
                        </div>
                      </div>
                    </div>
                    <div class="control-group">
                      <div class="col-md-3">
                        <label for="normal-field" class="control-label">Email to send to</label>
                      </div>
                      <div class="col-md-9">
                        <div class="form-group">
                          <input type="email" placeholder="emailtosend@example.com" class="form-control" name="email" id="email">
                        </div>
                      </div>
                    </div>
                    <div class="control-group">
                      <div class="col-md-3">
                        <label for="normal-field" class="control-label">Date to Send</label>
                      </div>
                      <div class="col-md-9">
                        <div class="form-group">
                          <input type="text" class="form-control" name="date-to-send" id="datepicker">
                        </div>
                      </div>
                    </div>                  
                    <div class="control-group">
                      <div class="col-md-3">
                        <label for="normal-field" class="control-label">Email Template</label>
                      </div>
                      <div class="col-md-9">
                        <select class="form-control" name="email-template">
                          @foreach($email_templates as $et)
                          <option value="{{ $et->id }}" >{{ $et->filename }}</li>
                            @endforeach
                          </select>
                        </div>
                      </div>

                    </fieldset>
                    <div class="form-actions">
                      <div>
                        <button class="btn btn-primary" type="submit">Save</button>
                        <a href="{{ URL::to('/admin/email/jobs') }}"><button class="btn btn-default" type="button">Back to Email Jobs list</button></a>
                      </div>
                    </div>
                  </form>

                </div>
                <!-- Content End -->

              </div>
            </div>

            
          </div>

        </div>
        <script>
        var url = '{{ $url }}';
        </script>
        @stop