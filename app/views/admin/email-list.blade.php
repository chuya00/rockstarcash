@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box"><h2>Manage Email List</h2></div>
        <div class="box">
            <!-- Title Bar Start -->
            <div class="box-title green">
                <span>Contests</span>
            </div>
            <!-- Content Start -->
            <div class="content no-padding">

             <div class="box">                          

                <!-- Content Start -->
                
                <div class="content">

                    <table class="regular-table non-stripped bordered hoverable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>email</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                           
                            @if(isset($email_list))
                            @foreach($email_list as $el)
                            <!-- Table Row Start -->
                            
                            <tr>
                                <td>
                                    {{ $el->id }}
                                </td>
                                <td class="audio_title">
                                    {{ $el->email }}
                                </td>

                                <td style="text-align: left;">
                                    <?php /*
                                    <a class="btn btn-s-md btn-default btn-sm edit-contest" count-row="{{ $count }}" contest-id="{{ $el->id }}" href="javascript:void(0)" >Edit</a>
                                    <a class="btn btn-s-md btn-default btn-sm contest-link" count-row="{{ $count }}" contest-id="{{ $el->id }}" href="javascript:void(0)" >Show Link</a>                                    
                                    */
                                    ?>
                                </td>
                            </tr>
                            <tr id="tr{{ $count }}" style="display: none" >
                             <td colspan="5">
                                <form method="post" action="contest/update" class="basic-form">
                                    <table class="regular-table non-stripped bordered hoverable">
                                        <tbody id="tbody{{ $count }}">
                                            <tr>
                                                <td colspan="2" style="text-align: left;">Media to show on page:</td>
                                            </tr>
                                            <tr>
                                                <td><input type="radio" name="media-selected" value="rsc" checked></td>
                                                <td style="text-align: left">RockstarCash Player</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    
                                    <div class="col-md-12 text-left">
                                        <input type="hidden" name="contest_id" value="{{ $el->id }}" />
                                        <button type="reset" class="btn btn-sm btn-warning">Cancel</button>
                                        <button type="submit" class="btn btn-sm btn-success">Save</button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        <!-- Table Row End -->
                        @endforeach
                        @endif


                    </tbody>
                </table>

            </div>

            <!-- Content End -->

        </div>


    </div>
    <!-- Content End -->

</div>


</div>

</div>

<script>
var url = '{{ $url }}';
</script>
@stop