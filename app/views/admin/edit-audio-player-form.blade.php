                            <div class="box">

                                <!-- Title Bar Start -->
                                <div class="box-title green">
                                    <span>Select Audio File</span>
                                </div>
                                <!-- Title Bar End -->

                                <!-- Content Start -->
                                <div class="content">
                                    <table class="regular-table non-stripped bordered hoverable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Title</th>
                                                <th>Filename</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @if(isset($audio_files))
                                                @foreach($audio_files as $file)
                                                <!-- Table Row Start -->
                                                <tr id="tr{{ $count }}">
                                                    <td>
                                                        {{ $file->id }}
                                                    </td>
                                                    <td class="audio_title">
                                                        {{ $file->title }}
                                                    </td>
                                                    <td>
                                                        {{ $file->filename }}
                                                    </td>
                                                    
                                                    <td style="text-align: left;">
                                                        <a class="btn btn-warning add-audio-saved" 
                                                           audio-id="{{ $file->id }}" 
                                                           href="javascript: void(0)"                                                             
                                                           @if(isset($details['audio_ses_prev'][$file->id]))
                                                                style="display: none"
                                                           @endif
                                                           >
                                                           Include to Player
                                                       </a>
                                                        <a class="btn btn-danger remove-audio-saved" 
                                                           audio-id="{{ $file->id }}" 
                                                           href="javascript: void(0)"                                                             
                                                           @if(!isset($details['audio_ses_prev'][$file->id]))
                                                                style="display: none"
                                                           @endif
                                                           >
                                                           Remove
                                                       </a>
                                                        <!--
                                                        <button class="btn btn-danger delete delete-audio-file{{ $count }}" data-toggle="modal" data-target="#myModalForm{{ $count }}" >
                                                            <i class="icon-trash "></i>
                                                            <span>Delete</span>
                                                        </button>
                                                        <div class="modal fade" id="myModalForm{{ $count }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-body text-center">
                                                                        <h3 style="font-weight: bold">Are you sure you want to delete file <span id="delete-filename">{{ $file->filename }}</span>?</h3>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                        <button type="button" data-id="{{ $file->id }}" data-file="{{ $file->filename }}" row-count="{{ $count++ }}" class="btn btn-primary delete-audio-file-confirm" data-dismiss="modal" >Yes, I am sure</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>-->
                                                    </td>
                                                </tr>
                                                <!-- Table Row End -->
                                                @endforeach
                                            @endif
                                            

                                        </tbody>
                                    </table>
                                </div>
                                <!-- Content End -->

                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="box" style="border-top: 1px solid #CFD3D6;">
                                        <div class="box-title">
                                            <span class="gray">Player Details</span>
                                        </div>
                                        <div class="content">
                                            <form method="post" action="#" class="basic-form">
                                                <div class="form-group">
                                                    <label for="search-input">Artist Name</label>
                                                    <input type="text" id="artist_name_saved" value="{{ isset($details['artist_name']) ? $details['artist_name'] : '' }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="search-input">Album Name</label>
                                                    <input type="text" id="album_name_saved" value="{{ isset($details['album_name'])? $details['album_name'] : '' }}">
                                                </div>
                                                <div class="form-group">
                                                    <a class="btn btn-s-md btn-success select-album-image-edit" href="javascript: void(0)">Select Image</a>
                                                </div>

                                                <h3>Or</h3>

                                                <div class="form-group" id="audio-img-container1">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="widget">
                                                                <div class="widget-header"> <i class="icon-external-link"></i>
                                                                    <h3> Audio Image Upload </h3>
                                                                </div>
                                                                <div class="widget-content">
                                                                    <input type="hidden" id="a-filename">
                                                                    <br>
                                                                    <br>
                                                                    <!-- The fileinput-button span is used to style the file input field as button -->
                                                                    <span class="btn btn-success fileinput-button">
                                                                                                <i class="glyphicon glyphicon-plus"></i>
                                                                                                <span>Select files...</span>
                                                                    <!-- The file input field used as target for the file upload widget -->
                                                                    <input id="player-image" type="file" name="files[]" multiple="">
                                                                    </span>
                                                                    <br>
                                                                    <br>
                                                                    <!-- The global progress bar -->
                                                                    <div id="progress" class="progress">
                                                                        <div class="progress-bar progress-bar-success"></div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>




                                                <a class="btn btn-s-md btn-warning preview-player-saved" href="javascript: void(0)">Preview</a>
                                                <a class="btn btn-s-md btn-primary update-player" href="javascript: void(0)">Save</a>
                                            </form>
                                        </div>
                                    </div>

                                </div>

                            </div>