@section('content')


<?php /*
<div class="row">

	<!-- Left Side Start -->
	<div class="col-lg-12">

		<!-- Box Start -->
		<div class="box">

			<!-- Title Bar Start -->
			<div class="box-title red">
				<span>Revenue</span>
				
			</div>
			<!-- Title Bar End -->

			<!-- Content Start -->
			<div class="content">

				<ul class="nav nav-tabs rev-chart">
					<li class="active">
						<a href="#revenue-daily" stat-ignore="1" chart-name="revenue" tab="daily" data-toggle="tab">Daily</a>
					</li>
					<li>
						<a href="#revenue-weekly" stat-ignore="1" chart-name="revenue" tab="weekly" data-toggle="tab">Weekly</a>
					</li>
					<li>
						<a href="#revenue-monthly" stat-ignore="1" chart-name="revenue" tab="monthly" data-toggle="tab">Monthly</a>
					</li>
				</ul>

				<div class="tab-content">
						<div class="tab-pane active" id="revenue-daily">
							<canvas class="default-chart" id="revenue-daily-chart" height="20" width="100%" data-info="revenue_daily_data"></canvas>
						</div>

						<div class="tab-pane" id="revenue-weekly">
							<canvas id="revenue-weekly-chart" height="20" width="100%" data-info="revenue_weekly_data"></canvas>
						</div>

						<div class="tab-pane" id="revenue-monthly">
							<canvas id="revenue-monthly-chart" height="20" width="100%" data-info="revenue_monthly_data"></canvas>
						</div>

						
					</div>

				

			</div>
			<!-- Content End -->

		</div>
		<!-- Box End -->
	</div>
	<!-- Left Side End -->
</div> */ ?>
<?php /*
<div class="row">
	<div class="col-lg-12">

		<!-- Box Start -->
		<div class="box">

			<!-- Title Bar Start -->
			<div class="box-title purple">
				<span>Ad Stats</span>
				
			</div>
			<!-- Title Bar End -->

			<!-- Content Start -->
			<div class="content">

				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#ad-stats-daily" data-toggle="tab">Daily</a>
					</li>
					<li>
						<a href="#ad-stats-weekly" data-toggle="tab">Weekly</a>
					</li>
					<li>
						<a href="#ad-stats-monthly" data-toggle="tab">Monthly</a>
					</li>
				</ul>

				<div class="tab-content">
						<div class="tab-pane active" id="ad-stats-daily">
							<canvas class="default-chart" id="ad-stats-daily-chart" height="30" width="100%" data-info="ad_stats_daily_data"></canvas>
						</div>

						<div class="tab-pane" id="ad-stats-weekly">
							<canvas id="ad-stats-weekly-chart" height="30" width="100%" data-info="ad_stats_weekly_data"></canvas>
						</div>

						<div class="tab-pane" id="ad-stats-monthly">
							<canvas id="ad-stats-monthly-chart" height="30" width="100%" data-info="ad_stats_monthly_data"></canvas>
						</div>

						
					</div>

				

			</div>
			<!-- Content End -->

		</div>
		<!-- Box End -->

		

	</div>
</div>
*/ ?>

<div class="row">

	<div class="col-lg-6">
		<!-- Box Start -->
		<div class="box">

			<!-- Title Bar Start -->
			<div class="box-title red">
				<span>Top Users</span>
				
			</div>
			<!-- Title Bar End -->

			<!-- Content Start -->
			<div class="content">

				<ul class="nav nav-tabs" data-set="country">
					<li class="active">
						<a href="#country-today" stat-name="today" data-toggle="tab">Today</a>
					</li>
					<?php /*
					<li>
						<a href="#country-weekly" stat-name="week" data-toggle="tab">Weekly</a>
					</li>
					<li>
						<a href="#country-monthly" stat-name="month" data-toggle="tab">Monthly</a>
					</li>
					<li>
						<a href="#country-year-to-date" stat-name="year" data-toggle="tab">Year to Date</a>
					</li>
					<!--
					<li>
						<a href="#top-sites-search" data-toggle="tab">Search</a>
					</li>
				-->*/ ?>
				</ul>

				<div class="tab-content">
						<div class="tab-pane active" id="country-today">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>No.</th>
									<th>Email</th>
									<th>Contest</th>
									<th class="hidden-xs">Views</th>
									<th class="hidden-xs">Shares</th>
									
								</thead>

								<tbody>
							        <?php 
									if($users){
										foreach($users as $row){ ?>
										<tr>
										  <td>{{ $us_count++ }}</td>
								          <td>{{ $row->email }}</td>
								          <td>{{ $row->contest_title }}</td>
								          <td>{{ $row->views }}</td>
								          <td>{{ $row->shares }}</td>						         
								        </tr>
									<?php	}
									}else{ ?>
										<tr>
											<td colspan="4" style="text-align: center;">No Data</td>
										</tr>
									<?php }
									?>
							        
							       
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="country-weekly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Country</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="country-monthly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Country</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="country-year-to-date">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Country</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="top-sites-search">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Country</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						
					</div>
			</div>
			<!-- Content End -->

		</div>
		<!-- Box End -->
	</div>

	<div class="col-lg-6">
		<!-- Box Start -->
		<div class="box">

			<!-- Title Bar Start -->
			<div class="box-title red">
				<span>Top Songs</span>
				
			</div>
			<!-- Title Bar End -->

			<!-- Content Start -->
			<div class="content">

				<ul class="nav nav-tabs" data-set="song">
					<li class="active">
						<a href="#top-songs-daily" stat-name="today" data-toggle="tab">Daily</a>
					</li>
					<?php /*
					<li>
						<a href="#top-songs-weekly" stat-name="week" data-toggle="tab">Weekly</a>
					</li>
					<li>
						<a href="#top-songs-monthly" stat-name="month" data-toggle="tab">Monthly</a>
					</li>
					<li>
						<a href="#top-songs-yearly" stat-name="year" data-toggle="tab">Year to Date</a>
					</li>*/ ?>
				</ul>

				<div class="tab-content">
						<div class="tab-pane active" id="top-songs-daily">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>No.</th>
									<th class="hidden-xs">Song</th>
									<!--<th class="hidden-xs">Plays</th>-->
									<th>Views</th>
									<th>Shares</th>
									<!--<th>Total</th>-->
								</thead>

								<tbody>
									<?php 
									if($songs && count($songs) > 0){
										foreach($songs as $row){ ?>
										<tr>
										  <td>{{ $so_count++; }}</td>
								          <td>{{ $row->title }}</td>
								          <td>{{ $row->views }}</td>
								          <td>{{ $row->shares }}</td>						         
								        </tr>
									<?php	}
									}else{ ?>
										<tr>
											<td colspan="6" style="text-align: center;">No Data</td>
										</tr>
									<?php }
									?>
							      </tbody>
							</table>
						</div>

						<div class="tab-pane" id="top-songs-weekly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>No.</th>
									<th class="hidden-xs">Song</th>
									<th class="hidden-xs">Plays</th>
									<th>Downloads</th>
									<th>Impressions</th>
									<th>Total</th>
								</thead>

								<tbody>
							      </tbody>
							</table>
						</div>

						<div class="tab-pane" id="top-songs-monthly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>No.</th>
									<th class="hidden-xs">Song</th>
									<th class="hidden-xs">Plays</th>
									<th>Downloads</th>
									<th>Impressions</th>
									<th>Total</th>
								</thead>

								<tbody>
							      </tbody>
							</table>
						</div>

						<div class="tab-pane" id="top-songs-yearly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>No.</th>
									<th class="hidden-xs">Song</th>
									<th class="hidden-xs">Plays</th>
									<th>Downloads</th>
									<th>Impressions</th>
									<th>Total</th>
								</thead>

								<tbody>
							      </tbody>
							</table>
						</div>

						
					</div>
			</div>
			<!-- Content End -->

		</div>
		<!-- Box End -->
	</div>

</div>

<div class="row">
			<div class="col-lg-12">
		<!-- Box Start -->
		<div class="box">

			<!-- Title Bar Start -->
			<div class="box-title green">
				<span>Top Sites</span>
				
			</div>
			<!-- Title Bar End -->

			<!-- Content Start -->
			<div class="content">

				<ul class="nav nav-tabs" data-set="site">
					<li class="active">
						<a href="#top-sites-today" stat-name="today" data-toggle="tab">Today</a>
					</li>
					<?php /*
					<li>
						<a href="#top-sites-weekly" stat-name="week" data-toggle="tab">Weekly</a>
					</li>
					<li>
						<a href="#top-sites-monthly" stat-name="month" data-toggle="tab">Monthly</a>
					</li>
					<li>
						<a href="#top-sites-year-to-date" stat-name="year" data-toggle="tab">Year to Date</a>
					</li>
					<!--
					<li>
						<a href="#top-sites-search" data-toggle="tab">Search</a>
					</li>
				-->*/ ?>
				</ul>

				<div class="tab-content">
						<div class="tab-pane active" id="top-sites-today">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>No.</th>
									<th class="hidden-xs">Site</th>
									<th class="hidden-xs">Reference Count</th>
								</thead>

								<tbody>
									<?php
									if($site){
										foreach($site as $row){ ?>
										<tr>
										  <td>{{ $si_count++ }}</td>
								          <td>{{ CustomHelper::limitText($row->site,20) }}</td>
								          <td>{{ $row->ref_count }}</td>
								          <!--<td class="center">0</td>	-->						         
								        </tr>
									<?php	}
									}else{ ?>
										<tr>
											<td colspan="4" style="text-align: center;">No Data</td>
										</tr>
									<?php }
									?>
							       
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="top-sites-weekly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Site</th>
									
									<th class="hidden-xs">Plays</th>
									<!--<th>Revenue</th>-->
								</thead>

								<tbody>							       
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="top-sites-monthly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Site</th>
									
									<th class="hidden-xs">Plays</th>
									<!--<th>Revenue</th>-->
								</thead>

								<tbody>							       
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="top-sites-year-to-date">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Site</th>
									
									<th class="hidden-xs">Plays</th>
									<!--<th>Revenue</th>-->
								</thead>

								<tbody>							       
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="top-sites-search">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Site</th>
									
									<th class="hidden-xs">Plays</th>
									<!--<th>Revenue</th>-->
								</thead>

								<tbody>							       
							    </tbody>

							</table>
						</div>

						
					</div>
			</div>
			<!-- Content End -->

		</div>
		<!-- Box End -->
	</div>
</div>

<div class="row">
	<div class="col-lg-6">
		<!-- Box Start -->
		<div class="box">

			<!-- Title Bar Start -->
			<div class="box-title purple">
				<span>Top Cities</span>
				
			</div>
			<!-- Title Bar End -->

			<!-- Content Start -->
			<div class="content">

				<ul class="nav nav-tabs" data-set="city">
					<li class="active">
						<a href="#city-today" stat-name="today" data-toggle="tab">Today</a>
					</li>
					<?php /*
					<li>
						<a href="#city-weekly" stat-name="week" data-toggle="tab">Weekly</a>
					</li>
					<li>
						<a href="#city-monthly" stat-name="month" data-toggle="tab">Monthly</a>
					</li>
					<li>
						<a href="#city-year-to-date" stat-name="year" data-toggle="tab">Year to Date</a>
					</li>
					<!--
					<li>
						<a href="#top-sites-search" data-toggle="tab">Search</a>
					</li>
				-->*/ ?>
				</ul>

				<div class="tab-content">
						<div class="tab-pane active" id="city-today">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>No.</th>
									<th>City</th>
									<th class="hidden-xs">Views</th>
									<th class="hidden-xs">Shares</th>
									<!--<th>Total</th>-->
								</thead>

								<tbody>
									<?php 
									if($city){
										foreach($city as $row){ ?>
										<tr>
											<td>{{ $ci_count++ }}</td>		
											<td>{{ $row->city }}</td>			         
											<td>{{ $row->views }}</td>
											<td>{{ $row->shares }}</td>
								        </tr>
									<?php	}
									}else{ ?>
										<!--<tr>
											<td colspan="4" style="text-align: center;">No Data</td>
										</tr>-->
									<?php }
									?>
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="city-weekly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>City</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="city-monthly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>City</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="city-year-to-date">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>City</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="top-sites-search">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>City</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						
					</div>
			</div>
			<!-- Content End -->

		</div>
		<!-- Box End -->
	</div>

	<div class="col-lg-6">
		<!-- Box Start -->
		<div class="box">

			<!-- Title Bar Start -->
			<div class="box-title red">
				<span>Top Countries</span>
				
			</div>
			<!-- Title Bar End -->

			<!-- Content Start -->
			<div class="content">

				<ul class="nav nav-tabs" data-set="country">
					<li class="active">
						<a href="#country-today" stat-name="today" data-toggle="tab">Today</a>
					</li>
					<?php /*
					<li>
						<a href="#country-weekly" stat-name="week" data-toggle="tab">Weekly</a>
					</li>
					<li>
						<a href="#country-monthly" stat-name="month" data-toggle="tab">Monthly</a>
					</li>
					<li>
						<a href="#country-year-to-date" stat-name="year" data-toggle="tab">Year to Date</a>
					</li>
					<!--
					<li>
						<a href="#top-sites-search" data-toggle="tab">Search</a>
					</li>
				-->*/ ?>
				</ul>

				<div class="tab-content">
						<div class="tab-pane active" id="country-today">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>No.</th>
									<th>Country</th>
									<th class="hidden-xs">Views</th>
									<th class="hidden-xs">Shares</th>
									
								</thead>

								<tbody>
							        <?php 
									if($country){
										foreach($country as $row){ ?>
										<tr>
										  <td>{{ $co_count++ }}</td>
								          <td>{{ ucfirst($row->country) }}</td>
								          <td>{{ $row->views }}</td>
								          <td>{{ $row->shares }}</td>						         
								        </tr>
									<?php	}
									}else{ ?>
										<tr>
											<td colspan="4" style="text-align: center;">No Data</td>
										</tr>
									<?php }
									?>
							        
							       
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="country-weekly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Country</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="country-monthly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Country</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="country-year-to-date">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Country</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="top-sites-search">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Country</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						
					</div>
			</div>
			<!-- Content End -->

		</div>
		<!-- Box End -->
	</div>
</div>
<script type="text/javascript">
var url = '{{ $url }}';
//console.log(dateArray);
</script>

@stop