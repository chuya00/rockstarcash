@section('content')
<div class="row">

    <div class="col-lg-12">
        <div class="box"><h2>Manage Audio Files</h2></div>
        <div class="box">
            <button type="button" class="btn btn-primary upload-audio-file-btn">Upload Audio File/s</button>
            <button type="button" class="btn btn-primary hide-uploader-btn" style="display: none">Hide Uploader</button>
            <div id="uploader-container" style="display: none">
                <div class="row">

                    <div class="col-lg-12">
                        <!-- Box Start -->
                        <div class="box smaller">

                            <!-- Content Start -->
                            <div class="content">
                                    <!-- The file upload form used as target for the file upload widget -->
                                    <form id="fileupload" action="#" method="POST" enctype="multipart/form-data">
                                        <!-- Redirect browsers with JavaScript disabled to the origin page -->
                                        <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
                                        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                                        <div class="row fileupload-buttonbar">
                                            <div class="col-lg-7">
                                                <!-- The fileinput-button span is used to style the file input field as button -->
                                                <span class="btn btn-success fileinput-button">
                                                    <i class="glyphicon glyphicon-plus"></i>
                                                    <span>Add files...</span>
                                                    <input type="file" name="files[]" multiple>
                                                </span>
                                                <button type="submit" class="btn btn-primary start">
                                                    <i class="glyphicon glyphicon-upload"></i>
                                                    <span>Start upload</span>
                                                </button>
                                                <button type="reset" class="btn btn-warning cancel">
                                                    <i class="glyphicon glyphicon-ban-circle"></i>
                                                    <span>Cancel upload</span>
                                                </button>
                                                <button type="button" class="btn btn-danger delete">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                    <span>Delete</span>
                                                </button>
                                                <input type="checkbox" class="toggle">
                                                <!-- The global file processing state -->
                                                <span class="fileupload-process"></span>
                                            </div>
                                            <!-- The global progress state -->
                                            <div class="col-lg-5 fileupload-progress fade">
                                                <!-- The global progress bar -->
                                                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                                </div>
                                                <!-- The extended global progress state -->
                                                <div class="progress-extended">&nbsp;</div>
                                            </div>
                                        </div>
                                        <!-- The table listing the files available for upload/download -->
                                        <table role="presentation" class="table table-striped files-list"><tbody class="files"></tbody></table>
                                    </form>
                                    <!-- The blueimp Gallery widget -->
                                    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
                                        <div class="slides"></div>
                                        <h3 class="title"></h3>
                                        <a class="prev">‹</a>
                                        <a class="next">›</a>
                                        <a class="close">×</a>
                                        <a class="play-pause"></a>
                                        <ol class="indicator"></ol>
                                    </div>
                                    <!-- The template to display files available for upload -->
                                    <script id="template-upload" type="text/x-tmpl">
                                    {% for (var i=0, file; file=o.files[i]; i++) { %}
                                        <tr class="template-upload fade">
                                            <td>
                                                <span class="preview"></span>
                                            </td>
                                            <td>
                                                <p class="name">{%=file.name%}</p>
                                                <strong class="error text-danger"></strong>
                                            </td>
                                            <td>
                                                <p class="size">Processing...</p>
                                                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
                                            </td>
                                            <td>
                                                {% if (!i && !o.options.autoUpload) { %}
                                                    <button class="btn btn-primary start" disabled>
                                                        <i class="glyphicon glyphicon-upload"></i>
                                                        <span>Start</span>
                                                    </button>
                                                {% } %}
                                                {% if (!i) { %}
                                                    <button class="btn btn-warning cancel">
                                                        <i class="glyphicon glyphicon-ban-circle"></i>
                                                        <span>Cancel</span>
                                                    </button>
                                                {% } %}
                                            </td>
                                        </tr>
                                    {% } %}
                                    </script>
                                    <!-- The template to display files available for download -->
                                    <script id="template-download" type="text/x-tmpl">
                                    </script>
                            </div>
                            <!-- Content End -->

                        </div>
                        <!-- Box End -->
                    </div>

                </div>
            </div>
        </div>
        <div class="box">

            <!-- Title Bar Start -->
            <div class="box-title green">
                <span>Uploaded Audio Files</span>
            </div>
            <!-- Title Bar End -->

            <!-- Content Start -->
            <div class="content">
                <table class="regular-table non-stripped bordered hoverable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Filename</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @if(isset($audio_files))
                            @foreach($audio_files as $file)
                            <!-- Table Row Start -->
                            <tr id="tr{{ $count }}">
                                <td>
                                    {{ $file->id }}
                                </td>
                                <td class="audio_title">
                                    {{ $file->title }}
                                </td>
                                <td>
                                    {{ $file->filename }}
                                </td>
                                
                                <td style="text-align: left;">
                                    <!--<a class="btn btn-default show-details-section" details-count="" href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i>Edit</a>-->
                                    <button class="btn btn-danger delete delete-audio-file{{ $count }}" data-toggle="modal" data-target="#myModalForm{{ $count }}" >
                                        <i class="icon-trash "></i>
                                        <span>Delete</span>
                                    </button>
                                    <div class="modal fade" id="myModalForm{{ $count }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body text-center">
                                                    <h3 style="font-weight: bold">Are you sure you want to delete file <br /><br />"<span id="delete-filename">{{ $file->filename }}</span>"?</h3>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                    <button type="button" data-id="{{ $file->id }}" data-file="{{ $file->filename }}" row-count="{{ $count++ }}" class="btn btn-primary delete-audio-file-confirm" data-dismiss="modal" >Yes, I am sure</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!-- Table Row End -->
                            @endforeach
                        @endif
                        

                    </tbody>
                </table>
            </div>
            <!-- Content End -->

        </div>
    </div>

</div>
<script>
var url = '{{ $url }}';
</script>
@stop