@section('content')
<div class="row">

    <div class="col-lg-12">
        <div class="box"><h2>Manage Email Templates</h2></div>
        <div class="box">
            <a href="{{ Url::to('/admin/email/templates/create') }}" target="_blank"><button type="button" class="btn btn-primary">Create Email Template</button></a>
        </div>
        <div class="box">

            <!-- Title Bar Start -->
            <div class="box-title green">
                <span>Created Email Templates</span>
            </div>
            <!-- Title Bar End -->

            <!-- Content Start -->
            <div class="content">
                <table class="regular-table non-stripped bordered hoverable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Template Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @if(isset($email_templates))
                            @foreach($email_templates as $et)
                            <!-- Table Row Start -->
                            <tr id="tr{{ $count }}">
                                <td>
                                    {{ $et->id }}
                                </td>
                                <td>
                                    {{ $et->filename }}
                                </td>
                                
                                <td style="text-align: left;">
                                    <!--<a class="btn btn-default show-details-section" details-count="" href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i>Edit</a>-->
                                    <button class="btn btn-danger delete delete-audio-file{{ $count }}" data-toggle="modal" data-target="#myModalForm{{ $count }}" >
                                        <i class="icon-trash "></i>
                                        <span>Delete</span>
                                    </button>
                                    <div class="modal fade" id="myModalForm{{ $count }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body text-center">
                                                    <h3 style="font-weight: bold">Are you sure you want to delete template<br /><br />"<span id="delete-filename">{{ $et->filename }}</span>"?</h3>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                    <button type="button" data-id="{{ $et->id }}" data-file="{{ $et->filename }}" row-count="{{ $count++ }}" class="btn btn-primary delete-audio-file-confirm" data-dismiss="modal" >Yes, I am sure</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!-- Table Row End -->
                            @endforeach
                        @endif
                        

                    </tbody>
                </table>
            </div>
            <!-- Content End -->

        </div>
    </div>

</div>
<script>
var url = '{{ $url }}';
</script>
@stop