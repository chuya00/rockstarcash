@section('content')



<div class="row">

	<!-- Left Side Start -->
	<div class="col-lg-12">

		<!-- Box Start -->
		<div class="box">

			<!-- Title Bar Start -->
			<div class="box-title red">
				<span>Revenue</span>
				
			</div>
			<!-- Title Bar End -->

			<!-- Content Start -->
			<div class="content">

				<ul class="nav nav-tabs rev-chart">
					<li class="active">
						<a href="#revenue-daily" stat-ignore="1" chart-name="revenue" tab="daily" data-toggle="tab">Daily</a>
					</li>
					<li>
						<a href="#revenue-weekly" stat-ignore="1" chart-name="revenue" tab="weekly" data-toggle="tab">Weekly</a>
					</li>
					<li>
						<a href="#revenue-monthly" stat-ignore="1" chart-name="revenue" tab="monthly" data-toggle="tab">Monthly</a>
					</li>
				</ul>

				<div class="tab-content">
						<div class="tab-pane active" id="revenue-daily">
							<canvas class="default-chart" id="revenue-daily-chart" height="20" width="100%" data-info="revenue_daily_data"></canvas>
						</div>

						<div class="tab-pane" id="revenue-weekly">
							<canvas id="revenue-weekly-chart" height="20" width="100%" data-info="revenue_weekly_data"></canvas>
						</div>

						<div class="tab-pane" id="revenue-monthly">
							<canvas id="revenue-monthly-chart" height="20" width="100%" data-info="revenue_monthly_data"></canvas>
						</div>

						
					</div>

				

			</div>
			<!-- Content End -->

		</div>
		<!-- Box End -->
	</div>
	<!-- Left Side End -->

	<!-- Left Side Start -->
	<div class="col-lg-12">

		<!-- Box Start -->
		<div class="box">

			<!-- Title Bar Start -->
			<div class="box-title green">
				<span>Revenue Source</span>
				
			</div>
			<!-- Title Bar End -->

			<!-- Content Start -->
			<div class="content">

				<ul class="nav nav-tabs rev-chart">
					<li class="active">
						<a href="#revenue-source-daily" stat-ignore="1" chart-name="revenue-source" tab="daily" data-toggle="tab">Daily</a>
					</li>
					<li>
						<a href="#revenue-source-weekly" stat-ignore="1" chart-name="revenue-source" tab="weekly" data-toggle="tab">Weekly</a>
					</li>
					<li>
						<a href="#revenue-source-monthly" stat-ignore="1" chart-name="revenue-source" tab="monthly" data-toggle="tab">Monthly</a>
					</li>
				</ul>

				<div class="tab-content">
						<div class="tab-pane active" id="revenue-source-daily">
							<canvas class="default-chart" id="revenue-source-daily-chart" height="20" width="100%" data-info="revenue_source_daily_data"></canvas>
						</div>

						<div class="tab-pane" id="revenue-source-weekly">
							<canvas id="revenue-source-weekly-chart" height="20" width="100%" data-info="revenue_source_weekly_data"></canvas>
						</div>

						<div class="tab-pane" id="revenue-source-monthly">
							<canvas id="revenue-source-monthly-chart" height="20" width="100%" data-info="revenue_source_monthly_data"></canvas>
						</div>

						
					</div>

				

			</div>
			<!-- Content End -->

		</div>
		<!-- Box End -->
	</div>
	<!-- Left Side End -->
</div>
<?php /*
<div class="row">
	<div class="col-lg-12">

		<!-- Box Start -->
		<div class="box">

			<!-- Title Bar Start -->
			<div class="box-title purple">
				<span>Ad Stats</span>
				
			</div>
			<!-- Title Bar End -->

			<!-- Content Start -->
			<div class="content">

				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#ad-stats-daily" data-toggle="tab">Daily</a>
					</li>
					<li>
						<a href="#ad-stats-weekly" data-toggle="tab">Weekly</a>
					</li>
					<li>
						<a href="#ad-stats-monthly" data-toggle="tab">Monthly</a>
					</li>
				</ul>

				<div class="tab-content">
						<div class="tab-pane active" id="ad-stats-daily">
							<canvas class="default-chart" id="ad-stats-daily-chart" height="30" width="100%" data-info="ad_stats_daily_data"></canvas>
						</div>

						<div class="tab-pane" id="ad-stats-weekly">
							<canvas id="ad-stats-weekly-chart" height="30" width="100%" data-info="ad_stats_weekly_data"></canvas>
						</div>

						<div class="tab-pane" id="ad-stats-monthly">
							<canvas id="ad-stats-monthly-chart" height="30" width="100%" data-info="ad_stats_monthly_data"></canvas>
						</div>

						
					</div>

				

			</div>
			<!-- Content End -->

		</div>
		<!-- Box End -->

		

	</div>
</div>
*/ ?>

<div class="row">
	<div class="col-lg-12">
		<!-- Box Start -->
		<div class="box">

			<!-- Title Bar Start -->
			<div class="box-title green">
				<span>Top Sites</span>
				
			</div>
			<!-- Title Bar End -->

			<!-- Content Start -->
			<div class="content">

				<ul class="nav nav-tabs" data-set="site">
					<li class="active">
						<a href="#top-sites-today" stat-name="today" data-toggle="tab">Today</a>
					</li>
					<li>
						<a href="#top-sites-weekly" stat-name="week" data-toggle="tab">Weekly</a>
					</li>
					<li>
						<a href="#top-sites-monthly" stat-name="month" data-toggle="tab">Monthly</a>
					</li>
					<li>
						<a href="#top-sites-year-to-date" stat-name="year" data-toggle="tab">Year to Date</a>
					</li>
					<!--
					<li>
						<a href="#top-sites-search" data-toggle="tab">Search</a>
					</li>
				-->
				</ul>

				<div class="tab-content">
						<div class="tab-pane active" id="top-sites-today">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Site</th>
									<!--<th class="hidden-xs">Impression</th>-->
									<th class="hidden-xs">Plays</th>
									<!--<th>Revenue</th>-->
								</thead>

								<tbody>
									<?php 
									if($plays_by_site){
										foreach($plays_by_site as $row){ ?>
										<tr>
								          <td>{{ $row->site }}</td>
								          <!--<td>20</td>-->
								          <td class="hidden-xs">{{ $row->plays }}</td>
								          <!--<td class="center">0</td>	-->						         
								        </tr>
									<?php	}
									}else{ ?>
										<tr>
											<td colspan="4" style="text-align: center;">No Data</td>
										</tr>
									<?php }
									?>
							       
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="top-sites-weekly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Site</th>
									
									<th class="hidden-xs">Plays</th>
									<!--<th>Revenue</th>-->
								</thead>

								<tbody>							       
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="top-sites-monthly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Site</th>
									
									<th class="hidden-xs">Plays</th>
									<!--<th>Revenue</th>-->
								</thead>

								<tbody>							       
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="top-sites-year-to-date">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Site</th>
									
									<th class="hidden-xs">Plays</th>
									<!--<th>Revenue</th>-->
								</thead>

								<tbody>							       
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="top-sites-search">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Site</th>
									
									<th class="hidden-xs">Plays</th>
									<!--<th>Revenue</th>-->
								</thead>

								<tbody>							       
							    </tbody>

							</table>
						</div>

						
					</div>
			</div>
			<!-- Content End -->

		</div>
		<!-- Box End -->
	</div>
</div>

<div class="row">


	<div class="col-lg-6">
		<!-- Box Start -->
		<div class="box">

			<!-- Title Bar Start -->
			<div class="box-title red">
				<span>Top Songs</span>
				
			</div>
			<!-- Title Bar End -->

			<!-- Content Start -->
			<div class="content">

				<ul class="nav nav-tabs" data-set="song">
					<li class="active">
						<a href="#top-songs-daily" stat-name="today" data-toggle="tab">Daily</a>
					</li>
					<li>
						<a href="#top-songs-weekly" stat-name="week" data-toggle="tab">Weekly</a>
					</li>
					<li>
						<a href="#top-songs-monthly" stat-name="month" data-toggle="tab">Monthly</a>
					</li>
					<li>
						<a href="#top-songs-yearly" stat-name="year" data-toggle="tab">Year to Date</a>
					</li>
				</ul>

				<div class="tab-content">
						<div class="tab-pane active" id="top-songs-daily">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>No.</th>
									<th class="hidden-xs">Song</th>
									<th class="hidden-xs">Plays</th>
									<th>Downloads</th>
									<th>Impressions</th>
									<th>Total</th>
								</thead>

								<tbody>
									<?php 
									if($top_songs && count($top_songs) > 0){
										$count = 1;
										foreach($top_songs as $row){ ?>
										<tr>
										  <td>{{ $count++; }}</td>
								          <td>{{ $row['title'] }}</td>
								          <td>{{ $row['plays'] }}</td>
								          <td>{{ $row['downloads'] }}</td>
								          <td class="hidden-xs">{{ $row['impressions'] }}</td>
								          <td class="center">{{ $row['total'] }}</td>							         
								        </tr>
									<?php	}
									}else{ ?>
										<tr>
											<td colspan="6" style="text-align: center;">No Data</td>
										</tr>
									<?php }
									?>
							      </tbody>
							</table>
						</div>

						<div class="tab-pane" id="top-songs-weekly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>No.</th>
									<th class="hidden-xs">Song</th>
									<th class="hidden-xs">Plays</th>
									<th>Downloads</th>
									<th>Impressions</th>
									<th>Total</th>
								</thead>

								<tbody>
							      </tbody>
							</table>
						</div>

						<div class="tab-pane" id="top-songs-monthly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>No.</th>
									<th class="hidden-xs">Song</th>
									<th class="hidden-xs">Plays</th>
									<th>Downloads</th>
									<th>Impressions</th>
									<th>Total</th>
								</thead>

								<tbody>
							      </tbody>
							</table>
						</div>

						<div class="tab-pane" id="top-songs-yearly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>No.</th>
									<th class="hidden-xs">Song</th>
									<th class="hidden-xs">Plays</th>
									<th>Downloads</th>
									<th>Impressions</th>
									<th>Total</th>
								</thead>

								<tbody>
							      </tbody>
							</table>
						</div>

						
					</div>
			</div>
			<!-- Content End -->

		</div>
		<!-- Box End -->
	</div>

	<div class="col-lg-6">
		<!-- Box Start -->
		<div class="box">

			<!-- Title Bar Start -->
			<div class="box-title yellow">
				<span>Daily Stats</span>
			</div>
			<!-- Title Bar End -->

			<!-- Content Start -->
			<div class="content">
				<div class="row">

  				<section class="col-md-6">
            

		            <div class="row">

		              	<div class="col-md-4">
		            		<span class="label label-success">Songs Plays</span></div>


				              <div class="col-md-8">
						            <div class="progress progress-striped" title="{{ $daily_stat['count_plays'] }}">
									  <div style="width: {{ $daily_stat['plays'] }}%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-success">
									    <span class="sr-only">40% Complete (success)</span>
									  </div>
									</div>
								</div>
							</div>



							 <div class="row">

							              <div class="col-md-4">
							           <span class="label label-warning">Download</span></div>

							<div class="col-md-8">
							<div class="progress progress-striped" title="{{ $daily_stat['count_downloads'] }}">
							  <div style="width: {{ $daily_stat['downloads'] }}%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar progress-bar-warning">
							    <span class="sr-only">60% Complete (warning)</span>
							  </div> </div>
							</div></div>

					</section>


					<section class="col-md-3 text-center"><h1 class="mp0">{{ $daily_stat['conversion'] }}%</h1>Conversion Ratio</section>



					  <section class="col-md-3  text-center ">
					  	@if($daily_stat['conversion'] < 1)
					  	<span class="glyphicon glyphicon-thumbs-down" style="font-size:40px;"></span>
					  	<div>Bad</div>
					  	@else
					  	<span class="glyphicon glyphicon-thumbs-up" style="font-size:40px;"></span>
					  	<div>Good</div>
					  	@endif					  	
					  	
					  </section>



					</div>
			</div>
			<!-- Content End -->

		</div>
		<!-- Box End -->
	</div>

</div>

<div class="row">
	<div class="col-lg-6">
		<!-- Box Start -->
		<div class="box">

			<!-- Title Bar Start -->
			<div class="box-title purple">
				<span>Plays by Cities</span>
				
			</div>
			<!-- Title Bar End -->

			<!-- Content Start -->
			<div class="content">

				<ul class="nav nav-tabs" data-set="city">
					<li class="active">
						<a href="#city-today" stat-name="today" data-toggle="tab">Today</a>
					</li>
					<li>
						<a href="#city-weekly" stat-name="week" data-toggle="tab">Weekly</a>
					</li>
					<li>
						<a href="#city-monthly" stat-name="month" data-toggle="tab">Monthly</a>
					</li>
					<li>
						<a href="#city-year-to-date" stat-name="year" data-toggle="tab">Year to Date</a>
					</li>
					<!--
					<li>
						<a href="#top-sites-search" data-toggle="tab">Search</a>
					</li>
				-->
				</ul>

				<div class="tab-content">
						<div class="tab-pane active" id="city-today">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>City</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
									<?php 
									if($plays_by_city){
										foreach($plays_by_city as $row){ ?>
										<tr>
								          <td>{{ ucfirst($row->city) }}</td>
								          <td>{{ $row->plays }}</td>
								          <td class="hidden-xs">0</td>
								          <td class="center">{{ $row->plays }}</td>							         
								        </tr>
									<?php	}
									}else{ ?>
										<tr>
											<td colspan="4" style="text-align: center;">No Data</td>
										</tr>
									<?php }
									?>
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="city-weekly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>City</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="city-monthly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>City</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="city-year-to-date">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>City</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="top-sites-search">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>City</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						
					</div>
			</div>
			<!-- Content End -->

		</div>
		<!-- Box End -->
	</div>

	<div class="col-lg-6">
		<!-- Box Start -->
		<div class="box">

			<!-- Title Bar Start -->
			<div class="box-title red">
				<span>Plays by Countries</span>
				
			</div>
			<!-- Title Bar End -->

			<!-- Content Start -->
			<div class="content">

				<ul class="nav nav-tabs" data-set="country">
					<li class="active">
						<a href="#country-today" stat-name="today" data-toggle="tab">Today</a>
					</li>
					<li>
						<a href="#country-weekly" stat-name="week" data-toggle="tab">Weekly</a>
					</li>
					<li>
						<a href="#country-monthly" stat-name="month" data-toggle="tab">Monthly</a>
					</li>
					<li>
						<a href="#country-year-to-date" stat-name="year" data-toggle="tab">Year to Date</a>
					</li>
					<!--
					<li>
						<a href="#top-sites-search" data-toggle="tab">Search</a>
					</li>
				-->
				</ul>

				<div class="tab-content">
						<div class="tab-pane active" id="country-today">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Country</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							        <?php 
									if($plays_by_country){
										foreach($plays_by_country as $row){ ?>
										<tr>
								          <td>{{ ucfirst($row->country) }}</td>
								          <td>{{ $row->plays }}</td>
								          <td class="hidden-xs">0</td>
								          <td class="center">{{ $row->plays }}</td>							         
								        </tr>
									<?php	}
									}else{ ?>
										<tr>
											<td colspan="4" style="text-align: center;">No Data</td>
										</tr>
									<?php }
									?>
							        
							       
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="country-weekly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Country</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="country-monthly">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Country</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="country-year-to-date">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Country</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						<div class="tab-pane" id="top-sites-search">
							<table class="regular-table non-stripped bordered hoverable">
								<thead>
									<th>Country</th>
									<th class="hidden-xs">Plays</th>
									<th class="hidden-xs">Downloads</th>
									<th>Total</th>
								</thead>

								<tbody>
							    </tbody>

							</table>
						</div>

						
					</div>
			</div>
			<!-- Content End -->

		</div>
		<!-- Box End -->
	</div>
</div>
<script type="text/javascript">
var url = '{{ $url }}';
//console.log(dateArray);
</script>

@stop