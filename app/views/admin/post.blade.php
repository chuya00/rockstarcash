@section('content')
<div class="col-lg-12">

	<!-- Box Start -->
	<div class="box">

		<!-- Title Bar Start -->
		<div class="box-title">
			<span class="gray">Create Post</span>
		</div>
		<!-- Title Bar End -->

		<!-- Content Start -->
		<div class="content">
			@if(Session::has('message'))
			<div class="row">
				<div class="notification notification-success">
					<strong>Success!</strong> {{ Session::get('message') }}
				</div>
			</div>
			@endif
			@if(Session::has('error_message'))
			<div class="row">
				<div class="notification notification-error">
					<strong>Save error!</strong> {{ Session::get('error_message') }}
				</div>
			</div>
			@endif
			@if ($errors->has())
			<div class="row">
				<div class="notification notification-error">
					<strong>Error!</strong><br />
					@foreach ($errors->all() as $error)
							{{ $error.'<br />' }}		
					@endforeach
				</div>
			</div>
			@endif

			<form method="post" action="post/save-post" id="post-form" class="basic-form">
				<div class="row">
					<h4 for="normal-field" class="control-label">Title</h4>
					<input type="text" name="title" />
				</div>
				<div class="row">
					<div class="summernote-full"></div>
				</div>

				<textarea name="message" id="message" style="display:none"></textarea>
				<div class="row">
					<label for="normal-field" class="control-label">Date to Send</label>
					<input type="text" class="form-control" name="date-to-send" id="datepicker" style="width: 200px;">			                       
				</div>


				<div class="row">
					<div class="col-md-6 col-md-offset-6 text-right">
						<button type="submit" class="btn btn-sm btn-success">Save</button>
					</div>
				</div>

			</form>

		</div>
		<!-- Content End -->

	</div>
	<!-- Box End -->

</div>
@stop