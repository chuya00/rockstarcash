@section('content')
<div class="row">

    <div class="col-lg-6">
        <div class="box"><h2>Manage Videos</h2></div>
        <div class="box">
            <button type="button" class="btn btn-primary add-video-button">Add Video</button>
        </div>
        @include('error')
        <!-- Box Start -->
        <div class="box add-video" style="display: none">

            <!-- Title Bar Start -->
            <div class="box-title">
                <span class="gray">Video Information</span>
            </div>
            <!-- Title Bar End -->

            <!-- Content Start -->
            <div class="content">

                <form method="post" action="video/save" class="basic-form add-video-form">
                    <label for="artist">Artist</label>
                    <input type="text" name="artist_name" id="artist" placeholder="Enter artist name">
                    <label for="album">Album</label>
                    <input type="text" name="album_name" id="album" placeholder="Enter album name">
                    <label for="title">Title</label>
                    <input type="text" name="title" id="title" placeholder="Enter video title">
                    <label for="url">URL</label>
                    <input type="text" name="link" id="url" placeholder="Enter video url. (e.g https://www.youtube.com/watch?v=tntOCGkgt98)">
                    <label for="source">Video Source</label>
                    <select id="source" name="source">
                        <option value="youtube">Youtube</option>
                        <option value="vimeo">Vimeo</option>
                    </select>
                    <div class="col-md-12 text-right">
                        <button type="reset" class="btn btn-sm btn-warning">Cancel</button>
                        <button type="submit" class="btn btn-sm btn-success">Save</button>
                    </div>
                </form>

            </div>
            <!-- Content End -->

        </div>
        <!-- Box End -->

    </div>

    <div class="col-lg-12">
        <div class="box">
            <!-- Title Bar Start -->
            <div class="box-title green">
                <span>Videos</span>
            </div>
            <!-- Content Start -->
            <div class="content no-padding">

             <div class="box">                          

                <!-- Content Start -->
                <div class="content">
                    <table class="regular-table non-stripped bordered hoverable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>URL</th>
                                <th>Source</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if(isset($videos))
                            @foreach($videos as $vid)
                            <!-- Table Row Start -->
                            <tr id="tr{{ $count }}">
                                <td>
                                    {{ $vid->id }}
                                </td>
                                <td>
                                    {{ $vid->title }}
                                </td>
                                <td>
                                    {{ $vid->link; }}
                                </td>
                                <td>
                                    {{ $vid->source; }}
                                </td>
                                
                                <td style="text-align: left;">
                                    
                                </td>
                            </tr>
                            <!-- Table Row End -->
                            @endforeach
                            @endif
                            

                        </tbody>
                    </table>
                </div>
                <!-- Content End -->

            </div>


        </div>
        <!-- Content End -->

    </div>


</div>

</div>
<script>
var url = '{{ $url }}';
</script>
@stop