<!doctype html>
<html class="fuelux" lang="en">
<head>

	@include('includes.head')
	
</head>
<body class="no-margin-top">
	@include('includes.sidebar')

	@include('includes.topbar')

	<div class="content" id="content-container">
		<div class="main-container">
            <div class="container">
			     @yield('content')
            </div>
		</div>
		<div style="text-align: center">No. of queries: {{ count(DB::getQueryLog()) }}</div>
	</div>

	@include('includes.javascript')

	@include('includes.ga')

	@include('includes.footer')
</body>
</html>