<div style="width: 500px;margin: 0 auto;">
	<form action="process" method="post" id="contact-form" >
	<div id="step1" step="1">
		<label>Username</label>
		<input class="input-field" type="text" name="username" /><br />
		
		<label>Password <span style="color: red">*</span></label>
		<input class="input-field" type="password" name="password" id="password" /><br />
		
		<div>Tell Us About Your Site/App</div>
		
		<label>What’s your Site/App Name</label>
		<input class="input-field" type="text" name="site_name" id="site_name" value="{{ Input::old('site_name') }}" /><br />
		
		<label>URL</label>
		<input class="input-field" type="text" name="site_url" id="site_url" value="{{ Input::old('site_url') }}" /><br />

		<label>Instagram - URL</label>
		<input class="input-field" type="text" name="instagram-url" id="instagram-url" value="{{ Input::old('instagram-url') }}" /><br />
		
		<label>Twitter - URL</label>
		<input class="input-field" type="text" name="twitter-url" id="twitter-url" value="{{ Input::old('twitter-url') }}" /><br />
		
		<button type="button"  class="next btn btn-large btn-success" >Next</button>
	</div>
	
	<div id="step2" step="2" style="display: none;">
		
		<label>What Genre’s Do You Post/Promote Most Often (Choose all that apply)</label>
		
		<label>Genres</label><br />
		<input type="checkbox" name="genre[]" class="genre" />Rock<br />
		<input type="checkbox" name="genre[]" class="genre" />Pop<br />
		<input type="checkbox" name="genre[]" class="genre" />Love Song<br />
		
		<button type="button"  class="prev btn btn-large btn-warning" >Prev</button> <button type="button"  class="next btn btn-large btn-success" >Next</button>
	</div>
	
	<div id="step3" step="3" style="display: none;">
		<div>How do you want to be paid?</div>
		
		<label>Name to be shown on check:</label>
		<input type="checkbox" name="fb_name" checked="checked" id="fb_name" value="<?php echo $user_profile['name']; ?>" />
		<span><?php echo $user_profile['name']; ?></span>
		<br />
		
		<label>Or use different:</label>
		<input class="input-field" type="text" name="mod_name" id="mod_name" value="{{ Input::old('mod_name') }}" /><br />

		<label>Address</label>
		<input class="input-field" type="text" name="address" id="address" value="{{ Input::old('address') }}" /><br />
		
		<label>City</label>
		<input class="input-field" type="text" name="city" id="city" value="<?php echo $user_profile['location']['name']; ?>" /><br />
		
		<label>State</label>
		<select id="AccountCreateState" class="form-input" name="region">
									<option value="AK">Alaska</option>
									<option value="AL">Alabama</option>
									<option value="AR">Arkansas</option>
									<option value="AZ">Arizona</option>
									<option value="CA">California</option>
									<option value="CO">Colorado</option>
									<option value="CT">Connecticut</option>
									<option value="DC">District Of Columbia</option>
									<option value="DE">Delaware</option>
									<option value="FL">Florida</option>
									<option value="GA">Georgia</option>
									<option value="HI">Hawaii</option>
									<option value="IA">Iowa</option>
									<option value="ID">Idaho</option>
									<option value="IL">Illinois</option>
									<option value="IN">Indiana</option>
									<option value="KS">Kansas</option>
									<option value="KY">Kentucky</option>
									<option value="LA">Louisiana</option>
									<option value="MA">Massachusetts</option>
									<option value="MD">Maryland</option>
									<option value="ME">Maine</option>
									<option value="MI">Michigan</option>
									<option value="MN">Minnesota</option>
									<option value="MO">Missouri</option>
									<option value="MS">Mississippi</option>
									<option value="MT">Montana</option>
									<option value="NC">North Carolina</option>
									<option value="ND">North Dakota</option>
									<option value="NE">Nebraska</option>
									<option value="NH">New Hampshire</option>
									<option value="NJ">New Jersey</option>
									<option value="NM">New Mexico</option>
									<option value="NV">Nevada</option>
									<option value="NY">New York</option>
									<option value="OH">Ohio</option>
									<option value="OK">Oklahoma</option>
									<option value="OR">Oregon</option>
									<option value="PA">Pennsylvania</option>
									<option value="RI">Rhode Island</option>
									<option value="SC">South Carolina</option>
									<option value="SD">South Dakota</option>
									<option value="TN">Tennessee</option>
									<option value="TX">Texas</option>
									<option value="UT">Utah</option>
									<option value="VA">Virginia</option>
									<option value="VT">Vermont</option>
									<option value="WA">Washington</option>
									<option value="WI">Wisconsin</option>
									<option value="WV">West Virginia</option>
									<option value="WY">Wyoming</option>
								</select>
		<br />
		
		<label>Zip</label>
		<input class="input-field" type="text" name="zip" id="zip" value="{{ Input::old('zip') }}" /><br />
		
		<label>Social Security or Tax ID #:</label>
		<input class="input-field" type="text" name="tax_id" id="tax_id" value="{{ Input::old('tax_id') }}" /><br />
		<input type="hidden" name="gender" value="<?php echo $user_profile['gender']; ?>" />
		<input type="hidden" name="email" value="<?php echo $user_profile['email']; ?>" />
		<input type="hidden" name="user_type" value="developer" />
		<button type="button"  class="prev btn btn-large btn-warning" >Prev</button><button type="submit" class="submit btn btn-large btn-inverse" >Submit</button>
	</div>
	
	</form>
</div>