<div style="width: 500px;margin: 0 auto;">
	<form action="process" id="contact-form" method="post" >
	<div id="step1" step="1">
		<label>Password <span style="color: red">*</span></label>
		<input type="password" name="password" id="password" class="input-field" style="margin-bottom: 21px;" /><br />
		
		<div>Enter the first song you would like to make money with</div>
		
		<label>Song Title</label>
		<input class="input-field" type="text" name="song_title" id="song_title" value="{{ Input::old('song_title') }}" /><br />
		
		<label>Artist Name</label>
		<input class="input-field" type="text" name="artist_name" id="artist_name" value="{{ Input::old('artist_name') }}" /><br />

		<label>Producer</label>
		<input class="input-field" type="text" name="producer" id="producer" value="{{ Input::old('producer') }}" /><br />
		
		<label>Song writer</label>
		<input class="input-field" type="text" name="song_writer" id="song_writer" value="{{ Input::old('song_writer') }}" /><br />
		
		<button type="button"  class="next btn btn-large btn-success" >Next</button>
	</div>
	
	<div id="step2" step="2" style="display: none;">
		<div>Enter Artist info</div>
		
		<label>Hometown</label>
		<input class="input-field" type="text" name="hometown" id="hometown" value="{{ Input::old('hometown') }}" /><br />
		
		<label>Genre</label>
		<input class="input-field" type="text" name="genre" id="genre" value="{{ Input::old('genre') }}" /><br />

		<label>FB Page - URL</label>
		<input class="input-field" type="text" name="fb-url" id="fb-url" value="{{ Input::old('fb-url') }}" /><br />
		
		<label>Instagram - URL</label>
		<input class="input-field" type="text" name="instagram-url" id="instagram-url" value="{{ Input::old('instagram-url') }}" /><br />
		
		<label>Twitter - URL</label>
		<input class="input-field" type="text" name="twitter-url" id="twitter-url" value="{{ Input::old('twitter-url') }}" /><br />
		
		<label>Youtube - URL</label>
		<input class="input-field" type="text" name="yt-url" id="yt-url" value="{{ Input::old('yt-url') }}" /><br />
		
		<button type="button"  class="next btn btn-large btn-success" >Next</button>
	</div>
	
	<div id="step3" step="3" style="display: none;">
		<div>Label info</div>
		
		<label>Label Name</label>
		<input class="input-field" type="text" name="label_name" id="label_name" value="{{ Input::old('label_name') }}" /><br />
		
		<label>Address</label>
		<input class="input-field" type="text" name="label_address" id="label_address" value="{{ Input::old('label_address') }}" /><br />
		
		<label>City</label>
		<input class="input-field" type="text" name="label_city" id="label_city" value="<?php echo $user_profile['location']['name']; ?>" /><br />
		
		<label>State</label>
								<select id="AccountCreateState" class="form-input" name="label_region">
									<option value="AK">Alaska</option>
									<option value="AL">Alabama</option>
									<option value="AR">Arkansas</option>
									<option value="AZ">Arizona</option>
									<option value="CA">California</option>
									<option value="CO">Colorado</option>
									<option value="CT">Connecticut</option>
									<option value="DC">District Of Columbia</option>
									<option value="DE">Delaware</option>
									<option value="FL">Florida</option>
									<option value="GA">Georgia</option>
									<option value="HI">Hawaii</option>
									<option value="IA">Iowa</option>
									<option value="ID">Idaho</option>
									<option value="IL">Illinois</option>
									<option value="IN">Indiana</option>
									<option value="KS">Kansas</option>
									<option value="KY">Kentucky</option>
									<option value="LA">Louisiana</option>
									<option value="MA">Massachusetts</option>
									<option value="MD">Maryland</option>
									<option value="ME">Maine</option>
									<option value="MI">Michigan</option>
									<option value="MN">Minnesota</option>
									<option value="MO">Missouri</option>
									<option value="MS">Mississippi</option>
									<option value="MT">Montana</option>
									<option value="NC">North Carolina</option>
									<option value="ND">North Dakota</option>
									<option value="NE">Nebraska</option>
									<option value="NH">New Hampshire</option>
									<option value="NJ">New Jersey</option>
									<option value="NM">New Mexico</option>
									<option value="NV">Nevada</option>
									<option value="NY">New York</option>
									<option value="OH">Ohio</option>
									<option value="OK">Oklahoma</option>
									<option value="OR">Oregon</option>
									<option value="PA">Pennsylvania</option>
									<option value="RI">Rhode Island</option>
									<option value="SC">South Carolina</option>
									<option value="SD">South Dakota</option>
									<option value="TN">Tennessee</option>
									<option value="TX">Texas</option>
									<option value="UT">Utah</option>
									<option value="VA">Virginia</option>
									<option value="VT">Vermont</option>
									<option value="WA">Washington</option>
									<option value="WI">Wisconsin</option>
									<option value="WV">West Virginia</option>
									<option value="WY">Wyoming</option>
								</select><br />
		
		<label>Zip</label>
		<input class="input-field" type="text" name="label_zip" id="label_zip" value="{{ Input::old('label_zip') }}" /><br />
		
		<label>Phone</label>
		<input class="input-field" type="text" name="label_phone" id="label_phone" value="{{ Input::old('label_phone') }}" /><br />
		
		<input type="checkbox" name="label_post_auth" value="{{ (Input::old('label_post_auth'))? 'checked="checked"' : '' }}" />
		I am authorized to post music by this label.  All rights
		<br />
		
		<button type="button"  class="next btn btn-large btn-success" >Next</button>
	</div>
	
	<div id="step4" step="4" style="display: none;">
		<div>How do you want to be paid?</div>
		
		<label>Name to be shown on check:</label>
		<input type="checkbox" name="fb_name" checked="checked" id="fb_name" value="<?php echo $user_profile['name']; ?>" />
		<span><?php echo $user_profile['name']; ?></span>
		<br />
		
		<label>Or use different:</label>
		<input class="input-field" type="text" name="mod_name" id="mod_name" value="{{ Input::old('mod_name') }}" /><br />

		<label>Address</label>
		<input class="input-field" type="text" name="address" id="address" value="{{ Input::old('address') }}" /><br />
		
		<label>City</label>
		<input class="input-field" type="text" name="city" id="city" value="<?php echo $user_profile['location']['name']; ?>" /><br />
		
		<label>State</label>
								<select id="AccountCreateState" class="form-input" name="region">
									<option value="AK">Alaska</option>
									<option value="AL">Alabama</option>
									<option value="AR">Arkansas</option>
									<option value="AZ">Arizona</option>
									<option value="CA">California</option>
									<option value="CO">Colorado</option>
									<option value="CT">Connecticut</option>
									<option value="DC">District Of Columbia</option>
									<option value="DE">Delaware</option>
									<option value="FL">Florida</option>
									<option value="GA">Georgia</option>
									<option value="HI">Hawaii</option>
									<option value="IA">Iowa</option>
									<option value="ID">Idaho</option>
									<option value="IL">Illinois</option>
									<option value="IN">Indiana</option>
									<option value="KS">Kansas</option>
									<option value="KY">Kentucky</option>
									<option value="LA">Louisiana</option>
									<option value="MA">Massachusetts</option>
									<option value="MD">Maryland</option>
									<option value="ME">Maine</option>
									<option value="MI">Michigan</option>
									<option value="MN">Minnesota</option>
									<option value="MO">Missouri</option>
									<option value="MS">Mississippi</option>
									<option value="MT">Montana</option>
									<option value="NC">North Carolina</option>
									<option value="ND">North Dakota</option>
									<option value="NE">Nebraska</option>
									<option value="NH">New Hampshire</option>
									<option value="NJ">New Jersey</option>
									<option value="NM">New Mexico</option>
									<option value="NV">Nevada</option>
									<option value="NY">New York</option>
									<option value="OH">Ohio</option>
									<option value="OK">Oklahoma</option>
									<option value="OR">Oregon</option>
									<option value="PA">Pennsylvania</option>
									<option value="RI">Rhode Island</option>
									<option value="SC">South Carolina</option>
									<option value="SD">South Dakota</option>
									<option value="TN">Tennessee</option>
									<option value="TX">Texas</option>
									<option value="UT">Utah</option>
									<option value="VA">Virginia</option>
									<option value="VT">Vermont</option>
									<option value="WA">Washington</option>
									<option value="WI">Wisconsin</option>
									<option value="WV">West Virginia</option>
									<option value="WY">Wyoming</option>
								</select>
							
		
		<br />
		
		<label>Zip</label>
		<input class="input-field" type="text" name="zip" id="zip" value="{{ Input::old('zip') }}" /><br />
		
		<label>Social Security or Tax ID #:</label>
		<input class="input-field" type="text" name="tax_id" id="tax_id" value="{{ Input::old('tax_id') }}" /><br />
		<input type="hidden" name="gender" value="<?php echo $user_profile['gender']; ?>" />
		<input type="hidden" name="email" value="<?php echo $user_profile['email']; ?>" />
		<input type="hidden" name="user_type" value="label" />
		<input type="hidden" name="fb" value="<?php echo $user_profile['id']; ?>" />
		<button type="submit" class="submit btn btn-large btn-inverse" >Submit</button>
	</div>
	
	</form>
</div>