<style>
div.field input{
	margin-bottom: 0px !important;
}
div.field{
	margin-bottom: 15px;
}
</style>
<div id="contact" class="section-heading">
    <div class="section-title">Register as {{ ucfirst(str_replace('_',' ',$user)) }}</div>
    <div class="section-subtitle"></div>
</div>
<div style="width: 500px;margin: 0 auto;">
	@include('error')
	<form action="process" method="post" id="contact-form" >
	<div id="step1" step="1">
		<div class="field">
			<label>Password <span style="color: red">*</span></label>
			<input type="password" name="password" id="password" class="input-field" />
		</div>

		<div class="field">
			<label>Confirm Password <span style="color: red">*</span></label>
			<input type="password" name="cpassword" id="cpassword" class="input-field" />
		</div>

		<div class="field">
			<label>Full Name <span style="color: red">*</span></label>
			<input type="text" name="name" id="name" class="input-field" value="{{ (Input::old('name'))? Input::old('name') : $user_profile['name'] }}" />
		</div>

		<div style="font-size: 15px;margin-bottom: 7px;">Enter the first song you would like to make money with</div>
		<div class="field">
			<label>Song Title</label>
			<input class="input-field" type="text" name="song_title" id="song_title" value="{{ Input::old('song_title') }}" />
		</div>

		<div class="field">
			<label>Artist Name</label>
			<input class="input-field" type="text" name="artist_name" id="artist_name" value="{{ Input::old('artist_name') }}" />
		</div>

		<div class="field">
			<label>Producer</label>
			<input class="input-field" type="text" name="producer" id="producer" value="{{ Input::old('producer') }}" />
		</div>

		<div class="field">
			<label>Song writer</label>
			<input class="input-field" type="text" name="song_writer" id="song_writer" value="{{ Input::old('song_writer') }}" />
		</div>

		<div class="field">
			<label>Label</label>
			<input class="input-field" type="text" name="label" id="label" value="{{ Input::old('label') }}" />
		</div>

		<input type="hidden" name="label_id" id="label_id" value="{{ Input::old('label_id') }}" />
		
		<div class="field">
			<input type="checkbox" name="post_auth" value="{{ (Input::old('post_auth'))? 'checked="checked"' : '' }}" />
			<span style="position: relative;top: 2px;">I am authorized to post music by this label.</span>
		</div>
		<br />
		
		<button type="button"  class="next btn btn-large btn-success" >Next</button>
	</div>
	
	<div id="step2" step="2" style="display: none;">
		<div>Enter Artist info</div>
		<label>Hometown</label>
		<input class="input-field" type="text" name="hometown" id="hometown" value="{{ Input::old('hometown') }}" /><br />
		
		<label>Genre</label>
		<input class="input-field" type="text" name="genre" id="genre" value="{{ Input::old('genre') }}" /><br />

		<label>FB Page - URL</label>
		<input class="input-field" type="text" name="fb-url" id="fb-url" value="{{ Input::old('fb-url') }}" /><br />
		
		<label>Instagram - URL</label>
		<input class="input-field" type="text" name="instagram-url" id="instagram-url" value="{{ Input::old('instagram-url') }}" /><br />
		
		<label>Twitter - URL</label>
		<input class="input-field" type="text" name="twitter-url" id="twitter-url" value="{{ Input::old('twitter-url') }}" /><br />
		
		<label>Youtube - URL</label>
		<input class="input-field" type="text" name="yt-url" id="yt-url" value="{{ Input::old('yt-url') }}" /><br />
		
		<!--<button type="button"  class="prev btn btn-large btn-warning" >Prev</button>--> <button type="button"  class="next btn btn-large btn-success" >Next</button>
	</div>
	
	<div id="step3" step="3" style="display: none;">
		<div>How do you want to be paid?</div>
		<label>Name to be shown on check:</label>
		<input type="checkbox" name="fb_name" checked="checked" id="fb_name" value="{{ $user_profile['name'] }}" />
		<span><?php echo $user_profile['name']; ?></span>
		<br />
		
		<label>Or use different:</label>
		<input class="input-field" type="text" name="mod_name" id="mod_name" value="{{ Input::old('mod_name') }}" /><br />

		<label>Address</label>
		<input class="input-field" type="text" name="address" id="address" value="{{ Input::old('address') }}" /><br />
		
		<label>City</label>
		<input class="input-field" type="text" name="city" id="city" value="<?php echo isset($user_profile['location']['name'])? $user_profile['location']['name'] : ''; ?>" /><br />
		
		<label>State</label>
		<select id="AccountCreateState" class="form-input" name="region">
									<option value="AK">Alaska</option>
									<option value="AL">Alabama</option>
									<option value="AR">Arkansas</option>
									<option value="AZ">Arizona</option>
									<option value="CA">California</option>
									<option value="CO">Colorado</option>
									<option value="CT">Connecticut</option>
									<option value="DC">District Of Columbia</option>
									<option value="DE">Delaware</option>
									<option value="FL">Florida</option>
									<option value="GA">Georgia</option>
									<option value="HI">Hawaii</option>
									<option value="IA">Iowa</option>
									<option value="ID">Idaho</option>
									<option value="IL">Illinois</option>
									<option value="IN">Indiana</option>
									<option value="KS">Kansas</option>
									<option value="KY">Kentucky</option>
									<option value="LA">Louisiana</option>
									<option value="MA">Massachusetts</option>
									<option value="MD">Maryland</option>
									<option value="ME">Maine</option>
									<option value="MI">Michigan</option>
									<option value="MN">Minnesota</option>
									<option value="MO">Missouri</option>
									<option value="MS">Mississippi</option>
									<option value="MT">Montana</option>
									<option value="NC">North Carolina</option>
									<option value="ND">North Dakota</option>
									<option value="NE">Nebraska</option>
									<option value="NH">New Hampshire</option>
									<option value="NJ">New Jersey</option>
									<option value="NM">New Mexico</option>
									<option value="NV">Nevada</option>
									<option value="NY">New York</option>
									<option value="OH">Ohio</option>
									<option value="OK">Oklahoma</option>
									<option value="OR">Oregon</option>
									<option value="PA">Pennsylvania</option>
									<option value="RI">Rhode Island</option>
									<option value="SC">South Carolina</option>
									<option value="SD">South Dakota</option>
									<option value="TN">Tennessee</option>
									<option value="TX">Texas</option>
									<option value="UT">Utah</option>
									<option value="VA">Virginia</option>
									<option value="VT">Vermont</option>
									<option value="WA">Washington</option>
									<option value="WI">Wisconsin</option>
									<option value="WV">West Virginia</option>
									<option value="WY">Wyoming</option>
								</select>
		
		<br />
		
		<label>Zip</label>
		<input class="input-field" type="text" name="zip" id="zip" value="{{ Input::old('zip') }}" /><br />
		
		<label>Social Security or Tax ID #:</label>
		<input class="input-field" type="text" name="tax_id" id="tax_id" value="{{ Input::old('tax_id') }}" />
		<div class="two fields" style="margin-top: -30px;">
			
			<div class="field captcha-container" style="margin-bottom: 40px;">
	            <span>How much is <strong id="captcha-img">2 + 2</strong> =</span>
	            
	        </div>
	    	
	        <div class="field">
	        	<div>
	            <input type="text" class="captcha-input-field" id="captcha" name="captcha" style="margin-left: 5px;width: 50px;">
	            <div>
	        </div>
    	</div>
		<input type="hidden" name="gender" value="<?php echo $user_profile['gender']; ?>" />
		<input type="hidden" name="email" value="<?php echo $user_profile['email']; ?>" />
		<input type="hidden" name="user_type" value="artist" />
		<input type="hidden" name="fb" value="<?php echo $user_profile['id']; ?>" />
		<textarea style="display:none" name="other_details" id="other_details" ></textarea>
		<!--<button type="button"  class="prev btn btn-large btn-warning" >Prev</button>-->
		<button type="submit" class="submit btn btn-large btn-inverse" >Submit</button>
	</div>
	
	</form>
</div>