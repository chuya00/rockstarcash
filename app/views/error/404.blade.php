<!doctype html>
<html class="fuelux" lang="en">
<head>

	@include('includes.head')
	
</head>
<body class="no-margin-top">

	<!-- Content Start -->
	<div class="content">
		<div class="main-container" style="margin-left: 0px">

			<div class="container">
				<div class="row">

					<div class="col-lg-12">

						<!-- Error Message Start -->
						<div class="error-404 text-center">
							<i class="fa fa-frown-o"></i>
							<h1>Whooops!</h1>
							<h4>We couldn't find the page you are looking for...</h4>
							<form class="search-form">
								<input type="text" class="search form-control" placeholder="Input your search here..." />
							</form>
							<p>or <a href="{{ URL::previous(); }}">get back to home page</a></p>
						</div>
						<!-- Error Message End -->

					</div>

				</div>
			</div>

		</div>
	</div>
	<!-- Content End -->

	@include('includes.ga')

	@include('includes.footer')
</body>
</html>