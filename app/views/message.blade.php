@if (Session::has('message'))
	<div id="message-msg" class="notification notification-success">
	{{ Session::get('message') }}
	</div>
@endif