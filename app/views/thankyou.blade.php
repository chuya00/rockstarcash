@include('head')
<body class="fullwidth-slider">

 <section>
  <div class="container cont-background">
    <div class="row">
      <div class="span12">
        <div id="contact" class="section-heading">
          <div class="section-title">Thanks For Signing Up</div>
          <div class="section-subtitle">We Promise It Will Be Worth The Wait.</div>
        </div>
      </div>
      <div class="clear"></div>
      <div class="span12">
        <div class="inner" style="text-align: center;">
          <div class="content-logo"><img src="http://www.rockstarcash.com/images/logo.png" alt="Ensconce"></div>
		  <h1>We will notify you as soon as we can!</h1>
        </div>
      </div>
    </div>
  </div>
</section>
@include('footer')