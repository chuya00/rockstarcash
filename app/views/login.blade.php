<!doctype html>

<html class="fuelux" lang="en">

<head>

  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">



  <title>Login | Rockstarcash</title>



  <link rel="stylesheet" type="text/css" href="{{ asset('assets/bootstrap3/css/bootstrap.css') }}">

  <link rel="stylesheet" type="text/css" href="{{ asset('assets/fontawesome/css/font-awesome.min.css') }}">



  <!-- Google Fonts -->

  <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>

  <link href='http://fonts.googleapis.com/css?family=Lato:400,100italic,100,300italic,300,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>



  <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

  <![endif]-->



  <!-- Theme Styles -->

  <link rel="stylesheet" type="text/css" href="{{ asset('css/demo.css') }}">

  <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">

  <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">

  <link rel="stylesheet" type="text/css" href="{{ asset('css/login.css') }}">



  <link rel="shortcut icon" href="favicon.ico" type="image/png">

  <link rel="shortcut icon" type="image/png" href="favicon.ico" />



</head>



<body class="login-page">



  <div class="login-box">

    

    <div class="login-box-title red">Login</div>

    <div class="login-box-content">

      @if(Session::has('message'))
      <div class="notification notification-error">
          <strong>Error!</strong> {{ Session::get('message') }}
      </div>
      @endif
    

     <form  method="POST" action="login">

       

       <input type="text" name="email" placeholder="Your E-Mail..." value="" /> 

       

       <input type="password" name="password" placeholder="Your Password..." value="" /> 

       

       <input type="submit" name="submit" id="submit" value="Sign in!" />

     

     </form>

     

     <div class="half"><a href="forgot-password">Forgot your password?</a></div>

     <div class="half last"></div>

    

    </div>

    

  </div>



<!-- Javascript -->

<script src="{{ asset('assets/jquery/jquery.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/bootstrap3/js/bootstrap.min.js') }}" type="text/javascript"></script>



<script>

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');



  ga('create', 'UA-53707145-1', 'auto');

  ga('send', 'pageview');



</script>

</body>

</html>