<script>

$(document).ready(function(){
	<?php if(isset($_GET['err'])){ ?>
	$("#msg-container").show();
	<?php
	} ?>

	selectUserForm();
	validateForm();
	@if ($errors->has())
		{{ 'showAllFormSection();	signUpFormSubmit();' }}
	@endif

	
});

function triggerOnSubmit(){
	$('#contact-form').submit(function() {
		//alert();
		var data_obj = $(this).serializeObject();
		delete data_obj.password;
		delete data_obj.cpassword;
		delete data_obj.name;

		$('#other_details').html(JSON.stringify(data_obj));

		//alert($('#other_details').html());
    });
}

function validateForm(){
	$('#contact-form')
	  .form(getRules(), {
		inline : true,
		on     : 'blur'
	  });
}

function getRules(){
	return {
    firstName: {
      identifier  : 'name',
      rules: [
        {
          type   : 'empty',
          prompt : 'Please enter your first name'
        }
      ]
    },
    password: {
      identifier : 'password',
      rules: [
        {
          type   : 'empty',
          prompt : 'Please enter a password'
        },
        {
          type   : 'length[6]',
          prompt : 'Your password must be at least 6 characters'
        }
      ]
    },

    cpassword: {
      identifier : 'cpassword',
      rules: [
        {
          type   : 'empty',
          prompt : 'Please enter a password'
        },
        {
          type   : 'match[password]',
          prompt : 'Password doesn\'t match'
        }
      ]
    },

    captcha: {
      identifier : 'captcha',
      rules: [
        {
          type   : 'empty',
          prompt : 'Are you really a human?'
        },
      ]
    },
    
  };
}

function signUpFormSubmit(){
	  // load captcha question for contact form
	  if ($('#captcha-img').length) {
	    $.get('captcha/?generate', function(response) {
	      $('#captcha-img').html(response);
	    }, 'html');
	  }
	//$('#contact-form').submit(function(){
		//return false;
		//alert();
		//if ($('#contact-form').length > 0) {
		validateForm();
		/*
	    $('#contact-form').validate({ 
	    	rules: { 
	    		password: { required: true},
	    		name: {required: true},
	            captcha: { required: true, remote: 'captcha/' }
	        },
	        messages: { 
	        	password: 'This field is required.',
	        	full_name: 'This field is required.',
	            captcha: 'Are you really a human?'
	        },
	        submitHandler: function(form) {  
	        	form.submit();
	            //console.log(form);
	            //return false;
	            //$(form).ajaxSubmit({ success: contactFormResponse, error: function(x,y,z){ console.log(x); console.log(y+" "+z) } }); 
			}
	    });
	    */

	  //} // if contact form
	//});
}

function selectUserForm(){
	$('.user-type-btn').click(function(){
		var user_type = $(this).attr('user-type');
		$('#user-types').hide();
		$.ajax({
			url: '{{ URL::to('register-forms'); }}',
			type: 'POST',
			data: 'u='+user_type,
			success: function(r){
				$('#reg-form').html(r);
				nextPrev();
				triggerOnSubmit();
				//autocompleteLabel();
				//activateValidation();
				signUpFormSubmit();
				//alert();
				
				//console.log(r);
			},
			error: function(x,y,z){
				console.log(x);
				console.log(y+" "+z);
			}
		});
	});
}

function activateValidation_bakup(){
	$("#contact-form").validate({
		rules: {
			artist_name: "required",
			/*lastname: "required",
			username: {
				required: true,
				minlength: 2
			}, */
			password: {
				required: true,
				minlength: 6
			},
			label: "required",
			/* confirm_password: {
				required: true,
				minlength: 5,
				equalTo: "#password"
			},
			email: {
				required: true,
				email: true
			},
			topic: {
				required: "#newsletter:checked",
				minlength: 2
			},
			agree: "required" */
		},
		messages: {
			/* firstname: "Please enter your firstname",
			lastname: "Please enter your lastname",
			username: {
				required: "Please enter a username",
				minlength: "Your username must consist of at least 2 characters"
			}, */
			password: {
				required: "Please provide a password",
				minlength: "Your password must be at least 6 characters long"
			},
			/* confirm_password: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long",
				equalTo: "Please enter the same password as above"
			},
			email: "Please enter a valid email address",
			agree: "Please accept our policy" */
		}
	});
}

function checkData(){
	$("#register-artist").submit(function(){
		var label = $("#label").val();
		var label_id = $("#label_id").val();
		if(label_id == ""){
			if(label != ""){
				$("#label_id").val(label);
			}else{
				alert("Label not specified");
				return false;
			}
			
		}
	});
}

function scrollTo(id){
	$('html, body').animate({
		    scrollTop: $(id).offset().top
		}, 1000);
}

function nextPrev(){
	$('.next').click(function(){
		$(this).hide();
		var step = $(this).parent().attr('step');
		
		$('#step'+(parseInt(step)+1)).prepend('<hr />').fadeIn();
		$('#step'+(parseInt(step)+1)).fadeIn();
		scrollTo('#step'+(parseInt(step)+1));
		//$(this).parent().hide();
	});
	
	$('.prev').click(function(){
		var step = $(this).parent().attr('step');
		
		$('#step'+(parseInt(step)-1)).show();
		$(this).parent().hide();
	});
}

function showAllFormSection(){
	$('.next').hide();
	var i = 1;
	while(true){
		//console.log('#step'+i);
		//console.log($('#step'+i).length);
		if($('#step'+i).length > 0){
			$('#step'+i).prepend('<hr />').show();
			i++;
		}else{
			break;
		}
	}
}

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
/*
function autocompleteLabel(){
	var availableTags = [
	
	
      
    ];
    $( "#label" ).autocomplete({
      source: availableTags,
	  select: function(event,ui){
		$( "#label" ).val(ui.item.label);
		$("#label_id").val(ui.item.value);
		return false;
	  }
    });
}*/
</script>