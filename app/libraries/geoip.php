<?php
use GeoIp2\Database\Reader;

class RSC_GeoIp{
	
	public static function getGeo($ip)
	{
		$reader = new Reader('/usr/local/share/GeoIP/GeoLite2-City.mmdb');

		// Replace "city" with the appropriate method for your database, e.g.,
		// "country".
		$record = $reader->city($ip);

		return json_decode(json_encode($record));
	}

}