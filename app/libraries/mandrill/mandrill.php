<?php
require_once 'lib/mandrill/src/Mandrill.php';

class Mandrill_Api{
	
	private $mandrill = null;
	private $api_key = "RnxPNg7KhNmqmvWRKIfrsw";
	
	function __construct(){
		try{
			$this->mandrill = new Mandrill($this->api_key);
		}catch(Mandrill_Error $e){
			// Mandrill errors are thrown as exceptions
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			// A mandrill error occurred: Mandrill_Invalid_Key - Invalid API key
			throw $e;
		}
	}
	
	function addTemplate($opts){
		if((isset($opts['name']) && $opts['name'] != "") && (isset($opts['code']) && $opts['code'] != "")){
			$result = null;
			
			$name = $opts['name'];
			$from_email = isset($opts['from_email'])? $opts['from_email']: "";
			$from_name = isset($opts['from_name'])? $opts['from_name']: "";
			$subject = isset($opts['subject'])? $opts['subject']: "";
			$code = $opts['code'];
			$publish = true;
			
			try{
				$result = $this->mandrill->templates->add($name, $from_email, $from_name, $subject, $code, "", $publish, array());
			}catch(Mandrill_Error $e){
				// Mandrill errors are thrown as exceptions
				echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
				// A mandrill error occurred: Mandrill_Invalid_Key - Invalid API key
				throw $e;
			}
			
			return $result;
		}else{
			die('Parameter name and code must be defined.');
		}
	}
	
	function updateTemplate($opts){
		if((isset($opts['name']) && $opts['name'] != "") && (isset($opts['code']) && $opts['code'] != "")){
			$result = null;
			
			$name = $opts['name'];
			$from_email = isset($opts['from_email'])? $opts['from_email']: "";
			$from_name = isset($opts['from_name'])? $opts['from_name']: "";
			$subject = isset($opts['subject'])? $opts['subject']: "";
			$code = $opts['code'];
			$publish = true;
			
			try{
				$result = $this->mandrill->templates->update($name, $from_email, $from_name, $subject, $code, null, $publish, null);
			}catch(Mandrill_Error $e){
				// Mandrill errors are thrown as exceptions
				echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
				// A mandrill error occurred: Mandrill_Invalid_Key - Invalid API key
				throw $e;
			}
			
			return $result;
		}else{
			die('Parameter name and code must be defined.');
		}
	}
	
	function deleteTemplate($opts){
		if((isset($opts['name']) && $opts['name'] != "")){
			$result = null;
			
			$name = $opts['name'];
			
			try{
				$result = $this->mandrill->templates->delete($name);
			}catch(Mandrill_Error $e){
				// Mandrill errors are thrown as exceptions
				echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
				// A mandrill error occurred: Mandrill_Invalid_Key - Invalid API key
				throw $e;
			}
			
			return $result;
		}else{
			die('Parameter name must be defined.');
		}
	}
	
	/*function sendTemplate($opts){
		if((isset($opts['name']) && $opts['name'] != "") && (isset($opts['to']) && count($opts['to']) > 0)){
			$result = null;
			
			$template_name = $opts['name'];
			$template_content = array(array('name' => 'Jellan Quitos',
									  'content' => 'test content here'));
			$message = array('subject' => $opts['subject'],
							 'from_email' => 'pbradshaw@gmail.com',
							 'from_name' => 'admin',
							 'to' => array(array('email' => $opts['to'],
										   'name' => 'Jellan Quitos to')));
			
			$async = false;
			$ip_pool = 'Main Pool';
			$send_at = isset($opts['send_at'])? $opts['send_at'] : null;
			
			try{
				$result = $this->mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool, $send_at);
			}catch(Mandrill_Error $e){
				// Mandrill errors are thrown as exceptions
				echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
				// A mandrill error occurred: Mandrill_Invalid_Key - Invalid API key
				throw $e;
			}
			
			return $result;
		}else{
			die('Parameter name and message[] must be defined.');
		}
	}*/
	/*
	*$opts = array();
	*key: name, to, user_name, artist_name, contest_title, subject
	*/
	function sendTemplate($opts){
		if((isset($opts['name']) && $opts['name'] != "") && (isset($opts['to']) && count($opts['to']) > 0)){
			$result = null;
			
			$template_name = $opts['name'];
			$user_name = isset($opts['user_name'])? $opts['user_name'] : '' ;
			$artist_name = isset($opts['artist_name'])? $opts['artist_name'] : '' ;
			$contest_title = isset($opts['contest_title'])? $opts['contest_title'] : '' ;
			$contest_link = isset($opts['contest_link'])? $opts['contest_link'] : '' ;
			
			$template_content = array(array('name' => 'user_name', 'content' => $user_name),
									  array('name' => 'artist_name', 'content' => $artist_name),
									  array('name' => 'contest_title', 'content' => $contest_title),
									  array('name' => 'contest_link', 'content' => $contest_link));
			$message = array('subject' => $opts['subject'],
							 'from_email' => 'pbradshaw@gmail.com',
							 'from_name' => 'admin',
							 'to' => array(array('email' => $opts['to'],
												 'name'  => $user_name)));
			
			$async = false;
			$ip_pool = 'Main Pool';
			$send_at = isset($opts['send_at'])? $opts['send_at'] : null;
			
			try{
				$result = $this->mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool, $send_at);
			}catch(Mandrill_Error $e){
				// Mandrill errors are thrown as exceptions
				echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
				// A mandrill error occurred: Mandrill_Invalid_Key - Invalid API key
				throw $e;
			}
			
			return $result;
		}else{
			die('Parameter name and message[] must be defined.');
		}
	}
	
	function cancelJob($id){
		if((isset($id) && $id != "")){
			$result = null;
			
			try{
				$result = $this->mandrill->messages->cancelScheduled($id);
			}catch(Mandrill_Error $e){
				// Mandrill errors are thrown as exceptions
				echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
				// A mandrill error occurred: Mandrill_Invalid_Key - Invalid API key
				throw $e;
			}
			
			return $result;
		}else{
			die('Parameter name must be defined.');
		}
	}
	
	function getJobInfo($id){
		if((isset($id) && $id != "")){
			$result = null;
			
			try{
				$result = $this->mandrill->messages->info($id);
			}catch(Mandrill_Error $e){
				// Mandrill errors are thrown as exceptions
				echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
				// A mandrill error occurred: Mandrill_Invalid_Key - Invalid API key
				throw $e;
			}
			
			return $result;
		}else{
			die('Parameter name must be defined.');
		}
	}
	
	function isJobDone($id){
		if((isset($id) && $id != "")){
			
			$res = $this->getJobInfo($id);
			
			return $res;
		}else{
			die('Parameter name must be defined.');
		}
	}
	
	function toString($x){
		echo '<pre>';
		var_dump($x);
		echo '</pre>';
	}
	
	function getJobId($job){
		return $job[0]['"_id'];
	}
	
}