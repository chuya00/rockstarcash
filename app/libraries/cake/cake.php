<?php

class Cake_Api{
	
	private $domain = 'http://rsc.network-stats.com/';
	private $cake_api_key = 'u0jVLuBLdfozIYc0fxKW5a10cRWBzm';
	private $advertiser_ids = array();
	private $creatives = array();

	public function setAdvertiserIds($ids){
		$this->advertiser_ids = $ids;
	}

	public function getCreatives($advertiser_ids){
		$offers = $this->getOffers($advertiser_ids);
		$res = array();

		foreach($offers as $offer){
			$temp = $this->callCreativesApi($offer->offer_id);
			foreach($temp as $t){
				$res[] = $temp->creative;
			}
		}

		return $res;
	}

	private function callCreativesApi($offer_id){

		$base = $this->domain.'api/3/export.asmx/Creatives?';

		$params = array(
			'api_key' => $this->cake_api_key,
			'creative_id' => 0,
			'creative_name' => '',
			'offer_id' => $offer_id,
			'creative_type_id' => 0,
			'creative_status_id' => 0,
			'start_at_row' => 1,
			'row_limit' => 3,
			'sort_field' => 'creative_id',
			'sort_descending' => 'FALSE'
			);

		$url = $base . http_build_query( $params );
		$result = simplexml_load_file($url);
		
		return $result->creatives;

	}

	public function getOffers($advertiser_ids){
		$res = array();
		$ads = $advertiser_ids;
		for($x = 0; $x < count($ads); $x++){
			$res[] = $this->callOfferApi($ads[$x]);
		}

		return $res;
	}

	private function callOfferApi($advertiser_id){

		$base = $this->domain.'api/5/export.asmx/Offers?';

		$params = array(
			'api_key'         => $this->cake_api_key,
			'offer_id'        => 0,
			'offer_name'      => '',
			'advertiser_id'   => $advertiser_id,
			'vertical_id'     => 0,
			'offer_type_id'   => 0,
			'media_type_id'   => 0,
			'offer_status_id' => 0,
			'tag_id'          => 0,
			'start_at_row'    => 1,
			'row_limit'       => 3,
			'sort_field'      => 'offer_id',
			'sort_descending' => 'FALSE'
			);

		$url = $base . http_build_query( $params );
		$result = simplexml_load_file($url);
		
		return $result->offers;
	}
}