<?php 

class CustomHelper{
	public static function getFbProfilePicture(){
		if (Session::has('fb-id')){
			$response = file_get_contents('http://graph.facebook.com/'.Session::get('fb-id').'/picture?width=180&height=220&redirect=false');
			$array = json_decode($response, true);
			return $array['data']['url'];
		}else{
			return asset('images/default-user.png');
		}
	}

	public static function displayName(){
		if (Session::has('name')){
			return Session::get('name');
		}else{
			return 'Unkown User';
		}
	}

	public static function convertNameToUrlThing($name){
		$res = str_replace(' ', '%20', $name);
		$res = str_replace('(', '%28', $res);
		$res = str_replace(')', '%29', $res);

		return $res;
	}

	public static function getDateToSend($job,$date){
		if($job == 6){
			return 'Immediately';
		}else if(isset($date) && $date != ''){
			$raw = explode(' ',$date);
			return $raw[0];
		}else{
			return '';
		}
	}

	public static function getPlayerDownloadLink($user_id,$player_id){
		$salt = 'wahtdoesthefoxsay';
		$player_id = $player_id;
		$enc = md5($salt.$player_id.$salt);
		return 'http://dev.amplify.fm/wp/download-page/?player='.$enc.'&q='.$user_id;
	}

	public static function getDownloadShortLink($link,$artist=""){
		$postdata = http_build_query(
			array(
				'longurl'       => $link
			)
		);

		$opts = array('http' =>
			array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => $postdata
			)
		);
		
		$context  = stream_context_create($opts);
		
		$html = file_get_contents('http://dev.amplify.fm/r/shorten.php', false, $context);
		return self::editShortlink($artist,$html);
	}

	public static function hasPerformed($job_id){

		$mj = new MandrillJob;
		$count = $mj->where('job_id','=',$job_id)
					->whereNotNull('mandrill_job_id')
					->count();

		/*$query = "SELECT COUNT(job_id) AS count FROM mandrill_job WHERE job_id = $job_id AND mandrill_job_id IS NOT NULL";
		$res = mysqli_query($con,$query);
		$row = mysqli_fetch_array($res);
		$count = $row['count'];*/
		
		if($count > 0){
			return true;
		}
		
		return false;
	}

	public static function isJobDone($job_id){
		
		$mj = new MandrillJob;
		$count = $mj->where('job_id','=',$job_id)
					->count();

		/*$query = "SELECT SUM(send) AS count FROM mandrill_job WHERE job_id = $job_id";
		$res = mysqli_query($con,$query);
		$row = mysqli_fetch_array($res);
		$count = $row['count'];*/
		
		if($count > 0){
			return true;
		}
		
		return false;
	}

	public static function getClientIp() {
	    $ipaddress = '';
	    if (getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
	        $ipaddress = getenv('HTTP_X_FORWARDED');
	    else if(getenv('HTTP_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_FORWARDED_FOR');
	    else if(getenv('HTTP_FORWARDED'))
	       $ipaddress = getenv('HTTP_FORWARDED');
	    else if(getenv('REMOTE_ADDR'))
	        $ipaddress = getenv('REMOTE_ADDR');
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}

	public static function getMediaOnPage($player_id){
		$amplify = new AmplifyPage;

		$res = $amplify->where('user_id','=',Session::get('id'))
					   ->where('player_id','=',$player_id)
					   ->first();

		Log::info($res);
		if(isset($res->source)){
			switch($res->source){
				case 'rsc':
					$title = Audio::find($res->media_id)->first()->title;
					return (object) array('source' => 'RockstarCash Player', 
										  'media_id' => $res->media_id,
										  'title' => $title);
				case 'soundcloud':
					$title = Audio::find($res->media_id)->whereNotNull('source')->first()->title;
					return (object) array('source' => 'SoundCloud', 
										  'media_id' => $res->media_id,
										  'title' => $title);
				case 'youtube':
					$title = Video::find($res->media_id)->first()->title;
					return (object) array('source' => 'youtube', 
										  'media_id' => $res->media_id,
										  'title' => $title);
				case 'vimeo':
					$title = Video::find($res->media_id)->first()->title;
					return (object) array('source' => 'vimeo', 
										  'media_id' => $res->media_id,
										  'title' => $title);
			}
		}else{
			return (object) array('source' => 'RockstarCash Player', 'media_id' => 0);
		}

	}

	public static function limitText($text, $limit) {
      if (str_word_count($text, 0) > $limit) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          $text = substr($text, 0, $pos[$limit]) . '...';
      }
      return $text;
    }

    public static function getShortLink($link){
		$postdata = http_build_query(
			array(
				'longurl'       => $link
			)
		);

		$opts = array('http' =>
			array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => $postdata
			)
		);
		
		$context  = stream_context_create($opts);
		
		$html = file_get_contents('http://amplify.fm/r/shorten.php', false, $context);
		return $html;
	}

	public static function playerIDEnc($player_id){
		$salt = 'wahtdoesthefoxsay';
		return md5($salt.$player_id.$salt);
	}

	public static function getContestLink($player_id,$user_id,$contest_id,$amp_id,$artist){
		$link = 'http://amplify.fm/music/?player='.self::playerIDEnc($player_id).'&q='.$user_id.'&c='.$contest_id.'&u='.$amp_id;

		return self::editShortlink($artist,self::getShortLink($link));
	}

	public static function editShortlink($artist,$sl){
		$artist = strtolower($artist);
		$artist = str_replace(' ', '-', $artist);
		return str_replace('r/','r/'.$artist.'/',$sl);
	}

	public static function getLists($api_key){
		$MailChimp = new MailChimp($api_key);
		$result = $MailChimp->call('lists/list', array());
		
		return $result;
	}

	public static function getPlayerFromUrl($model, $player_url){
		$salt = 'wahtdoesthefoxsay';
		$player_id = $player_url;

		$player_model = $model;
		
		$p = $player_model->get();

		$player_found = 0;
		$player_row = null;
		foreach($p as $p_item){
					
			$p_id = $p_item->id;
			$enc = md5($salt.$p_id.$salt);
			if($enc == $player_id){
				$player_row = $p_item;
				break;
			}
		}
		
		return $player_row;
	}

	public static function toString($obj){
		echo '<pre>';
		var_dump($obj);
		echo '</pre>';
		die;
	}

	public static function parseUrl($site){
		$array = array();
		parse_str(parse_url($site, PHP_URL_QUERY), $array);

		return $array;
	}
}

				
