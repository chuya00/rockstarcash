<?php
/**
 * This example runs a report equal to the "Whole network report" on the DFP
 * website. To download the report see DownloadReportExample.php.
 *
 * Tags: ReportService.runReportJob
 * Tags: ReportService.getReportJob
 *
 * PHP version 5
 *
 * Copyright 2013, Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    GoogleApiAdsDfp
 * @subpackage v201403
 * @category   WebServices
 * @copyright  2013, Google Inc. All Rights Reserved.
 * @license    http://www.apache.org/licenses/LICENSE-2.0 Apache License,
 *             Version 2.0
 * @author     Eric Koleda
 * @author     Paul Rashidi
 */
error_reporting(E_STRICT | E_ALL);

// You can set the include path to src directory or reference
// DfpUser.php directly via require_once.
// $path = '/path/to/dfp_api_php_lib/src';
$path = dirname(__FILE__) . '/../../../../src';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);

require_once 'Google/Api/Ads/Dfp/Lib/DfpUser.php';
require_once 'Google/Api/Ads/Dfp/Util/StatementBuilder.php';
require_once dirname(__FILE__) . '/../../../Common/ExampleUtils.php';

class _InventoryReport{

    private function constructPlayerIdQuery($user_id, $player_ids){
      $res = "(";
      for($x = 0; $x < count($player_ids); $x++){
        if($x == (count($player_ids)-1)){
          $res .= "'TOP_BANNER_".$user_id."_PLAYER_".$player_ids[$x]."',"
         ."'LEFT_BANNER_".$user_id."_PLAYER_".$player_ids[$x]."',"
         ."'LEFT_BANNER_RESP_".$user_id."_PLAYER_".$player_ids[$x]."',"
         ."'BOTTOM_BANNER_".$user_id."_PLAYER_".$player_ids[$x]."'";
        }else{
          $res .= "'TOP_BANNER_".$user_id."_PLAYER_".$player_ids[$x]."',"
         ."'LEFT_BANNER_".$user_id."_PLAYER_".$player_ids[$x]."',"
         ."'LEFT_BANNER_RESP_".$user_id."_PLAYER_".$player_ids[$x]."',"
         ."'BOTTOM_BANNER_".$user_id."_PLAYER_".$player_ids[$x]."',";
        }
        
      }
      $res .= ")";

      return $res;
    }

    public function createInventoryReport($user_id,$player_ids,$start_date="",$end_date=""){

      try {
        // Get DfpUser from credentials in "../auth.ini"
        // relative to the DfpUser.php file's directory.
        $user = new DfpUser();

        // Log SOAP XML request and response.
        $user->LogDefaults();

        // Get the ReportService.
        $reportService = $user->GetService('ReportService', 'v201502');

        // Get the NetworkService.
        $networkService = $user->GetService('NetworkService', 'v201502');

        /* // Get the root ad unit ID to filter on.
        $rootAdUnitId =
            $networkService->getCurrentNetwork()->effectiveRootAdUnitId;

        // Create bind variables.
        $vars = MapUtils::GetMapEntries(array('ancestorAdUnitId' =>
            new NumberValue($rootAdUnitId)));

        // Create statement text to filter based on an order id.
        $filterStatementText =
            'WHERE AD_UNIT_ANCESTOR_AD_UNIT_ID = :ancestorAdUnitId';

        // Create statement object from text.
        $filterStatement = new Statement($filterStatementText, $vars); */

        // Create report job.
        $reportJob = new ReportJob();

        // Create report query.
        $reportQuery = new ReportQuery();
        

        $syear = "";
        $smonth = "";
        $sday = "";  
        $eyear = "";
        $emonth = "";
        $eday = "";  
        if($start_date == ""){
          $syear = date('Y');
          $smonth = date('m');
          $sday = date('d');

          $eyear = date('Y');
          $emonth = date('m');
          $eday = date('d');          
        }else{
          $sdate = explode("-",$start_date);
          $syear = intval($sdate[0]);
          $smonth = intval($sdate[1]);
          $sday = intval($sdate[2]);

          $edate = explode("-",$end_date);
          $eyear = intval($edate[0]);
          $emonth = intval($edate[1]);
          $eday = intval($edate[2]); 
        }
        
        $reportQuery->dateRangeType = 'CUSTOM_DATE';
        $reportQuery->startDate = new Date($syear,$smonth,$sday);
        $reportQuery->endDate = new Date($eyear,$emonth,$eday);

        // Create statement to filter for an order.
        $statementBuilder = new StatementBuilder();
        $in = $this->constructPlayerIdQuery($user_id,$player_ids);
        $statementBuilder->Where("AD_UNIT_NAME IN $in");
        
        $reportQuery->statement = $statementBuilder->ToStatement();//"WHERE adSlotName = 'TOP_BANNER_81_PLAYER_60'";
        $reportQuery->dimensions = array('AD_UNIT_ID','AD_UNIT_NAME','DATE');
        $reportQuery->columns = array( 
            //'ADSENSE_LINE_ITEM_LEVEL_REVENUE'
          //'AD_SERVER_CPM_AND_CPC_REVENUE',
          'TOTAL_INVENTORY_LEVEL_CPM_AND_CPC_REVENUE'
          //'AD_SERVER_CPD_REVENUE',
            //'ADSENSE_LINE_ITEM_LEVEL_TARGETED_IMPRESSIONS',
          //'AD_SERVER_CPA_REVENUE',
            //'AD_SERVER_ALL_REVENUE'
            //'ADSENSE_LINE_ITEM_LEVEL_TARGETED_CLICKS',
            //'ADSENSE_LINE_ITEM_LEVEL_CTR',
            //'ADSENSE_LINE_ITEM_LEVEL_REVENUE',
          //'ADSENSE_LINE_ITEM_LEVEL_AVERAGE_ECPM',
          //'ADSENSE_LINE_ITEM_LEVEL_PERCENT_IMPRESSIONS',
          //'ADSENSE_LINE_ITEM_LEVEL_PERCENT_CLICKS',
          //'ADSENSE_LINE_ITEM_LEVEL_WITHOUT_CPD_PERCENT_REVENUE',
          //'ADSENSE_LINE_ITEM_LEVEL_WITH_CPD_PERCENT_REVENUE'
          );
          /* ,
            'DYNAMIC_ALLOCATION_INVENTORY_LEVEL_IMPRESSIONS',
            'DYNAMIC_ALLOCATION_INVENTORY_LEVEL_CLICKS',
            'TOTAL_INVENTORY_LEVEL_IMPRESSIONS',
            'TOTAL_INVENTORY_LEVEL_CPM_AND_CPC_REVENUE'); */
        //$reportQuery->statement = $filterStatement;
        //$reportQuery->adUnitView = 'HIERARCHICAL';
        $reportJob->reportQuery = $reportQuery;

        // Run report job.
        $reportJob = $reportService->runReportJob($reportJob);
        /*echo '<pre>1';
        var_dump($reportJob);
        echo '</pre>';*/
        do {
          //printf("Report with ID '%s' is running.\n", $reportJob->id);
          sleep(30);
          // Get report job.
          $reportJob = $reportService->getReportJob($reportJob->id);
        } while ($reportJob->reportJobStatus == 'IN_PROGRESS');
        
        if ($reportJob->reportJobStatus == 'FAILED') {
          Log::info("Report job with ID '%s' failed to finish successfully.\n",
              $reportJob->id);
          return false;
        } 

        return $reportJob->id;

        //$reportJob = $reportService->getReportJob($reportJob->id);
        //$reportJob->reportQuery = $reportQuery;
        //echo '<pre>2';
        //var_dump($reportJob);
        //echo '</pre>';
      } catch (OAuth2Exception $e) {
        ExampleUtils::CheckForOAuth2Errors($e);
      } catch (ValidationException $e) {
        ExampleUtils::CheckForOAuth2Errors($e);
      } catch (Exception $e) {
        print $e->getMessage() . "\n";
      }
    }

    public function getReportResult($job_id){
      try {
        // Get DfpUser from credentials in "../auth.ini"
        // relative to the DfpUser.php file's directory.
        $user = new DfpUser();

        // Log SOAP XML request and response.
        $user->LogDefaults();

        // Get the ReportService.
        $reportService = $user->GetService('ReportService', 'v201403');

        // Set the ID of the completed report.
        $reportJobId = $job_id;

        // Set the format of the report (e.g., CSV_DUMP) and download without
        // compression so we can print it.
        $reportDownloadOptions = new ReportDownloadOptions();
        $reportDownloadOptions->exportFormat = 'XML';
        $reportDownloadOptions->useGzipCompression = false;

        $downloadUrl = $reportService->getReportDownloadUrlWithOptions($reportJobId,
            $reportDownloadOptions);
          //var_dump($downloadUrl);

        //printf("Downloading report from URL '%s'.\n", $downloadUrl);
        //echo "Download report here: ";
        //echo "<a href='".$downloadUrl."'><button>Download Report</button></a><br />";
        //$report = ReportUtils::DownloadReport($downloadUrl);
        //var_dump($report);
        $xml = simplexml_load_file($downloadUrl);

        //return $xml->ReportData->DataSet;
        return $xml->ReportData->DataSet;
        //echo '<pre>';
        //echo $xml->ReportData->DataSet->Total->Column[2]->Val;
        //echo '</pre>';
        //printf("\n%s\n", $report);
      } catch (OAuth2Exception $e) {
        ExampleUtils::CheckForOAuth2Errors($e);
      } catch (ValidationException $e) {
        ExampleUtils::CheckForOAuth2Errors($e);
      } catch (Exception $e) {
        printf("%s\n", $e->getMessage());
      }
    }
}