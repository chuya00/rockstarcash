<?php
/**
 * This example creates new ad units under the effective root ad unit. To
 * determine which ad units exist, run GetInventoryTreeExample.php or
 * GetAllAdUnitsExample.php.
 *
 * Tags: NetworkService.getCurrentNework
 * Tags: InventoryService.createAdUnits
 *
 * PHP version 5
 *
 * Copyright 2013, Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package    GoogleApiAdsDfp
 * @subpackage v201403
 * @category   WebServices
 * @copyright  2013, Google Inc. All Rights Reserved.
 * @license    http://www.apache.org/licenses/LICENSE-2.0 Apache License,
 *             Version 2.0
 * @author     Adam Rogal
 * @author     Eric Koleda
 */
//error_reporting(E_STRICT | E_ALL);


$path = dirname(__FILE__) . '/../../../../src';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);

require_once 'Google/Api/Ads/Dfp/Lib/DfpUser.php';
require_once dirname(__FILE__) . '/../../../Common/ExampleUtils.php';

class DfpApi{

	private $ini;

	public function __construct($ini){
		$this->ini = $ini;
		//error_log('1',3,'error.log');
		// You can set the include path to src directory or reference
		// DfpUser.php directly via require_once.
		// $path = '/path/to/dfp_api_php_lib/src';
		
	}

	public function updateAdUnit($name){
		$user = new DfpUser($this->ini);
		$user->LogDefaults();
		$inventoryService = $user->GetService('InventoryService', 'v201502');

		$filterStatement = new Statement("WHERE name IN ('$name')");

		$page = $inventoryService->getAdUnitsByStatement($filterStatement);

		if (isset($page->results)) {
    		$adUnits = $page->results;

    		/*foreach ($adUnits as $adUnit) {
		   		$adUnit->targetPlatform = "ANY";
		   		$adUnit->mobilePlatform = "SITE";
		    	/*echo '<pre>';
		    	var_dump($adUnit);
		    	echo '</pre>';
		    }*/
		    $adUnits[0]->targetPlatform = "MOBILE";
		    //$adUnits[0]->mobilePlatform = "SITE";

		    // Update the ad units on the server.
		    $res = $inventoryService->updateAdUnits($adUnits);
		    echo '<pre>';
		    var_dump($res);
		    echo '</pre>';
		}
	}

	public function createAdunit($user_id,$player_id){
		
	//try {
					$user = new DfpUser($this->ini);
					$user->LogDefaults();
					
					$inventoryService = $user->GetService('InventoryService', 'v201403');
					
					$networkService = $user->GetService('NetworkService', 'v201403');
					$network = $networkService->getCurrentNetwork();
				
					$effectiveRootAdUnitId = $network->effectiveRootAdUnitId;
					
					$adUnits = array();

					//for ($i = 0; $i < 5; $i++) {
						//Top Ad banner Desktop
						$adUnit = new AdUnit();
						$adUnit->name = 'TOP_BANNER_'.$user_id.'_PLAYER_'.$player_id;
						//	$adUnit->adUnitCode = 'TOP_BANNER_'.$user_id.'_PLAYER_'.$player_id;
						$adUnit->parentId = $effectiveRootAdUnitId;
						$adUnit->description = 'Top ad banner for user '.$user_id;
						$adUnit->targetWindow = 'BLANK';

    					// Create ad unit size.
						$adUnitSize = new AdUnitSize();
						//$adUnitSize->sizes = array(new Size(728, 90, FALSE),new Size(320, 50));
						$adUnitSize->size = new Size(728, 90, FALSE);
						$adUnitSize->environmentType = 'BROWSER';

   						 // Set the size of possible creatives that can match this ad unit.
						$adUnit->adUnitSizes = array($adUnitSize);

						$adUnits[] = $adUnit;
						//Log::info('4');
						//Left side banner
						$adUnit = new AdUnit();
						$adUnit->name = 'LEFT_BANNER_'.$user_id.'_PLAYER_'.$player_id;
						//$adUnit->adUnitCode = 'LEFT_BANNER_'.$user_id.'_PLAYER_'.$player_id;
						$adUnit->parentId = $effectiveRootAdUnitId;
						$adUnit->description = 'Left ad banner for user '.$user_id;
						$adUnit->targetWindow = 'BLANK';

    					// Create ad unit size.
						$adUnitSize = new AdUnitSize();
						$adUnitSize->size = new Size(120, 600, FALSE);
						$adUnitSize->environmentType = 'BROWSER';

   						 // Set the size of possible creatives that can match this ad unit.
						$adUnit->adUnitSizes = array($adUnitSize);

						$adUnits[] = $adUnit;
						//Log::info('5');
						//left side responsive ad banner
						$adUnit = new AdUnit();
						$adUnit->name = 'LEFT_BANNER_RESP_'.$user_id.'_PLAYER_'.$player_id;
						//$adUnit->adUnitCode = 'LEFT_BANNER_RESP_'.$user_id.'_PLAYER_'.$player_id;
						$adUnit->parentId = $effectiveRootAdUnitId;
						$adUnit->description = 'Left ad banner responsive for user '.$user_id;
						$adUnit->targetWindow = 'BLANK';

    					// Create ad unit size.
						$adUnitSize = new AdUnitSize();
						$adUnitSize->size = new Size(300, 250, FALSE);
						$adUnitSize->environmentType = 'BROWSER';

   						 // Set the size of possible creatives that can match this ad unit.
						$adUnit->adUnitSizes = array($adUnitSize);

						$adUnits[] = $adUnit;
						//Log::info('6');
						//bottom side banner
						$adUnit = new AdUnit();
						$adUnit->name = 'BOTTOM_BANNER_'.$user_id.'_PLAYER_'.$player_id;
						//$adUnit->adUnitCode = 'BOTTOM_BANNER_'.$user_id.'_PLAYER_'.$player_id;
						$adUnit->parentId = $effectiveRootAdUnitId;
						$adUnit->description = 'Bottom ad banner for user '.$user_id;
						$adUnit->targetWindow = 'BLANK';

    					// Create ad unit size.
						$adUnitSize = new AdUnitSize();
						//$adUnitSize->sizes = array(new Size(728, 90, FALSE),new Size(320, 50));
						$adUnitSize->size = new Size(728, 90, FALSE);
						$adUnitSize->environmentType = 'BROWSER';

   						 // Set the size of possible creatives that can match this ad unit.
						$adUnit->adUnitSizes = array($adUnitSize);

						$adUnits[] = $adUnit;

						/*
						//top mobile ad
						$adUnit = new AdUnit();
						$adUnit->name = uniqid('Ad_Unit_Test');
						$adUnit->parentId = $effectiveRootAdUnitId;
						$adUnit->description = 'Ad unit description.';
						$adUnit->targetWindow = 'BLANK';

    					// Create ad unit size.
						$adUnitSize = new AdUnitSize();
						$adUnitSize->size = new Size(300, 250, FALSE);
						$adUnitSize->environmentType = 'BROWSER';

   						 // Set the size of possible creatives that can match this ad unit.
						$adUnit->adUnitSizes = array($adUnitSize);

						$adUnits[] = $adUnit;

						//bottom mobile ad
						$adUnit = new AdUnit();
						$adUnit->name = uniqid('Ad_Unit_Test');
						$adUnit->parentId = $effectiveRootAdUnitId;
						$adUnit->description = 'Ad unit description.';
						$adUnit->targetWindow = 'BLANK';

    					// Create ad unit size.
						$adUnitSize = new AdUnitSize();
						$adUnitSize->size = new Size(300, 250, FALSE);
						$adUnitSize->environmentType = 'BROWSER';

   						 // Set the size of possible creatives that can match this ad unit.
						$adUnit->adUnitSizes = array($adUnitSize);

						$adUnits[] = $adUnit;*/
					//}
					//echo '<pre>';print_r($adUnits);echo '</pre>';
  					// Create the ad units on the server.
					$adUnits = $inventoryService->createAdUnits($adUnits);
					//Log::info('7');
  					// Display results.
  					$ad_unit_ids = array();
					if (isset($adUnits)) {
						//Log::info('8');
						foreach ($adUnits as $adUnit) {
							$ad_unit_ids[] = $adUnit->id;
							Log::info($adUnit->id.' '.$adUnit->parentId);
						}
						//Log::info('9');
						return $ad_unit_ids;
					} else {
						Log::info("No ad units created.\n");
					}
					return false;
		/*		} catch (OAuth2Exception $e) {
					ExampleUtils::CheckForOAuth2Errors($e);
					//Log::info(ExampleUtils::CheckForOAuth2Errors($e));
				} catch (ValidationException $e) {
					ExampleUtils::CheckForOAuth2Errors($e);
					//Log::info(ExampleUtils::CheckForOAuth2Errors($e));
				} catch (Exception $e) {
					//Log::info($e->getMessage());
				}

				return false;*/
	}

	public function createUserAdvertiser($user_id, $user_type){
		//try {
			
		  // Get DfpUser from credentials in "../auth.ini"
		  // relative to the DfpUser.php file's directory.
		  $user = new DfpUser($this->ini);
		  
		  // Log SOAP XML request and response.
		  $user->LogDefaults();
		  //echo 'sulod1';exit;
		  // Get the CompanyService.
		  $companyService = $user->GetService('CompanyService', 'v201403');
		  //echo 'sulod2';exit;
		  // Create an array to store local company objects.
		  $companies = array();

		  $company = new Company();
		  $company->name = 'RSC-'.$user_type.'-'. $user_id;
		  $company->type = 'ADVERTISER';

		  $companies[] = $company;

		  // Create the companies on the server.
		  $companies = $companyService->createCompanies($companies);

		  $advertiser_id = 0;
		  // Display results.
		  if (isset($companies)) {
		    foreach ($companies as $company) {
		    	$advertiser_id = $company->id;
				Log::info('Company id: '.$company->id);
		    }

		    return $advertiser_id;
		  } else {
		    print "No companies created.\n";
		  }
		  return false;
		/*} catch (OAuth2Exception $e) {
			echo '<pre>1';
			var_dump($e);
			echo '</pre>';
		  ExampleUtils::CheckForOAuth2Errors($e);
		} catch (ValidationException $e) {
			echo '<pre>2';
			var_dump($e);
			echo '</pre>';
		  ExampleUtils::CheckForOAuth2Errors($e);
		} catch (Exception $e) {
			echo '<pre>3';
			var_dump($e);
			echo '</pre>';
		  print $e->getMessage() . "\n";
		}*/
	}

	public function getAllOrders(){
		$user = new DfpUser($this->ini);

		$user->LogDefaults();

	    // Get the OrderService.
	    $orderService = $user->GetService('OrderService', 'v201403');

	    // Set defaults for page and statement.
	    $page = new OrderPage();
	    $filterStatement = new Statement();
	    $offset = 0;

	    $orders = array();

	    do {
	    // Create a statement to get all orders.
	    $filterStatement->query = 'LIMIT 500 OFFSET ' . $offset;
	    //echo 'wa';
	    // Get orders by statement.
	    $page = $orderService->getOrdersByStatement($filterStatement);
	    //var_dump($page);
	    // Display results.
	    if (isset($page->results)) {
	      $i = $page->startIndex;
	      foreach ($page->results as $order) {
	        $orders[] = array('id'            => $order->id,
	        				  'name'          => $order->name,
	        				  'advertiser_id' => $order->advertiserId);
	        $i++;
	      }
	    }

	    $offset += 500;
	    } while ($offset < $page->totalResultSetSize);

	  	return $orders;
	}
}