<?php

include('config.php');

class DB{

	private $conn;

	public __construct(){
		$this->$conn = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
		if (mysqli_connect_errno()) {
		    printf("Connect failed: %s\n", mysqli_connect_error());
		    exit();
		}
	}

	public function insert($log, $user_id, $date, $time, $timestamp, $city, $country, $country_code){
		$table = $this->getTable($log);
		$sql = "INSERT INTO $table 
				(user_id, city, country, country_code, date, time, timestamp)
				VALUE
				(?, ?, ?, ?, ?, ?, ?)";

		$stmt = $this->conn->prepare($sql);
		$stmt->bind_param('ss', $user_id, $city, $country, $country_code, $date, $time, $timestamp);

		return $stmt->execute;
	}

	public function select($log, $user_id, $params = array()){
		$table = $this->getTable($log);
		$and = $this->getAnd($params);
		$sql = "SELECT COUNT(id) AS count FROM $table
				WHERE user_id = ?
				$and";

		$stmt = $this->conn->prepare($sql);
		$stmt->bind_param('ss', $user_id);		
		$stmt->execute;
		
		return $stmt->get_result();
	}

	private function getTable($log){
		switch($log){
			case 'view':
				return LOG;

			case 'share':
				return SHARE;

			case 'click':
				return CLICK;

			default:
				return false;
		}	
	}

	private function getAnd($params){
		$and = "";
		foreach($params as $key => $value){
			$and .= " AND $key = $value ";
		}

		return $and;
	}

}