<?php 

include('db.php');
include('geoip.php');

class Tracker{

	//RSC user id
	private $user_id;

	//DB class
	private $conn;

	public __construct($user_id){
		$this->$conn = new DB();
		$this->user_id = $user_id;
	}

	public function logView(){
		return $conn->insert('view', $this->user_id, $date, $time, $timestamp, $city, $country, $country_code);
	}

	public function logShare(){
		return $conn->insert('share', $this->user_id, $date, $time, $timestamp, $city, $country, $country_code);
	}

	public function logClick(){
		return $conn->insert('click', $this->user_id, $date, $time, $timestamp, $city, $country, $country_code);
	}

	//$params = array(date, time, timestamp, city, country, country_code)
	public function getViews($params = array()){
		$res = $conn->select('view', $this->user_id, $params);

		return $res->fetch_object();
	}

	//$params = array(date, time, timestamp, city, country, country_code)
	public function getShares($params = array()){
		$res = $conn->select('share', $this->user_id, $params);

		return $res->fetch_object();
	}

	//$params = array(date, time, timestamp, city, country, country_code)
	public function getClicks($params = array()){
		$res = $conn->select('click', $this->user_id, $params);

		return $res->fetch_object();
	}

	private function restrictions(){

	}

}