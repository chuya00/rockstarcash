<?php

class Navigation{
	
	public static function getMenu(){

		return array(
		    'items' => array(
		    				array(
		    					'href'  => URL::to('admin'),
		    					'icon'  => 'fa-home',
		    					'title' => 'Dashboard',
		    				),/*
		    				array(
		    					'href'    => '#',
		    					'icon'    => 'fa-file-text-o',
		    					'title'   => 'Stats',
		    					'subMenu' => array(
		    									array(
			    									'href'  => URL::to('admin/stats'),
			    									'icon'  => '',
			    									'title' => 'Stats'
		    									),
		    								    array(
			    								 	'href' => URL::to('admin/stats/songs'),
			    								 	'icon'  => '',
			    									'title' => 'Songs'
		    								 	),
		    								    array(
			    								 	'href' => URL::to('admin/stats/referrers'),
			    								 	'icon'  => '',
			    									'title' => 'Referrers'
		    								 	),
		    								    array(
			    								 	'href' => URL::to('admin/stats/tickets'),
			    								 	'icon'  => '',
			    									'title' => 'Tickets'
		    								 	),
		    								    array(
			    								 	'href' => URL::to('admin/stats/geo'),
			    								 	'icon'  => '',
			    									'title' => 'Geo Reports'
		    								 	)
		    								 )
		    				),*/
							array(
		    					'href'    => URL::to('admin/contest'),
		    					'icon'    => 'fa-file-text-o',
		    					'title'   => 'Manage Contests',
		    					'subMenu' => array(
		    								    array(
			    								 	'href' => URL::to('admin/contest'),
			    								 	'icon'  => '',
			    									'title' => 'Manage Contests'
		    								 	),
		    								 	array(
			    								 	'href' => URL::to('admin/contest/stats'),
			    								 	'icon'  => '',
			    									'title' => 'Contest Stats'
		    								 	)
		    								 )
		    				),
		    				array(
		    					'href'  => '#',
		    					'icon'  => 'fa-envelope-o',
		    					'title' => 'Emails',
		    					'subMenu' => array(
		    									/*array(
			    									'href'  => URL::to('admin/email/inbox'),
			    									'icon'  => '',
			    									'title' => 'Inbox'
		    									),*/
		    								    array(
			    								 	'href' => URL::to('admin/email/templates'),
			    								 	'icon'  => '',
			    									'title' => 'Email Templates'
		    								 	),
		    								    array(
			    								 	'href' => URL::to('admin/email/jobs'),
			    								 	'icon'  => '',
			    									'title' => 'Email Jobs'
		    								 	),
		    								 	array(
			    								 	'href' => URL::to('admin/email/list'),
			    								 	'icon'  => '',
			    									'title' => 'Email List'
		    								 	),
		    								 )
		    				),
		    				array(
		    					'href'  => '#',
		    					'icon'  => 'fa-rocket',
		    					'title' => 'Audio',
		    					'subMenu' => array(
		    									array(
			    									'href'  => URL::to('admin/media/audio-upload'),
			    									'icon'  => '',
			    									'title' => 'Upload Audio File'
		    									),
		    								    array(
			    								 	'href' => 'admin/media/add-audio',
			    								 	'icon'  => '',
			    									'title' => 'Add 3rd Party Audio'
		    								 	),
		    								    array(
			    								 	'href' => URL::to('admin/media/audio-players'),
			    								 	'icon'  => '',
			    									'title' => 'Audio Players'
		    								 	),
		    								 )
		    				),
		    				array(
		    					'href'  => '#',
		    					'icon'  => 'fa-rocket',
		    					'title' => 'Video',
		    					'subMenu' => array(
		    									array(
			    									'href'  => URL::to('admin/video/'),
			    									'icon'  => '',
			    									'title' => 'Add Video'
		    									),
		    								    
		    								 )
		    				),
		    				array(
		    					'href'    => URL::to('admin/manage-amplify-page'),
		    					'icon'    => 'fa-file-text-o',
		    					'title'   => 'Manage Amplify Page',
		    				),
		                    array(
		                        'href'  => URL::to('admin/logout'),
		                        'icon'  => 'fa-power-off',
		                        'title' => 'Logout',
		                    ),
		    			),
		);
	}

}