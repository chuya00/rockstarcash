<?php
class StatReports{

	//private $user_id = 42;//Session::get('id');
	private $domain = 'http://rsc.network-stats.com/';
	private $cake_api_key = 'u0jVLuBLdfozIYc0fxKW5a10cRWBzm';

	function getSites($user_id){
		$player_log = new PlayerLog;
		$player = new Player;

		$res_player = $player->where('user_id','=',$user_id)->select('id')->get();
		$res = $player_log->whereIn('player_id',$res_player)->distinct('site')->select('site','player_id')->get();

		/*$sql = "SELECT DISTINCT(site), player_id FROM player_log 
				WHERE player_id IN (SELECT id FROM player WHERE user_id = $user_id)";*/
		//echo $sql;
		//$res = mysqli_query($con,$sql);
		return $res;
	}

	function getPlayer($user_id){
		//$sql = "SELECT id, audios, offer_file_id, artist_name, album_name FROM player WHERE user_id = $user_id";
		//echo $sql;
		//$res = mysqli_query($con,$sql);
		$player = new Player;
		$res = $player->where('user_id','=',$user_id)->select('id', 'audios', 'offer_file_id', 'artist_name', 'album_name')->get();
		return $res;
	}

	function getSinglePlayer($id){
		/*$sql = "SELECT id, audios, offer_file_id, artist_name, album_name FROM player WHERE id = $id LIMIT 1";
		//echo $sql;
		$res = mysqli_query($con,$sql);
		$row = mysqli_fetch_array($res);*/
		$player = new Player;
		$res = $player->where('id','=',$id)->select('id', 'audios', 'offer_file_id', 'artist_name', 'album_name')->first();
		return $res;
	}

	function setTopSitesTable($sites,$user_type){
		/*while($row = mysqli_fetch_array($res)){
			$offer_file_id = getSinglePlayer($con, $row['player_id']);
			$hasoffer = getStats($offer_file_id['offer_file_id'],$_SESSION['u_type']);
			//print_r($hasoffer);
			$player_plays_count = getPlayerPlaysCountPerSite($con,$row['player_id'],$row['site']);
			
			echo '<tr class="gradeX">';
			echo '<td>'.$row['site'].'</td>';
			echo '<td>'.$hasoffer[0]->Stat->impressions.'</td>';
			echo '<td>'.$player_plays_count.'</td>';
			echo '<td>'.round($hasoffer[0]->Stat->revenue,2).'</td>';
			echo '</tr>';
		}*/

		foreach($sites as $s){
			$offer_file_id = $this->getSinglePlayer($s->player_id);
			$hasoffer = $this->getStats($offer_file_id->offer_file_id,$user_type);
			$player_plays_count = $this->getPlayerPlaysCountPerSite($s->player_id,$s->site);

			echo '<tr class="gradeX">';
			echo '<td>'.$s->site.'</td>';
			echo '<td>'.$hasoffer[0]->Stat->impressions.'</td>';
			echo '<td>'.$player_plays_count.'</td>';
			echo '<td>'.round($hasoffer[0]->Stat->revenue,2).'</td>';
			echo '</tr>';
		}
	}

	function getAudioDetails($id){
		//$sql = "SELECT id, title FROM audio WHERE id = $id LIMIT 1";
		//echo $sql;
		$audio = new Audio;
		$row = $audio->where('id','=',$id)->first();

		return $row;
	}

	function getPlayerDownloads($id,$date = ""){
		/*$sql = "SELECT COUNT(id) AS count FROM download_log 
				WHERE player_id = $id
				AND date = $date";*/

		$download_log = new DownloadLog;
		$count = $download_log->where('player_id','=',$id)
							  ->where('date','=',$date)
							  ->count();

		if($date == ""){
			$count = $download_log->where('player_id','=',$id)
							  	  ->count();
		}
		
		return $count;
	}

	function setTopSongsTable($row,$user_type){
		/*while($row = mysqli_fetch_array($res)){
			$hasoffer = getStats($row['offer_file_id'],$_SESSION['u_type']);
			//print_r($hasoffer);
			$player_plays_count = getPlayerPlaysCount($con,$row['id']);
			$song_title = getAudioDetails($con,$row['audios']);
			$downloads = getPlayerDownloads($con,$row['id']);
			echo '<tr class="gradeX">';
			echo '<td>'.$row['id'].'</td>';
			echo '<td>'.$song_title['title'].'</td>';
			echo '<td>'.$player_plays_count.'</td>';
			echo '<td>'.$downloads.'</td>';	
			echo '<td>'.round($hasoffer[0]->Stat->revenue,2).'</td>';
			echo '<td>'.round($hasoffer[0]->Stat->revenue,2).'</td>';
			echo '</tr>';
		}*/

		foreach($row as $r){
			$reports = $this->getStats();
			$player_plays_count = $this->getPlayerPlaysCount($r->id);
			$song_title = $this->getAudioDetails($r->audios);
			$downloads = $this->getPlayerDownloads($r->id);

			echo '<tr class="gradeX">';
			echo '<td>'.$r->id.'</td>';
			echo '<td>'.$song_title->title.'</td>';
			echo '<td>'.$player_plays_count.'</td>';
			echo '<td>'.$downloads.'</td>';	
			//echo '<td>'.round($hasoffer[0]->Stat->revenue,2).'</td>';
			//echo '<td>'.round($hasoffer[0]->Stat->revenue,2).'</td>';
			echo '<td>'.date('Y-m-d', time() + (60 * 60 * 24 * -30) ).'</td>';
			echo '<td></td>';
			echo '</tr>';
		}
	}

	function setUpTable($res,$user_type){
		/*while($row = mysqli_fetch_array($res)){
			$hasoffer = getStats($row['offer_file_id'],$_SESSION['u_type']);
			//print_r($hasoffer);
			$player_plays_count = getPlayerPlaysCount($con,$row['id']);
			echo '<tr>';
			echo '<td>Player: '.$row['album_name'].' ('.$row['artist_name'].')</td>';
			echo '<td>'.$hasoffer->Stat->impressions.'</td>';
			if($player_plays_count <= 0){
				echo '<td>'.$player_plays_count.'</td>';
			}else{
				echo '<td><a href="javascript:void()" player-id="'.$row['id'].'" class="player-count" >'.$player_plays_count.'</a></td>';
			}		
			echo '<td>'.$hasoffer->Stat->clicks.'</td>';
			echo '<td>'.$hasoffer->Stat->conversions.'</td>';
			echo '</tr>';
		}*/

		foreach($res as $row){
			$hasoffer = $this->getStats($row->offer_file_id,$user_type);
			$player_plays_count = $this->getPlayerPlaysCount($row->id);
			echo '<tr>';
			echo '<td>Player: '.$row->album_name.' ('.$row->artist_name.')</td>';
			echo '<td>'.$hasoffer->Stat->impressions.'</td>';
			if($player_plays_count <= 0){
				echo '<td>'.$player_plays_count.'</td>';
			}else{
				echo '<td><a href="javascript:void()" player-id="'.$row->id.'" class="player-count" >'.$player_plays_count.'</a></td>';
			}		
			echo '<td>'.$hasoffer->Stat->clicks.'</td>';
			echo '<td>'.$hasoffer->Stat->conversions.'</td>';
			echo '</tr>';
		}
	}

	function setUpChartDataMusician($user_id){
		$clicks = 0;
		$impressions = 0;
		$conversions = 0;
		$revenue = 0;
		$date = "";
		$data = array();
		//while($row = mysqli_fetch_array($res)){
		$hasoffer = $this->getStats($user_id,'offer');
		$count = count($hasoffer);
		for($x = 0; $x < $count; $x++){
			$clicks += intval($hasoffer[$x]->Stat->clicks);
			$impressions += intval($hasoffer[$x]->Stat->impressions);
			$conversions += intval($hasoffer[$x]->Stat->conversions);
			$revenue += intval($hasoffer[$x]->Stat->revenue);
			$date = $hasoffer[$x]->Stat->date;
			$temp = array("clicks" => $clicks,
						  "impressions" => $impressions,
						  "conversions" => $conversions,
						  "revenue" => $revenue,
						  "date" => $date);
			array_push($data,$temp);
		}
			
		$js = "";
		$res = array();
		$clicks_temp = "";
		$imp_temp = "";
		$conv_temp = "";
		$rev_temp = "";
		for($x = 0; $x < count($data); $x++){
			if($x == 0 && (count($data) == 1)){
				$clicks_temp .= "['".$data[$x]['date']."','".$data[$x]['clicks']."']";
				$imp_temp .= "['".$data[$x]['date']."','".$data[$x]['impressions']."']";
				$conv_temp .= "['".$data[$x]['date']."','".$data[$x]['conversions']."']";
				$rev_temp .= "['".$data[$x]['date']."','".$data[$x]['revenue']."']";
			}else if($x == 0){
				$clicks_temp .= "['".$data[$x]['date']."','".$data[$x]['clicks']."'],";
				$imp_temp .= "['".$data[$x]['date']."','".$data[$x]['impressions']."'],";
				$conv_temp .= "['".$data[$x]['date']."','".$data[$x]['conversions']."'],";
				$rev_temp .= "['".$data[$x]['date']."','".$data[$x]['revenue']."'],";
			}else if($x == (count($data) - 1)){
				$clicks_temp .= "['".$data[$x]['date']."','".$data[$x]['clicks']."']";
				$imp_temp .= "['".$data[$x]['date']."','".$data[$x]['impressions']."']";
				$conv_temp .= "['".$data[$x]['date']."','".$data[$x]['conversions']."']";
				$rev_temp .= "['".$data[$x]['date']."','".$data[$x]['revenue']."']";
			}else{
				$clicks_temp .= "['".$data[$x]['date']."','".$data[$x]['clicks']."'],";
				$imp_temp .= "['".$data[$x]['date']."','".$data[$x]['impressions']."'],";
				$conv_temp .= "['".$data[$x]['date']."','".$data[$x]['conversions']."'],";
				$rev_temp .= "['".$data[$x]['date']."','".$data[$x]['revenue']."'],";
			}		
		}
		
		return array("Clicks" => $clicks_temp, "Impressions" => $imp_temp, "Conversions" => $conv_temp, "Revenue" => $rev_temp);
	}

	function setUpTableOther($user_id,$user_type,$user_name){
		//while($row = mysqli_fetch_array($res)){
			$hasoffer = $this->getStats($user_id,$user_type);
			//print_r($hasoffer);
			//$player_plays_count = getPlayerPlaysCount($con,$row['id']);
			echo '<tr>';
			echo '<td>'.$user_name.'</td>';
			echo '<td>'.$hasoffer->Stat->impressions.'</td>';
			echo '<td>'.$hasoffer->Stat->clicks.'</td>';
			echo '<td>'.$hasoffer->Stat->conversions.'</td>';
			echo '</tr>';
		//}
	}

	function getPlayerPlaysCount($id,$date = ""){
		//$sql = "SELECT COUNT(id) AS count FROM player_log WHERE player_id = $id AND date = $date";
		$player_log = new PlayerLog;
		$count = $player_log->where('player_id','=',$id)
							->where('date','=',$date)
							->count();
		if($date == ""){
			//$sql = "SELECT COUNT(id) AS count FROM player_log WHERE player_id = $id";
			$count = $player_log->where('player_id','=',$id)
								->count();
		}
		
		return $count;
	}

	function getPlayerPlaysCountPerSite($id,$site,$date = ""){
		//$sql = "SELECT COUNT(id) AS count FROM player_log WHERE player_id = $id AND date = $date AND site = '$site'";
		$player_log = new PlayerLog;
		$count = $player_log->where('player_id','=',$id)
							->where('date','=',$date)
							->where('site','=',$site)->count();

		if($date == ""){
			//$sql = "SELECT COUNT(id) AS count FROM player_log WHERE player_id = $id  AND site = '$site'";
			$count = $player_log->where('player_id','=',$id)
								->where('site','=',$site)->count();
		}
		
		return $count;
	}

	function getPlayerPlaysCountAll($user_id,$date = ""){
		$player = new Player;
		$player_log = new PlayerLog;

		$players =  $player->where('user_id','=',$user_id)->select('id')->get();

		$count = $player_log->where('date','=',$date)
							->whereIn('player_id',$players)							
							->count();

		/*$sql = "SELECT COUNT(id) AS count FROM player_log 
				WHERE player_id IN (SELECT id FROM player WHERE user_id = $user_id)
				AND date = $date";*/
		if($date == ""){
			/*$sql = "SELECT COUNT(id) AS count FROM player_log 
					WHERE player_id IN (SELECT id FROM player WHERE user_id = $user_id)";*/
			$count = $player_log->whereIn('player_id',$players)							
								->count();
		}
		
		return $count;
	}

	function getPlayerDownloadsAll($user_id,$date = ""){
		/*$sql = "SELECT COUNT(id) AS count FROM download_log 
				WHERE player_id IN (SELECT id FROM player WHERE user_id = $user_id)
				AND date = $date";*/
		$download_log = new DownloadLog;
		$player = new Player;

		$players = $player->where('user_id',$user_id)->select('id')->get();
		$count = $download_log->whereIn('player_id',$players)
							  ->where('date','=',$date)
							  ->count();
		if($date == ""){
			/*$sql = "SELECT COUNT(id) AS count FROM download_log 
					WHERE player_id IN (SELECT id FROM player WHERE user_id = $user_id)";*/

			$count = $download_log->whereIn('player_id',$players)->count();
		}
		
		return $count;
	}

	function getRatios($p,$d){
		$p_ratio = ($p / ($p + $d)) * 100;
		$d_ratio = ($d / ($p + $d)) * 100;
		$c_ratio = $d_ratio;
		
		return array("player_ratio" => round($p_ratio, 2),
					 "download_ratio" => round($d_ratio, 2),
					 "conversion_ratio" => round($c_ratio, 2));
	}
	/*
	function calculateDownloadRatio($count){
		$user_id = $_SESSION['id'];
		$sql = "SELECT COUNT(id) AS count FROM player_log 
				WHERE player_id IN (SELECT id FROM player WHERE user_id = $user_id)
				AND date = $date";
		if($date == ""){
			$sql = "SELECT COUNT(id) AS count FROM download_log 
					WHERE player_id IN (SELECT id FROM player WHERE user_id = $user_id)";
		}
		//echo $sql;
		$res = mysqli_query($con,$sql);
		$row = mysqli_fetch_array($res);
		
		return (intval($count) / (intval($count) + intval($row['count']))) * 100;
	}*/

	function getStats($start_date = "",$end_date = ""){
		/*$stat_attr = 0;
		switch($user){
			case 'artist':
				$stat_attr = 'Stat.offer_file_id';
				break;
			
			case 'label':
				$stat_attr = 'Stat.advertiser_id';
				break;
			
			case 'offer':
				$stat_attr = 'Stat.offer_id';
				break;
				
			default:
				$stat_attr = 'Stat.affiliate_id';
		}
		
		$base = 'https://api.hasoffers.com/Api?';

		$params = array(
			'Format' => 'json'
			,'Target' => 'Report'
			,'Method' => 'getStats'
			,'Service' => 'HasOffers'
			,'Version' => 2
			,'NetworkId' => 'rockstarcashllc'
			,'NetworkToken' => 'NET6OIPi0DblLSXgrGv3T5p2eILGKK'
			,'fields' => array(
				'Stat.impressions'
				,'Stat.clicks'
				//,'Stat.unique_clicks'
				//,'Stat.gross_clicks'
				//,'Stat.ctr'
				,'Stat.conversions'
				//,'Stat.ltr'
				//,'Stat.payout'
				//,'Stat.cpm'
				//,'Stat.cpc'
				//,'Stat.cpa'
				,'Stat.revenue'
				//,'Stat.rpm'
				//,'Stat.rpc'
				//,'Stat.rpa'
				//,'Stat.sale_amount'
				//,'Stat.profit'
				,'Country.name'
				,'Stat.date'
			)
			,'filters' => array(
				$stat_attr => array (
					'conditional' => 'EQUAL_TO'
					,'values' => array(
						$id
					)
				)
			)
			,'sort' => array(
				'Stat.date' => 'desc'
				,'Stat.revenue' => 'desc'
			)
			,'limit' => 100
			,'page' => 1
		);

		$url = $base . http_build_query( $params );

		$result = file_get_contents( $url );
		$data = json_decode( $result );
		/*
		echo '<pre>';
		print_r( json_decode( $result ) );
		echo '</pre>';*/
		$base = $this->domain.'api/2/reports.asmx/CreativeSummary?';

		if($start_date == ""){
			$start_date = date('Y-m-d');
		}
		if($end_date == ""){
			$end_date = date('Y-m-d');
		}

		$params = array(
			'api_key' => $this->cake_api_key,
			'start_date' => $start_date,
			'end_date' => $end_date,
			'offer_id' => Session::get('offer_id'),
			'campaign_id' => 0,
			'event_id' => 0,
			'revenue_filter' => 'conversions'
			);

		$url = $base . http_build_query( $params );
		$reports = simplexml_load_file($url);
		
		return $reports;
	}

	function getDfpReports(){
		$reportId = 0;
		$xml_results = null;
		require_once '../dfp/examples/Dfp/v201403/ReportService/RunInventoryReport.php';
		
		return $xml_results;
	}

	function getDfpData(){
		$res = array();
		
		
	}

	function getStatReports(){
		$reports = array();
		$start_date = date('Y-m-d');

		//Daily
		$reports['daily'] = $this->getStats();

		//Weekly
		$end_date = date('Y-m-d', time() + (60 * 60 * 24 * -7) );
		$reports['weekly'] = $this->getStats($start_date,$end_date);

		//Monthly
		$end_date = date('Y-m-d', time() + (60 * 60 * 24 * -30) );
		$reports['monthly'] = $this->getStats($start_date,$end_date);

		Cache::put('reports', $reports, 10);

		return $reports;
	}
}