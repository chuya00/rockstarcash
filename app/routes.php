<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//RSC ADMIN
Route::get('admin/login/', array('as' => 'login', 'uses' => 'AdminHomeController@viewLogin'));
Route::post('admin/login','AdminHomeController@login');
Route::get('admin/forgot-password/', array('uses' => 'AdminHomeController@showForgotPassword'));
Route::resource('geoip', 'GeoIPController');

Route::group(['before' => 'auth', 'prefix' => 'admin'],function(){
	Route::get('/', 'AdminHomeController@index');

	Route::post('/get-stat','AdminHomeController@getStat');

	Route::post('/get-report','AdminHomeController@getReports');

	Route::post('/get-source-report','AdminHomeController@getReports');

	Route::post('/get-init-report','AdminHomeController@getInitReports');

	Route::get('/user','ProfileController@index');

	Route::group(['prefix' => 'media'],function(){
		
		Route::get('audio-upload', 'MediaController@showAudioUploadPage');

		//Route::get('media/video-upload', array('before' => 'auth', 'uses' => 'MediaController@showVideoUploadPage'));

		Route::get('audio-players', 'MediaController@showAudioPlayersPage');	

		Route::post('save-audio','MediaController@saveAudio');

		Route::post('delete-audio','MediaController@deleteAudio');

		Route::post('audio-players/add-audio','MediaController@addAudio');

		Route::post('audio-players/add-saved-player-details','MediaController@addSavedPlayerDetails');

		Route::post('audio-players/save-audio-player','MediaController@saveAudioPlayer');

		Route::post('audio-players/save-album-img','MediaController@saveAlbumImage');

		Route::post('audio-players/add-player-details','MediaController@addPlayerDetails');

		Route::get('audio-players/embed-preview','MediaController@showEmbedPlayerPreviewPage');	

		Route::post('audio-players/set-sessions-embed','MediaController@setSessionsEmbed');

		Route::post('audio-players/add-saved-audio','MediaController@addSavedAudio');

		Route::get('add-audio', 'MediaController@showAddAudioPage');

		Route::post('save-third', 'MediaController@saveThirdPartyAudio');
	});

	Route::group(['prefix' => 'email'],function(){

		Route::get('templates','EmailController@showTemplatesPage');

		Route::get('templates/create/{from_contest?}','EmailController@showCreateTemplatesPage');

		Route::post('templates/create/download','EmailController@doTemplateDownload');

		Route::get('jobs','EmailController@showEmailJobsPage');

		Route::post('jobs/ajax-job-list','EmailController@renderJobList');

		Route::get('jobs/create','EmailController@showCreateEmailJobs');

		Route::post('jobs/save','EmailController@handleSaveEmailJob');

		Route::post('jobs/perform','EmailController@performJob');

		Route::get('list','EmailController@showEmailList');
	});	

	Route::group(['prefix' => 'video'],function(){

		Route::get('/','VideoController@index');

		Route::post('save','VideoController@save');

	});	

	Route::group(['prefix' => 'manage-amplify-page'],function(){

		Route::get('/','AmplifyController@manage');

		Route::post('save','AmplifyController@save');

		Route::post('get-media','AmplifyController@getMedia');

	});

	Route::group(['prefix' => 'contest'],function(){

		Route::get('/','ContestController@index');

		Route::post('/save','ContestController@save');

		Route::get('/stats','ContestStatController@index');

		Route::post('/save-album-img','ContestController@saveAlbumImage');

		Route::post('/get-contest','ContestController@getContestDetails');

		Route::post('/update','ContestController@update');

		Route::post('/get-mc-lists','ContestController@getMCLists');
	});

	Route::get('logout','AdminHomeController@logout');	

	Route::get('post','PostController@showPostPage');

	Route::post('post/save-post','PostController@savePost');

});

//RSC home
Route::get('/','HomeController@index');

Route::get('/captcha/','HomeController@captcha');

Route::post('/contact-us','HomeController@contactUs');

Route::get('/thank-you','HomeController@thankyou');

Route::get('/beta','HomeController@beta');

Route::get('/sign-up','RegisterController@showSignUp');

Route::post('/register-forms','RegisterController@index');

Route::post('/process','RegisterController@process');

Route::get('/test','TestController@index');

//Error pages
App::missing(function($exception)
{
    return Response::view('error.404', array(), 404);
});

//Rockstarcash DFP ad tracker
Route::get('/dfp','LogController@trackDFP');
Route::get('/c','LogController@logConversion');

//RSC Resource Controllers
Route::resource('download', 'DownloadController');
Route::resource('owner', 'AudioOwnerController');

//RSC superadmin
Route::group(['before' => 'auth|superadmin', 'prefix' => 'super-admin'],function(){
	Route::get('/','SuperAdminController@index');
	Route::get('/orders','SuperAdminController@showOrdersView');
	Route::post('/save-order','SuperAdminController@saveOrder');
	Route::get('/email-templates','EmailTemplatesManagerController@index');
});
