<?php
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;
use Facebook\FacebookRedirectLoginHelper;
class RegisterController extends \BaseController {

	private $facebook = null;
	private $ApiDomain = "http://rsc.network-stats.com";
	private $cake_api_key = "u0jVLuBLdfozIYc0fxKW5a10cRWBzm";

	public function __construct(){
		$this->facebook = new Facebook(array(
			'appId'  => Config::get('facebook.appId'),
			'secret' => Config::get('facebook.secret'),
			'allowSignedRequest' => false
			));
	}

	public function index()
	{
		$u_type = Input::get('u');
		$user_profile = Session::get('fb-user-profile');
		
		$user = "";
		$form = null;
		switch($u_type){
			case "artist":
			$user = "Musician";
			echo $this->renderRegFormView($user_profile, $u_type, $user);
			break;
			case "label":
			$user = "Label";
			echo $this->renderRegFormView($user_profile, $u_type, $user);
			break;
			case "site_owner":
			$user = "Blogger";
			echo $this->renderRegFormView($user_profile, $u_type, $user);
			break;
			case "developer":
			$user = "Developer";
			echo $this->renderRegFormView($user_profile, $u_type, $user);
			break;
		}
		/*$inline_js = View::make('register_js.register_js', array('labels' => $this->getLabels()));
		return View::make('register', array('form' => $form, 
											'user' => $user, 
											//'css' => $css, 
											'inline_js' => $inline_js, 
											'exclude_nagging_js' => 1,
											//'test' => $test
											));
											*/
}

	private function renderRegFormView($user_profile, $u_type, $user, $other_var = array()){
		Session::put('user-type', $u_type);
		$defaults = array('user_profile' => $user_profile, 'user' => $user);
		$merged = array_merge($defaults,$other_var);
		$view = View::make('register_forms.artist', $merged);
		Session::put('reg-form-user-type', $u_type);
		Session::put('reg-form-user', $u_type);
		return $view;
	}

	private function getLabels(){
		$labels = User::where('user_type_id','=','label');
		$l_count = User::where('user_type_id','=','label')->get();
		$count = 0;
		$js = '';

		foreach($labels as $label){
				if($l_count == 1){ //1 result only
					$js .= '{"label":"'.$label->name.'","value":"'.$label->id.'"}';
					break;
				}else if($count == $l_count - 1){ //last item
					$js .= '{"label":"'.$label->name.'","value":"'.$label->id.'"}';
				}else{
					$js .= '{"label":"'.$label->name.'","value":"'.$label->id.'"},';
				}
				$count++;
			}

			return $js;
	}

	public function showSignUp(){
		$code = Input::get( 'code' );
		//$test = (Session::has('errors'))? Session::get('errors') : '';
	    // get fb service

		//$user = $this->facebook->getUser();
		//$user_profile = null;
		//$test = $this->facebook->getUser();
		$fb = OAuth::consumer( 'Facebook' );
		//if ($user != 0) {
		$buttons = '';
		if ( !Session::has('fb-user-profile') ) {
			
		  //header("location: register");
		  //return Redirect::to('register');
			if(isset( $code )){
				$token = $fb->requestAccessToken( $code );
				$resp = $fb->request( '/me' );
				//$pp = $fb->request( '/me/picture' );
				//Session::put('fb-user-profile-picture', $pp);
				Session::put('fb-user-profile-str', $resp);

				$user_profile = json_decode( $resp , true );
				Session::put('fb-user-profile', $user_profile);

				$buttons = $this->renderRegForm($user_profile);
				
			}else{
				$url = $fb->getAuthorizationUri();

        		// return to facebook login url
				$login = (string)$url;
				
				$buttons = '<h3>Start here:</h3>
				<a href="'.$login.'" ><button class="fb-btn"><img src="'.asset('home/images/fb-btn500.jpg').'" /></button></a>
				<div style="margin-top: 5px;">Note: We will not post anything on your wall. Promise!</div>';
			}
			

		}else{
			$buttons = $this->renderRegForm(Session::get('fb-user-profile'));			
		}

		
		$inline_js = View::make('register_js.register_js');
		$js = array(asset('assets/semantic/packaged/javascript/semantic.js'));
		$css = array(asset('home/css/sign-up.css'),
			asset('assets/semantic/packaged/css/semantic.css'));
		return View::make('signup', array('title' => 'Beta Signup - Rockstarcash.com', 
			'css' => $css,
			'js' => $js,
			'inline_js' => $inline_js,
			'exclude_nagging_js' => 1,
			'buttons' => $buttons,
										  //'test' => $test
			));
	}

	private function renderRegForm($user_profile){
		$buttons = '';
		if(!Session::has('errors')){
			$uts = UserType::all();
			$i = 0;
			$style = array('btn-success','btn-inverse','btn-warning','btn-danger');

			$buttons .= '<div id="user-types">';
			foreach($uts as $t){
				//$url = $fb->getAuthorizationUri();
				$buttons .=	'<a href="javascript:void(0)" style="margin-right: 10px;" class="user-type-btn" user-type="'.strtolower(str_replace(' ', '_', $t->name)).'" ><button class="btn btn-large '.$style[$i].'" type="button">'.$t->name.'</button></a>';
				if($i == count($style) - 1){
					$i = 0;
				}else{
					$i++;
				}
			}
			$buttons .= '</div>';
			$buttons .= '<div id="reg-form"></div>';
		}else{
			$buttons .= '<div id="reg-form">'.$this->renderRegFormView($user_profile, Session::get('reg-form-user-type'), Session::get('reg-form-user'), array('errors' => Session::get('errors'))).'</div>';
		}

		return $buttons;
	}

	public function process(){
		Input::flash();
		$user = new User;
		$user_profile = Session::get('fb-user-profile');
		$name = Input::get('name');
		$validator = Validator::make(array('name'      => $name,
										   'password'  => Input::get('password'),
										   'cpassword' => Input::get('cpassword'),
										   'email'     => $user_profile['email']
			), 
		$user->rules(),
		$user->messages());
		
		if($validator->fails()){
			$messages = $validator->messages();
			return Redirect::to('sign-up')->withErrors($validator)->withInput();;
		}

		//$offer_id = $this->generateTrackerOfferId($name);

		/*if(Session::has('reg-form-user-type')){
			Session::forget('reg-form-user-type');
		}*/
		
		
			//$label_id = Input::get('label_id');
			//$label = (isset($label_id) && $label_id != "")? $label_id : 0 ;
			
		$l_name = Input::get('label');
		$label_name = (isset($l_name) && $l_name != "")? $l_name : "No label";

		$username = $user_profile['email'];
		$password = Hash::make(Input::get('password'));			
			
		$user->name = $name;
		//$user->offer_id = $offer_id;
		$user->fb_id = $user_profile['id'];
		$user->email = $username;
		$user->password = $password;
		$user->label_name = $label_name;
		//$user->advertiser_id = $label_id;
		$user->user_type = Session::get('user-type');
		//Session::forget('reg-form-user-type');

		$user->save();
		$saved_id = $user->id;
		
		//$offer_id = 0; //DFP advertiser ID

		if($saved_id){
			/*$offer_id = $this->createAdvertiserId($saved_id, Session::get('user-type'));
			if($offer_id){
				$up_user = User::find($saved_id);
				$up_user->offer_id = $offer_id;
				$up_user->save();
			}*/
			/*
			$ad_unit_ids = $this->createDFPAdunit($saved_id);
			if($ad_unit_ids){
				$dfp = array();
				for($x = 0; $x < 6; $x++){
					$position = '';

					switch($x){
						case 0:
							$position = 'top';
							break;
						case 1:
							$position = 'left';
							break;
						case 2:
							$position = 'left_resp';
							break;
						case 3:
							$position = 'bottom';
							break;
						case 4:
							$position = 'smart_banner1';
							break;
						case 5:
							$position = 'smart_banner2';
							break;
					}

					$dfp[] = array('user_id' => $saved_id,
								   'position' => $position,
								   'ad_unit_id' => $ad_unit_ids[$x]);
				}
					
				DB::table('dfp')->insert($dfp);
			}else{
				Log::info('No ad unit created');
			}		
			*/
			$fb_detail = new FbDetail;
			$fb_detail->user_id = $saved_id;
			$fb_detail->details = Session::get('fb-user-profile-str');
				//$fb_detail->profile_picture = Session::get('fb-user-profile-picture');
			$fb_detail->save();

			$other_details = new OtherDetail;
			$other_details->user_id = $saved_id;
			$other_details->details = Input::get('other_details');
			$other_details->save();

			$this->sendWelcomeEmail($username,$name);

			$credentials = array(
				'email' => $username,
				'password' => Input::get('password')
				);

			if (Auth::attempt($credentials)) {
				/*Session::put('offer_id',$offer_id);
				Session::put('id',$saved_id);
				Session::put('fb_id',$user_profile['id']);
				$u_type = Session::get('user-type');
				Session::forget('user-type');
				Session::put('u_type',$u_type);*/
				Session::flush();
				return Redirect::to('admin/login');
			}
		}else{
			Log::info('Something went wrong');
		}
	}

	private function sendWelcomeEmail($email,$name){
		$m = new Mandrill_Api();

		$m->sendTemplate(array('name' => '6_welcome.html',
							   'user_name' => $name,
							   'to' => $email, 
							   'subject' => 'Welcome to Rockstarcash!',
							   'send_at' => null));
	}

	private function createAdvertiserId($user_id, $user_type){
		$dfp = new DfpApi("auth-rsc.ini");
		$res = $dfp->createUserAdvertiser($user_id, $user_type);
		Log::info($res);
		return $res;
	}

	private function createDFPAdunit($user_id){
		$dfp = new DfpApi("auth-amplify.ini");

		return $dfp->createAdunit($user_id);
	}

	private function createDFPAdvertiser(){

	}

	private function generateTrackerOfferId($name){
		$base = $this->ApiDomain.'/api/3/addedit.asmx/Offer?';
		
		$year = date('Y');
		$year = intval($year) + 10;
		
		$params = array(
			'api_key' => $this->cake_api_key
			,'offer_id' => 0
			,'advertiser_id' => 1
			,'vertical_id' => 1
			,'offer_name' => $name
			,'third_party_name' => $name
			,'hidden' => 'off'
			,'offer_status_id' => 1
			,'offer_type_id' => 1
			,'currency_id' => 1
			,'ssl' => 'off'
			,'click_cookie_days' => 30
			,'impression_cookie_days' => 30
			,'redirect_offer_contract_id' => 0
			,'redirect_404' => 'off'
			,'enable_view_thru_conversion' => 'off'
			,'click_trumps_impression' => 'off'
			,'disable_click_deduplication' => 'off'
			,'last_touch' => 'off'
			,'enable_transaction_id_deduplication' => 'off'
			,'postbacks_only' => 'off'
			,'postback_url' => 'NULL'
			,'postback_url_ms_delay' => 100
			,'fire_global_pixel' => 'off'
			,'fire_pixel_on_nonpaid_conversions' => 'off'
			,'static_suppression' => 0
			,'conversion_cap_behavior' => 1
			,'conversion_behavior_on_redirect' => 0
			,'expiration_date' => '12/31/2016 13:59:59'
			,'expiration_date_modification_type' => 'change'
			,'offer_contract_name' => 'NULL'
			,'offer_contract_hidden' => 'on'
			,'price_format_id' => 1
			,'payout' => 0
			,'received' => 0
			,'received_percentage' => 'on'
			,'offer_link' => 'http://dev.amplify.fm/wp/download-page/'
			,'preview_link' => 'http://dev.amplify.fm/wp/download-page/'
			,'thankyou_link' => 'http://dev.amplify.fm/wp/download-page/'
			,'from_lines' => 'Admin'
			,'subject_lines' => 'Thank you test'
			,'enable_view_thru_conversions' => 'off'
			,'pixel_html' => 'NULL'
			,'fire_pixel_on_non_paid_conversions' => 'off'
			,'thumbnail_file_import_url' => 'NULL'
			,'offer_description' => 'artist'
			,'restrictions' => 'NULL'
			,'advertiser_extended_terms' => 'NULL'
			,'testing_instructions' => 'NULL'
			,'tags' => 'music'
			,'allow_affiliates_to_create_creatives' => 'off'
			,'unsubscribe_link' => 'NULL'
			);
			/*echo '<pre>';
			print_r($params);
			echo '</pre>';
			*/
		$url = $base . http_build_query( $params );
			//echo $url.'<br /><br />';
			//$result = file_get_contents( $url );
		$offer = simplexml_load_file($url);
			/*echo '<pre>';
			var_dump($result);
			echo '</pre>';*/
			//$offer = simplexml_load_string( $result );
			//var_dump($result);
		if(!$offer){

		}

		return $offer->success_info->offer_id;
	}
}
