<?php

class LogController extends \BaseController {

	public function trackDFP(){
		if($this->checkLastActivity('click')){
			$creative_id = (Input::has('c_id'))? Input::get('c_id') : 0;
			$advertiser_id = (Input::has('a_id'))? Input::get('a_id') : 0;
			$order_id = (Input::has('o_id'))? Input::get('o_id') : 0;
			$ref_site = (Input::has('ref_site'))? Input::get('ref_site') : '';
			$ad_site = (Input::has('ad_site'))? Input::get('ad_site') : 0;
			$email = (Input::has('email'))? Input::get('email') : ''; 

			$player_hash_code = CustomHelper::parseUrl($ref_site);
			$player = CustomHelper::getPlayerFromUrl(new Player, $player_hash_code['player']);

			$geo = $this->getGeo();
			$date = date('Y-m-d');
			$time = date('G:i:s');
			$dfp = new DfpLog;

			$dfp->creative_id = $creative_id;
			$dfp->advertiser_id = $advertiser_id;
			$dfp->order_id = $order_id;
			$dfp->player_id = $player->id;
			$dfp->user_id = $player->q; //user id ni. q lng sa artist page.
			$dfp->city = $geo['city'];
			$dfp->country = $geo['country'];
			$dfp->country_code = $geo['country_code'];
			$dfp->date = $date;
			$dfp->time = $time;
			$dfp->save();

			if($dfp->id){
				$has_mark = strpos($ad_site, '?');
				if($has_mark === false){
					$ad_site .= '?i='.$dfp->id.'&e='.$email;
				}else{
					$ad_site .= '&i='.$dfp->id.'&e='.$email;
				}

				return Redirect::away($ad_site);
			}

			return Response::make('There was an error while doing this request. Please contact the site administrator.');
		}
	}

	public function logConversion(){
		if(Input::has('i')){
			$log_id = Input::get('i');
			$log = DfpLog::find($log_id);
			$log->converted = 1;
			$log->save();
			Log::info('conversion log success');
			return $log->id;
		}
		Log::info('conversion log fail. i is not defined');
		return false;
	}

	private function getGeo(){
		$res = RSC_GeoIp::getGeo(CustomHelper::getClientIp());
		$city = (isset($res->city))? $res->city->names->en : 'No City Data';
		$state = 'No State Data';
		if(isset($res->subdivisions)){
			if(isset($res->subdivisions->names->en)){
				$state = $res->subdivisions->names->en;
			}
			if(isset($res->subdivisions[0]->names->en)){
				$state = $res->subdivisions[0]->names->en;
			}
		}

		return array("country" => $res->country->names->en,
					 "country_code" => $res->country->iso_code,
					 "state_code" => $state,
					 "city" => $city);
	}

	private function checkLastActivity($activity){
		$time = date('H:i:s');
		if(!isset($_SESSION[$activity]['time'])){
			$_SESSION[$activity]['time'] = $time;			
			return true;
		}
		$time_diff = $this->getTimeDiff($_SESSION[$activity]['time'], $time);
		if($time_diff > 20){ //20mins have passed since first share log
			$_SESSION[$activity]['time'] = $time;
			return true;
		}
		return false;
	}
	
	private function getTimeDiff($time1, $time2){
		$time1_arr = explode(':',$time1);
		$time2_arr = explode(':',$time2);

		$hour_diff = ($time2_arr[0] - $time1_arr[0]) * 60;
		$min_diff = $time2_arr[1] - $time1_arr[1];

		return $hour_diff + $min_diff;
	}
	
}
