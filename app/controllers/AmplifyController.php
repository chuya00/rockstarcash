<?php

class AmplifyController extends \BaseController {

	protected $layout = 'layouts.default';
	
	public function manage()
	{
		$this->layout->title = 'Manage Amplify Page';
		$this->layout->active = array('title' =>'Manage Amplify Page');
		$this->layout->css = array(asset('css/tables.css'));
		$this->layout->js = array(asset('js/manage-amplify-page.js'));

		$this->layout->content = View::make('admin.manage-amplify',array('videos' => $this->getVideos(),
																		 'count' => 0,
																		 //'selected' => $this->getSelected(),
																		 'players' => $this->getPlayers(),
																		 'url' => URL::to('/')));
	}

	public function save()
	{
		$amplify = new AmplifyPage;
		$media = Input::get('media-selected');
		$player_id = Input::get('player_id');

		$tmp = explode('-',$media);

		$id = 0;
		$source = 'rsc';

		if(count($tmp) > 1){
			$id = $tmp[0];
			$source = $tmp[1];
		}

		$exist = $amplify->where('player_id','=',$player_id)
					     ->where('user_id','=',Session::get('id'))
					     ->first();

		if($exist->id){
			$update = AmplifyPage::find($exist->id);
			$update->media_id = $id;
			$update->source = $source;
			$update->save();

			if($update->id){
				return Redirect::to(URL::to('admin/manage-amplify-page'));
			}else{
				Log::info($amplify);
				return Redirect::to(URL::to('admin/manage-amplify-page'))->with('message', 'Something went wrong');;
			}
		}else{
			$amplify->user_id = Session::get('id');
			$amplify->player_id = $player_id;
			$amplify->media_id = $id;
			$amplify->source = $source;
			$amplify->save();

			if($amplify->id){
				return Redirect::to(URL::to('admin/manage-amplify-page'));
			}else{
				Log::info($amplify);
				return Redirect::to(URL::to('admin/manage-amplify-page'))->with('message', 'Something went wrong');;
			}
		}

		
	}

	private function getVideos()
	{
		$video = new Video;
		return $video->where('user_id','=',Session::get('id'))->get();
	}
	
	private function getAudios(){
		$audio = new Audio;

		return $audio->where('artist_id','=',Session::get('id'))
					 ->whereNotNull('source')
					 ->get();
	}

	private function getPlayers(){
		$player = new Player;
		return $player->where('user_id','=',Session::get('id'))->get();
	}

	public function getMedia(){
		$videos = $this->getVideos();
		$audios = $this->getAudios();
		$res = array();

		foreach($audios as $a){
			$res[] = array('id'     => $a->id,
						 'source' => $a->source,
						 'title'  => $a->title);
		}

		foreach($videos as $v){
			$res[] = array('id'     => $v->id,
						 'source' => $v->source,
						 'title'  => $v->title);
		}

		return Response::make(json_encode($res));
	}
}
