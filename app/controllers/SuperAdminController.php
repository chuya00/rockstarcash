<?php

class SuperAdminController extends \BaseController {

	protected $layout = 'layouts.default';
	private $user_id;

	function __construct(){
		$this->user_id = Session::get('id');
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->title = 'Super Admin';
		$this->layout->active = array('title' =>'Dashboard');
		$this->layout->css = array(asset('css/tables.css'),
								   asset('css/dashboard.css'));
		/*$this->layout->js = array(asset('assets/chart/chart.min.js'),
								  asset('js/chart.js'),
								  asset('js/admin-home.js')); */

		$this->layout->content = View::make('superadmin.home',array('url' => URL::to('/')));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function showOrdersView()
	{
		$this->getDFPOrders();
		$this->layout->title = 'Super Admin | Orders CPA';
		$this->layout->active = array('title' =>'Dashboard');
		$this->layout->css = array(asset('css/tables.css'),
								   //asset('admin/jquery-ui.min.css')
								   );
		$this->layout->js = array(asset('sadmin/functions.js'));

		$this->layout->content = View::make('superadmin.order',array('url' => URL::to('/'),
																	 'orders' => $this->getDFPOrders()
																	 ));
	}

	private function getDFPOrders(){
		$dfp = new DfpApi("auth-rsc.ini");

		return $dfp->getAllOrders();
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function saveOrder()
	{
		$order = new DfpOrder;
		$order_id = Input::get('order_id');
		$order_name = Input::get('order_name');
		$cpa = Input::get('cpa');

		$id = $order->getIdByOrderId($order_id);

		if(isset($id) && $id != null){
			$update_order = DfpOrder::find($id);
			$update_order->cpa = $cpa;
			$update_order->save();
		}else{
			$order->saveData(array("order_id" => $order_id,
								   "order_name" => $order_name,
								   "cpa" => $cpa));
		}

		return Redirect::to('super-admin/orders');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
