<?php

class ContestStatController extends \BaseController {

	protected $layout = 'layouts.default';

	public function index()
	{
		$this->layout->title = 'Contests Stats';
		$this->layout->active = array('title' =>'Manage Contests');
		$this->layout->css = array(asset('css/tables.css'));
		//$this->layout->js = array(asset('js/contest-stat.js'));

		$this->layout->content = View::make('admin.contest-stat',array('city'     => $this->getTopCities(),
															   		   'country'  => $this->getTopCountries(),
																	   'site'     => $this->getTopRefSites(),
																	   'songs'    => $this->getTopSongs(),
																	   'players'  => $this->getPlayers(),
																	   'users'    => $this->getTopUsers(),
																	   'us_count' => 1,
																	   'ci_count' => 1,
																	   'co_count' => 1,
																	   'si_count' => 1,
																	   'so_count' => 1,
															      	   'url'      => URL::to('/')));
	}


	private function getPlaysData($from, $stat = ''){
		$user_id = Session::get('id');
		$where_stat = "";

		$date_now = date('Y-m-d');
		$date = date('Y-m-d');

		if($stat != ''){
			
			switch($stat){
				case 'week':
					$date = date('Y-m-d', strtotime('-1 week'));
					break;

				case 'month':
					$date = date('Y-m-d', strtotime('-1 month'));
					break;

				case 'year':
					$date = date('Y-m-d', strtotime('-1 year'));					
					break;
			}
			
		}

		$where_stat = " AND (date BETWEEN '$date' AND '$date_now') ";

		$sql = "SELECT DISTINCT($from), 
				(SELECT COUNT(ccs.id) FROM contest_share ccs, player pp WHERE $from = cs.$from AND ccs.player_id = pp.id AND pp.user_id = $user_id) AS plays
				FROM contest_share cs, player p
				WHERE cs.player_id = p.id
				AND p.user_id = $user_id
				
				ORDER BY plays DESC
				LIMIT 5";

		$res = DB::select( DB::raw($sql) );

		return $res;
	}

	private function getTopUsers(){
		$user_id = Session::get('id');
		$sql = "SELECT DISTINCT(cpv.contest_id), 
				cu.email,
				c.title AS contest_title,
				(SELECT COUNT(id) 
				FROM contest_page_views 
				WHERE contest_id = cpv.contest_id AND fan_id = cpv.fan_id) AS views,
				(SELECT COUNT(id) 
				FROM contest_share 
				WHERE contest_id = cpv.contest_id AND fan_id = cpv.fan_id) AS shares 
				FROM contest_page_views cpv, contest_user cu, contest c
				WHERE cpv.contest_id = cu.contest_id
				AND cpv.fan_id = cu.amplify_id
				AND cpv.contest_id = c.id
				AND c.user_id = $user_id
				ORDER BY views DESC
				LIMIT 5";
		//CustomHelper::toString($sql);
		$res = DB::select( DB::raw($sql) );

		return $res;
	}

	private function getTopCities(){
		$user_id = Session::get('id');
		$sql = "SELECT DISTINCT(city),
				(SELECT COUNT(id) 
				FROM contest_page_views 
				WHERE contest_id = cpv.contest_id AND city = cpv.city) AS views,
				(SELECT COUNT(id) 
				FROM contest_share 
				WHERE contest_id = cpv.contest_id AND city = cpv.city) AS shares 
				FROM contest_page_views cpv, contest c
		  		WHERE cpv.contest_id = c.id
		  		AND c.user_id = $user_id
		  		AND city != ''
		  		ORDER BY views DESC
		  		LIMIT 5";

		$res = DB::select( DB::raw($sql) );

		return $res;
	}

	private function getTopCountries(){
		$user_id = Session::get('id');
			$sql = "SELECT DISTINCT(country),
					(SELECT COUNT(id) 
					FROM contest_page_views 
					WHERE contest_id = cpv.contest_id AND country = cpv.country) AS views,
					(SELECT COUNT(id) 
					FROM contest_share 
					WHERE contest_id = cpv.contest_id AND country = cpv.country) AS shares 
					FROM contest_page_views cpv, contest c
			  		WHERE cpv.contest_id = c.id
			  		AND c.user_id = $user_id
			  		AND country != ''
			  		ORDER BY views DESC
			  		LIMIT 5";

		$res = DB::select( DB::raw($sql) );

		return $res;
	}

	private function getTopRefSites(){
		$user_id = Session::get('id');
		$sql = "SELECT DISTINCT(site), 
				(SELECT COUNT(id) 
				FROM contest_ref_site 
				WHERE contest_id = cpv.contest_id AND site = cpv.site) AS ref_count 
				FROM contest_ref_site cpv
		  		WHERE contest_id IN (SELECT id FROM contest WHERE user_id = $user_id)
		  		AND site != ''
		  		ORDER BY ref_count DESC
		  		LIMIT 5";

		$res = DB::select( DB::raw($sql) );

		return $res;
	}

	private function getTopSongs(){
		$user_id = Session::get('id');
		$sql = "SELECT DISTINCT(cpv.contest_id),
				v.title AS title,
				(SELECT COUNT(id) 
				FROM contest_page_views 
				WHERE contest_id = cpv.contest_id) AS views,
				(SELECT COUNT(id) 
				FROM contest_share 
				WHERE contest_id = cpv.contest_id) AS shares 
				FROM contest_page_views cpv, contest c, player p, video v
		  		WHERE cpv.contest_id = c.id
		  		AND c.player_id = p.id
		  		AND p.video_id = v.id
		  		AND c.user_id = $user_id
		  		ORDER BY views DESC
		  		LIMIT 5";

		$res = DB::select( DB::raw($sql) );

		return $res;
	}

	private function getPlayers(){
		$player = new Player;
		$res = $player->where('user_id','=',Session::get('id'))
					  ->select('id','audios','offer_file_id','artist_name','album_name')
					  ->get();

		return $res;
	}

	private function getCreativeSummary($offer_file_id,$start_date,$end_date){
		$stat = new StatReports;
		$resp = $stat->getStats($start_date,$end_date);
		if(isset($resp->success) && $resp->success == 'true'){
			return $resp->creatives->creative_summary;
		}
		
		return false;
	}

	private function getSongDetails($ids){
		$ids_arr = explode(',',$ids);
		$audio = new Audio;

		return $audio->where('id','=',$ids_arr[(count($ids_arr)-1)])->first();
	}

}
