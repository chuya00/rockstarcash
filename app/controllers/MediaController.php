<?php

class MediaController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	protected $layout = 'layouts.default';

	public function index()
	{
		//
	}

	public function showAudioUploadPage()
	{
		$this->layout->title = 'Media | Upload Audio File ';
		$this->layout->active = array('title' =>'Audio');
        $this->layout->css = array(asset('css/tables.css'),
        						   '//blueimp.github.io/Gallery/css/blueimp-gallery.min.css',
        						   asset('assets/jquery_fileuploader/css/jquery.fileupload.css'),
        						   asset('assets/jquery_fileuploader/css/jquery.fileupload-ui.css'));

       	$this->layout->js = array(asset('assets/jquery_fileuploader/js/vendor/jquery.ui.widget.js'),
        						  '//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js',
        						  '//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js',
        						  '//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js',
        						  '//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js',
        						  asset('assets/jquery_fileuploader/js/jquery.iframe-transport.js'),
        						  asset('assets/jquery_fileuploader/js/jquery.fileupload.js'),
        						  asset('assets/jquery_fileuploader/js/jquery.fileupload-process.js'),
        						  //asset('assets/jquery_fileuploader/js/jquery.fileupload-image.js'),
        						  asset('assets/jquery_fileuploader/js/jquery.fileupload-audio.js'),
        						  //asset('assets/jquery_fileuploader/js/jquery.fileupload-video.js'),
        						  asset('assets/jquery_fileuploader/js/jquery.fileupload-validate.js'),
        						  asset('assets/jquery_fileuploader/js/jquery.fileupload-ui.js'),
        						  asset('assets/jquery_fileuploader/js/upload-audio.js'),);
		
		$this->layout->content = View::make('admin.audio-upload',array('audio_files' => $this->getAudioFiles(),
																	   'count' => 0,
																	   'url' => URL::to('/')));
	}

	public function showAudioPlayersPage()
	{
		$this->layout->title = 'Media | Audio Players ';
		$this->layout->active = array('title' =>'Audio');
        $this->layout->css = array(asset('css/tables.css'),
        						   '//blueimp.github.io/Gallery/css/blueimp-gallery.min.css',
        						   asset('assets/jquery_fileuploader/css/jquery.fileupload.css'),
        						   asset('assets/jquery_fileuploader/css/jquery.fileupload-ui.css'),
        						   asset('assets/image-picker/image-picker.css'),
        						   asset('css/audio-player.css'));

       	$this->layout->js = array(asset('assets/jquery_fileuploader/js/vendor/jquery.ui.widget.js'),
        						  //'//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js',
        						  //'//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js',
        						  //'//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js',
        						  //'//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js',
        						  asset('assets/jquery_fileuploader/js/jquery.iframe-transport.js'),
        						  asset('assets/jquery_fileuploader/js/jquery.fileupload.js'),
        						  //asset('assets/jquery_fileuploader/js/jquery.fileupload-process.js'),
        						  //asset('assets/jquery_fileuploader/js/jquery.fileupload-image.js'),
        						  //asset('assets/jquery_fileuploader/js/jquery.fileupload-audio.js'),
        						  //asset('assets/jquery_fileuploader/js/jquery.fileupload-video.js'),
        						  //asset('assets/jquery_fileuploader/js/jquery.fileupload-validate.js'),
        						  //asset('assets/jquery_fileuploader/js/jquery.fileupload-ui.js'),
        						  asset('assets/image-picker/image-picker.js'),
        						  asset('js/audio-player.js'),);
		
		$player_details = Session::get('player.details');
		$cover = (isset($player_details[0]['image']))? URL::to('/').'/assets/album_image/files/'.$player_details[0]['image'] : 'cover.jpg';
		$cover_id = (isset($player_details[0]['image_id']))? $player_details[0]['image_id'] : 0;
		$artist = (isset($player_details[0]['artist_name']))? $player_details[0]['artist_name'] : 'Enter artist name';
		$album = (isset($player_details[0]['album_name']))? $player_details[0]['album_name'] : 'Enter album name';
		$this->layout->content = View::make('admin.audio-players',array('audio_files' => $this->getAudioFiles(),
																		'players' => $this->getPlayers(),
																	    'count' => 0,
																	    //'images' => $this->getAlbumImage(),
																	    'cover' => $cover,
																	    'image_filename' => isset($player_details[0]['image'])? $player_details[0]['image']: '',
													  					'cover_id' => $cover_id,
																	    'artist' => $artist,
													  				    'album' => $album,
																	    'url' => URL::to('/')));
	}

	public function showEmbedPlayerPreviewPage()
	{
		$player_details = Session::get('player.details');
		//Log::info($player_details);
		$cover = (isset($player_details[0]['image']))? URL::to('/').'/assets/album_image/files/'.$player_details[0]['image'] : 'cover.jpg';
		$cover_id = (isset($player_details[0]['image_id']))? $player_details[0]['image_id'] : 0;
		$artist = (isset($player_details[0]['artist_name']))? $player_details[0]['artist_name'] : 'Enter artist name';
		$album = (isset($player_details[0]['album_name']))? $player_details[0]['album_name'] : 'Enter album name';
		return View::make('embed.create-player',array('file' => $this->getAudiosFromSession(),
													  'cover' => $cover,
													  'image_filename' => isset($player_details[0]['image'])? $player_details[0]['image']: '',
													  'cover_id' => $cover_id,
													  'artist' => $artist,
													  'album' => $album,
													  'count' => 0,
													  'player_details' => $player_details,
													  'url' => URL::to('/')));
	}

	public function showAddAudioPage()
	{
		$this->layout->title = 'Media | Add 3rd Party Audio ';
		$this->layout->active = array('title' =>'Audio');
        $this->layout->css = array(asset('css/tables.css'));

       	$this->layout->js = array(asset('js/add-audio.js'));
		
		$this->layout->content = View::make('admin.add-audio',array('audios' => $this->getAudios(),
																	'count' => 0,
																	'url' => URL::to('/')));
	}

	public function saveThirdPartyAudio(){

		$audio = new Audio;
		$title = Input::get('title');
		$link = Input::get('link');
		$source = Input::get('source');

		$validator = Validator::make(array('title'  => $title,
										   'link'   => $link,
										   'source' => $source
			), 
		$audio->rules());
		Log::info('sulod');
		if($validator->fails()){
			$messages = $validator->messages();
			return Redirect::to('admin/media/add-audio')->withErrors($validator);
		}
		
		$audio->artist_id = Session::get('id');
		$audio->title = $title;
		$audio->link = $link;
		$audio->source = $source;
		$audio->save();
		if($audio->id){
			return Redirect::to(URL::to('admin/media/add-audio'));
		}
		Log::info($audio);
	}

	private function getAudios(){
		$audio = new Audio;

		return $audio->where('artist_id','=',Session::get('id'))
					 ->whereNotNull('source')
					 ->get();
	}

	private function getPlayers(){
		$player = new Player;
		return $player->where('user_id','=',Session::get('id'))->get();
	}

	public function saveAlbumImage(){
		$success = 0;
		$last_id = 0;
		$filename = '';
		if(Input::has('filename')){
			$image = $this->insertPlayerImage();
			if($image->id){
				$last_id = $image->id;
				$filename = $image->filename;
					//echo $last_id;
				$success = 1;
			}			
		}

		if($success == 1){
			return Response::make(json_encode(array("resp" => "1", "id" => $last_id,"filename" => $filename)));
		}else{
			return Response::make(json_encode(array("resp" => "0")));
		}
	}

	private function insertPlayerImage(){
		$filename = Input::get('filename');
		$user_id = Session::get('id');
		
		$album_image = new AlbumImage;
		$album_image->user_id = $user_id;
		$album_image->filename = $filename;
		$album_image->save();

		/*
		$sql = "INSERT INTO album_image (user_id, filename)
				VALUES (".$user_id.",'".$filename."')";
		
		$res = mysqli_query($con,$sql);*/
							
		return $album_image;
	}

	private function getLastId(){
		$album_image = new AlbumImage;
		$res = $album_image->orderBy('id','desc')->first();
		//$sql = "SELECT id FROM album_image ORDER BY id DESC LIMIT 1";
		//$res = mysqli_query($con,$sql);
		//$row = mysqli_fetch_array($res);
		
		return $res->id;
	}

	private function getAlbumImage(){
		$album_image = new AlbumImage;

		return $album_image->where('user_id','=',Session::get('id'))->select('id', 'filename')->get();
	}

	public function addAudio(){
		if(Input::has('id') && Input::has('method')){
			if(!Session::has('audio_ses')){
				Session::put('audio_ses',array());
			}
			if(Input::get('method') == "add"){
				$temp = Session::get('audio_ses');
				if(!isset($temp[Input::get('id')])){
					$temp[Input::get('id')] = 1;
					Session::put('audio_ses',$temp);
					//$_SESSION['audio_ses'][$_GET['id']] = 1;
				}	
			}else if(Input::get('method') == "remove"){
				$temp = Session::get('audio_ses');
				if(isset($temp[Input::get('id')])){
					unset($temp[Input::get('id')]);
					Session::put('audio_ses',$temp);
				}
			}
			
		}
	}

	public function addSavedPlayerDetails(){
		if(Input::has('artist_name') && Input::has('album_name')){
			//if(!isset($_SESSION['player']['details'])){
			if(!Session::has('player_prev.details')){
				//$_SESSION['player_prev']['details'] = array();
				Session::push('player_prev.details',array());
			}
			
			$temp = Session::get('player_prev.details');

			$temp['artist_name'] = Input::get('artist_name');
			$temp['album_name'] = Input::get('album_name');
			
			if(Input::has('image')){
				$temp['image'] = Input::get('image');
				$temp['image_id'] = Input::get('image_id');
			}

			Session::push('player_prev.details',$temp);
		}
	}

	public function saveAudioPlayer(){
		if(Session::has('audio_ses') && count(Session::get('audio_ses')) > 0 && Input::has('artist_name')){
			//$offer_file_id = $this->createOfferFile(Input::get('image'));
			//echo $offer_file_id;
			//if($offer_file_id > 0 || $offer_file_id !== false){
				//echo 'sulod2';
				//$save_player = $this->insertAudioPlayer($offer_file_id);
			$save_player = $this->insertAudioPlayer();
			if($save_player){
				$this->createDfpAdUnit($save_player);
				Session::forget('audio_ses');
				Session::forget('player');
				$success = 1;
			}	
			//}
					
		}else if(!Session::has('audio_ses') && count(Session::get('audio_ses')) > 0){
			return Response::make('Please add MP3s.');
		}else if(!(Input::has('artist_name')) && trim(Input::has('artist_name')) == ""){
			return Response::make("Artist name can't be empty!");
		}

		if($success == 1){
			return Response::make('1');
		}
	}

	private function createDfpAdUnit($id){
		$dfp = new DfpApi("auth-amplify.ini");
		$user_id = Session::get('id');
		Log::info($user_id.' '.$id.' debug here');
		$ad_unit_ids = $dfp->createAdunit($user_id,$id);
		if($ad_unit_ids){
			$dfp = array();
			for($x = 0; $x < 4; $x++){
				$position = '';

				switch($x){
					case 0:
						$position = 'top';
						break;
					case 1:
						$position = 'left';
						break;
					case 2:
						$position = 'left_resp';
						break;
					case 3:
						$position = 'bottom';
						break;
				}

				$dfp[] = array('user_id' => $user_id,
							   'player_id' => $id,
							   'position' => $position,
						       'ad_unit_id' => $ad_unit_ids[$x]);
			}

			DB::table('dfp')->insert($dfp);
		}else{

			Log::info('No ad unit created');
		}				
	}

	private function insertAudioPlayer($offer_file_id = 0){
		$artist_name = Input::get('artist_name');
		$album_name = Input::get('album_name');
		$user_id = Session::get('id');
		$image_id = Input::has('image_id')? Input::get('image_id') : 0;
		
		$ids_arr = $this->getIds();
		$ids = $this->idsToString($ids_arr);
		
		$audios = $ids;
		
		$player = new Player;
		$player->user_id = $user_id;
		$player->audios = $audios;
		$player->artist_name = $artist_name;
		$player->album_name = $album_name;
		$player->image_id = $image_id;
		$player->offer_file_id = $offer_file_id;
		$player->save();

		/*$sql = "INSERT INTO player (user_id, audios, artist_name, album_name,  image_id, offer_file_id)
				VALUES (".$user_id.",'".$audios."','".$artist_name."','".$album_name."',".$image_id.",".$offer_file_id.")";
		//echo $sql;
		$res = mysqli_query($con,$sql);*/
							
		return $player->id;
	}

	private function getIds(){
		$ids = Session::get('audio_ses');
		$out = array();
        Log::info($ids);
		foreach($ids as $key => $value){
			if($key != "" && $key != 0 && $key != null && $key != 'undefined'){
				$out[] = $key;
			}
									
		}
								
		return $out;
	}
							
	private function idsToString($ids){
		$res = implode(",",$ids);
		return $res;
	}

	private function createOfferFile($img){	
		$cake_api_key = "u0jVLuBLdfozIYc0fxKW5a10cRWBzm";
		$ApiDomain = "http://rsc.network-stats.com";
		$base = $ApiDomain.'/api/1/add.asmx/Creative?';
		 
		$params = array(
			'api_key' => $cake_api_key
			,'offer_id' => Session::get('offer_id')
			,'creative_name' => 'player for offer id '.Session::get('offer_id')
			,'creative_type_id' => 4
			,'creative_status_id' => 1
			,'offer_link' => 'http://dev.amplify.fm/wp/download-page/'
			,'notes' => 'Player creative'
		);
		 
		$url = $base . http_build_query( $params );
		//echo $url;
		$result = simplexml_load_file($url);
		
		//echo '<pre>';
		//echo $result[0];
		//echo '</pre>';
		//$data = json_decode( $result );
		//$data = $data->response->data;
		//$offer = $data->{$offer_id}; //ang number 4 kay pwede kwaon as GET or SESSION
		if(isset($result[0])){
			return $result[0];
		}else{
			Session::put('error_msg', 'Error on API');
		}
		return false;
		//echo '</pre>';
	}

	private function getAudioFiles(){
		$audio = new Audio;
		return $audio->where('artist_id','=',Session::get('id'))->get();
	}

	public function saveAudio(){
		$files = $this->prepareAudioData(Input::get('names'));

		$save = DB::table('audio')->insert($files);
		
		/*$name = $this->removeExt($filename);

		$audio = new Audio;
		$audio->filename = $filename;
		$audio->title = $name;
		$audio->artist_id = Session::get('id');*/
		if($save){
			return Response::make('1');
		}else{
			return Response::make('0');
		}

	}

	public function deleteAudio(){
		$audio = Audio::find(Input::get('id'));

		$deleted = $audio->delete();
		$removed = $this->deleteFromDirectory(Input::get('filename'));

		if($deleted && $removed){
			return Response::make('1');
		}else{
			return Response::make('0');
		}
	}

	private function deleteFromDirectory($filename){
		return unlink(public_path().'/assets/audio/files/'.$filename);
	}

	private function removeExt($name){
		//$res = str_replace('.mp3', '', $name);
		//$res = str_replace('.MP3', '', $name);

		return preg_replace('/\\.[^.\\s]{3,4}$/', '', $name);;
	}

	private function getTimestamp(){
		return date('Y-m-d H:i:s');
	}

	private function prepareAudioData($data){
		$res = array();
		for($x = 0; $x < count($data); $x++){
			$res[$x] = array('filename' => $data[$x], 
							 'title' => $this->removeExt($data[$x]),
							 'artist_id' => Session::get('id'),
							 'created_at' => $this->getTimestamp(),
							 'updated_at' => $this->getTimestamp());
		}

		return $res;
	}

	public function addPlayerDetails(){
		if(Input::has('artist_name') && Input::has('album_name')){
			/*if(!Session::has('player.details')){
				//$_SESSION['player']['details'] = array();
				Session::push('player.details',array());
			}*/
			
			if(Session::has('player.details')){
				Session::forget('player.details');
			}

			$temp = array();
			$temp['artist_name'] = Input::get('artist_name');
			$temp['album_name'] = Input::get('album_name');
			
			if(Input::has('image')){
				$temp['image'] = Input::get('image');
				$temp['image_id'] = Input::get('image_id');

				
			}

			Session::push('player.details',$temp);
		}
	}

	private function getAudiosFromSession(){
		if(Session::has('audio_ses') && count(Session::get('audio_ses')) > 0){
			$ids_arr = $this->getIds();
			
			//$ids = $this->idsToString($ids_arr);
            if(count($ids_arr) > 0){
                $audio = new Audio;
                $file = $audio->whereIn('id',$ids_arr)->first();				
                //$query = "SELECT * FROM audio WHERE id IN (".$ids.")";
                                        //echo $query;
                /*$query_count = "SELECT COUNT(id) AS count FROM audio WHERE id IN (".$ids.")";
                $res_count = mysqli_query($con,$query_count);
                $count_row = mysqli_fetch_array($res_count);
                $total_count = $count_row['count'];
                                        //echo $total_count;*/



                //$res = mysqli_query($con,$query);
                //$count = 0;
                //$row = mysqli_fetch_array($res);
                if(isset($file->filename)){
                    return URL::to('/').'/assets/audio/files/'.$file->filename;
                }
            }else{
                return URL::to('/').'/assets/audio/files/';
            }
            
			
		}
	}

	public function setSessionsEmbed(){
		$id = Input::get('id');

		$player = new Player;

		$p = $player->where('user_id','=',Session::get('id'))
					->where('id','=',$id)
					->first();

		$image_id = (!is_null($p->image_id)) ? $p->image_id : 0;

		$album = new AlbumImage;

		$image = $album->where('user_id','=',Session::get('id'))
					   ->where('id','=',$image_id)
					   ->first();

		$player_prev = array();
		if(Session::has('player_prev.details')){
			$player_prev = Session::get('player_prev.details');
		}

		$player_prev['image'] = $image->filename;
		$player_prev['image_id'] = $image->id;
		$player_prev['artist_name'] = $p->artist_name;
		$player_prev['album_name'] = $p->album_name;

		$this->idsToSession($p->audios,$player_prev);
		
		Session::put('player_prev.details',$player_prev);

		return Response::make(View::make('admin.edit-audio-player-form',array('audio_files' => $this->getAudioFiles(),
																			  'count' => 0,
																			  'details' => Session::get('player_prev.details'))));
	}

	private function idsToSession($ids,&$player_prev){
		$res = explode(",",$ids);
		//var_dump($res);
		foreach($res as $key => $value){
			$player_prev['audio_ses_prev'][$value] = 1;
		}
	}

	public function addSavedAudio(){
		if(Input::has('id') && Input::has('method')){
			if(!Session::has('audio_ses_prev')){
				Session::put('audio_ses_prev',array());
			}
			if(Input::get('method') == "add"){
				$temp = Session::get('audio_ses_prev');
				if(!isset($temp[Input::get('id')])){
					$temp[Input::get('id')] = 1;
					Session::put('audio_ses_prev',$temp);
					//$_SESSION['audio_ses'][$_GET['id']] = 1;
				}	
			}else if(Input::get('method') == "remove"){
				$temp = Session::get('audio_ses_prev');
				if(isset($temp[Input::get('id')])){
					unset($temp[Input::get('id')]);
					Session::put('audio_ses_prev',$temp);
				}
			}
			
		}
	}

}
