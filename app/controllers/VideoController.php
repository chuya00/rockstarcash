<?php

class VideoController extends \BaseController {

	protected $layout = 'layouts.default';

	public function index()
	{
		$this->layout->title = 'Videos';
		$this->layout->active = array('title' =>'Video');
		$this->layout->css = array(asset('css/tables.css'));
		$this->layout->js = array(asset('js/video.js'));

		$this->layout->content = View::make('admin.video',array('videos' => $this->getVideos(),
																'count' => 0,
																'url' => URL::to('/')));
	}

	public function save()
	{
		$video = new Video;
		$title = Input::get('title');
		$link = Input::get('link');
		$source = Input::get('source');

		$validator = Validator::make(array('title'  => $title,
										   'link'   => $link,
										   'source' => $source
			), 
		$video->rules());
		
		if($validator->fails()){
			if(Input::get('submit') == 'add-video-contest'){
				return Response::make('{"success": 1, "player_id": '.$save_player.', "player_title": "'.$title.'"}');
			}else{
				$messages = $validator->messages();
				return Redirect::to('admin/video')->withErrors($validator);
			}			
		}
		
		$video->user_id = Session::get('id');
		$video->title = $title;
		$video->link = $link;
		$video->source = $source;
		$video->save();
		if($video->id){
			$save_player = $this->insertAudioPlayer(0,$video->id);
			if($save_player){
				$this->createDfpAdUnit($save_player);
				Session::forget('audio_ses');
				Session::forget('player');
				$success = 1;
			}	
			if(Input::get('submit') == 'add-video-contest'){
				return Response::make('{"success": 1, "player_id": '.$save_player.', "player_title": "'.$title.'"}');
			}
			return Redirect::to(URL::to('admin/video'));
		}
		Log::info($video->id);
	}

	public function getVideos()
	{
		$video = new Video;
		return $video->where('user_id','=',Session::get('id'))->get();
	}

	private function insertAudioPlayer($offer_file_id,$video_id){
		$artist_name = Input::get('artist_name');
		$album_name = Input::get('album_name');
		$user_id = Session::get('id');
		$image_id = Input::has('image_id')? Input::get('image_id') : 0;
		
		$player = new Player;
		$player->user_id = $user_id;
		$player->video_id = $video_id;
		$player->artist_name = $artist_name;
		$player->album_name = $album_name;
		$player->image_id = $image_id;
		$player->offer_file_id = $offer_file_id;
		$player->save();
							
		return $player->id;
	}

	private function createOfferFile(){	
		$cake_api_key = "u0jVLuBLdfozIYc0fxKW5a10cRWBzm";
		$ApiDomain = "http://rsc.network-stats.com";
		$base = $ApiDomain.'/api/1/add.asmx/Creative?';
		 
		$params = array(
			'api_key' => $cake_api_key
			,'offer_id' => Session::get('offer_id')
			,'creative_name' => 'player for offer id '.Session::get('offer_id')
			,'creative_type_id' => 4
			,'creative_status_id' => 1
			,'offer_link' => 'http://dev.amplify.fm/wp/download-page/'
			,'notes' => 'Player creative'
		);
		 
		$url = $base . http_build_query( $params );
		//echo $url;
		$result = simplexml_load_file($url);
		
		//echo '<pre>';
		//echo $result[0];
		//echo '</pre>';
		//$data = json_decode( $result );
		//$data = $data->response->data;
		//$offer = $data->{$offer_id}; //ang number 4 kay pwede kwaon as GET or SESSION
		if(isset($result[0])){
			return $result[0];
		}else{
			Session::put('error_msg', 'Error on API');
		}
		return false;
		//echo '</pre>';
	}

	private function createDfpAdUnit($id){
		$dfp = new DfpApi;
		$user_id = Session::get('id');
		$ad_unit_ids = $dfp->createAdunit($user_id,$id);
		if($ad_unit_ids){
			$dfp = array();
			for($x = 0; $x < 4; $x++){
				$position = '';

				switch($x){
					case 0:
					$position = 'top';
					break;
					case 1:
					$position = 'left';
					break;
					case 2:
					$position = 'left_resp';
					break;
					case 3:
					$position = 'bottom';
					break;
				}

				$dfp[] = array('user_id' => $user_id,
							   'player_id' => $id,
							   'position' => $position,
						       'ad_unit_id' => $ad_unit_ids[$x]);
			}

			DB::table('dfp')->insert($dfp);
		}else{

			Log::info('No ad unit created');
		}				
	}

}
