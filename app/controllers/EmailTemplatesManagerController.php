<?php

class EmailTemplatesManagerController extends \BaseController {

	protected $layout = 'layouts.default';
	private $user_id;

	function __construct(){
		$this->user_id = Session::get('id');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->title = 'Super Admin';
		$this->layout->active = array('title' =>'Dashboard');
		$this->layout->css = array(asset('css/tables.css'),
								   asset('css/dashboard.css'));
		/*$this->layout->js = array(asset('assets/chart/chart.min.js'),
								  asset('js/chart.js'),
								  asset('js/admin-home.js')); */

		$this->layout->content = View::make('superadmin.email-templates',array('url' => URL::to('/')));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
