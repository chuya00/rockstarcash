<?php

class AudioOwnerController extends \BaseController {

	public function __construct(){
		$this->afterFilter(function ($route, $request, $response) {
	        $response->headers->set('Access-Control-Allow-Origin', '*');
	        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
	        $response->headers->set('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept, Authorization, X-Request-With');
	        return $response;
	    });
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$dfp_log_id = Input::get('i');
		$player = DfpLog::where('id','=',$dfp_log_id)->first();
		$date = new DateTime();

		$auth = Hash::make($date->getTimestamp());

		$audio_owner = new AudioOwner;
		$audio_owner->player_id = $player->player_id;
		$audio_owner->auth_code = $auth;
		$audio_owner->user_email = Input::get('email');
		$audio_owner->save();

		$player_model = Player::where('id','=',$player->player_id)->first();

		$this->sendDownloadLink(Input::get('email'),$player_model->artist_name,$player_model->album_name,$auth,$player->player_id);

		return Response::make(json_encode(array('code' => $auth)));
	}

	private function sendDownloadLink($email,$artist_name,$album_name,$auth,$player){

		$m = new Mandrill_Api();
		$admin = AdminSettings::orderBy('id', 'DESC')->first();
		$email_template = isset($admin->EmailTemplate->filename) ? $admin->EmailTemplate->filename : "6_download-email2.html";
		$song = $album_name.' by '.$artist_name;

		$m->sendTemplate(array('name' => $email_template,
							   'artist_name' => $song,
							   'contest_title' => '<a href="http://rockstarcash.com/download?auth='.$auth.'&player='.$player.'" >Download Link</a>',
							   'to' => $email, 
							   'subject' => 'Rockstarcash Download link',
							   'send_at' => null));

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
