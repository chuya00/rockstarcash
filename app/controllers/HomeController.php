<?php
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;
use Facebook\FacebookRedirectLoginHelper;
class HomeController extends \BaseController {

	private $facebook = null;

	public function __construct(){
		$this->facebook = new Facebook(array(
			  'appId'  => Config::get('facebook.appId'),
			  'secret' => Config::get('facebook.secret'),
			  'allowSignedRequest' => false
		));
	}

	public function index()
	{
		return View::make('home',array('title' => 'Rockstar Cash - We make music make money!'));
	}

	public function thankyou()
	{
		return View::make('thankyou', array('title' => 'Thank you - Rockstarcash.com'));
	}

	public function beta(){
		$css = '<style>
				.radio-name{
					margin-left: 10px;
					position: relative;
					top: 4px;
				}
				.radio-adjust{
					position: relative;
				}
				#msg-container{
					display: none;
					border: 1px solid #FF0000;
					background-color: #FF9999;
					color: #000;
					padding: 4px;
					margin-bottom: 5px;
				}
				.content-logo{
					text-align: center;
				}
				</style>';

		return View::make('beta', array('title' => 'Beta Signup - Rockstarcash.com', 'css' => $css, 'exclude_nagging_js' => 1));
	}

	public function contactUs(){
		/*
		 * Ensconce v1.0 - simple email contact script
		 * (c) Web factory Ltd
		 * www.webfactoryltd.com
		**/

		/* Enter the email address on which you want to receive the contat form data
		**/
		  //$myEmail = 'pb@rockstarlabs.fm';
		//$myEmail = 'chuya000@gmail.com';

		/* Enter the subject of the email with contact form data
		**/
		  //$emailSubject = 'New Rockstar Cash form submission';

		/**** DO NOT EDIT BELOW THIS LINE ****/
		/**** DO NOT EDIT BELOW THIS LINE ****/
		  //ob_start();

		  // only AJAX calls allowed
		  /*if (!isset($_SERVER['X-Requested-With']) && !isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		    response('err', 'ajax');
		  }*/
		  /*
		  // prepare headers
		  $headers  = "MIME-Version: 1.0\n";
		  $headers .= "Content-type: text/plain; charset=UTF-8\n";
		  $headers .= "X-Mailer: PHP " . PHP_VERSION . "\n";
		  $headers .= "From: {$myEmail}\n";
		  $headers .= "Return-Path: {$myEmail}";

		  // construct the message
		  $tmp = date('r');
		  $message = "The form was submited from {$_SERVER["HTTP_HOST"]} on $tmp by a person who's IP is: {$_SERVER['REMOTE_ADDR']}\n";
		  foreach ($_POST as $field => $value) {
		    $message .= $field . ': ' . $value . "\n";
		  }
		  $message .= "\nHave a good one!";


		  if (@mail($myEmail, $emailSubject, $message, $headers)) {
		  	$this->saveContactUsDetails();
		    $this->response('ok', 'sent');
		  } else {
		    $this->response('err', 'notsent');
		  }

		  $this->response('err', 'undefined');
			*/
		  $id = $this->saveContactUsDetails();
		  if(isset($id) || $id != null){
		  	$this->response('ok', 'sent');
		  }else{
		  	$this->response('err', 'notsent');
		  }
		  
	}

	private function saveContactUsDetails(){
		$name = Input::get('name');
		$email = Input::get('email');
		$message = Input::get('message');

		$cu = new ContactUs;
		$cu->name = $name;
		$cu->email = $email;
		$cu->message = $message;
		$cu->save();
		return $cu->id;
	}

	private function response($responseStatus, $responseMsg) {
	    $out = json_encode(array('responseStatus' => $responseStatus, 'responseMsg' => $responseMsg));

	    ob_end_clean();
	    die($out);
	}

	public function captcha($captcha = ''){
		/*
		 * Ensconce v1.0 - captacha anti SPAM script
		 * (c) Web factory Ltd
		 * www.webfactoryltd.com
		**/

		define('MAX_RAND_NB', 9);

		/**** DO NOT EDIT BELOW THIS LINE ****/
		/**** DO NOT EDIT BELOW THIS LINE ****/
		  ob_start();
		  session_start();

		  // only AJAX calls allowed
		  if (!isset($_SERVER['X-Requested-With']) && !isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		    die("0");
		  }
		  $generate = Input::get('generate');
		  $captcha = Input::get('captcha');
		  if (isset($generate)) {
		    $a = rand(1, MAX_RAND_NB);
		    $b = rand(1, MAX_RAND_NB);

		    if ($a > $b) {
		      $out = "$a - $b";
		      $_SESSION['captcha'] = $a - $b;
		    } else {
		      $out = "$a + $b";
		      $_SESSION['captcha'] = $a + $b;
		    }
		    die($out);
		  } elseif (isset($captcha)) {
		      if($captcha == $_SESSION['captcha']) {
		        die('true');
		      } else {
		        die('false');
		      }
		  }

		  die("0");
	}
}
