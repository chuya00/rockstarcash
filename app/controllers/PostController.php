<?php

class PostController extends \BaseController {

	protected $layout = 'layouts.default';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	public function showPostPage(){
		$this->layout->title = 'Post Message';
		$this->layout->active = array('title' =>'Dashboard');
		$this->layout->css = array(asset('assets/summernote/summernote.css'),
								   asset('assets/summernote/summernote-bs3.css'),
								   '//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css');
		$this->layout->js = array(asset('assets/summernote/summernote.min.js'),
								  asset('js/post.js'));
		$this->layout->content = View::make('admin.post');
	}

	public function savePost(){
		$post = new Post;

		$user_id = Session::get('id');
		$title = Input::get('title');
		$message = Input::get('message');
		$date = Input::get('date-to-send');
		$validator = Validator::make(array('title' => $title,
			'message' => $message,
			'date_to_send' => $date
			), 
		$post->rules());

		if($validator->fails()){
			$messages = $validator->messages();
			return Redirect::to('admin/post')->withErrors($validator);
		}

		$post->title = $title;
		$post->message = $message;
		$post->date_to_send = $date;
		$post->user_id = $user_id;
		$post->save();

		$saved_id = $post->id;

		if($saved_id){
			return Redirect::to('admin/post')->with('message','Post saved and scheduled');
		}else{
			return Redirect::to('admnin/post')->with('error_message','Something went wrong while saving your post.');
		}
	}

}
