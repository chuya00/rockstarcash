<?php

class AdminHomeController extends BaseController {

	protected $layout = 'layouts.default';

	public function index()
	{
		$this->layout->title = 'Home';
		$this->layout->active = array('title' =>'Dashboard');
		$this->layout->css = array(asset('css/tables.css'),
								   asset('css/dashboard.css'));
		$this->layout->js = array(asset('assets/chart/chart.min.js'),
								  asset('js/chart.js'),
								  asset('js/admin-home.js'));

		$this->layout->content = View::make('admin.home',array('stats' => new StatReports,
															   'plays_by_city' => $this->getPlaysData('city'),
															   'plays_by_country' => $this->getPlaysData('country'),
															   'plays_by_site' => $this->getPlaysData('site'),
															   'daily_stat' => $this->getPlayDownloadRatio(),
															   'top_songs' => $this->getTopSongs(),
															   'players' => $this->getPlayers(),
															   'url' => URL::to('/')));
	}

	public function getReports(){
		$date_type  = Input::get('date_type');
		$range      = $this->getDateArray($date_type);
		$reports    = new InventoryReport();
		$player     = new Player;
		$players    = $player->where('user_id','=',Session::get('id'))->select('id')->get();
		$chart_name = Input::get('chart_name'); 

		////Log::info($this->playerIdToArray($players));
		////Log::info($range);
		$report_id = $reports->createInventoryReport(Session::get('id'),$this->playerIdToArray($players),$range['start'],$range['end']);
		////Log::info($players);
		////Log::info($this->playerIdToArray($players));
		//Log::info($range);
		//Log::info($report_id);
		$cols = $reports->getReportResult($report_id);
		////Log::info($cols);

		if($chart_name == 'revenue-source'){
			////Log::info($cols);
			if(!is_object($cols)){
				return Response::make(json_encode(array('message' => 'Error: cols not an object')));
			}
			if($cols->Row){
				$cake = $this->getDownloadRevenue($range);
				$resp['cake'] = $cake;
				$resp['dfp'] = $this->cleanDfpResult($cols,$range,$date_type);
				return Response::make(json_encode($resp));
				//return json_encode(array('message' => 'Naa'));
			}
			if($cols->Total){
				$cake = $this->getDownloadRevenue($range);
				$resp['cake'] = $cake;
				$resp['dfp'] = $this->cleanDfpNoResult($cols,$range);
				return Response::make(json_encode($resp));
				//return json_encode(array('message' => 'Naa'));
			}
		}else{

			if(!is_object($cols)){
				return Response::make(json_encode(array('message' => 'Error: cols not an object')));
			}
			if($cols->Row){
				//$resp['dfp'] = $this->cleanDfpResult($cols,$range,$date_type);
				//return Response::make(json_encode($resp));
				return Response::make(json_encode($this->cleanDfpResult($cols,$range,$date_type)));
				//return json_encode(array('message' => 'Naa'));
			}
			if($cols->Total){
				//$cake = $this->getDownloadRevenue($range);
				//$resp['cake'] = $cake;
				//$resp['dfp'] = $this->cleanDfpNoResult($cols,$range);
				//return Response::make(json_encode($resp));
				return Response::make(json_encode($this->cleanDfpNoResult($cols,$range)));
				//return json_encode(array('message' => 'Naa'));
			}
		}
		
		return Response::make(json_encode(array('message' => 'Error')));
		
	}

	public function getInitReports(){
		$date_type = Input::get('date_type');
		$range     = $this->getDateArray($date_type);
		$reports   = new InventoryReport();
		$player    = new Player;
		$players   = $player->where('user_id','=',Session::get('id'))->select('id')->get();
		////Log::info($this->playerIdToArray($players));
		////Log::info($range);
		$report_id = $reports->createInventoryReport(Session::get('id'),$this->playerIdToArray($players),$range['start'],$range['end']);
		////Log::info($players);
		////Log::info($this->playerIdToArray($players));
		//Log::info($range);
		//Log::info($report_id);
		$cols = $reports->getReportResult($report_id);
		////Log::info($cols);
		if(!is_object($cols)){
			return Response::make(json_encode(array('message' => 'Error: cols not an object')));
		}
		if($cols->Row){
			$cake = $this->getDownloadRevenue($range);
			$resp['cake'] = $cake;
			$resp['dfp'] = $this->cleanDfpResult($cols,$range,$date_type);
			return Response::make(json_encode($resp));
			//return json_encode(array('message' => 'Naa'));
		}
		if($cols->Total){
			$cake = $this->getDownloadRevenue($range);
			$resp['cake'] = $cake;
			$resp['dfp'] = $this->cleanDfpNoResult($cols,$range);
			return Response::make(json_encode($resp));
			//return json_encode(array('message' => 'Naa'));
		}
		return Response::make(json_encode(array('message' => 'Error')));
		
	}

	private function getDownloadRevenue($range){

		//$offer_complete_log = new OfferCompleteLog;
		$user_id = Session::get('id');
		//$CPA = 0.7;

		$start = date('Y-m-d',strtotime($range['start']));
		$end = date('Y-m-d',strtotime($range['end']));

		$q = "SELECT 
			  DISTINCT(ocl.date) AS date,
			  (SELECT COUNT(ocll.id) FROM dfp_log ocll, player pp WHERE date = ocl.date AND ocll.player_id = pp.id AND pp.user_id = $user_id AND converted = 1) AS count,
			  IFNULL((SELECT cpa FROM dfp_orders do WHERE do.order_id = ocl.order_id AND do.date <= ocl.date ORDER BY id DESC LIMIT 1),0) AS cpa,
			  (SELECT COUNT(ocll.id) FROM dfp_log ocll, player pp WHERE ocll.player_id = pp.id AND pp.user_id = $user_id AND converted = 1 AND ocll.date BETWEEN '$start' AND '$end') AS total
			  FROM dfp_log ocl, player p
			  WHERE ocl.player_id = p.id
			  AND p.user_id = $user_id
			  AND converted = 1
			  AND ocl.date BETWEEN '$start' AND '$end'";
		//Log::info($q);
		//Log::info($range);
		//Log::info($start.' '.$end);
		$log = DB::select(DB::raw($q));

		$res = array();
		$start_date = $this->changeMonthFormat($range['start']);
		$end_date = $this->changeMonthFormat($range['end']);
		foreach($log as $l){
			$date = "";

			if(isset($l->date)){
				$temp = explode('-',$l->date);
				$date = intval($temp[1]).'/'.intval($temp[2]).'/'.$temp[0];
				$date = date('n/j/y',strtotime($date));
			}			
			$CPA = $l->cpa;
			$res[] = array('start_date'     => $start_date,
						   'end_date'       => $end_date,
						   'date'           => $date,
						   'revenue'        => '$'.floatval($l->count) * $CPA,
						   'total'          => $l->total);
		}

		if(count($res) == 0){
			$res[] = array('start_date'     => $start_date,
						   'end_date'       => $end_date,
						   'date'           => "",
						   'revenue'        => '$0.00',
						   'total'          => '$0.00');
		}

		return $res;
	}

	private function getCPA(){

	}

	private function changeMonthFormat($date){ //walaon ang leading zeros sa month
		$temp = explode("-", $date);
		$y = $temp[0];
		$m = $temp[1];
		$d = $temp[2];

		return $y."-".intval($m)."-".$d;
	}

	private function cleanDfpNoResult($dfp,$range){
		$res = array();

		//$row = $dfp->Row;
		$total = $dfp->Total->Column[3]->Val;
		$res[] = array('start_date' => $range['start'],
					   'end_date'   => $range['end'],
			           'date'       => '', 
					   'ad_name'    => '',
					   'revenue'    => '$0.00',
					   'total'      => $total);

		return $res;
	}

	private function cleanDfpResult($dfp,$range,$date_type){
		$res = array();

		$row = $dfp->Row;
		$total = $dfp->Total->Column[3]->Val;

		for($x = 0; $x < count($row); $x++){

			$res[] = array('start_date'     => $range['start'],
						   'end_date'       => $range['end'],
						   'date'           => $row[$x]->Column[2]->Val, 
						   'ad_name'        => $row[$x]->Column[1]->Val,
						   'revenue'        => $row[$x]->Column[3]->Val,
						   //'revenue_source' => $rev_source,
						   'total'          => $total);
		}

		return $res;
	}

	private function playerIdToArray($pids){
		$res = array();

		foreach($pids as $p){
			$res[] = $p->id;
		}

		return $res;
	}

	private function getDateArray($range){
		switch($range){
			case 'daily':
				return array('start' => date('Y-m-d'),'end' => date('Y-m-d'));
				break;

			case 'weekly':
				return array('start' => date('Y-m-d', strtotime('-1 week')), 'end' => date('Y-m-d'));
				break;

			case 'monthly':
				return array('start' => date('Y-m-d', strtotime('-1 month')), 'end' => date('Y-m-d'));
				break;
		}
	}

	public function test(){
		//$stats = new FbDetail;
		$test = FbDetail::find(2);
        
		return View::make('test',array('test' => $test));
	}

	public function viewLogin()
	{
		if (Auth::check())
		{
		    return Redirect::to('admin');
		}
		return View::make('login');
	}

	public function login()
	{
		$email = Input::get('email');
		$password = Input::get('password');
		if(Auth::attempt(Input::only('email', 'password'))){
			Session::put('offer_id',Auth::user()->offer_id);
			Session::put('id',Auth::user()->id);
			Session::put('name',Auth::user()->name);
			Session::put('u_type',Auth::user()->user_type);
			$fb_d = new FbDetail;
			$fb = $fb_d->where('user_id','=',Auth::user()->id)->select('details')->first();
			Session::put('fb-details',$fb->details);
			Session::put('fb-id',Auth::user()->fb_id);
			//return Redirect::to('admin');
			return Redirect::intended('admin');
		}

		return Redirect::to('admin/login')->with('message','Invalid email or password');
	}

	public function logout(){
		Auth::logout();
		return Redirect::to('/');
	}

	public function showForgotPassword(){
		if (Auth::check())
		{
		    return Redirect::to('admin');
		}
		return View::make('forgot-password');
	}

	private function getPlayers(){
		$player = new Player;
		$res = $player->where('user_id','=',Session::get('id'))
					  ->select('id','audios','offer_file_id','artist_name','album_name')
					  ->get();

		return $res;
	}

	private function getPlaysData($from, $stat = ''){
		$user_id = Session::get('id');
		$where_stat = "";

		$date_now = date('Y-m-d');
		$date = date('Y-m-d');

		if($stat != ''){
			
			switch($stat){
				case 'week':
					$date = date('Y-m-d', strtotime('-1 week'));
					break;

				case 'month':
					$date = date('Y-m-d', strtotime('-1 month'));
					break;

				case 'year':
					$date = date('Y-m-d', strtotime('-1 year'));					
					break;
			}
			
		}

		$where_stat = " AND (date BETWEEN '$date' AND '$date_now') ";

		$sql = "SELECT DISTINCT($from), 
				(SELECT COUNT(count_plays) FROM player_log pplog, player pp WHERE $from = plog.$from AND pplog.player_id = pp.id AND pp.user_id = $user_id) AS plays
				FROM player_log plog, player p
				WHERE plog.player_id = p.id
				AND p.user_id = $user_id
				$where_stat
				ORDER BY plays DESC
				LIMIT 5";

		$res = DB::select( DB::raw($sql) );

		return $res;
	}

	private function getDailyStat(){
		$user_id = Session::get('id');

		$sql = "SELECT COUNT(count_plays) AS plays FROM player_log plog, player p
				WHERE plog.player_id = p.id
				AND p.user_id = $user_id";

		$res = DB::select( DB::raw($sql) );
		$stats = array();
		if($res){
			foreach($res as $row){
				$stats['plays'] = $row->plays;
			}
		}
		
		return $stats;
	}

	private function getPlayDownloadRatio(){
		$stats = $this->getDailyStat();
		$ratio = array('plays' => 0.0, 'downloads' => 0.0);
		if(count($stats) > 0){
			$plays = $stats['plays'];
			$downloads = 0.0; //temporary
			$total = $plays + $downloads;

			if($total > 0){
				$ratio['plays'] = ( $plays / $total ) * 100;
				$ratio['downloads'] = ( $downloads / $total ) * 100;
			}else{
				$ratio['plays'] = 0;
				$ratio['downloads'] = 0;
			}
		}
		
		if($ratio['plays'] > 0){
			$ratio['conversion'] = ($ratio['downloads'] / $ratio['plays']) * 100;
		}else{
			$ratio['conversion'] = 0;
		}
		
		$ratio['count_plays'] = $stats['plays'];
		$ratio['count_downloads'] = 0; //temporary

		return $ratio;
	}

	private function getTopSongs($stat = ''){
		return false;
		$user_id = Session::get('id');
		$top_songs = array();
		$where_stat = "";

		$date_now = date('Y-m-d');
		$date = date('Y-m-d');
		if($stat != ''){
			
			switch($stat){
				case 'week':
					$date = date('Y-m-d', strtotime('-1 week'));
					$where_stat = " AND (date BETWEEN '$date' AND '$date_now') ";
					break;

				case 'month':
					$date = date('Y-m-d', strtotime('-1 month'));
					$where_stat = " AND (date BETWEEN '$date' AND '$date_now') ";
					break;

				case 'year':
					$date = date('Y-m-d', strtotime('-1 year'));
					$where_stat = " AND (date BETWEEN '$date' AND '$date_now') ";
					break;
			}
			
		}

		$sql = "SELECT DISTINCT(audios),
				p.offer_file_id,
				(SELECT COUNT(count_plays) FROM player_log WHERE player_id = plog.player_id) AS plays
				FROM player_log plog, player p
				WHERE plog.player_id = p.id
				AND p.user_id = $user_id
				$where_stat
				ORDER BY plays DESC
				LIMIT 5";

		$res = DB::select( DB::raw($sql) );

		if($res){
			foreach($res as $row){
				////Log::info($this->getCreativeSummary($row->offer_file_id)->success);
				$creative = $this->getCreativeSummary($row->offer_file_id,$date,$date_now);
				$impressions = 0;
				$conversions = 0;
				if($creative){
					$impressions = $creative->views;
					$conversions = $creative->conversions;
				}
				$top_songs[] = array('title' => $this->getSongDetails($row->audios)->title,
									 'plays' => $row->plays,
									 'downloads' => $conversions, //temporary
									 'impressions' => $impressions, //temporary
									 'total' => (($row->plays) + 0 + 0));
			}
		}

		return $top_songs;
	}

	private function getCreativeSummary($offer_file_id,$start_date,$end_date){
		$stat = new StatReports;
		$resp = $stat->getStats($start_date,$end_date);
		if(isset($resp->success) && $resp->success == 'true'){
			return $resp->creatives->creative_summary;
		}
		
		return false;
	}

	private function getSongDetails($ids){
		$ids_arr = explode(',',$ids);
		$audio = new Audio;

		return $audio->where('id','=',$ids_arr[(count($ids_arr)-1)])->first();
	}

	public function getStat(){
		$stat = Input::get('stat');
		$data_set = Input::get('data_set');
		$res = null;

		if($data_set == 'site'){
			$res = $this->getPlaysData($data_set,$stat);
			return Response::make($this->renderTopSitesTable($res,$data_set));
		}
		if($data_set == 'song'){
			$res = $this->getTopSongs($stat);
			return Response::make($this->renderTopSongsTable($res,$data_set));
		}

		$res = $this->getPlaysData($data_set,$stat);
		return Response::make($this->renderStatTable($res,$data_set));
	}

	private function renderStatTable($res,$data){
		$html = '';
		foreach($res as $row){
			$html .= '
			<tr>
			<td>'.ucfirst($row->{$data}).'</td>
			<td>'.$row->plays.'</td>
			<td>0</td>
			<td>'.$row->plays.'</td></tr>';
		}

		return $html;
	}

	private function renderTopSitesTable($res,$data){
		$html = '';
		foreach($res as $row){
			$html .= '
			<tr>
			<td>'.$row->{$data}.'</td>
			
			<td>'.$row->plays.'</td>
			</tr>';
		}

		return $html;
	}

	private function renderTopSongsTable($res,$data){
		$html = '';
		$count = 1;
		foreach($res as $row){
			$html .= '
			<tr>
			<td>'.($count++).'</td>
			<td>'.$row['title'].'</td>
			<td>'.$row['plays'].'</td>
			<td>0</td>
			<td>0</td>
			<td>'.(intval($row['plays']) + intval($row['downloads'])).'</td>
			</tr>';
		}

		return $html;
	}
	/*
	public function getDownloadRevenue(){

		$offer_complete_log = new OfferCompleteLog;

		$offer_complete_log->player_id     = Input::get('player_id');
		$offer_complete_log->cake_offer_id = Input::get('offer_id');
		$offer_complete_log->date          = date('Y-m-d');
		$offer_complete_log->time          = date('G:i:s');
		$offer_complete_log->country_code  = Input::get('country_code');
		$offer_complete_log->country       = Input::get('country');
		$offer_complete_log->city          = Input::get('city');
		$offer_complete_log->ip_address    = CustomHelper::getClientIp();
		$offer_complete_log->save();

		return $offer_complete_log;

	}*/

}
