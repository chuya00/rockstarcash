<?php

class ContestController extends \BaseController {

	protected $layout = 'layouts.default';
	private $user_id;

	function __construct(){
		$this->user_id = Session::get('id');
	}

	public function index()
	{
		$this->layout->title = 'Manage Contests';
		$this->layout->active = array('title' =>'Manage Contests');
		$this->layout->css = array(asset('css/tables.css'),
								   asset('css/contest.css'));
		$this->layout->js = array(asset('assets/jquery_fileuploader/js/vendor/jquery.ui.widget.js'),
								  asset('assets/jquery_fileuploader/js/jquery.iframe-transport.js'),
        						  asset('assets/jquery_fileuploader/js/jquery.fileupload.js'),
        						  asset('assets/image-picker/image-picker.js'),
								  asset('js/contest.js'));

		$this->layout->content = View::make('admin.contest',array('players'         => $this->getYTPlayers(),
																  'contests'        => $this->getContests(),
																  'contest_types'   => $this->getContestTypes(),
																  'email_templates' => $this->getEmailTemplates(),
																  'mailchimp' 		=> $this->getMailchimpKeys(),
																  'count'           => 0,
															      'url'             => URL::to('/')));
	}

	private function getContestTypes(){
		$types = new ContestType;
		return $types->get();
	}

	private function getPlayers(){
		$player = new Player;
		$res = $player->where('user_id','=',$this->user_id)
					  ->get();

		return $res;
	}

	private function getYTPlayers(){
		//$video = new Player;
		$res = Player::with(array('video' => function($q){
						$q->where('source','=','youtube');
				     }))
					 ->whereNotIn('player.id',function($q){
							$q->from(with(new Contest)->getTable())
							  ->select('player_id')
							  ->where('user_id','=',$this->user_id);
					 })
					 ->where('user_id','=',$this->user_id)
					 ->whereNotNull('video_id')
					 ->distinct()
					 ->get();
		return $res;
	}

	private function getContests($contest_id = null){
		//$contest = new Contest;
		if($contest_id == null){
			return Contest::with('player.video')
			->where('user_id','=',$this->user_id)
			->whereNotNull('player_id')
			->orderBy('id','DESC')
			->get();
		}else{
			return Contest::with('video')->with('contestAlbumImage')
									     ->where('user_id','=',$this->user_id)
										 ->where('contest.id','=',$contest_id)
									 	 ->whereNotNull('video_id')
									 	 ->first();
		}
		
	}

	public function getContestDetails(){
		$c_id = Input::get('c_id');
		$contest = $this->getContests($c_id);
		$res = array();
		//Log::info($contest);
		$res['id'] = $contest->id;
		$res['title'] = $contest->title;
		$res['video_id'] = $contest->video_id;
		$res['play'] = $contest->play;
		$res['itunes'] = $contest->itunes;
		$res['amazon'] = $contest->amazon;
		$res['description'] = $contest->description;

		return Response::make(json_encode($res));
	}

	private function getEmailTemplateId($filename){
		return EmailTemplate::where('filename',$filename)
							  ->where('user_id',$this->user_id)
							  ->select('id')
							  ->first();
	}

	public function save()
	{
		$contest = new Contest;
		//$video_id = Input::get('video');
		$title = Input::get('title');
		$play = (Input::get('play') != "" || Input::get('play') != null)? Input::get('play') : 0;
		$itunes = Input::get('itunes-link');
		$amazon = Input::get('amazon-link');
		$player_id = Input::get('video');
		$image_id = Input::get('image-id');
		

		$validator = Validator::make(array('player_id' => $player_id,
										   'title'  => $title
			), 
		$contest->rules(),
		$contest->messages());
		
		if($validator->fails()){
			$messages = $validator->messages();
			return Redirect::to('admin/contest')->withErrors($validator);
		}
		
		$contest->user_id = $this->user_id;
		//$contest->video_id = $video_id;
		$contest->player_id        = $player_id;
		$contest->title            = $title;
		$contest->itunes           = $itunes;
		$contest->amazon           = $amazon;
		$contest->image_id         = $image_id;

		if(Input::get('email-template-sel') == '' || Input::get('email-template-sel') == 'rsc-email'){
			$template_id = $this->getEmailTemplateId(Input::get('email-template'));
			$welcome_email_id = ($template_id)? $template_id->id : 0;
			$contest->welcome_email_id = $welcome_email_id;
		}else{
			$contest->mailchimp_id = Input::get('mc_id');
			$contest->list_id = Input::get('mc_list_id');
		}	

		$contest->play             = $play;
		$contest->save();
		if($contest->id){
			/*if(Input::get('image-id') != ''){
				$contest_album_image = new ContestAlbumImage;
				$contest_album_imag->find(Input::get('image-id'));
				$contest_album_image->contest_id = $contest->id;
				$contest_album_image->save();
			}*/
			$contest_module = new ContestModule;
			$contest_module->contest_id = $contest->id;
			$contest_module->contest_type_id = Input::get('contest_type');
			$contest_module->top_left = Input::get('top_left');
			$contest_module->bottom = Input::get('bottom');
			$contest_module->save();

			if($contest_module->id){
				return Redirect::to(URL::to('admin/contest'));
			}
			
		}
		Log::info($contest->id);
	}

	public function update()
	{
		$contest = Contest::find(Input::get('c_id'));
		$video_id = Input::get('music');
		$title = Input::get('title');
		$desc = Input::get('description');
		$play = (Input::get('play') != "" || Input::get('play') != null)? Input::get('play') : 0;
		$itunes = Input::get('itunes-link');
		$amazon = Input::get('amazon-link');
		/*
		$validator = Validator::make(array('video_id' => $video_id,
										   'title'  => $title,
										   'description'   => $desc
			), 
		$contest->rules(),
		$contest->messages());
		
		if($validator->fails()){
			$messages = $validator->messages();
			return Redirect::to('admin/contest')->withErrors($validator);
		}
		*/
		$contest->user_id = $this->user_id;
		$contest->video_id = $video_id;
		$contest->title = $title;
		$contest->itunes = $itunes;
		$contest->amazon = $amazon;
		$contest->description = $desc;
		$contest->play = $play;
		$contest->save();
		if($contest->id){
			/*$contest_album_image = ContestAlbumImage::find(Input::get('image-id'));
			$contest_album_image->contest_id = $contest->id;
			$contest_album_image->save();*/
			return Redirect::to(URL::to('admin/contest'))->with('message','Update Successful!');
		}
		//Log::info($contest->id);
	}

	public function saveAlbumImage(){
		$success = 0;
		$last_id = 0;
		$filename = '';
		if(Input::has('filename')){
			$image = $this->insertPlayerImage();
			if($image->id){
				$last_id = $image->id;
				$filename = $image->filename;
					//echo $last_id;
				$success = 1;
			}			
		}

		if($success == 1){
			return Response::make(json_encode(array("resp" => "1", "id" => $last_id,"filename" => $filename)));
		}else{
			return Response::make(json_encode(array("resp" => "0")));
		}
	}

	private function insertPlayerImage(){
		$filename = Input::get('filename');
		$user_id = $this->user_id;
		
		$album_image = new ContestAlbumImage;
		$album_image->user_id = $user_id;
		$album_image->filename = $filename;
		$album_image->save();

		/*
		$sql = "INSERT INTO album_image (user_id, filename)
				VALUES (".$user_id.",'".$filename."')";
		
		$res = mysqli_query($con,$sql);*/
							
		return $album_image;
	}

	private function getEmailTemplates(){
		$et = new EmailTemplate;

		return $et->where('user_id',$this->user_id)
		          ->get();
	}

	public function getMCLists(){
		$api_key = Input::get('key');

		$mc_lists = CustomHelper::getLists($api_key);
		if(!isset($mc_lists['status'])){
			$mc_id = $this->saveMailchimp($api_key);
			$mc_lists['mc_id'] = $mc_id;
		}
		return Response::make(json_encode($mc_lists));
	}

	private function saveMailchimp($api_key){
		$mc = new MailchimpModel;
		$count = $mc->where('api_key','=',$api_key)
		            ->where('user_id','=',$this->user_id)
		            ->count();

		if($count <= 0){
			$mc->user_id = $this->user_id;
			$mc->api_key = $api_key;
			$mc->save();
		}

		if($mc->id){
			return $mc->id;
		}else{
			$model = $mc->where('api_key','=',$api_key)
		             ->where('user_id','=',$this->user_id)
		             ->select('id')
		             ->first();

		    return $model->id;
		}
	}

	private function getMailchimpKeys(){
		$mc = new MailchimpModel;

		return $mc->where('user_id',$this->user_id)->get();
	}
}
