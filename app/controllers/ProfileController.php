<?php

class ProfileController extends \BaseController {

	protected $layout = 'layouts.default';

	public function index()
	{
		$user = $this->getUserData();

		$this->layout->title = $user->name;
		$this->layout->active = array('title' =>'');
		$this->layout->css = array(asset('css/tables.css'),
								   asset('css/buttons.css'));

		$this->layout->content = View::make('admin.profile',array('url' => URL::to('/'),
																  'user' => $this->getUserData()));
	}

	private function getUserData(){
		$user = new User;
		return $user->where('id','=',Session::get('id'))->first();
	}

	private function getUserFacebookData(){
		$fb = new FbDetail;

		return $fb->where('user_id','=',Session::get('id'))->first();
	}

}
