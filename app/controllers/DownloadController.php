<?php

class DownloadController extends \BaseController {

	public function __construct(){
		$this->afterFilter(function ($route, $request, $response) {
	        $response->headers->set('Access-Control-Allow-Origin', '*');
	        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
	        $response->headers->set('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept, Authorization, X-Request-With');
	        return $response;
	    });
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		//$client = new soapclient("http://rockstarcash.com/WebServiceSOAP/server.php?wsdl");
		//$enc = $client->getEncCode(Input::get('encode'));
		$auth = Input::get('auth');
		$auth_code = AudioOwner::where('auth_code','=',$auth)
								->where('player_id','=',Input::get('player'))
							    ->first();

		if($auth_code->id){
			$player = Player::where('id','=',Input::get('player'))->first();
			
			$geo = $this->getGeo();

			$cn_c = $geo['country_code'];
			$cn = $geo['country'];
			$city = $geo['city'];
			$date = date('Y-m-d');
			$time = date('g:iA');

			$download_log = new DownloadLog;
			$download_log->player_id = $auth_code->player_id;
			$download_log->date = $date;
			$download_log->time = $time;
			$download_log->country_code = $cn_c;
			$download_log->country = $cn;
			$download_log->city = $city;
			$download_log->save();

			$audio = Audio::where('id',intval($player->audios))->first();
			//$filepath = asset("assets/audio/".$audio->filename);
			$filepath = public_path()."/assets/audio/files/".$audio->filename;
			Log::info($filepath);
			$this->downloadAudioFile($filepath);
		}else{
			return Response::make('Auth code is invalid.');
		}
	}

	private function downloadAudioFile($file) { // $file = include path 
		/*if(file_exists($file)) {
			Log::info('file exists');
			$this->afterFilter(function ($route, $request, $response) {
		        $response->headers->set('Content-Description', 'File Transfer');
		        $response->headers->set('Content-Type', 'application/octet-stream');
		        $response->headers->set('Content-Disposition', 'attachment; filename='.basename($file));
		        $response->headers->set('Content-Transfer-Encoding', 'binary');
		        $response->headers->set('Expires', '0');
		        $response->headers->set('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
		        $response->headers->set('Pragma', 'public');
		        $response->headers->set('Content-Length', filesize($file));
		        readfile($file);
		        return $response;
		    });
		}*/
		
        if(file_exists($file)) {
        	Log::info('file exists');
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }

    }

    private function getGeo(){
		$res = RSC_GeoIp::getGeo(CustomHelper::getClientIp());
		$city = (isset($res->city))? $res->city->names->en : 'No City Data';
		$state = 'No State Data';
		if(isset($res->subdivisions)){
			if(isset($res->subdivisions->names->en)){
				$state = $res->subdivisions->names->en;
			}
			if(isset($res->subdivisions[0]->names->en)){
				$state = $res->subdivisions[0]->names->en;
			}
		}

		return array("country" => $res->country->names->en,
					 "country_code" => $res->country->iso_code,
					 "state_code" => $state,
					 "city" => $city);
	}

}
