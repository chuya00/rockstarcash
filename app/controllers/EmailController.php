<?php

class EmailController extends \BaseController {

	protected $layout = 'layouts.default';
	private $user_id;

	function __construct(){
		$this->user_id = Session::get('id');
	}

	public function showTemplatesPage()
	{
		$this->layout->title = 'Email Templates ';
		$this->layout->active = array('title' =>'Emails');
		$this->layout->css = array(asset('css/tables.css'));

		$this->layout->content = View::make('admin.email-templates',array('email_templates' => $this->getEmailTemplates(),
																		  'count' => 0,
																		  'url' => URL::to('/')));
	}

	public function showEmailJobsPage(){
		$this->layout->title = 'Email Jobs ';
		$this->layout->active = array('title' =>'Emails');
		$this->layout->css = array(asset('css/tables.css'),
							       '//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css');
		$this->layout->js = array(asset('js/email-jobs.js'),
								  asset('js/jquery.confirm.js'));
		$this->layout->content = View::make('admin.email-jobs',array('email_jobs' => $this->getEmailJobs(),
																		  'count' => 0,
																		  'url' => URL::to('/')));
	}

	public function showCreateEmailJobs(){
		$this->layout->title = 'Email Jobs ';
		$this->layout->active = array('title' =>'Emails');
		$this->layout->css = array(asset('css/tables.css'),
								   asset('css/create-email-jobs.css'),
								   '//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css');
		$this->layout->js = array(asset('js/create-email-jobs.js'),
								  asset('js/jquery.confirm.js'));
		$this->layout->content = View::make('admin.create-email-jobs',array('email_job_types' => $this->getEmailJobTypes(),
																			'email_templates' => $this->getEmailTemplates(),
																		    'count' => 0,
																		    'url' => URL::to('/')));
	}

	public function showEmailList(){
		$this->layout->title = 'Email List ';
		$this->layout->active = array('title' =>'Emails');
		$this->layout->css = array(asset('css/tables.css'),
								   asset('css/create-email-jobs.css'),
								   '//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css');
		$this->layout->js = array(asset('js/create-email-jobs.js'),
								  asset('js/jquery.confirm.js'));
		$this->layout->content = View::make('admin.email-list',array('email_list' => $this->getEmailList(),
																     'count' => 0,
																     'url' => URL::to('/')));
	}

	private function getEmailList(){
		$res = ContestUser::whereIn('contest_id',function($q){
						  	$q->from(with(new Contest)->getTable())
							  ->select('id')
							  ->where('user_id','=',$this->user_id);
						  })
						  ->groupBy('email')
						  ->get();

		/*
		$res = Contest::with(array('contestUser' => function($q){
							$q->select('id','email')
						  	  ->groupBy('email')
						  	  ->get();
						  }))
						  ->where('user_id',$this->user_id)
						  ->get();
		*/

		Log::info(DB::getQueryLog());
		Log::info($res);
		return $res;
	}

	public function handleSaveEmailJob(){
		if(Session::get('id') == 6 && Input::get('email-job-type') == '21'){
			$adminSettings = new AdminSettings;
			$adminSettings->admin_id = Session::get('id');
			$adminSettings->email_template_audio_file = Input::get('email-template');
			$adminSettings->save();

			return Redirect::to('admin/email/jobs/create')->with('message-suc','Successfully added a new email job!');
		}
		$validator = Validator::make(
		    array(
		        'name' => Input::get('job-name'),
		        'job_type' => Input::get('email-job-type'),
		        'subject' => Input::get('subject'),
		        'date_to_send' => Input::get('date-to-send')
		    ),
		    array(
		        'name' => 'required',
		        'job_type' => 'required',
		        'subject' => 'required',
		        'date_to_send' => 'required'
		    )
		);

		if($validator->fails()){
			$messages = $validator->messages();
			return Redirect::to('admin/email/jobs/create')->withErrors($validator);
		}else{
			$suc = $this->saveEmailJob();
			if($suc){
				return Redirect::to('admin/email/jobs/create')->with('message-suc','Successfully added a new email job!');
			}else{
				return Redirect::to('admin/email/jobs/create')->with('message','Something went wrong.');
			}			
		}	
	}

	public function showCreateTemplatesPage($from_contest = 0){
		return View::make('admin.create-email',array('url'          => URL::to('/'),
													 'js'           => asset('js/email-template-create.js'),
													 'from_contest' => $from_contest));
	}

	private function getEmailTemplates(){
		$et = new EmailTemplate;

		return $et->where('user_id','=',Session::get('id'))->get();
	}

	private function getEmailJobs(){
		return EmailJob::with('emailJobType')
					   ->where('trash','=',0)
					   ->where('user_id','=',Session::get('id'))
					   ->get();
	}

	private function getSingleEmailJob($id){
		return EmailJob::with('emailTemplate')
					   ->where('trash','=',0)
					   ->where('id','=',$id)
					   ->where('user_id','=',Session::get('id'))
					   ->first();
	}

	public function doTemplateDownload(){
		$filename = preg_replace('/[^a-z0-9\-\_\.]/i','',Input::get('filename'));
		if(!Session::has('id')){
			header("Cache-Control: ");
			header("Content-type: text/plain");
			header('Content-Disposition: attachment; filename="'.$filename.'"');
		}else{
			$sesid = preg_replace('/[^a-z0-9\-\_\.]/i','',Session::get('id'));
			$myfile = fopen(public_path()."/assets/email/email_templates/".$sesid."_".$filename, "w") or die("Unable to open file!");
			$txt = Input::get('content');
			fwrite($myfile, $txt);
			fclose($myfile);
			$this->saveTemplateToDB($sesid,$filename);

			$this->saveMandrillTemplate($sesid."_".$filename,public_path()."/assets/email/email_templates/".$sesid."_".$filename);
		}
	}

	private function saveMandrillTemplate($filename, $path){
		$m = new Mandrill_Api();
		$code = file_get_contents($path);
		$res = $m->addTemplate(array('name' => $filename, 'code' => $code));
	}

	private function saveTemplateToDB($sesid,$filename){
		$et = new EmailTemplate;
		$done = null;

		$exists = $et->where('filename','=',$filename)
					 ->where('user_id','=',$sesid)
					 ->count();

		if($exists > 0){
			$id = $et->where('filename','=',$filename)
					 ->where('user_id','=',$sesid)
					 ->select('id')
					 ->first();

			$template = EmailTemplate::find($id->id);	
			$template->filename = $filename;
			$done = $template->save();
		}else{
			$et->user_id = $sesid;
			$et->filename = $filename;
			$done = $et->save();
		}

		return $done;
	}

	private function hasPerformed($job_id){

		$mj = new MandrillJob;
		$count = $mj->where('job_id','=',$job_id)
					->whereNotNull('mandrill_job_id')
					->count();

		/*$query = "SELECT COUNT(job_id) AS count FROM mandrill_job WHERE job_id = $job_id AND mandrill_job_id IS NOT NULL";
		$res = mysqli_query($con,$query);
		$row = mysqli_fetch_array($res);
		$count = $row['count'];*/
		
		if($count > 0){
			return true;
		}
		
		return false;
	}

	private function isJobDone($job_id){
		
		$mj = new MandrillJob;
		$count = $mj->where('job_id','=',$job_id)
					->count();

		/*$query = "SELECT SUM(send) AS count FROM mandrill_job WHERE job_id = $job_id";
		$res = mysqli_query($con,$query);
		$row = mysqli_fetch_array($res);
		$count = $row['count'];*/
		
		if($count > 0){
			return true;
		}
		
		return false;
	}

	public function renderJobList(){

		if(Input::has('status')){
			$user_id = Session::get('id');
			$query = '';
			$html = '';
			if(Input::get('status') == 'waiting'){	
				$query = "SELECT ej.id, ej.name, ejt.name AS job_type, ej.job_type AS type_id, ej.assigned_date AS date
				FROM email_jobs ej, email_job_types ejt, mandrill_job mj
				WHERE ej.job_type = ejt.id
				AND mj.job_id = ej.id
				AND ej.trash = 0
				AND (SELECT COUNT(job_id) AS count FROM mandrill_job WHERE job_id = ej.id AND mandrill_job_id IS NOT NULL) = 0
				AND (SELECT SUM(send) AS count FROM mandrill_job WHERE job_id = ej.id) = 0
				AND user_id = ".$user_id."";
			}else
			if(Input::get('status') == 'scheduled'){	
				$query = "SELECT ej.id, ej.name, ejt.name AS job_type, ej.job_type AS type_id, ej.assigned_date AS date
				FROM email_jobs ej, email_job_types ejt, mandrill_job mj
				WHERE ej.job_type = ejt.id
				AND mj.job_id = ej.id
				AND ej.trash = 0
				AND (SELECT COUNT(job_id) AS count FROM mandrill_job WHERE job_id = ej.id AND mandrill_job_id IS NOT NULL) > 0
				AND (SELECT SUM(send) AS count FROM mandrill_job WHERE job_id = ej.id) = 0
				AND user_id = ".$user_id."";
			}else 
			if(Input::get('status') == 'done'){
				$query = "SELECT ej.id, ej.name, ejt.name AS job_type, ej.job_type AS type_id, ej.assigned_date AS date
				FROM email_jobs ej, email_job_types ejt, mandrill_job mj
				WHERE ej.job_type = ejt.id
				AND mj.job_id = ej.id
				AND ej.trash = 0
				AND (SELECT COUNT(job_id) AS count FROM mandrill_job WHERE job_id = ej.id AND mandrill_job_id IS NOT NULL) > 0
				AND (SELECT SUM(send) AS count FROM mandrill_job WHERE job_id = ej.id) = 1
				AND user_id = ".$user_id."";
			}else 
			if(Input::get('status') == 'trash'){
				$query = "SELECT ej.id, ej.name, ejt.name AS job_type, ej.job_type AS type_id, ej.assigned_date AS date
				FROM email_jobs ej, email_job_types ejt
				WHERE ej.job_type = ejt.id
				AND ej.trash = 1
				AND user_id = ".$user_id."";
			}
	//echo $query;
			//$res = mysqli_query($con,$query);
			Log::info($query);
			Log::info(Input::get('status'));
			$res = DB::select( DB::raw($query) );
			$count = 0;
			//while($row = mysqli_fetch_array($res)){
			foreach($res as $row){
				$html = '
				<tr id="tr'.$count.'">
				<td>'.$row->id.'</td>
				<td>'.$row->name.'</td>
				<td>'.$row->job_type.'</td>
				<td>'.CustomHelper::getDateToSend($row->type_id,$row->date).'</td>
				<td id="action'.$row->id.'">';
				if(Input::get('status') == 'job-pending'){
					$html .= '<button class="btn btn-s-md btn-primary perform-job" data-id="'.$row->id.'" data-job-type-name="'.$row->job_type.'" data-job-type="'.$row->type_id.'" >Start Job</button>';
				}else if(Input::get('status') == 'job-done'){
					$html .= '<a class="btn btn-s-md btn-warning" href="javascript:void(0)">Job Done</a>';
				}else if(Input::get('status') == 'job-progress'){
					$html .= '<button class="btn btn-s-md btn-warning cancel-job" data-id="'.$row->id.'" data-job-type-name="'.$row->job_type.'" data-job-type="'.$row->type_id.'" >Cancel Job</button>';
				}else if(Input::get('status') == 'job-trash'){
					$html .= '<button class="btn btn-s-md btn-warning restore-job" data-id="'.$row->id.'" data-job-type-name="'.$row->job_type.'" data-job-type="'.$row->type_id.'" >Restore</button>';
				}
				if(Input::get('status') != 'job-trash'){
					$html .= '<button class="btn btn-danger delete" data-id="'.$row->id.'" data-type="DELETE" >
								<i class="icon-trash "></i>
								<span>Delete</span>
							  </button>';
				}
				$html .= '</td></tr>';
				$count++; 
			}
		}else{
			echo 'No post data.';
		}

		return Response::make($html);

	}

	private function getEmailJobTypes(){
		$ejt = new EmailJobType;

		return $ejt->orderBy('order_list')->get();
	}

	private function saveEmailJob(){
		$user_id = Session::get('id');
		$job_name = Input::get('job-name');
		$email = Input::get('email');
		$template = Input::get('email-template');
		$job_type = Input::get('email-job-type');
		//$schedule = '';
		$a_d = Input::get('date-to-send').' 00:00:00';
		$subj = (Input::has('subject') && Input::get('subject') != '')? Input::get('subject') : 'No subject';
		/*
		if($job_type == '2' || $job_type == '3' || $job_type == '4'){
			if(isset($data['month-sched'])){
				$schedule = monthSched($data['month-sched']);
				$a_d = $data['month-sched'];
			}else if(isset($data['week-sched'])){
				$schedule = weekSched($data['week-sched']);
				$a_d = $data['week-sched'];
			}else if(isset($data['bi-sched'])){
				$schedule = weekSched($data['bi-sched']).','.weekSched($data['bi-sched2']);
				$a_d = $data['bi-sched'].','.$data['bi-sched2'];
			}
		}*/
		$ej = new EmailJob;
		$exist_id = $ej->where('name','=',$job_name)
					   ->where('user_id','=',$user_id)
					   ->first();
		
		$res = null;
		if($exist_id){			
			$up_ej = EmailJob::find($exist_id->id);
			$up_ej->name = $job_name;
			$up_ej->email_template_id = $template;
			$up_ej->subject = $subj;
			$up_ej->user_emails = $email;
			$up_ej->job_type = $job_type;
			$up_ej->assigned_date = $a_d;
			$res = $up_ej->save();
		}else{
			$ej->name = $job_name;
			$ej->email_template_id = $template;
			$ej->subject = $subj;
			$ej->user_emails = $email;
			$ej->job_type = $job_type;
			$ej->assigned_date = $a_d;
			$ej->user_id = $user_id;
			$res = $ej->save();
		}
		
		if($res){
			return Response::make('[{suc: 1}]');
		}else{
			return Response::make('[{suc: 0}]');
		}
	}

	private function weekSched($date){
		$d = intval(date('j'));
		$m = intval(date('n'));
		$y = intval(date('Y'));
		$timestamp = '';
		$incre = 1;
		while(true){
			//$total_days_in_month = cal_days_in_month(CAL_GREGORIAN, $m,$y);
			$total_days_in_month = intval(date('t', mktime(0, 0, 0, $m, 1, $y))); 
			if(($d+$incre) > $total_days_in_month){
				if($m == 12){
					$m = 1;
					$d = 0;
					$y = $y + 1;
				}else if($m < 12){
					$m = $m + 1;
					$d = 0;
				}
			}
			$day = date("N", mktime(0, 0, 0, $m, ($d+$incre), $y));		
			
			if((1 == intval($date))){
				$timestamp = $y.'-'.date('m',mktime(0, 0, 0, $m, ($d+$incre), $y)).'-'.date('d',mktime(0, 0, 0, $m, ($d+$incre), $y));
				break;
			}else if(2 == intval($date)){
				$timestamp = $y.'-'.date('m',mktime(0, 0, 0, $m, ($d+$incre), $y)).'-'.date('d',mktime(0, 0, 0, $m, ($d+$incre), $y));
				break;
			}else if(3 == intval($date)){
				$timestamp = $y.'-'.date('m',mktime(0, 0, 0, $m, ($d+$incre), $y)).'-'.date('d',mktime(0, 0, 0, $m, ($d+$incre), $y));
				break;
			}else if(4 == intval($date)){
				$timestamp = $y.'-'.date('m',mktime(0, 0, 0, $m, ($d+$incre), $y)).'-'.date('d',mktime(0, 0, 0, $m, ($d+$incre), $y));
				break;
			}else if(5 == intval($date)){
				$timestamp = $y.'-'.date('m',mktime(0, 0, 0, $m, ($d+$incre), $y)).'-'.date('d',mktime(0, 0, 0, $m, ($d+$incre), $y));
				break;
			}else if(6 == intval($date)){
				$timestamp = $y.'-'.date('m',mktime(0, 0, 0, $m, ($d+$incre), $y)).'-'.date('d',mktime(0, 0, 0, $m, ($d+$incre), $y));
				break;
			}else if(7 == intval($date)){
				$timestamp = $y.'-'.date('m',mktime(0, 0, 0, $m, ($d+$incre), $y)).'-'.date('d',mktime(0, 0, 0, $m, ($d+$incre), $y));
				break;
			}
			
			$incre++;
		}
		
		return $timestamp.' 00:00:00';
	}

	private function monthSched($day){
		$d = intval(date('j'));
		$m = intval(date('m'));
		$y = intval(date('Y'));
		
		if($m == 12 && $d > intval($day) ){
			$y = intval($y) + 1;
			$m = date('m',mktime(0, 0, 0, 1, intval($day), $y));
		}else if($m == 12 && $d < intval($day) ){
			$m = date('m',mktime(0, 0, 0, $m, intval($day), $y));
		}else if($m < 12 && $d < intval($day)){
			$m = date('m',mktime(0, 0, 0, $m, intval($day), $y));
		}else if($m < 12 && $d > intval($day)){
			$m = date('m',mktime(0, 0, 0, $m+1, intval($day), $y));
		}
		
		return $y.'-'.$m.'-'.$day.' 00:00:00';
	}

	public function performJob(){
		if(Input::has('job_id')){
			$job = $this->getSingleEmailJob(Input::get('job_id'));			
			$res = $this->sendJob($job);
			
			if($res){
				//addMandrillJobId($con,$_POST['job_id'],$res['_id']);
				return Response::make('1');
			}else{
				$resp = 'Error: 1<br />';
				return Response::make($resp);
			}
			
			//$m->toString($res);
		}else{
			$resp = 'Error: 2<br />';
			return Response::make($resp);
		}
	}

	private function sendJob($job){
		$m = new Mandrill_Api();
		$send_at = (isset($job->assigned_date))? $job->assigned_date : null;
		$job_type = $job->job_type;
		$all_fans_job_types = array(2,3,4,7,8,9,10,11,12,13,14,15,16,17);
		$send = null;
		$mandrill = array();
		$index = 0;
		if(in_array($job_type,$all_fans_job_types)){
			$emails = $this->getFansEmail();
			
			foreach($emails as $row){
				$send = $m->sendTemplate(array('name' => $job->user_id.'_'.$job->email_template->filename, 
											   'to' => $row['email'], 
									           'subject' => $job->subject,
									           'send_at' => $send_at));
				$res = $send[0];
				if(isset($res) && isset($res['status']) && ($res['status'] == 'sent' || $res['status'] == 'scheduled')){
					$mandrill[] = array('job_id' => $job->id,
										'mandrill_job_id' => $res['_id']); 
					//addMandrillJobId($con,$_POST['job_id'],$res['_id']);
					//echo '1';
				}/*else{
					echo 'Error: 1<br />';
					echo '<pre>';
					//var_dump($_POST);
					//var_dump($send);
					
					echo '</pre>';
				}*/			
			}
			return $this->addMandrillJob($mandrill);
		}else{
			$send = $m->sendTemplate(array('name' => $job->user_id.'_'.$job->email_template->filename,
										   'to' => $job->user_emails, 
									       'subject' => $job->subject,
									       'send_at' => $send_at));
			
			$res = $send[0];
			if(isset($res) && isset($res['status']) && ($res['status'] == 'sent' || $res['status'] == 'scheduled')){
				$mandrill[] = array('job_id' => $job->id,
									'mandrill_job_id' => $res['_id']); 
					//addMandrillJobId($con,$_POST['job_id'],$res['_id']);
					//echo '1';
			}
			return $this->addMandrillJob($mandrill);
		}
	}

	private function getFansEmail(){
		$user_id = Session::get('id');
		/*
		$sql = "SELECT first_name, last_name, email, birthday
				FROM fans 
				WHERE user_id = $user_id";	
		//echo $sql;
		$res = mysqli_query($con,$sql);
		*/
		$fans = new Fan;
		$res = $fans->where('user_id','=',$user_id)->get();

		return $res;
	}

	private function addMandrillJob($man){
		//$q = "INSERT INTO mandrill_job (job_id,mandrill_job_id) VALUES ";
		//$x = 0;
		//$count = count($man);
		$inserts = array();
		
		foreach($man as $m){
			/*
			if($x == ($count-1)){
				$q .= "(".$m['job_id'].",'".$m['mandrill_job_id']."')";
			}else
			if($x < $count){
				$q .= "(".$m['job_id'].",'".$m['mandrill_job_id']."'),";
			}
			$x++;*/
			$inserts[] = array('job_id' => $m['job_id'], 'mandrill_job_id' => $m['mandrill_job_id']);
		}
		//echo $q;
		//$res = mysqli_query($con,$q);
		Log::info($inserts);
		$id = DB::table('mandrill_job')->insert($inserts);

		return $id;
	}

}
