<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSiteOwnerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('site_owner', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 145)->nullable();
			$table->integer('affiliate_id')->nullable();
			$table->string('fb_id', 45)->nullable();
			$table->string('username', 45)->nullable();
			$table->string('password', 45)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('site_owner');
	}

}
