<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDownloadLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('download_log', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('player_id')->nullable();
			$table->string('date', 45)->nullable();
			$table->string('time', 45)->nullable();
			$table->string('country_code', 45)->nullable();
			$table->string('country', 45)->nullable();
			$table->string('city', 45)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('download_log');
	}

}
