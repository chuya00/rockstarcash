<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGoalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('goal', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('artist_offer_id')->nullable();
			$table->integer('ad_offer_id')->nullable();
			$table->integer('goal_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('goal');
	}

}
