<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAudioOwnerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('audio_owner', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('audio_id', 45)->nullable();
			$table->string('fb_id', 45)->nullable();
			$table->string('auth_code', 145)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('audio_owner');
	}

}
