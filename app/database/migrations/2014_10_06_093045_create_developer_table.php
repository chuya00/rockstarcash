<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeveloperTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('developer', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('username', 45)->nullable();
			$table->string('password', 45)->nullable();
			$table->string('name', 45)->nullable();
			$table->integer('affiliate_id')->nullable();
			$table->string('fb_id', 45)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('developer');
	}

}
