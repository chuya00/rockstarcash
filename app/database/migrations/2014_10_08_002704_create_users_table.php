<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 145)->nullable();
			$table->integer('advertiser_id')->nullable();
			$table->integer('offer_id')->nullable();
			$table->integer('affiliate_id')->nullable();
			$table->string('fb_id', 45)->nullable();
			$table->string('username', 45)->nullable();
			$table->string('password', 45)->nullable();
			$table->string('label_name', 45)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
