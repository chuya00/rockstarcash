<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlayerLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('player_log', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('player_id')->nullable();
			$table->integer('count_plays')->nullable();
			$table->string('date', 11)->nullable();
			$table->string('time', 10)->nullable();
			$table->string('country', 3)->nullable();
			$table->string('city', 45)->nullable();
			$table->string('site', 45)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_log');
	}

}
