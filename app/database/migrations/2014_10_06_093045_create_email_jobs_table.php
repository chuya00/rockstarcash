<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmailJobsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('email_jobs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 45)->nullable();
			$table->string('slug', 45)->nullable();
			$table->integer('email_template_id')->nullable();
			$table->string('subject', 145)->nullable();
			$table->string('user_emails', 45)->nullable();
			$table->string('user_id', 45)->nullable();
			$table->integer('job_type')->nullable();
			$table->string('schedule', 45)->nullable();
			$table->string('assigned_date', 45)->nullable();
			$table->integer('trash')->nullable()->default(0);
			$table->timestamp('date_added')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('email_jobs');
	}

}
