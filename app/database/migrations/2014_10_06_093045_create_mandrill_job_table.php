<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMandrillJobTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mandrill_job', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('job_id')->nullable();
			$table->string('mandrill_job_id', 145)->nullable();
			$table->integer('send')->nullable()->default(0);
			$table->integer('bounce')->nullable()->default(0);
			$table->integer('open')->nullable()->default(0);
			$table->integer('spam')->nullable()->default(0);
			$table->integer('reject')->nullable()->default(0);
			$table->integer('unsub')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mandrill_job');
	}

}
