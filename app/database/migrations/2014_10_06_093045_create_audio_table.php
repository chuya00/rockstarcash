<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAudioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('audio', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('album', 155)->nullable();
			$table->string('title', 155)->nullable();
			$table->string('filename', 155)->nullable();
			$table->integer('artist_id')->nullable();
			$table->string('tag')->nullable();
			$table->string('img', 155)->nullable();
			$table->string('soundwave', 155)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('audio');
	}

}
