<?php

class Post extends Eloquent{

	protected $table = 'post';

	public function rules(){
		return array(
			'title'             => 'required', 
			'message'         	=> 'required',
			'date_to_send'	    => 'required'
		);
	}
	
}