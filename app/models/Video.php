<?php

class Video extends Eloquent{

	protected $table = 'video';

	public function rules(){
		return array(
			'link'   => 'required', 						// required and must be unique in the ducks table
			'title'  => 'required',
			'source' => 'required'
		);
	}
	
	public function amplifyPage(){
		return $this->belongsTo('AmplifyPage','media_id');
	}

	public function player(){
		return $this->hasMany('Player','id');
	}
}