<?php

class Audio extends Eloquent{

	protected $table = 'audio';

	public function rules(){
		return array(
			'link'   => 'required', 						// required and must be unique in the ducks table
			'title'  => 'required',
			'source' => 'required'
		);
	}

	public function amplifyPage(){
		return $this->belongsTo('AmplifyPage');
	}
	
}