<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public function rules(){
		return array(
			'name'             => 'required', 						// required and must be unique in the ducks table
			'password'         => 'required|alpha_num|digits_between:8,20',
			'email'			   => 'required|email|unique:users',
			'cpassword' 	   => 'required|same:password' 			// required and has to match the password field
		);
	}

	public function messages(){
		return array(
		    'name.required' 		  => 'The Full Name field is required.',
		    'password.required' 	  => 'The Password field is required.',
		    'password.digits_between' => 'Password must be between 8 and 20 characters.',
		    'cpassword.same'		  => 'Passwords do not match.',
		    'password.alpha_num'      => 'Password must be composed of letters and numbers.',
		    'email.unique'            => 'You are already registered. Please sign in <a href="admin/login">here</a>.'
		);
	}

}
