<?php

class ContestUser extends Eloquent{

	protected $table = 'contest_user';

	public function contest(){
		return $this->belongsTo('Contest','contest_id');
	}
	
}