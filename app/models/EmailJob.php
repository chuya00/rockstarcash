<?php

class EmailJob extends Eloquent{

	protected $table = 'email_jobs';

	public function emailJobType(){
		return $this->belongsTo('EmailJobType','job_type');
	}

	public function emailTemplate(){
		return $this->belongsTo('EmailTemplate','email_template_id');
	}

	public function mandrillJob(){
		return $this->hasOne('MandrillJob','job_id');
	}
	
}