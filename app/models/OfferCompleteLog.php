<?php

class OfferCompleteLog extends Eloquent{

	protected $table = 'offer_complete_log';

	public function player(){
		return $this->belongsTo('Player','player_id');
	}
	
}