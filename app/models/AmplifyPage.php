<?php

class AmplifyPage extends Eloquent{

	protected $table = 'amplify_page';
	
	public function video(){
		return $this->hasMany('Video','id');
	}

	public function audio(){
		return $this->hasMany('Audio','id');
	}
}