<?php

class ContestAlbumImage extends Eloquent{

	protected $table = 'contest_album_image';
	
	public function contest(){
		return $this->belongsTo('Contest','contest_id');
	}
}