<?php

class PlayerLog extends Eloquent{

	protected $table = 'player_log';

	public function player(){
		return $this->belongsTo('Player');
	}
	
}