<?php

class Contest extends Eloquent{

	protected $table = 'contest';
	
	public function rules(){
		return array(
			'player_id'   => 'required|unique:contest',
			'title'       => 'required'
		);
	}

	public function player(){
		return $this->belongsTo('Player','player_id');
	}

	public function messages(){
		return array(
		    'player_id.unique' => 'The selected album already have an enabled contest.'
		);
	}

	public function contestAlbumImage(){
		return $this->hasMany('ContestAlbumImage','id');
	}

	public function contestUser(){
		return $this->hasMany('ContestUser','id');
	}
}