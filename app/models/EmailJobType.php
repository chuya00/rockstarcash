<?php

class EmailJobType extends Eloquent{

	protected $table = 'email_job_types';

	public function emailJob(){
		return $this->hasMany('EmailJob','job_type');
	}
	
}