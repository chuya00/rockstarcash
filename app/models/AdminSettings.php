<?php

class AdminSettings extends Eloquent{

	protected $table = 'admin_settings';
	
	public function emailTemplate(){
		return $this->belongsTo('EmailTemplate','email_template_audio_file');
	}
}