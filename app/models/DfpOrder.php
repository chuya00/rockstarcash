<?php

class DfpOrder extends Eloquent{

	protected $table = 'dfp_orders';
	
	public function getIdByOrderId($id){
		return $this->where('order_id','=',$id)->first();
	}

	public function saveData($data){
		$this->order_id = $data['order_id'];
		$this->order_name = $data['order_name'];
		$this->cpa = $data['cpa'];
		$this->date = date('Y-m-d');
		$this->save();
		
		return $this->id;
	}
}