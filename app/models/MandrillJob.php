<?php

class MandrillJob extends Eloquent{

	protected $table = 'mandrill_job';

	public function emailJob(){
		return $this->belongsTo('EmailJob','job_id');
	}

	public function getMandrillJobCount($id){
		//job_id = ej.id AND mandrill_job_id IS NOT NULL
		return $this->where('job_id','=',$id)
					->whereNotNull('mandrill_job_id')
					->get();
	}
	
}