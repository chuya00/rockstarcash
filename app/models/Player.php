<?php

class Player extends Eloquent{

	protected $table = 'player';

	public function contest(){
		return $this->hasMany('Contest','id');
	}

	public function video(){
		return $this->belongsTo('Video','video_id');
	}
}